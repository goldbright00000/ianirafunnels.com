<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', function () {
    return view('home');
});
Route::get('home', function () {
    return view('home');
});




Auth::routes();
Route::post('','Addfunnel@domianvalidate');
Route::post('domianvalidate','Addfunnel@domianvalidate');

Route::post('/stepdelete','Addfunnel@deletefunnelstep');

Route::get('logout', 'Auth\LoginController@logout');

Route::get('location','Funellpages@location');

Route::get('career','Funellpages@career');

Route::get('our-team','Funellpages@ourteam');

Route::get('14days-trial','Funellpages@days14trial');

Route::get('14days-trial-login','Funellpages@days14trialLogin');

Route::get('origin-story','Funellpages@orginstory');

Route::get('payment','Funellpages@payment');

Route::get('paypal','Funellpages@paypal');

Route::get('sign-now','Funellpages@signnow');

Route::get('admin-dashboard','Admincontrollers@create');

Route::post('login-admin','Admincontrollers@checklogin');

Route::get('login/sucess','Admincontrollers@sucess');



Route::get('login/dashboard','Admincontrollers@dashboard');
Route::get('login/register-member','Membercontrollers@allmember');
Route::get('logout/dashboard','Admincontrollers@logout');
Route::get('admin/profile-edit','Admincontrollers@profileedit');
Route::post('admin-dashboard/edit','Admincontrollers@edit');
Route::get('admin-dashboard/change-password','Admincontrollers@changepassword');
Route::get('select-funnel','Funnels@page');
Route::get('login/add-funnel-category','Admincontrollers@funnelcategoy');

Route::get('hero/market-funnel','Funnels@herofunnel');
Route::get('jason/market-funnel','Funnels@jasonfunnel');
Route::get('funnels','Funnels@purchases');
Route::get('funnels/purchases_status','Funnels@status');
Route::get('login/add-funnel','Funnelcategory@index');
Route::POST('funnel/add-category','Funnelcategory@insert');
Route::get('funnel/all-funnel-category','Funnelcategory@fetchdata');
Route::get('funnelcategory/{id}','Funnelcategory@edit');
Route::POST('funnel/edit-category','Funnelcategory@postedit');
Route::get('delete-funnel-category/{id}','Funnelcategory@delete');
Route::get('admin/add-funnel','Addfunnel@index');
Route::POST('added-funnel','Addfunnel@insert');
Route::get('admin/all-subcategory-funnel','Addfunnel@allfunnel');
Route::get('admin/funnel-sub-category/{id}','Addfunnel@subcategeryfunnel');
Route::get('delete/subcategory/{id}','Addfunnel@delete');
Route::POST('funnel/edit-sub-funnel','Addfunnel@edit');
Route::get('funnel-step','Crmcontrollers@funnelstepdashboard');

Route::get('crm/user-details/{id}','Crmcontrollers@crmuserdetails');

Route::get('login/funnel-add','Addfunnel@addedfunnel');
Route::POST('post/sub-funnel','Addfunnel@sucategorypost');
Route::get('admin/all-sub-funnel','Addfunnel@funnelfetchdata');
Route::get('admin/edit-funnel/{id}','Addfunnel@editform');
Route::POST('admin/funnel-update','Addfunnel@updatepost');
Route::get('admin/funnel-delete/{id}','Addfunnel@funneldelete');
Route::get('view-funnel/{id}','Funnels@viewpage');
Route::get('admin/funnel_page_add','Addfunnel@otherfunnel');
Route::POST('post/other-subfunnel','Addfunnel@postsubfunnel');
Route::get('admin/sub-funnel-edit','Addfunnel@editfetchdata');
Route::get('admin/edit-template/{id}','Funnels@edittemplatedata');
Route::POST('post/template','Addfunnel@posttemplate');
Route::get('post/templatedelete/{id}','Addfunnel@templatedelete');


Route::get('crm/home','Crmcontrollers@index');
Route::get('crm/customers','Crmcontrollers@customers');
Route::get('crm/leads','Crmcontrollers@leads');
Route::get('crm/proposals','Crmcontrollers@proposals');
Route::get('crm/invoices','Crmcontrollers@invoices');
Route::get('crm/projects','Crmcontrollers@projects');
Route::get('crm/expenses','Crmcontrollers@expenses');
Route::get('crm/staff','Crmcontrollers@staff');
Route::get('crm/tickets','Crmcontrollers@tickets');
Route::get('crm/task','Crmcontrollers@task');
Route::get('web/crm','Crmcontrollers@pagelogin');
Route::POST('auth/check','Crmcontrollers@checkuser');
Route::get('crm/logout','Crmcontrollers@logout');

Route::get('crm/addedtemplete','Crmcontrollers@addedtemplate');


Route::POST('funnel/added/step','Addfunnel@stepfunnels');


Route::get('funnel-edit','Crmcontrollers@funneledit');


Route::get('user/dashboard','Userscontroller@dashboard');
Route::get('user/funnel-dashboard/{id}','userscontroller@funneldashboard');
Route::get('crm/funnel-category','Crmcontrollers@funnelcategory');
Route::get('crm/funnel-category-edit/{id}','Crmcontrollers@funnelcategoryedit');


Route::get('crm/funnel-category-edit/{id}','Crmcontrollers@funnelcategoryedit');

Route::POST('add/funnel-step','Addfunnel@newfunnelstep');

Route::POST('funnel/added-new/step','Addfunnel@addnewstep');

Route::get('funnel/recent','Funnels@funnelrecent');

Route::POST('funnel/editadded/step','Addfunnel@subdomainedit');


Route::get('login1','Admincontrollers@login1');
Route::get('registration1','Admincontrollers@registration1');
Route::get('forget-password','Admincontrollers@forgot_password');
Route::POST('dataforget','Funnelcategory@dataforget');
Route::get('verify-otp/{id}','Funnelcategory@verifyotp');

Route::POST('userforget-password','Funnelcategory@userforgetpassword');
Route::get('password-forget-user/{id}','Funnelcategory@passwordforgetuser');

Route::POST('confirmpassword','Funnelcategory@confirmpassword');


Route::get('trial','Admincontrollers@trial');
Route::get('funnel-registration','Admincontrollers@register');
Route::get('pay','Admincontrollers@pay');

Route::get('funnelsdashboard','Dashboardcontrollers@dashboard');
//Route::get('ui-dashboard','Dashboardcontrollers@funnelsdashboardUI');
Route::POST('userdata','Funnelcategory@userdatavalidate');


//catch clear
Route::get('/clear-cache', function() {    $exitCode = Artisan::call('optimize:clear');    return redirect('/')->with('status', 'All caches were purged.');});


Route::domain('sarojtest.ianirafunnels.com')->group(function () {
    Route::get('/', function () {
      return view('home');
    });
  });
  




//Route::group(array('domain'=>'{subdomain}.ianirafunnels.com'),function ()
//{
//Route::get('/home','Crmcontrollers@subdomain');
//});


//Route::prefix('sarojtest')->group(function () {
    //Route::get('/home','Crmcontrollers@subdomain');
   
//});


Route::domain('sarojtest.ianirafunnels.com')->group(function () {
   Route::get('/home','Crmcontrollers@subdomain');
});

//Route::domain('sarojtest.ianirafunnels.com')->group(function ($router) {
    // view('home');
//});


//Route::group(array('domain' => 'sarojtest.ianirafunnels.com'), function(){
//Route::get('/', array(
//'as' => 'partner-home',
//'uses' => 'Crmcontrollers@subdomain'
 //   ));
//});



//Route::group(['domain' => '{domain}'], function() {
  //  Route::get('/', function($domain) {
///return 'This is the page for ' . $domain . '!';
//});
//});



//Route::domain('sarojtest.ianirafunnels.com') -> group(function () {
   // Route::get('hhh','Crmcontrollers@hhh');
//});


//Route::domain('sarojtest.ianirafunnels.com')->group(function () {
 
  //Route::get('/home', function () {
    //return view('home');
//});

//});




Route::get('/purge', function() {
    
      $exitCode = Artisan::call('cache:clear');  
      $exitCode = Artisan::call('config:clear');  
      $exitCode = Artisan::call('route:clear'); 
      $exitCode = Artisan::call('view:clear');
       return redirect('/')->with('status', 'All caches were purged.');
	       });