<?php namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Session;

class Subfunnelcategory extends Model
{

    protected $table = 'funnel_sub_category';

 protected $fillable = [
    'funnel_type','funnel_name'
];

}
?>