<?php namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Session;

class Template extends Model
{

    protected $table = 'templates';

 protected $fillable = [
    'template_price ', ' template_price_type ','template_name','template_heading','content','image'
];

}
?>