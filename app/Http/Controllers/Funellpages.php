<?php 

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Funellpages extends Controller
{
	public function location()
	{
		return view('location');
           
	}
        
        
        public function career()
        {
            return view('career');
        }
        
        public function ourteam()
        {
            return view('our-team');
        }
        public function days14trial()
        {
            return view('14days-trial');
        }
        public function days14trialLogin()
        {
            return view('14days-trial-login');
        }
        public function orginstory()
        {
            return view('origin-story');
        }
        public function payment()
        {
            return view('payment');
        }
        public function paypal()
        {
            return view('paypal');
        }
        public function signnow()
        {
            return view('signup-now');
        }

      public function adminlogin()
        {
            return view('career');
        }
        
        
        
}


?>