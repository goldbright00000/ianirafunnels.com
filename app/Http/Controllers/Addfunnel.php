<?php namespace App\Http\Controllers;
use App\Adminlogin;
use App\Funnelsname;
use App\Subfunnelcategory;
use App\Allfunnel;
use App\Otherfunnel;
use App\Template;
use DB;
use Session;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Userfunnelsteps;
use App\Newstepfunneladded;


class Addfunnel extends Controller
{
	public function index()
	{
           $fetchdata= Funnelsname::get();
            
          return view('admin.add_funnel')->with('fetchdata', $fetchdata);   
          
        }
        
        
        
        public function insert(Request $request)
        {
        
            
            $funnel_type=$request->funnel_type; 
            
          $funnel_name=$request->funnel_name;
                   $funnel_table= new Subfunnelcategory;
                   $funnel_table->funnel_type=$funnel_type;
                   $funnel_table->funnel_name=$funnel_name;
                   
                  if($funnel_table->save()){

                    $fetchdata= Funnelsname::get();
                   return view('admin.add_funnel')->with(['sucess'=>'Funnel Name Insert Sucessfully','fetchdata'=>$fetchdata]);
                  }  
            
            
            
        }
        public function allfunnel()
        {
            
                  
                    
                 $fetchdata = DB::table('funnel_sub_category as f')
                ->rightJoin('select_panel as s', 'f.funnel_type', '=', 's.id')
                ->select('f.id as id','f.funnel_name as sub_funnel_name','s.funnel_name as funnel_name')
                ->orderBy('f.id','desc')
                ->get();
                return view('admin.all_sub_category_funnel')->with('fetchdata',$fetchdata);
            
            
        }
        
        public function subcategeryfunnel($id)
        {
            
         
            
            $data= Subfunnelcategory::where('id',$id)->first();
            $fetchdata= Funnelsname::get();
            return view('admin.edit_sub_category_funnel')->with(['data'=>$data,'fetchdata'=>$fetchdata]);
            
            
        }
        
        
         public function delete($id)
           {
                               $res=Subfunnelcategory::where('id',$id)->delete();
                               $fetchdata = DB::table('funnel_sub_category as f')
                               ->rightJoin('select_panel as s', 'f.funnel_type', '=', 's.id')
                               ->select('f.id as id','f.funnel_name as sub_funnel_name','s.funnel_name as funnel_name')
                               ->orderBy('f.id','desc')
                               ->get();
                               return view('admin.all_sub_category_funnel')->with(['fetchdata'=>$fetchdata,'sucess'=>'Sucessfully Funnel Delete !']);
               
           }
           
           public function edit(Request $request)
           {
                  $funnel_name=$request->funnel_name;
                   $funnel_category_id=$request->funnel_category_id;
                   $funnel_edit_id=$request->edit_id;
                  
                   $update_query= Subfunnelcategory::where('id',$funnel_edit_id)->first();
                   $update_query->funnel_name=$funnel_name;
                   $update_query->funnel_type=$funnel_category_id;
                   $update_query->save();
                   
                  
                  $fetchdata = DB::table('funnel_sub_category as f')
                               ->rightJoin('select_panel as s', 'f.funnel_type', '=', 's.id')
                               ->select('f.id as id','f.funnel_name as sub_funnel_name','s.funnel_name as funnel_name')
                               ->orderBy('f.id','desc')
                               ->get();
                               return view('admin.all_sub_category_funnel')->with(['fetchdata'=>$fetchdata,'sucess'=>'Sucessfully Funnel Edit !']);
                               
             
           }
           
           
           public function addedfunnel()
           {
               
               $fetchdata=Subfunnelcategory::get();
               
         
               return view('admin.added-funnel')->with(['fetchdata'=>$fetchdata]);
               
               
               
               
               
           }
           
           public function sucategorypost(Request $request)
           {
                   $funnel_type=$request->funnel_type;
                   $funnel_name=$request->funnel_name;
                   $funnel_other=$request->funnel_other;
                   $funnel_message=$request->message;
                   $funnel_othertype=$request->type;
                   $funnel_page=$request->page;
                   $funnel_time=$request->time;
                   if($request->funnel_price)
                   {
                  $funnel_price=$request->funnel_price;
                  $funnel_price_type="paidtemplate";
                   } else {
                       
                    $funnel_price_type="freetemplate";
                     $funnel_price="";  
                   }
                  
                   
                   
                   $funnel_subtable= new Allfunnel;
                   $funnel_subtable->funnel_type= $funnel_type; 
                   $funnel_subtable->funnel_name= $funnel_name;
                   $funnel_subtable->funnel_other=$funnel_other; 
                   $funnel_subtable->message=$funnel_message; 
                   $funnel_subtable->type=$funnel_othertype;
                   $funnel_subtable->page=$funnel_page;
                   $funnel_subtable->time=$funnel_time;
                   $funnel_subtable->funnel_price=$funnel_price;
                   $funnel_subtable->funnel_price_type=$funnel_price_type;
                   $attachment1="";
        if($request->hasFile('image'))    
        {    
            $file =$request->file('image');     
            $attachment1 = time() . '_'.$file->getClientOriginalName();
            $destinationPath = public_path('/uploads/funnel');
            $file->move($destinationPath, $attachment1);
        }
      
              $funnel_subtable->image=$attachment1;     
                   
                   
               
     
              $funnel_subtable->save(); 
     
        return redirect('login/funnel-add')->with('status', 'Sucessfully Insert');
              
           }
           
           
           
           
           
           
           
           
             public function funnelfetchdata()
	{
          
          
      
   
                $fetchdata = DB::table('addedfunnel as ad')
                               ->join('funnel_sub_category as fu', 'ad.funnel_type', '=', 'fu.id')                                              
                               ->select('ad.id as id','fu.funnel_name as sub_funnel_name','ad.funnel_name as funnel_name','ad.funnel_other as funnel_other',
                                       'ad.message as funnel_message','ad.image as funnel_image','ad.type as funnel_type','ad.page as funnel_page','ad.time as funnel_time','ad.funnel_price')
                               ->orderBy('ad.id','desc')
                               ->get();
   
   
   
 
                return view('admin.all-funnel')->with('fetchdata', $fetchdata);

        } 
           
         public function editform($id)
	{
        
               $data=Subfunnelcategory::get();
               
                $fetchdata= Allfunnel::where('id',$id)->first();
               
               return view('admin.edit_funnel')->with(['fetchdata'=>$fetchdata,'data'=>$data]);
               
                  
             
             
             
        }
        
        public function updatepost(Request $request)
        {
            
            
            $funnel_type=$request->funnel_type;
                   $funnel_name=$request->funnel_name;
                   $funnel_other=$request->funnel_other;
                   $funnel_message=$request->message;
                   $funnel_othertype=$request->type;
                   $funnel_page=$request->page;
                   $funnel_time=$request->time;
                    $funnel_id=$request->funnel_id;
                   if($request->funnel_price)
                   {
                  $funnel_price=$request->funnel_price;
                  $funnel_price_type="paidtemplate";
                   } else {
                       
                    $funnel_price_type="freetemplate";
                     $funnel_price="";  
                   }
          
            
            
            
                  
                   
                   $funnel_update=Allfunnel::where('id',$funnel_id)->first();
                   
                   $funnel_update->funnel_type=$funnel_type;
                   $funnel_update->funnel_name=$funnel_name;
                   $funnel_update->funnel_other=$funnel_other;
                   $funnel_update->type=$funnel_othertype;
                   $funnel_update->page=$funnel_page;
                   $funnel_update->time=$funnel_time;
                  $funnel_update->funnel_price_type=$funnel_price_type;
                   $funnel_update->message=$funnel_message;
                   
                   
                   
        if($request->hasFile('image'))    
        {    
            $file =$request->file('image');     
            $attachment1 = time() . '_'.$file->getClientOriginalName();
            $destinationPath = public_path('/uploads/funnel');
            $file->move($destinationPath, $attachment1);
             $funnel_update->image=$attachment1; 
        }
 
      
                 
              $funnel_update->save();  
              
              
                $fetchdata = DB::table('addedfunnel as ad')
                               ->join('funnel_sub_category as fu', 'ad.funnel_type', '=', 'fu.id')                                              
                               ->select('ad.id as id','fu.funnel_name as sub_funnel_name','ad.funnel_name as funnel_name','ad.funnel_other as funnel_other',
                                       'ad.message as funnel_message','ad.image as funnel_image','ad.type as funnel_type','ad.page as funnel_page','ad.time as funnel_time','ad.funnel_price')
                               ->orderBy('ad.id','desc')
                               ->get();
   
                return view('admin.all-funnel')->with('fetchdata', $fetchdata);
              
              
              
                   
                   
                   
        }
        
        public function funneldelete($id)
        {
          
         $res=Allfunnel::where('id',$id)->delete();   
            
              $fetchdata = DB::table('addedfunnel as ad')
                               ->join('funnel_sub_category as fu', 'ad.funnel_type', '=', 'fu.id')                                              
                               ->select('ad.id as id','fu.funnel_name as sub_funnel_name','ad.funnel_name as funnel_name','ad.funnel_other as funnel_other',
                                       'ad.message as funnel_message','ad.image as funnel_image','ad.type as funnel_type','ad.page as funnel_page','ad.time as funnel_time','ad.funnel_price')
                               ->orderBy('ad.id','desc')
                               ->get();
   
                return redirect('admin/all-sub-funnel')->with('sucess','Sucessfully Delete');
            
        }
        
        public function otherfunnel()
        {
            
      
        
          return view('admin.view_funnel_other_details');      
                
      
        }
        public function postsubfunnel(Request $request)
        {
           
               if($request->funnel_price)
                   {
                  $funnel_price=$request->funnel_price;
                  $funnel_price_type="paidtemplate";
                   } else {
                       
                    $funnel_price_type="freetemplate";
                     $funnel_price="";  
                   }
            
        
         $funnel_heading=$request->funnel_name;
         $funnel_other=$request->funnel_other;
         $funnelother_message=$request->message;
         $template_des_type=$request->template_des_type;
         
         
         
         
         $funnel_table= new Template;
         
         $funnel_table->template_price =$funnel_price;
         $funnel_table->template_price_type=$funnel_price_type;
          $funnel_table->template_name=$funnel_heading;
          $funnel_table->template_heading=$funnel_other;
          $funnel_table->template_des_type=$template_des_type;
      
         $funnel_table->message =$funnelother_message;
            
         
         
         
         $attachment1="";
        if($request->hasFile('image'))    
        {    
            $file ==$request->file('image');     
            $attachment1 = time() . '_'.$file->getClientOriginalName();
            $destinationPath = public_path('/uploads/funnel');
            $file->move($destinationPath, $attachment1);
        }
      
              $funnel_table->image=$attachment1;     
                   
                   
               
     
             $funnel_table->save(); 
        
 
     
        return redirect('admin/funnel_page_add')->with('sucess', 'Sucessfully Insert');
        }
        
        public function editfetchdata()
        {
            $fetchdata=Template::all();   
         return view('admin.alltemplate')->with('fetchdata', $fetchdata);   
            
        }
        
        
        
        public function posttemplate(Request $request)
        {
          
           if($request->funnel_price)
                   {
                  $funnel_price=$request->funnel_price;
                  $funnel_price_type="paidtemplate";
                   } else {
                       
                    $funnel_price_type="freetemplate";
                     $funnel_price="";  
                   }
            
        
         $funnel_heading=$request->funnel_name;
         $funnel_other=$request->funnel_other;
         $funnelother_message=$request->message;
         $template_des_type=$request->template_des_type;
         $template_id=$request->template_id;
         
         
         
              
          $funnel_table=Template::where('id',$template_id)->first();
        
         
         $funnel_table->template_price =$funnel_price;
         $funnel_table->template_price_type=$funnel_price_type;
          $funnel_table->template_name=$funnel_heading;
          $funnel_table->template_heading=$funnel_other;
          $funnel_table->template_des_type=$template_des_type;
      
         $funnel_table->message =$funnelother_message;
            
         
         
         
         $attachment1="";
        if($request->hasFile('image'))    
        {    
            $file =$request->file('image');     
            $attachment1 = time() . '_'.$file->getClientOriginalName();
            $destinationPath = public_path('/uploads/funnel');
            $file->move($destinationPath, $attachment1);
        }
      
              $funnel_table->image=$attachment1;     
                   
                   
               
     
             $funnel_table->save(); 
        
    
    return redirect('admin/sub-funnel-edit');
             
         }
         
         
         
         
         public function templatedelete($id)
        {
          
         $res=Template::where('id',$id)->delete();   
          return redirect('admin/sub-funnel-edit')->with('sucess', 'Delete Sucessfully');  
        }
        
       
        
        public function stepfunnels(Request $request)
        {
if (empty(Auth::check())) {

return redirect("login");
}
$subdomain_name=$request->subdomain_name;
$funnel_name =$request->funnel_name;
$funnel_tag =$request->funnel_tag;
$funnel_type =$request->funnel_type;

$user_id = Auth::user()->id;
$funnel_created_time=date('Y-m-d');
$funnel_step_times=$request->funnel_step_times;

//$to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', '2015-5-5 3:30:34');
//$from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', '2015-6-9 9:30:34');

//$diff_in_days = $to->diffInDays($from);

//print_r($diff_in_days); 
//dd();

$string =$subdomain_name;

$string = strtolower($string);

$string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
$sudomain_name= preg_replace('/-+/', '', $string);
$rootDomain="ianirafunnels.com";
$subDomain=$sudomain_name;

$cPanelUser="ianirafu";
$cPanelPass="ianiraF++";

$funnel_url_step=Userfunnelsteps::where('funnel_url', $subDomain)->count();
if($funnel_url_step>0)
{
return redirect('funnels')->with('error','Funnel name is already exit');

}
else{

$funnel_alter_url= "http://" .$rootDomain. "/".$subDomain."/";
$funnel_url = "http://" . $subDomain . "." . $rootDomain . "/";
$six_digit_random_number = mt_rand(100000, 999999);
$user_funnel_activity=new Userfunnelsteps;
$user_funnel_activity->user_id=$user_id;
$user_funnel_activity->funnel_step_name=$funnel_type;
$user_funnel_activity->funnel_type =$funnel_tag; 
$user_funnel_activity->funnel_name =$funnel_name;
$user_funnel_activity->funnel_step_times=$funnel_step_times;
$user_funnel_activity->funnel_created_time=$funnel_created_time;

$user_funnel_activity->funnel_id=$six_digit_random_number;
$user_funnel_activity->funnel_url=$subDomain;

$user_funnel_activity->funnel_full_url=$funnel_url;

$user_funnel_activity->funnel_alter_url=$funnel_alter_url;



if($user_funnel_activity->save()){


/*subdomain create*/

$buildRequest = "/frontend/Crystal/subdomain/doadddomain.html?domain=" . $subDomain . "&rootdomain=" . $rootDomain . "&dir=public_html/subdomainlist/" . $subDomain;

$openSocket = fsockopen('localhost',2082);
if(!$openSocket) {
  return "Socket error";
  exit();
}

$authString = $cPanelUser . ":" . $cPanelPass;
$authPass = base64_encode($authString);
$buildHeaders  = "GET " . $buildRequest ."\r\n";
$buildHeaders .= "HTTP/1.0\r\n";
$buildHeaders .= "Host:localhost\r\n";
$buildHeaders .= "Authorization: Basic " . $authPass . "\r\n";
$buildHeaders .= "\r\n";

fputs($openSocket, $buildHeaders);
while(!feof($openSocket)) {
fgets($openSocket,128);
}
fclose($openSocket);


/*subdomain create end*/



  $funnel_token=$six_digit_random_number;
  
$funnel_step_token1 = mt_rand(100000, 999999);

$launchpage[]=array('funnel_step_name'=>'Launch Page 1','funnel_url'=>'launch-page','funnel_token'=>$funnel_token,
'funnel_step_token'=>$funnel_step_token1,'user_id'=>$user_id,'funnel_id'=>$user_funnel_activity->id);



$funnel_step_token2 = mt_rand(100000, 999999);
$launchpage[]=array('funnel_step_name'=>'Launch Page 2','funnel_url'=>'launch-page','funnel_token'=>$funnel_token,
'funnel_step_token'=>$funnel_step_token2,'user_id'=>$user_id,'funnel_id'=>$user_funnel_activity->id);


$funnel_step_token3 = mt_rand(100000, 999999);
$launchpage[]=array('funnel_step_name'=>'Launch Page 3','funnel_url'=>'launch-page','funnel_token'=>$funnel_token,
'funnel_step_token'=>$funnel_step_token3,'user_id'=>$user_id,'funnel_id'=>$user_funnel_activity->id);


$funnel_step_token4 = mt_rand(100000, 999999);
$launchpage[]=array('funnel_step_name'=>'Launch Page 4','funnel_url'=>'launch-page','funnel_token'=>$funnel_token,
'funnel_step_token'=>$funnel_step_token4,'user_id'=>$user_id,'funnel_id'=>$user_funnel_activity->id);


$funnel_step_token5 = mt_rand(100000, 999999);
$launchpage[]=array('funnel_step_name'=>'Order','funnel_url'=>'order-page','funnel_token'=>$funnel_token,
'funnel_step_token'=>$funnel_step_token5,'user_id'=>$user_id,'funnel_id'=>$user_funnel_activity->id);


$funnel_step_token6 = mt_rand(100000, 999999);
$launchpage[]=array('funnel_step_name'=>'Thank You','funnel_url'=>'thank-you-page','funnel_token'=>$funnel_token,
'funnel_step_token'=>$funnel_step_token5,'user_id'=>$user_id,'funnel_id'=>$user_funnel_activity->id);



//print_r($launchpage);
$arr=array();

foreach($launchpage as $launchpageRow)
{
   // print_r($launchpageRow);
   $newstepFunnelAdd=new Newstepfunneladded;
   
   $newstepFunnelAdd->funnel_step_name=$launchpageRow['funnel_step_name'];
   $newstepFunnelAdd->funnel_url=$launchpageRow['funnel_url'];
   $newstepFunnelAdd->funnel_token=$launchpageRow['funnel_token'];
   $newstepFunnelAdd->funnel_step_token=$launchpageRow['funnel_step_token'];
   $newstepFunnelAdd->user_id=$launchpageRow['user_id'];
   $newstepFunnelAdd->funnel_id=$launchpageRow['funnel_id'];
   $newstepFunnelAdd->save();
   
       
   
//print_r($arr);

}

return redirect('user/funnel-dashboard/'.$user_funnel_activity->id); 
  
                 }  

}
}
        
        
  
  
  public function domianvalidate(Request $request)
  {
    $subdomain=$request->funneldomain;
    $subdomain_avl= Userfunnelsteps::where('funnel_url',$subdomain)->count();




if($subdomain_avl>0)
{

  echo "1";

}else{

echo "0";


}


  }
 
  public function newfunnelstep(Request $request)
  {
   



  $funnel_step_name=$request->funnel_step_name;
  $funnel_url=$request->funnel_url;
  $user_id = Auth::user()->id;
  $six_digit_random_number = mt_rand(100000, 999999);
if(!empty($user_id ) && !empty($funnel_step_name) )
{

  

$funnel_step=  new Newstepfunneladded;
$funnel_step->funnel_step_name=$funnel_step_name;
$funnel_step->funnel_url=$funnel_url;
$funnel_step->funnel_token=$six_digit_random_number;


}

  }
 
   public function addnewstep(Request $request)
   {

    $old_funnel_query=Userfunnelsteps::where('id',$request->funnel_id)->first();
   


$funnel_step_token = mt_rand(100000, 999999); 

$funnel_url=preg_replace('/[^a-z0-9-]+/','-', strtolower($request->funnel_url));
 $user_id = Auth::user()->id;
if(!empty($user_id ))
{


  $newstepFunnelAdd=new Newstepfunneladded;
   
  $newstepFunnelAdd->funnel_step_name=$request->funnel_step_name;
  $newstepFunnelAdd->funnel_url=$funnel_url;
  $newstepFunnelAdd->funnel_token= $old_funnel_query->funnel_id;
  $newstepFunnelAdd->funnel_step_token=$funnel_step_token ;
  $newstepFunnelAdd->user_id=$user_id;
  $newstepFunnelAdd->funnel_id=$request->funnel_id;
  $newstepFunnelAdd->save();


  return redirect('user/funnel-dashboard/'.$request->funnel_id)->with('insert','Create Sucessfully !');


}
else
{


}

   }     
    
    public function deletefunnelstep(Request $request)
    {

    $user_id = Auth::user()->id;
    if(!empty($user_id ))
    {
    $id =$request->stepid;
    
    $res=Newstepfunneladded::where('id',$id)->delete();
     
     echo "Delete Successfully !";

     }


    } 


    public function recent()
    {
     
    return view('funnelrecent');
    
      


    }

    public function subdomainedit(Request $request)
    {
      if (empty(Auth::check())) {

        return redirect("login");
        }
      $subdomain_name=$request->edit_subdomain_name;
        $funnel_name =$request->funnel_name;
         $funnel_tag =$request->funnel_tag;
     
         $funnel_id =$request->funnelmainid;


     
        $user_id = Auth::user()->id;
        $funnel_created_time=date('Y-m-d');
        $funnel_step_times=$request->funnel_step_times;

        $string = strtolower($subdomain_name);

        $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        $sudomain_name= preg_replace('/-+/', '', $string);

        echo $sudomain_name;
       


        $funnel_url_step=Userfunnelsteps::where('funnel_url', $sudomain_name)->count();
        if($funnel_url_step>0)
        {

           
          if (empty(Auth::check())) {

            return redirect("login");
                             }

               $user_id = Auth::user()->id;
               
               $id=$funnel_id;
               $data['id']=$funnel_id;
                
                
                  $isexit = Userfunnelsteps::where('user_id', $user_id)->where('id', $id)->first();

                  $user_correct = Userfunnelsteps::where('user_id', $user_id)->where('id', $id)->count();

                  $funnel_step=Newstepfunneladded::where('user_id', $user_id)->where('funnel_id', $id)->get();
                  
                    if($user_correct > 0 )
                       {
                        
                       return view("user-funnel-dashboard.user-funnel-dashboard",$data)->with(['funnel_name'=>$isexit->funnel_name,'funnel_step'=>$funnel_step]);      
                            
                       }
                           else {
                               
                              return redirect("login");
                           }








        //return redirect('funnels')->with('error','Funnel name is already exit');

        }
        else{
            
          echo "hee";

          

        }
        dd();
    }


        
        
}

