<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Template;
use App\Adminlogin;
use App\Memberlists;
use App\Funnelsname;
use App\Subfunnelcategory;
use App\Allfunnel;
use DB;
use Session;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Crmlogin;
use App\Userfunnelsteps;

class Crmcontrollers extends Controller {

    public function index() {
        if (Session::has('user_id')) {
            return view('crm.home');
        } else {
            return view('crm.login');
        }
    }

    public function customers() {

        if (Session::has('user_id')) {

            $member = Memberlists::all();


            return view('crm.customers')->with('member', $member);
        } else {

            return view('crm.login');
        }
    }

    public function leads() {
        if (Session::has('user_id')) {

            $alluser = Memberlists::all();
            //$fetchdata= DB::table('users as u')
            //->rightJoin('userfunnelsteps as uf', 'uf.user_id', '=', 'u.id')
            //->select('u.id','u.name')
            //->get();

            return view('crm.leads')->with('fetchdata', $alluser);
        } else {

            return view('crm.login');
        }
    }

    public function projects() {

        return view('crm.projects');
    }

    public function proposals() {

        return view('crm.proposals');
    }

    public function invoices() {

        return view('crm.invoices');
    }

    public function expenses() {

        return view('crm.expenses');
    }

    public function staff() {

        return view('crm.staff');
    }

    public function tickets() {

        return view('crm.tickets');
    }

    public function task() {

        return view('crm.task');
    }

    public function pagelogin() {

        if (Session::has('user_id')) {
            return redirect('crm/home');
        } else {
            return view('crm.login');
        }
    }

    public function checkuser(Request $request) {

        $email = $request->username;
        $password = $request->password;

        $isexit = Crmlogin::where('email', $email)->where('password', $password)->first();

        $user_correct = Crmlogin::where('email', $email)->where('password', $password)->count();

        $data = array('email' => $email, 'password' => $password);
        if ($user_correct) {
            Session::put(['user_id' => $isexit->id, 'name' => $isexit->name]);


            if (Session::has('user_id')) {
                echo "true";
                return redirect('crm/home')->with('sucess', 'You have logged in successfully.');
            }
        } else {

            return redirect('web/crm')->with(array('fail' => 'These credentials do not match our records.', 'email' => $request->username, 'password' => $password));
        }
    }

    public function logout() {

        if (!empty(Session::has('user_id'))) {
            Session::flush();
            if (!empty(Session::has('user_id'))) {

                return redirect('crm/home');
            } else {
                return redirect('web/crm')->with('fail', 'Logout Successfully');
            }
        } else {
            return redirect('web/crm')->with('fail', 'You are not Login');
        }
    }

    public function funneledit() {

        return view('funnel-edit');
    }

    public function funnelcategory() {


        if (!empty(Session::has('user_id'))) {



            return view('crm.added_funnel_category')->with('fetchdata', $fetchdata);
        } else {

            return redirect('web/crm')->with('fail', 'You are not Login');
        }
    }

    public function funnelstepdashboard() {
        if (empty(Auth::check())) {

            return redirect("login");
        }

        $user_id = Auth::user()->id;



        return view("user-funnel-dashboard.funnel-step");
    }

    public function crmuserdetails($id) {
        
         
        if (empty(Auth::check())) {
            return redirect("login");
        } 
             
      
        $data = Userfunnelsteps::where('user_id', $id)->get();
       
     
      return view('user-funnel-dashboard.funnel_user_step')->with(['data'=>$data]);
       
    }

    public function subdomain() {
      
    
 dd("hallo Sir");


    }
  

    public function addedtemplate()
    {

return view('crm.addedtemplatecategory');


    }
    

    public function domianvalidate()
    {

        echo "hia";
        dd();
    }




}
