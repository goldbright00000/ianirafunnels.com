<?php namespace App\Http\Controllers;
use App\Adminlogin;
use App\Memberlists;

use Session;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;


class Admincontrollers extends Controller
{
	public function create()
	{
		
                            if(empty(Session::has('user_id')))
                            {
                             return view('admin.login'); 
                            }
                            else {
                                 return redirect('login/dashboard');       
                            }

            
            
           
	}
        

        
        public function sucess()
        {
                $totalmember= Memberlists::count();
                $date = new \DateTime();
                $date->modify('-3 hours');
                $formatted_date = $date->format('Y-m-d H:i:s');
                $today_member=Memberlists::where('created_at', '>',$formatted_date)->count();
            
                if(empty(Session::get('user_id')))
                {
                 return redirect('admin-dashboard'); 
                }
                else
                {

               return view('admin.home')->with(['totalmember'=>$totalmember,'todaymember'=>$today_member]);  
                }
           
        }
        
        public function login1()
        {
          //echo "hello";
          return view('loginnew.login'); 
        }
        public function registration1()
        {
          //echo "hello";
          return view('loginnew.registration'); 
        }
        public function forgot_password()
        {
          //echo "hello";
          return view('loginnew.forgot-password'); 
        }
        public function trial()
        {
          //echo "hello";
          return view('loginnew.trial'); 
        }
        public function register()
        {
          //echo "hello";
          return view('loginnew.register'); 
        }
        public function pay()
        {
          //echo "hello";
          return view('loginnew.pay'); 
        }
        
        
        
        
        
        
        public function dashboard()
        {
           
                $totalmember= Memberlists::count();
                $date = new \DateTime();
                $date->modify('-3 hours');
                $formatted_date = $date->format('Y-m-d H:i:s');
                $today_member=Memberlists::where('created_at', '>',$formatted_date)->count();
            
                if(empty(Session::get('user_id')))
                 {
                  return redirect('admin-dashboard'); 
                 }
                 else
                 {

                 return view('admin.home')->with(['totalmember'=>$totalmember,'todaymember'=>$today_member]);  
                 }
            
            
        }
        
        public function logout()
        {
                 Session::flush();
                if(!empty(Session::has('user_id')))
               {

                 return redirect('login/dashboard');              
               }  
               else 
               {
                  return redirect('admin-dashboard'); 
               }           
        }
        
       public function profileedit()
       {
           
            if(empty(Session::has('user_id')))
               {
                return redirect('admin-dashboard'); 
               } 
               
              $user_id=Session::get('user_id');
               
              $query=Adminlogin::where('id',$user_id)->first();
                        
         return view('admin.profile-edit')->with(['email'=>$query->email,'name'=>$query->name]);
           
       }
       
       
       
       public function edit(Request $request)
       {
           
                if(empty(Session::has('user_id')))
               {
                return redirect('admin-dashboard'); 
               } 
              
              
                   
             session()->flash('succ', 'Edit Sucessfully !');  
              
             $query=Adminlogin::where('id', Session::get('user_id'))->update(['name'=>$request->name,'email'=>$request->email]);

             $query=Adminlogin::where('id',Session::get('user_id'))->first();
                        
             return view('admin.profile-edit')->with(['email'=>$query->email,'name'=>$query->name]);
           
       }
       
       public function changepassword()
       {
                if(empty(Session::has('user_id')))
               {
                return redirect('admin-dashboard'); 
               } 
               return view('admin.changepassword');
       }
       
       
        public function passwordchange(Request $request)
       {
                if(empty(Session::has('user_id')))
               {
                return redirect('admin-dashboard'); 
               } 
               
               $oldpassword=$request->oldpassword;
               $user_id=Session::has('user_id');
               $query=Adminlogin::where('password',$oldpassword)->where('id',$user_id)->count();
             
               if($query>0)
               {
                 return view('admin.changepassword')->with('sucess','Password change sucessfully');
               }
            else{
           return view('admin.changepassword')->with('wrong','Old password doesnt match');
               }
               
             
               
               
            
       }
        
          public function funnelcategoy()
               {
                  if(empty(Session::has('user_id')))
               {
                return redirect('admin-dashboard'); 
               }   
                  
                 return view('admin.changepassword');  
                   
               }
               
               
           
               
    


        
        
}


?>
