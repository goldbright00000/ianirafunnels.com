<?php namespace App\Http\Controllers;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Template;
use App\Adminlogin;
use App\Memberlists;
use App\Funnelsname;
use App\Subfunnelcategory;
use App\Allfunnel;
use DB;
use Session;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Userfunnelsteps;

class Funnels extends Controller{

public function page()
{
    
    $a= array();
    $funnel_name=Funnelsname::get();
    foreach($funnel_name as $funnel_row){
        
    $subfunnel=DB::table('funnel_sub_category')->where('funnel_type','=',$funnel_row->id)->get();
    $newarray = array();
    $newarray['funnal'] = $funnel_row;
    $newarray['funnaldata'] = $subfunnel;
    array_push($a,$newarray);
    
    $allfunnels=Allfunnel::get();
   
    
    
    }
 
   $template=Template::all();
          
    return view('select-funnel')->with(['a'=>$a,'allfunnels'=>$allfunnels,'template'=>$template ]);
    
    
    
}
    
public function viewpage($id)
{
    
    
     $template=Template::all();
    $funneldata=Allfunnel::WHERE('id',$id)->first();
    
    
    
    return view('lead-manage')->with(['funneldata'=>$funneldata,'template'=>$template]); 
}
public function edittemplatedata($id)
{
    
   $templatedata=Template::WHERE('id',$id)->first();
    
    
    
    return view('admin.edittemplate')->with(['templatedata'=>$templatedata]);  
    
    
    
}




 public function herofunnel()
 {
    
     return view('herofunnel');
 }
 public function jasonfunnel()
 {
     return view('jasonfunnel');
     
 }
 public function purchases()
 {
                                if (empty(Auth::check()))
                                    {

                                   return redirect("login");
                                    

                                    }
                                    
                               $ungrouprecord=DB::table('userfunnelsteps')->where('user_id','=',Auth::user()->id)->where('funnel_type','=','ungroup')->orderBy('id', 'desc')->get();
                               $cookbookrecord=DB::table('userfunnelsteps')->where('user_id','=',Auth::user()->id)->where('funnel_type','=','cookbook')->orderBy('id', 'desc')->get();
                               $sharefunnels = DB::table('userfunnelsteps')->where('user_id','=',Auth::user()->id)->where('funnel_type','=','sharefunnels')->orderBy('id', 'desc')->get();    
                                    
       //$to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', '2015-5-5 3:30:34');
      //$from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', '2015-6-9 9:30:34');

     //$diff_in_days = $to->diffInDays($from);

     //print_r($diff_in_days); 
        //dd(); 
        
        $from=date('Y-m-d');
        $seven= date('Y-m-d', strtotime('-7 days'));
    $recent_data= DB::table('userfunnelsteps')->whereBetween('funnel_created_time', [$seven,$from])->get();
     return view('contact_purchases')->with(['recent_data'=>$recent_data,'sharefunnels'=>$sharefunnels,'cookbookrecord'=>$cookbookrecord,'ungrouprecord'=>$ungrouprecord]);
     
 }
 
 public function status()
 {
     return view('funnel_status');
 }

 public function funnelrecent()
    {

      if (empty(Auth::check()))
      {

     return redirect("login");
      

      }
      
 $ungrouprecord=DB::table('userfunnelsteps')->where('user_id','=',Auth::user()->id)->where('funnel_type','=','ungroup')->orderBy('id', 'desc')->get();
 $cookbookrecord=DB::table('userfunnelsteps')->where('user_id','=',Auth::user()->id)->where('funnel_type','=','cookbook')->orderBy('id', 'desc')->get();
 $sharefunnels = DB::table('userfunnelsteps')->where('user_id','=',Auth::user()->id)->where('funnel_type','=','sharefunnels')->orderBy('id', 'desc')->get();    
      
//$to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', '2015-5-5 3:30:34');
//$from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', '2015-6-9 9:30:34');

//$diff_in_days = $to->diffInDays($from);

//print_r($diff_in_days); 
//dd(); 

$from=date('Y-m-d');
$seven= date('Y-m-d', strtotime('-7 days'));
$recent_data= DB::table('userfunnelsteps')->whereBetween('funnel_created_time', [$seven,$from])->get();



      return view('funnelrecent')->with(['recent_data'=>$recent_data,'sharefunnels'=>$sharefunnels,'cookbookrecord'=>$cookbookrecord,'ungrouprecord'=>$ungrouprecord]);




    }

  


    

}