<?php namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Session;

class Otherfunnel extends Model
{

    protected $table = 'other_funnel';

       protected $fillable = [
    'funnel_type','funnel_heading','funnel_sub_heading','message','image'];

}
?>