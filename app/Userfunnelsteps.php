<?php namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Session;

class Userfunnelsteps extends Model
{

    protected $table = 'userfunnelsteps';

 protected $fillable = [
    'user_id','funnel_step','funnel_type','funnel_name', 'updated_at', 'created_at', 'funnel_step_times', 'funnel_created_time', 'funnel_url', 'funnel_id', 'funnel_full_url'
];

}
?>