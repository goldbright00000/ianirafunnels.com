<?php namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Session;

class Allfunnel extends Model
{

    protected $table = 'addedfunnel';

       protected $fillable = [
    'funnel_type','funnel_name','funnel_other','message','image','type','page','time'];

}
?>