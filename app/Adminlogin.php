<?php namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Session;

class Adminlogin extends Model
{

    protected $table = 'admin_logins';

 protected $fillable = [
    'username', 'password'
];

}
?>