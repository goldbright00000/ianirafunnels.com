<?php namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Session;

class Crmlogin extends Model
{

    protected $table = 'crm_login';

       protected $fillable = ['name','email','password','created_at','updated_at'];

}
?>