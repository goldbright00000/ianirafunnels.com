jQuery(document).ready(function($){

$(".alertsucess_message").fadeIn(600);
$(".alertsucess_message").fadeOut(7200);


$(".effect_sidebar_special_tab").click(function(){
$(".dashboard_inside_icon").toggleClass('dashboard_inside_icon_add');
});


            $(".inside_icon").addClass('inside_icon2');
            $("#toggleNaviagtionCross").click(function() {
                $(".inside_icon").toggleClass('inside_icon2');
                $("#mySidenav").toggleClass('widthToggle');
                $(".main_wrapper").toggleClass('marginLeft');
                $("body").toggleClass('fadeBackground');
                $(".overlap_nav").toggleClass('overlap_nav_width');                 
            });

            $(".effect_sidebar").click(function(event) {
                if ($(this).find(".ripple_effect").length == 0) {
                    $(this).prepend("<b class='ripple_effect'></b>");
                }
                ripple_effect = $(this).find("b.ripple_effect").removeClass("animate");
                x = event.pageX - ripple_effect.width() / 2 - $(this).offset().left;
                y = event.pageY - ripple_effect.height() / 2 - $(this).offset().top;
                ripple_effect.css({
                    top: y + 'px',
                    left: x + 'px'
                }).addClass("animate"); 
            });
 

            // Date range picker ......
            $('#date_two_picker').daterangepicker({
                    "timePickerIncrement": 2,
                    opens: 'left',
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        // 'This Month': [moment().startOf('month'), moment().endOf('month')],
                        // 'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    "alwaysShowCalendars": true,
                    "startDate": "07/09/2019",
                    "endDate": "08/25/2019"
                }
                // , function(start, end, label) {
                //   console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
                // }
            );

            
            $(".applyBtn").text('Confirm');

            // open_dydnamic1.......
            $(".open_dydnamic1").click(function() {
                $(".open_dydnamic1").toggleClass('add_dydnamic1');
                $(".inthisweek").toggleClass('relative_add1');
                $(".pyramid_chart2_parent").fadeToggle();
            })
 
           

            $(".select_box_1").click(function() {
                $(".multi_slect_inner").toggleClass('multi_slect_inner_height');
                $(".plus_icon").toggleClass('arrow_transform')
            });

         


});








// Javascript.....................

var forEach = function(array, callback, scope) {
            for (var i = 0; i < array.length; i++) {
                callback.call(scope, i, array[i]);
            }
        };

        window.onload = function() {
            var max = -219.99078369140625;
            forEach(document.querySelectorAll('.progress1'), function(index, value) {
                percent = value.getAttribute('data-progress');
                value.querySelector('.fill').setAttribute('style', 'stroke-dashoffset: ' + ((100 - percent) / 100) * max);
                value.querySelector('.fill').setAttribute('style', 'stroke-dashoffset: ' + ((100 - percent) / 100) * max);
                value.querySelector('.value').innerHTML = percent
            });
        }

   function signOut() {
            var auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function() {
                console.log('User signed out.');
            });
        }

        function onLoad() {
            gapi.load('auth2', function() {
                gapi.auth2.init();
            });
        }


// column_chart1
        Highcharts.chart('column_chart1', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ' '
                }
            },
            tooltip: false,
            plotOptions: {
                column: {
                    pointPadding: 0.1,
                    borderWidth: 0.1
                }
            },
            series: [{
                name: '',
                data: [1, 2, 3, 4, 2, 3, 2, 4, 2, 6, 2, 4]

            }]
        });
        // End column_chart1.............

// Start pyramid_chart2................
        Highcharts.chart('pyramid_chart2', {
            chart: {
                type: 'columnpyramid'
            },
            title: {
                text: ''
            },
            colors: ['#777', '#080', '#777', '#080', '#777', '#080'],
            xAxis: {
                crosshair: true,
                labels: {
                    style: {
                        fontSize: '14px'
                    }
                },
                type: 'category'
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: false,
            series: [{
                name: 'Day',
                colorByPoint: true,
                data: [
                    ['Mon', 5],
                    ['Tue', 10],
                    ['Wed', 15],
                    ['Thu', 20],
                    ['Fri', 30],
                    ['Sat', 40],
                    ['Sun', 50]
                ],
                showInLegend: false
            }]
        });
        // End pyramid_chart2................




function myFunction(x) {
            x.classList.toggle("change");
            $(".dashboard_inside_icon").toggleClass('dashboard_ml');
            // $(".select_box_1_parent .arr_1").toggleClass('arr_1_add');
            $(".mydiv").toggleClass('circle_pos');
            $(".myChart1_overlay [data-highcharts-chart='0']").toggleClass('toggle_cls1grph');
            // $(".search_box").toggleClass('search_box_add1');
            // $(".limit_width").toggleClass('limit_width2');
            $(".add_dydnamic1").toggleClass('add_dydnamic1_add');
        }

        Highcharts.chart('areaChart', {
            chart: {
                type: 'area',
                spacingBottom: 0
            },
            title: {
                text: ''
            },
            subtitle: {
                text: '',
                floating: true,
                align: 'right',
                verticalAlign: 'bottom',
                y: 15
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: 100,
                y: 0,
                floating: true,
                borderWidth: 1,
                backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF'
            },
            xAxis: {
                // '02 Apr', ' 18 May', '19 Jun', '09 Jul', '06 Aug','Sep','23 Oct','05 Nov','08 Dec'
                categories: ['Jan 14', 'Feb 25', 'Mar 15', 'Apr 02', 'May 18']
            },
            yAxis: {
                title: {
                    text: ''
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            },
            tooltip: {
                formatter: function() {
                    // return '<b>' + /*this.series.name*/ + '</b><br/>' +
                    // this.x + ' ,  ' + this.series.name +' : ' + this.y;
                    return this.x + ' ,  ' + this.series.name + ' : ' + this.y;
                }
            },
            plotOptions: {
                area: {
                    fillOpacity: 0.5
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Visitor',
                data: [0, 100, 20, 25, 0]
            }]
        });



