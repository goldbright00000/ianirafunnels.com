<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Forgot Password</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link type="text/css" rel="stylesheet" href="{{ asset('css/style.css')}}" />
     <link rel="icon" href="{{ asset('images/brand/risorsa-lg.png')}}" type="image/png" /> 
</head>

<body>


@if(session('wrongotp')) 

 <div id="sucessfullyMessage" class="alert alert-success animated fadeIn sucess_message email-animation alert-warn">
        <button type="button" class="close s_close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
       {{session('wrongotp')}}
        </strong>
    </div>
@endif



@if(isset($sucess)) 

 <div id="sucessfullyMessage" class="alert alert-success animated fadeIn sucess_message">
        <button type="button" class="close s_close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
          {{$sucess}}
        </strong>
    </div>
@endif


@if(isset($fail)) 

 <div id="sucessfullyMessage" class="alert alert-danger animated fadeIn sucess_message">
        <button type="button" class="close s_close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
          {{$fail}}
        </strong>
    </div>
@endif


    <div class="container">
        <div class="back_icon use_left">
            <a href="javascipt:void(0)" onclick="window.history.go(-1); return false;"><img src="{{ asset('images/icons/arrow-left.png')}}" alt="arrow-left" /></a>
        </div>
        <section class="forgot-password">
            <header class="forgot_password_header">
                <figure>
                    <img src="{{asset('images/brand/logo-ianira-DEF.png')}}" alt="brand" class="img-responsive" />
                    <figcaption>
                        <p>Forget your password<span class="question_mark">?</span></p>
                    </figcaption>
                </figure>
            </header>

            <article class="input_box_main">

                <form method="post" action="{{ url('userforget-password')}}">
                     {{ csrf_field() }}



                       
                        <input type="hidden" class="form-control"  name="id" value="{{$id}}"  placeholder="1234">
                   

                    <div class="form-group">
                        <label>Insert Code:</label>
                        <input type="text" class="form-control insert_otp" onkeydown="return ( event.ctrlKey || event.altKey 
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9) 
                    || (event.keyCode>34 && event.keyCode<40) 
                    || (event.keyCode==46) )" maxlength="4" name="otp"  placeholder="1234">
                    </div>
                    <button type="submit" class="btn btn_submit">Confirm</button>
                

                <div class="forget_account">
                    <p>Don't have an account<span class="question_mark">?</span>&nbsp;<span class="clearfix visible-sm"></span> <a href="{{route('register')}}" class="registration">Registration</a></p>
                </div>
                </form>
            </article>
 		</section>
    </div>
    <!-- //container -->
   

<div class="container">
<footer class="footer">
    <div class="row">
        <div class="col-sm-7"></div>
        <div class="col-sm-3"><a href="#" class="faq_support">Faq Support</a></div>
        <div class="col-sm-2"></div>
    </div>
</footer>
</div>

 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">

$(function(){
    $(".email-animation").fadeIn();
setInterval(function(){
    $(".email-animation").fadeOut();
}, 12000 );






            
            function isInputNumber(evt){
                
                var ch = String.fromCharCode(evt.which);
                
                if(!(/[0-9]/.test(ch))){
                    evt.preventDefault();
                }
                
            }
            
  




})



</script>   


<style>
    .alert-warn{
    background: #d35351 !important;

}

.email-animation{
    animation-name:faded;
    animation-iteration-count: 1;
    animation-duration: 4s;
    animation-fill-mode: forwards;
}
@keyframes faded{
    0%{
       
        opacity:0;
position: absolute;
        left:100%
    }
      
    100%{
       opacity:1;
position: absolute;
        left:50%;
    
    }
}


     .sucess_message {
    margin: 0;
        margin-bottom: 0px;
    margin-bottom: 0px;
    position: absolute !important;
    top: 12%;
    left: 50%;
    transform: translate(-50%, -50%);
    position: absolute;
    width: 24%;
    height: 41px;
    line-height: 15px;
    text-align: left;
    z-index: 999999;
    color:white;
}
.s_close
{
   line-height: 15px; 
}

.faq_support{
    text-align: right;
    float: right;
    color: #FDBB17!important;
    margin-bottom: 30px;
    margin-top: 20px;
}
.login_page {
    padding-bottom: 30px;
}


@media only screen and (max-width: 767px){

.forgot-password {
    background-size: 100%;
        background-position: 0 230px;
    padding: 50px 15px;
}
.faq_support {
    padding-right: 15px;
}


}

</style>  


<script>
    
    
  
 </script>


</body>

</html>