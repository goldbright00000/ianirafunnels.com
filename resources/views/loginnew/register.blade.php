<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Registration</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link type="text/css" rel="stylesheet" href="css/style.css" />
     <link rel="icon" href="images/brand/risorsa-lg.png" type="image/png" /> 
</head>

<body id="overflow_y">

    <div class="container">

        <section class="registration">
            <article class="registration_article">
                <header class="registration_header">
                    <figure>
                        <img src="images/brand/logo-ianira-DEF.png" alt="brand" class="img-responsive" />
                        <p>Create content for your marketing funnel, with focus on seo</p>
                    </figure>
                </header>

                <div class="registration_block">
                    <p> <a href="register" class="registration_btn btn btn-warning">Registration</a>
                        <br> <a href="login" class="login_link">Login</a></p>
                </div>
            </article>
</section>

    </div>
    <!-- //container -->
    

<div class="container">
<footer class="footer">
    <div class="row">
        <div class="col-sm-8"></div>
        <div class="col-sm-3"><a href="#" class="faq_support">Faq Support</a></div>
        <div class="col-sm-1"></div>
    </div>
</footer>
</div>


<style>
    
.faq_support{
    text-align: right;
    float: right;
    color: #FDBB17!important;
}
.login_page {
    padding-bottom: 30px;
}

@media (max-width: 767px) {

.login_page {
    background-position: 15px 480px !important;
}
span::before {
    background: #ffffff;
}
.faq_support {
    padding-right: 15px;
    margin-top: 50px;
}



.registration {
    max-width: 100%;
    height: auto;
    background-size: 100%;
    background-position: 20px 120px;
    padding-top: 0;
}


} 
</style>


</body>

</html>