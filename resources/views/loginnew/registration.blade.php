<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Registration</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link type="text/css" rel="stylesheet" href="./css/style.css" />
    <link rel="icon" href="images/brand/risorsa-lg.png" type="image/png" /> 
</head>
<body id="overflow_y">

    <div class="container">
        <div class="back_icon use_left">
            <a href="#"><img src="images/icons/arrow-left.png" alt="arrow-left" /></a>
        </div>
        <section class="login_page">
            <header class="forgot_password_header">
                <figure>
                    <img src="images/brand/logo-ianira-DEF.png" alt="brand" class="img-responsive" />

                    <div class="heading_txt">
                        <p class="mp_sale">Map your sales funnel, the easy way</p>
                        <p class="create_strategy">Create strategy, build a template,start implementation</p>
                        <p class="header_registration_txt">Registration</p>
                    </div>
                </figure>
            </header>

            <article class="login_article">
                <form>
                    <div class="form-group">
                        <div class="use_google_account">
                            <span>Use <span class="google">Google</span> Account</span>
                        </div>
                    </div>

                    <div class="line">
                        <span>or</span>
                    </div>

                     <div class="form-group">
                        <label>Email:</label>
                        <input type="email" class="form-control" placeholder="prova@gmail.com">
                    </div>
                     <div class="form-group">
                        <label>Password:</label>
                        <input type="password" class="form-control"  placeholder="******************">
                    </div>

                     <div class="form-group">
                        <label>Confirm Password:</label>
                        <input type="password" class="form-control"  placeholder="******************">
                    </div>

                    <button type="submit" class="btn btn_login rgistration_btn1">Registration</button>
                    <!-- <a href="forgot_password" class="foget_txt">Forget your password<span class="question_mark">?</span></a><span class="clearfix"></span> -->
                </form>
<span class="clearfix"></span>
                <div class="login_forget_account">
                    <p>Have an account<span class="question_mark">?</span>&nbsp;<span class="clearfix visible-sm"></span> <a href="login1" class="registration" style="padding: 0">Log In</a></p>
                </div>
            </article>
</section>
    </div>
    <!-- //container -->
    
<div class="container">
<footer class="footer">
    <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-3"><a href="#" class="faq_support">Faq Support</a></div>
        <div class="col-sm-3"></div>
    </div>
</footer>
</div>

 
    

 


<style>
        
.faq_support{
    text-align: right;
    float: right;
    color: #FDBB17!important;
    margin-bottom: 30px;
    margin-top: 20px;
}
.login_page {
    padding-bottom: 30px;
}

 /*-------------------Start Registration ----------------------*/
.header_registration_txt{
    font-size: 24px !important;
    margin-top: 40px;
    color: #FDBB17 !important;
    letter-spacing: 1px;
    font-family: futuraBook;
    font-weight: 700;  
}   
/*-------------------End Registration ----------------------*/





/*-----------Apply this only not use another css file-----------*/
.login_page {
    background-position: 90px 290px !important;
}
/*-----------Apply this only not use another css file-----------*/
 





@media (max-width: 767px) {

.login_page {
    background-position: 15px 480px !important;
}
span::before {
    background: #ffffff;
}
.faq_support{padding-right: 15px}




} 


</style>



</body>
</html>