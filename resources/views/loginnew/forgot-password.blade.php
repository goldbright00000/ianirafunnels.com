<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Forgot Password</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link type="text/css" rel="stylesheet" href="{{ asset('css/style.css')}}" />
     <link rel="icon" href="{{ asset('images/brand/risorsa-lg.png')}}" type="image/png" /> 

</head>

<body>
  


@if(isset($sucess)) 

 <div id="sucessfullyMessage" class="alert alert-success animated fadeIn sucess_message">
        <button type="button" class="close s_close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
          {{$sucess}}
        </strong>
    </div>
@endif


@if(session('fail')) 

 <div id="sucessfullyMessage" class="alert alert-danger animated fadeIn sucess_message email-animation alert-warn">
        <button type="button" class="close s_close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
          {{session('fail')}}
        </strong>
    </div>
@endif




@if(session('expiryotp')) 

 <div id="sucessfullyMessage" class="alert alert-danger animated fadeIn expiry_message email-animation">
        <button type="button" class="close s_close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
          {{ session('expiryotp')}}
        </strong>
    </div>
@endif
   




@if(session('expirylink')) 

 <div id="sucessfullyMessage" class="alert alert-danger animated fadeIn expiry_message email-animation">
        <button type="button" class="close s_close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
          {{ session('expirylink')}}
        </strong>
    </div>
@endif



@if(session('sendotp'))
<div id="sucessfullyMessage" class="alert alert-success animated email-animation fadeIn alertsucess_message">
        <button type="button" class="close s_close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
          {{ session('sendotp') }}
        </strong>
    </div>
@endif



    <div class="container">
        <div class="back_icon use_left">
      
            <a href="{{url('login')}}" ><img src="images/icons/arrow-left.png" alt="arrow-left" /></a>
        </div>
        <section class="forgot-password">
            <header class="forgot_password_header">
                <figure>
                    <img src="images/brand/logo-ianira-DEF.png" alt="brand" class="img-responsive" />
                    <figcaption>
                        <p>Forget your password<span class="question_mark">?</span></p>
                    </figcaption>
                </figure>
            </header>

            <article class="input_box_main">

                <form method="post" action="{{ url('dataforget')}}">
                     {{ csrf_field() }}
                    <div class="form-group">
                        <label>Email or Nickname:</label>
                        <input type="email" class="form-control"  name="email"  placeholder="prova@gmail.com" required="true">
                    </div>
                    <button type="submit" class="btn btn_submit">Confirm</button>
                

                <div class="forget_account">
                    <p>Don't have an account<span class="question_mark">?</span>&nbsp;<span class="clearfix visible-sm"></span> <a href="{{ route('register') }}" class="registration">Registration</a></p>
                </div>
                </form>
            </article>
 		</section>
    </div>
    <!-- //container -->
   

<div class="container">
<footer class="footer">
    <div class="row">
        <div class="col-sm-7"></div>
        <div class="col-sm-3"><a href="#" class="faq_support">Faq Support</a></div>
        <div class="col-sm-2"></div>
    </div>
</footer>
</div>



 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">

$(function(){
    $(".email-animation").fadeIn();
setInterval(function(){
    $(".email-animation").fadeOut();
 }, 12000 );




})



</script>


 
    


<style>
    .expiry_message
    {
         background: #d35351 !important;
     margin: 0;
    margin-bottom: 0px;
margin-bottom: 0px;
margin-bottom: 0px;
position: absolute !important;
top: 12%;
left: 50%;
transform: translate(-50%, -50%);
position: absolute;
width: 32%;
height: 41px;
line-height: 15px;
text-align: left;
z-index: 999999;
color: white;   
    }
   .alertsucess_message {
    margin: 0;
        margin-bottom: 0px;
    margin-bottom: 0px;
    margin-bottom: 0px;
    position: absolute !important;
    top: 12%;
    left: 50%;
    transform: translate(-50%, -50%);
    position: absolute;
    width: 31%;
    height: 50px;
    line-height: 15px;
    text-align: left;
    z-index: 999999;
    color: white;
    background: #489148;
}

     .sucess_message {
    margin: 0;
        margin-bottom: 0px;
    margin-bottom: 0px;
    position: absolute !important;
    top: 12%;
    left: 50%;
    transform: translate(-50%, -50%);
    position: absolute;
    width: 24%;
    height: 41px;
    line-height: 15px;
    text-align: left;
    z-index: 999999;
    color:white;
}
.s_close
{
   line-height: 15px; 
}

.faq_support{
    text-align: right;
    float: right;
    color: #FDBB17!important;
    margin-bottom: 30px;
    margin-top: 20px;
}
.login_page {
    padding-bottom: 30px;
}
.alert-warn{
    background: #d35351 !important;

}

.email-animation,.alertsucess_message{
    animation-name:faded;
    animation-iteration-count: 1;
    animation-duration: 4s;
    animation-fill-mode: forwards;
}
@keyframes faded{
    0%{
       
        opacity:0;
position: absolute;
        left:100%
    }
      
    100%{
       opacity:1;
position: absolute;
        left:50%;
    
    }
}



@media only screen and (max-width: 767px){

.forgot-password {
    background-size: 100%;
        background-position: 0 230px;
    padding: 50px 15px;
}
.faq_support {
    padding-right: 15px;
}


}

@media only screen and (max-width: 1050px){

.sucess_message {
    top: 15%;
    left: 0;
    transform: translate(-50%, -50%);
    position: absolute;
    width: 100% !important;
    height: auto!important;
}

}  

</style>  


<script>
    
    
  
 </script>


</body>

</html>