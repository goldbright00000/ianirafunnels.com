<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
     <link type="text/css" rel="stylesheet" href="css/style.css" />
     <link rel="icon" href="images/brand/risorsa-lg.png" type="image/png" /> 
</head>
<body id="overflow_y">
    <div class="container">
        <div class="back_icon use_left">
            <a href="#"><img src="images/icons/arrow-left.png" alt="arrow-left" /></a>
        </div>
        <section class="login_page">
            <header class="forgot_password_header">
                <figure>
                    <img src="images/brand/logo-ianira-DEF.png" alt="brand" class="img-responsive" />

                    <div class="heading_txt">
                        <p class="mp_sale">Map your sales funnel, the easy way</p>
                        <p class="create_strategy">Create strategy, build a template,start implementation</p>
                    </div>
                </figure>
            </header>

            <article class="login_article">
                <form method="POST" action="{{ route('login') }}">
                     {{ csrf_field() }}
                    <div class="form-group">
                        <div class="use_google_account">
                            <span>Use <span class="google">Google</span> Account</span>
                        </div>
                    </div>

                    <div class="line">
                        <span>or</span>
                    </div>

                     <div class="form-group">
                        <label>Email or Nickname:</label>
                        <input type="email" class="form-control" id="" name="" placeholder="prova@gmail.com" name="email" value="{{ old('email') }}" required autofocus>
                        @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif


                    </div>
                     <div class="form-group">
                        <label>Password:</label>
                        <input type="password" class="form-control" id="password" placeholder="******************" name="password" required>
                         @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                    </div>
                     <span class="clearfix"></span>
                    <button type="submit" class="btn btn_login">Log In</button>
                    <a href="forgot_password" class="foget_txt">Forget your password<span class="question_mark">?</span></a>
                    <span class="clearfix"></span> 
                </form>
<span class="clearfix"></span>  
                <div class="login_forget_account"> 
                    <p>Don't have an account<span class="question_mark">?</span>&nbsp;<span class="clearfix visible-sm"></span> <a href="{{ route('register') }}" class="registration" style="padding-top:0">Registration</a></p>
                </div>
            </article>
</section>
    </div>
    <!-- //container -->
    
<div class="container">
<footer class="footer">
    <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-3"><a href="#" class="faq_support">Faq Support</a></div>
        <div class="col-sm-3"></div>
    </div>
</footer>
</div>

</body>
</html>