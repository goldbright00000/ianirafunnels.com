<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pay</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link type="text/css" rel="stylesheet" href="./css/style.css" />
    <link rel="icon" href="images/brand/risorsa-lg.png" type="image/png" /> 
</head>
<body>

    <div class="container">
        <div class="back_icon use_left">
            <a href="#"><img src="images/icons/arrow-left.png" alt="arrow-left" /></a>
        </div>


        <section class="login_page">
            <header class="forgot_password_header">
                <figure>
                    <img src="images/brand/logo-ianira-DEF.png" alt="brand" class="img-responsive" />

                    <div class="heading_txt_pay">
                        <p class="free_15_trial">Free 15 Trial <br> <a href="#" class="btn give_it_try">Give it a try</a></p>
                    </div>
                </figure>

                <div class="another_header"> 
                        <p class="buy_complete_txt">Buy complete package</p>
                        <div class="twince_btn">
                            <div class="twince_btn_1 twin1">Package: one year for only 150&euro;</div>
                            <div class="twince_btn_1 twin2">Package: every month 40&euro;</div>
                        </div>
                </div>
            </header>

           

<span class="clearfix"></span> <br><br>

<div class="all_pay_input_box">
     <div class="name">
        <label class="cnd_non">Name:</label>
        <input type="text" class="form-control" id="" placeholder="Mario Rossi">
    </div>
    
    <div class="card-number">
        <label class="cnd_non">Card Number:</label>
        <input type="text" class="form-control" id="" placeholder="0000 0000 0000 0000">        
    </div>


<div class="card-number width1">
        <label class="cnd_non">Date:</label>
        <input type="text" class="form-control datePicker" id="" placeholder="09/12" style="text-align: left;">        
    </div>

    <div class="card-number width2">
        <label class="cnd_non">CVC:</label>
        <input type="text" class="form-control" id="" placeholder="123" style="text-align: left;">        
    </div>
<br><br>
<span class="clearfix"></span>

<label class="chk1">Save this card
  <input type="checkbox" checked="checked">
  <span class="checkmark"></span>
</label>


</div>
 


<div class="claerfix"></div> <br><br>

 <div class="row">
     <div class="col-sm-6"><a href="#" class="pay_with_paypal">Pay with paypal <img src="images/icons/arrow-right.png" width="15" ></a></div>
     <div class="col-sm-6"><a href="#" class="btn btn_login confirm_btn2">Confirm</a></div>
 </div>

</div>

   <!--          <span class="clearfix"></span>
            <div class="login_forget_account">
                <p>Have an account<span class="question_mark">?</span>&nbsp;<span class="clearfix visible-sm"></span> <a href="login1" class="registration" style="padding: 0">Log In</a></p>
            </div> -->
            
 



</section>
    </div>
    <!-- //container -->
    

<div class="container">
<footer class="footer">
    <div class="row">
        <div class="col-sm-7"></div>
        <div class="col-sm-2"><a href="#" class="faq_support">Faq Support</a></div>
        <div class="col-sm-3"></div>
    </div>
</footer>
</div>

<style>
    
.faq_support{
    text-align: right;
    float: right;
    color: #FDBB17!important;
    margin-bottom: 30px;
    margin-top: 20px;
}
.width1{width:47%;float:left;}
.width2{width:46%;float:left;margin-left: 20px;}
 .pay_with_paypal{
   color: #FDBB17!important;
    font-size: 14px;
    font-family: futuraMedium;
    display: inline-block;
    text-decoration: underline;
 }   
.confirm_btn2 {
    line-height: 28px;
    margin: 0;
    border-radius: 10px;
    box-shadow: 0 0 1px 0 #FDBB17 !important;
    border-color: #FDBB17;
    width: 150px;
    height: 40px;
    float: right;
    margin-top: -20px;
}
.save_card_input {
    height: 50px;
    border: 1px solid #dedede;
    outline: 0;
    border-radius: 0;
    font-family: futuraBook;
    font-size: 18px;
    text-align: center;
    box-shadow: 0px 0px 2px 0 #ddd;
    margin-top: 5px;
    background: #fff;
    line-height: 50px;
    font-weight: 600;
}


.header_registration_txt{
    font-size: 24px !important;
    margin-top: 40px;
    color: #FDBB17 !important;
    letter-spacing: 1px;
    font-family: futuraBook;
    font-weight: 600;  
}   
 
.heading_txt_pay {
    text-align: center;
    margin-top: 40px;
    border: 1px solid #eee;
    padding: 15px;
    padding-bottom: 10px;
    font-family: futuraBook;
    font-weight: 600;
    letter-spacing: 1px;
    margin-left: -50px;
    margin-right: -50px;
}
 .free_15_trial{
    font-size: 18px;
 }
.give_it_try {
    border: 1px solid #FDBB17 !important;
    margin-top: 15px;
    width: 150px;
    height: 35px;
    box-shadow: 0 0 !important;
    outline: 0 !important;
    color: #FDBB17 !important;
    font-size: 14px;
    transition: 0.5s;
    font-weight: 600;
}
.give_it_try:hover{
    background: #FDBB17 !important;
    color: #fff !important;
}
.buy_complete_txt{
    font-family: futuraBook;
    font-weight: 600;
    letter-spacing: 1px;
    font-size: 18px;
    text-align: center;
    margin: 20px 0;
}
.twince_btn_1 {
    width: 48%;
    float: left;
    border: 1px solid #ddd;
    padding: 10px 5px;
    font-family: futuraBook;
    font-weight: 600;
    letter-spacing: 1px;
    font-size: 12px;
}

.name,.card-number {
    margin-bottom: 30px;
}
.twin2{margin-left: 20px;}
.cnd_non {
    float: left;
    font-size: 12px;
    margin: 0;
    padding: 0;
    color: #8c8c8c;
    font-family: futuraMedium;
    letter-spacing: 1px;
    font-weight: 400;
}
.all_pay_input_box {
    width: 350px;
    margin: auto;
    border: 1px solid #ddd;
    padding: 15px;
    border-radius: 10px;
}
.all_pay_input_box .form-control {
    height: 50px;
    outline: 0;
    border-radius: 0;
    font-family: futuraMedium;
    font-size: 18px;
    text-align: left;
    box-shadow: 0 0;
    border: 0;
    border-bottom: 1px solid #ccc;
    background: transparent;
    padding-left: 0;
}
.all_pay_input_box .form-control:focus{
    border-bottom: 1px solid #FDBB17 !important;
}
.chk1 {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 22px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    font-size: 16px;
    color: #fdbb17;
    letter-spacing: 1px;
}

 
.chk1 input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

 
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #fff;
    border: 1px solid #fde6a8;
}
 
/*.chk1:hover input ~ .checkmark {
  background-color: #ccc;
}*/

 /*fdbb17*/
.chk1 input:checked ~ .checkmark {
    background-color: #ffffff;
    border: 1px solid #fdbb17;
}

.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

 
.chk1 input:checked ~ .checkmark:after {
  display: block;
}

 
.chk1 .checkmark:after {
    left: 9px;
    top: 4px;
    width: 7px;
    height: 15px;
    border: solid #fdbb17;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}


/*-----------Apply this only not use another css file-----------*/
.login_page {
    background-position: 90px 250px !important;
    padding-bottom: 30px;
}
/*-----------Apply this only not use another css file-----------*/




 @media (max-width: 767px) {

.login_page {
    background-position: 15px 480px !important;
}
span::before {
    background: #ffffff;
}
.faq_support{padding-right: 15px}

.chk1 {
    display: inline-block;
}

.confirm_btn2 {
    width: 120px;
    }

    .all_pay_input_box {
    width: 100%;
}

.width2 {
    width: 40%;
}
.twince_btn_1 {
    width: 100%;
    margin-bottom: 10px;
    padding-left: 15px;
    }
    .heading_txt_pay {
    margin-left: 0;
    margin-right: 0;
}
.twin2{
        margin-left: 0;
}


} 

</style>

 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

<script>
    $(function(){

    $(".datePicker").datepicker();

    })
</script>

</body>
</html>