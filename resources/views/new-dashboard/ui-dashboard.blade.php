<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dashboard</title>
    <link rel="icon" href="{{ url('images/brand/risorsa-lg.png')}}" type="image/png" />
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <meta name="google-signin-client_id" content="859033187105-lngvjlsn3bk69fbfe21akilvdeuopu43.apps.googleusercontent.com">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('css/style.css')}}" />
    
</head>

<body>
    @if (session()->has('success'))
    <div id="sucessfullyMessage" class="alert alert-success animated email-animation fadeIn alertsucess_message">
        <button type="button" class="close s_close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
    {!! session()->get('success') !!}
        </strong>
    </div>
    @endif @if (session()->has('register'))
    <div id="sucessfullyMessage" class="alert alert-success animated email-animation fadeIn alertsucess_message">
        <button type="button" class="close s_close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
    {!! session()->get('register') !!}
        </strong>
    </div>
    @endif

    <!-- <div id="mySidenav" class="sidenav">
        <div class="overlap_nav" id="toggleNaviagtion">
            <a href="{{url('user/dashboard')}}">
                <img src="{{ url('images/brand/risorsa-lg.png')}}" alt="brand" class="brand_lg" width="40" />
            </a>
        </div>

        
        <div class="navigation_menu" id="toggleNaviagtionCross" >
            <a href="javascript:void(0)" onclick="myFunction(this); toggleNaviagtionMenu()" class="effect_sidebar mt_list1 effect_sidebar_special_tab">       
            <span class="menu_left"> 
                <div class="bar1"></div>                 
                <div class="bar2"></div> 
                <div class="bar3"></div>
            </span> 
          
            <span class="inside_icon dashboard_inside_icon">Dashboard</span>
        </a>           
            <a href="#" class="effect_sidebar"><span class="inside_icon">
                <img src="{{url('images/navigation-icons/white_icons/001-filter.png')}}" width="35"></span> Funnel</a>
            <a href="#" class="effect_sidebar"><span class="inside_icon">
                <img src="{{url('images/navigation-icons/white_icons/003-seo.png')}}" width="35"></span> Campaigns</a>
            <a href="#" class="effect_sidebar"><span class="inside_icon"><img src="{{url('images/navigation-icons/white_icons/team-leader.png')}}" width="35"></span> Leads</a>

            <a href="#" class="effect_sidebar"><span class="inside_icon"><img src="{{ url('images/navigation-icons/white_icons/gift.png')}}" width="35"></span> My products</a>

            <a href="#" class="effect_sidebar"><span class="inside_icon"><img src="{{ url('images/navigation-icons/white_icons/online-class.png')}}" width="35"></span> My courses</a>

            <a href="#" class="effect_sidebar"><span class="inside_icon"><img src="{{ url('images/navigation-icons/white_icons/006-progress-report.png')}}" width="35"></span> Stats</a>
            <a href="#" class="effect_sidebar"><span class="inside_icon"><img src="{{ url('images/navigation-icons/white_icons/012-domain-registration.png')}}" width="35"></span> Domain</a>
            <a href="#" class="effect_sidebar"><span class="inside_icon"><img src="{{ url('images/navigation-icons/white_icons/007-percentage.png')}}" width="35"></span> Sales</a>
            <a href="#" class="effect_sidebar"><span class="inside_icon"><img src="{{ url('images/navigation-icons/white_icons/009-logout-sign.png')}}" width="35"></span> Logout</a>
        </div>        
    </div>
     -->

     <div id="mySidenav" class="sidenav">
        <div class="overlap_nav" id="toggleNaviagtion">
            <a href="{{url('user/dashboard')}}">
                <img src="{{ url('images/brand/risorsa-lg.png')}}" alt="brand" class="brand_lg" width="40" />
            </a>
        </div>

        <div class="navigation_menu">
            <a href="javascript:void(0)" class="effect_sidebar mt_list1" id="toggleNaviagtionCross" onclick="myFunction(this)">
                <span class="inside_icon">
            <span class="menu_left"> 
                <div class="bar1"></div>                 
                <div class="bar2"></div> 
                <div class="bar3"></div>
            </span> <span class="inside_icon dashboard_inside_icon">Dashboard</span></a>
            <!-- <img src="images/navigation-icons/white_icons/list_menu1.png" width="30"></span> -->
            <a href="#" class="effect_sidebar"><span class="inside_icon">
                <img src="{{url('images/navigation-icons/white_icons/001-filter.png')}}" width="35"></span> Funnel</a>
            <a href="#" class="effect_sidebar"><span class="inside_icon">
                <img src="{{url('images/navigation-icons/white_icons/003-seo.png')}}" width="35"></span> Campaigns</a>
            <a href="#" class="effect_sidebar"><span class="inside_icon"><img src="{{url('images/navigation-icons/white_icons/team-leader.png')}}" width="35"></span> Leads</a>

            <a href="#" class="effect_sidebar"><span class="inside_icon"><img src="{{ url('images/navigation-icons/white_icons/gift.png')}}" width="35"></span> My products</a>

            <a href="#" class="effect_sidebar"><span class="inside_icon"><img src="{{ url('images/navigation-icons/white_icons/online-class.png')}}" width="35"></span> My courses</a>

            <a href="#" class="effect_sidebar"><span class="inside_icon"><img src="{{ url('images/navigation-icons/white_icons/006-progress-report.png')}}" width="35"></span> Stats</a>
            <a href="#" class="effect_sidebar"><span class="inside_icon"><img src="{{ url('images/navigation-icons/white_icons/012-domain-registration.png')}}" width="35"></span> Domain</a>
            <a href="#" class="effect_sidebar"><span class="inside_icon"><img src="{{ url('images/navigation-icons/white_icons/007-percentage.png')}}" width="35"></span> Sales</a>
            <a href="#" class="effect_sidebar"><span class="inside_icon"><img src="{{ url('images/navigation-icons/white_icons/009-logout-sign.png')}}" width="35"></span> Logout</a>
        </div>
        <!-- <div class="nav_footer">
            <a href="#"><span class="inside_icon"><img src="images/navigation-icons/white_icons/009-logout-sign.png" width="35"></span> Logout</a>
        </div> -->
    </div>
    <!-- #mySidenav -->

    <!-- #mySidenav -->

    <div id="main" class="main_wrapper">

        <div class="col-md-12 header_first">
            <div class="col-md-3 col-lg-3 p-0">
                <figure class="logo">
                      <a href="{{url('user/dashboard')}}">
                    <img src="{{ url('images/brand/risorsa-5.png')}}" alt="logo" width="250" />
                </a>
                </figure>
            </div>
            <div class="col-md-5 col-lg-6"></div>
            <div class="col-md-4 col-lg-3 p-0">
                <ul class="list_top_icon list-inline">
                    <li>
                        <div class="search_box">
                            <input type="text" class="search_box_txt" placeholder="Search">
                        </div>
                    </li>
                    <li class="loupe"><img src="{{ url('images/navigation-icons/support2.png')}}" width="20" /></li>
                    <li class="notifications"><img src="{{ url('images/navigation-icons/icons/notifications-button.png')}}" class="notifications_well" width="20" />
                        <div class="notification_inn notification_similar">
                            <article>
                                <ul class="notification_list list-group">
                                    <li class="list-group-item"><a href="#">Notifications 1</a></li>
                                    <li class="list-group-item"><a href="#">Notifications 2</a></li>
                                    <li class="list-group-item"><a href="#">Notifications 3</a></li>
                                    <li class="list-group-item"><a href="#">Notifications 4</a></li>
                                    <div class="notifications_delete">
                                        <img src="{{ url('images/navigation-icons/rubbish-bin.png')}}" width="15" />
                                    </div>
                                </ul>
                            </article>
                        </div>
                    </li>
                    <li class="settingsTool notifications"><img src="{{ url('images/navigation-icons/icons/settings-work-tool.png')}}" class="notifications_setting2" width="20" />

                        <div class="notification_inn2 notification_similar">
                            <article>
                                <ul class="notification_list list-group">
                                    <li class="list-group-item"><a href="#">Settings 1</a></li>
                                    <li class="list-group-item"><a href="#">Settings 2</a></li>
                                    <li class="list-group-item"><a href="#">Settings 3</a></li>
                                    <li class="list-group-item"><a href="#">Settings 4</a></li>
                                </ul>
                            </article>
                        </div>
                    </li>
                    <li class="userImg same_userImg notifications">
                        <div class="user_same_userImg">
                            <img src="{{ url('https://wowsciencecamp.org/wp-content/uploads/2018/07/dummy-user-img-1.png')}}" class="notifications_setting3 border_radius1" width="30" />
                        </div>

                        <div class="notification_inn3 notification_similar">
                            <article>
                                <ul class="notification_list list-group">
                                    <li class="list-group-item"><a href="#">user 1 </a></li>
                                    <li class="list-group-item"><a href="#">user 2 </a></li>
                                    <li class="list-group-item"><a href="#">user 3 </a></li>
                                    <li class="list-group-item"><a href="{{url('logout')}}" onclick="signOut();">Log Out</a></li>
                                </ul>
                            </article>
                        </div>

                    </li>
                </ul>
            </div>
        </div>

        <div class="container-fluid add_invisible_scroll">
            <div class="row">
                <div class="col-md-4 pos_top1">
                    <div class="col widget">
                        <article class="box_shadow bg_white p-15">
                            <p class="text-center">
                                <img src="{{ url('images/navigation-icons/plus-icon.svg')}}" alt="" width="20" style="margin-top: -5px;margin-right: 7px;" />
                                <span class="widget_txt"><b>Widget</b></span>
                            </p>
                        </article>

                        <span class="clearfix"></span>

                        <article class="box_shadow bg_white p-15 mt-10 mb-10 inweek_pos open_dydnamic1">
                            <div class="profit_txt">
                                <span class="widget_txt" style="margin-left: 5px;"><b>Profit</b></span>
                                <br>
                                <span class="clearfix"></span>

                                <svg width="100" class="progress1 noselect profit1" data-progress="14" x="0px" y="0px" viewBox="0 0 80 80">
                                    <path class="track" d="M5,40a35,35 0 1,0 70,0a35,35 0 1,0 -70,0" />
                                    <path class="fill" d="M5,40a35,35 0 1,0 70,0a35,35 0 1,0 -70,0" />
                                    <text class="value" x="40%" y="55%">0</text>
                                    <text class="value" x="65%" y="55%">%</text>
                                </svg>

                                <div class="inthisweek">
                                    <br>
                                    <span class="plus_5_percent" style="margin-bottom: 14px;display: inline-block;"><i>In this Week </i></span>
                                    <br>
                                    <svg width="50" class="progress1 blue noselect profit2" data-progress="25" x="0px" y="0px" viewBox="0 0 100 100">
                                        <path class="track" d="M5,40a35,35 0 1,0 70,0a35,35 0 1,0 -70,0" />
                                        <path class="fill" d="M5,40a35,35 0 1,0 70,0a35,35 0 1,0 -70,0" />
                                        <text class="value" x="37%" y="45%">0</text>
                                        <text class="value" x="56%" y="45%">%</text>
                                        <text class="value" x="22%" y="45%">+</text>
                                    </svg>

                                </div>
                                <br>
                                <div class="pyramid_chart2_parent">
                                    <div id="pyramid_chart3"></div>
                                </div>

                            </div>
                        </article>
                    </div>

                    <article class="chartCss1">
                        <div class="new_leads_pos">
                            <span class="new_leads"><b>New Leads</b></span>
                            <br>
                            <span class="new_leads_txt">55</span>
                        </div>
                        <div class="myChart1_overlay">
                            <div id="column_chart1"></div>
                        </div>
                    </article>
                </div>

                <div class="col-md-8 pl-0 pr-0 new_width_class1__aaaa___">

                    <div class="col mb-15">
                        <div class="col-md-7 acquisti_effettuati pl-0 pr-5 box_shadow bg_white p-15 mb-10">
                            <div class="row">
                                <div class="col-sm-6 pr-0">
                                    <p class="pl-15"><b>Purchases made</b></p>
                                    <div class="cricle_progress1">
                                        <center>
                                            <svg width="100" class="progress1 blue noselect acquisti_graph1" data-progress="75" x="0px" y="0px" viewBox="0 0 80 80">
                                                <path class="track" d="M5,40a35,35 0 1,0 70,0a35,35 0 1,0 -70,0" />
                                                <path class="fill" d="M5,40a35,35 0 1,0 70,0a35,35 0 1,0 -70,0" />
                                                <text class="value value3" x="50%" y="60%">0</text>
                                            </svg>
                                        </center>
                                    </div>
                                    <!-- </article> -->
                                </div>
                                <div class="col-sm-6 p-0">
                                    <p class="media_acquisti_giornallera"><i>Average daily purchases</i></p>
                                    <div class="cricle_progress1 cricle_progress2">
                                        <center>
                                            <svg width="50" class="progress1 blue noselect acquisti_graph2" data-progress="3" x="0px" y="0px" viewBox="0 0 100 100">
                                                <path class="track" d="M5,40a35,35 0 1,0 70,0a35,35 0 1,0 -70,0" />
                                                <path class="fill" d="M5,40a35,35 0 1,0 70,0a35,35 0 1,0 -70,0" />
                                                <text class="value yellow_txt" x="35%" y="45%">0</text>
                                                <text class="value_2 yellow_txt" x="42%" y="45%">,</text>
                                                <text class="value_3 yellow_txt" x="50%" y="45%">9</text>
                                            </svg>
                                        </center>
                                    </div>
                                    <!-- </article> -->
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5 acquisti_effettuati pr-0 pl-5">
                            <article class="box_shadow bg_white p-15 mb-10" style="margin-left: 5px;">
                                <p><b>Online users</b></p>
                                <center>
                                    <svg width="100" class="progress1 blue noselect realtime" data-progress="35" x="0px" y="0px" viewBox="0 0 80 80">
                                        <path class="track" d="M5,40a35,35 0 1,0 70,0a35,35 0 1,0 -70,0" />
                                        <path class="fill" d="M5,40a35,35 0 1,0 70,0a35,35 0 1,0 -70,0" />
                                        <text class="value value2" x="50%" y="60%">0</text>
                                    </svg>
                                    <br>
                                    <span><i>Realtime 02:35:15</i></span>
                                </center>
                            </article>
                        </div>
                    </div>

                    <span class="clearfix"></span>

                    <article class="facebook_ads bg_white p-15 box_shadow mb-10 open_dydnamic2">
                        <p class="facebookAds_p"><b>Facebook Ads</b></p>
                        <p class="euro_sign_p"><span class="euro_sign"></span> <span class="cls_doller_257">&euro;257</span></p>

                        <div class="chart_facebook">
                            <div id="pyramid_chart2" style="overflow: auto !important;"></div>
                        </div>
                    </article>

                    <span class="clearfix"></span>

                    <div class="row">
                        <div class="col-md-12 p-0">
                            <article class="pyramid_block">
                                <div class="new_chart_pos1">
                                    <span class="visiter"><b>Visitor</b></span>
                                    <br> <span class="visiter_num">1256</span>
                                    <br>
                                    <br>
                                    <span class="this_week">This Week</span>
                                    <br> <span class="this_week_num">+36</span>
                                </div>

                                <div class="col-sm-12 p-0 new_chart_pos2">
                                    <div id="areaChart"></div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- .container-fluid -->

        <div class="pos_top2 fiexd_right">
            <div class="row">
                <div class="col-md-12 utenti_online">
                    <article class="limit_width">
                        <a href="#" class="btn create_funnel_btn create_funnel_bg"><img src="{{ url('images/plus-icon.svg')}}" alt="" width="20" /> <span class="create_funnel">Create Funnel</span></a>
                        <br>
                        <a href="javascript:void(0)" class="btn create_funnel_btn import_funnel"><img src="{{ url('images/navigation-icons/import-icon1.svg')}}" alt="" width="18" /> <span class="create_funnel">Import Funnel</span></a>

                        <span class="clearfix"></span>

                        <div class="select_box_1_parent">
                            <div class="select_box_1">
                                <span class="visualize_txt">Visualize</span>
                                <span class="plus_icon"><img src="{{ url('images/down_arr.png')}}" width="20" /></span>
                            </div>
                            <span class="arr_1" id="date_two_picker"><i class="fa fa-calendar"></i></span>
                        </div>

                        <ul class="multi_slect_inner">
                            <li>
                                <label class="chk1">All
                                    <input type="checkbox" value="All"> <span class="checkmark"></span></label>
                            </li>
                            <li>
                                <label class="chk1">New Leads
                                    <input type="checkbox" value="New Leads"> <span class="checkmark"></span></label>
                            </li>
                            <li>
                                <label class="chk1">New Sales of the week
                                    <input type="checkbox" value="New Sales of the week"> <span class="checkmark"></span></label>
                            </li>
                            <li>
                                <label class="chk1">Today's Sales
                                    <input type="checkbox" value="Today's Sales"> <span class="checkmark"></span></label>
                            </li>
                            <li>
                                <label class="chk1">Profit of the week
                                    <input type="checkbox" value="Profit of the week"> <span class="checkmark"></span></label>
                            </li>
                            <li class="percentage_span_list">
                                <label class="chk1"><span class="percentage_span">Percentage of growth compared to previous week </span>
                                    <input type="checkbox" value="Percentage of growth"> <span class="checkmark"></span></label>
                            </li>
                        </ul>

                    </article>
                </div>
            </div>

            <div class="width_percentage_100"></div>

            <div class="mydiv" id="divResize" style="display: none;">
                <div id="mydivheader" class="mydivheader">
                    <figure class="figure_icon">
                        <img src="{{ url('images/navigation-icons/robot.png')}}" alt="robot" width="70">
                    </figure>

                    <div class="showHide">
                        <div class="row">
                            <div class="col-sm-6 border_right2">
                                <p class="text_funnel1">Create Funnel</p>
                            </div>
                            <div class="col-sm-6">
                                <p class="text_funnel1">Guarda Statistiche</p>
                            </div>
                        </div>
                        <div class="row border_top2">
                            <div class="col-sm-6 border_right2">
                                <p class="text_funnel1">Import Funnel</p>
                            </div>
                            <div class="col-sm-6">
                                <p class="text_funnel1">Modification Pop-up</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <!-- .main_wrapper -->




<!-- css -->
    <link type="text/css" rel="stylesheet" href="{{asset('css/custom.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('css/animate.min.css')}}" />
    <link href="{{ asset('css/jquery-ui.css')}}" rel="stylesheet" type="text/css" defer/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/daterangepicker.css')}}" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />


<!-- js -->
    <script type="text/javascript" src="{{asset('js/jquery-ui.min.js')}}" defer></script>
    <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>

    <!-- :::::::::::::::::::::Chart2:::::::::::::::::::::::: -->
    <script src="{{asset('js/highcharts.js')}}"></script>
    <script src="{{asset('js/highcharts-more.js')}}"></script>
    <script src="{{asset('js/exporting.js')}}"></script>
    <script src="{{asset('js/export-data.js')}}"></script>

    <!-- :::::::::::::::::::::::DateRangepicker::::::::::::::: -->
    <script type="text/javascript" src="{{asset('js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/daterangepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/script.js')}}"></script>
    <script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script>
    
 
<style>
  .inside_icon {
    margin-right: 10px !important; 
}
  .inside_icon2 {
    margin-right: 30px !important; 
}
 
.add_dydnamic1 {
    width: 750px !important;
    height: 420px !important;
    z-index: 999;
    display: block !important;
    -webkit-transition: 1s;
    transition: 1s;
}

.chart_facebook{
    transform: none !important;
}

.myChart1_overlay{
    padding: 15px;
}


@media (min-width: 1920px){
    
#areaChart .highcharts-container svg { 
    left: 0!important;    
}

.widget .add_dydnamic1 {
    width: 750px !important;
}
.add_dydnamic1{
   width:  750px !important;  
}

}


 

</style>


<script>
    // Start pyramid_chart3................
        Highcharts.chart('pyramid_chart3', {
            chart: {
                type: 'columnpyramid'
            },
            title: {
                text: ''
            },
            colors: ['#080', '#777', '#080', '#777', '#080', '#777'],
            xAxis: {
                crosshair: true,
                labels: {
                    style: {
                        fontSize: '14px'
                    }
                },
                type: 'category'
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: false,
            series: [{
                name: 'Day',
                colorByPoint: true,
                data: [
                    ['Mon', 1],
                    ['Tue', 2],
                    ['Wed', 3],
                    ['Thu', 2],
                    ['Fri', 1],
                    ['Sat', 3],
                    ['Sun', 4]
                ],
                showInLegend: false             
            }]
        });
        // End pyramid_chart2................

$(function(){

 

$("#column_chart1 .highcharts-root").attr({
'width': '480',
'height': '400',
'viewBox': '30 200 480 370'
}); 


$("#pyramid_chart2 .highcharts-root").attr({
'width': '900',
'height': '400',
'viewBox': '200 0 600 470'
});

 
// $("#pyramid_chart3 .highcharts-root").attr('viewBox','0 0 700 500');
$("#pyramid_chart3 .highcharts-root").attr({
'width': '600',
'height': '477',
'viewBox': '50 -25 600 550'
});




 
// areaChart
$("#areaChart .highcharts-root").attr("viewBox", "-170 50 850 480");
// $("#areaChart .highcharts-root").css({
// 'width': 640,
// 'height': 350
// });

// if ($(window).width(1920)) {
// $('#areaChart .highcharts-root').attr('viewBox', '-100 20 1500 450');
// }

$(".facebook_ads").click(function() {
    $(".open_dydnamic2").toggleClass('add_dydnamic2_add');
    // $(".pyramid_block").toggle(300);
    // $('.chart_facebook').slideToggle();
}); 




if($(window).innerWidth() <= 1280){
            // $("#areaChart svg").attr('viewBox','0 -250 300 800');

        }
        else if($(window).innerWidth() <= 1366){ 
            // $("#areaChart svg").attr('viewBox','0 20 650 450');

        }
        else if($(window).innerWidth() >= 1681 || $(window).outerWidth() <= 1919){ 
             $("#areaChart svg").attr('viewBox','100 50 820 520');
//             alert( 1366 +' > '+  1919); 

        }
else if($(window).innerWidth() >= 1920){ 
             $("#areaChart svg").attr('viewBox','70 120 900 350');
//             alert( 1366 +' > '+  1919); 
$("#column_chart1 svg").attr('viewBox','50 200 480 370');
        }






})

</script>
    <script>
        function myFunction(x) {
            x.classList.toggle("change");
            $(".dashboard_inside_icon").toggleClass('dashboard_ml');
            $(".mydiv").toggleClass('circle_pos');
            $(".myChart1_overlay [data-highcharts-chart='0']").toggleClass('toggle_cls1grph');
            $(".add_dydnamic1").toggleClass('add_dydnamic1_add');

        }

 
        
    
</script>


</body>
</html>