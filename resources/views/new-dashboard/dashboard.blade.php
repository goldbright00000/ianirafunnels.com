<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dashboard </title>
    <link rel="icon" href="images/brand/risorsa-lg.png" type="image/png" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="css/style.css" />
    <link type="text/css" rel="stylesheet" href="css/animate.min.css" />
 
</head> 

<body>

    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" id="toggleNaviagtion">
            <img src="images/brand/risorsa-lg.png" alt="brand" class="brand_lg" width="40" />  
        </a>

        <a href="#" class="effect_sidebar mt_list1"><span class="inside_icon"><img src="images/navigation-icons/white_icons/list_menu1.png" width="30"></span> Dashboard</a>
        <a href="#" class="effect_sidebar"><span class="inside_icon"><img src="images/navigation-icons/white_icons/001-filter.png" width="35"></span> Funnel</a>
        <a href="#" class="effect_sidebar"><span class="inside_icon"><img src="images/navigation-icons/white_icons/003-seo.png" width="35"></span> Campaigns</a>
        <a href="#" class="effect_sidebar"><span class="inside_icon"><img src="images/navigation-icons/white_icons/team-leader.png" width="35"></span> Backpack</a>


 <a href="#" class="effect_sidebar"><span class="inside_icon"><img src="images/navigation-icons/white_icons/gift.png" width="35"></span> Gift</a>

  <a href="#" class="effect_sidebar"><span class="inside_icon"><img src="images/navigation-icons/white_icons/online-class.png" width="35"></span> Online Class</a>

        <a href="#" class="effect_sidebar"><span class="inside_icon"><img src="images/navigation-icons/white_icons/006-progress-report.png" width="35"></span> Stats</a>
        <a href="#" class="effect_sidebar"><span class="inside_icon"><img src="images/navigation-icons/white_icons/012-domain-registration.png" width="35"></span> Domain</a>
        <a href="#" class="effect_sidebar"><span class="inside_icon"><img src="images/navigation-icons/white_icons/007-percentage.png" width="35"></span> Sales</a>

        <div class="nav_footer">
            <a href="#"><span class="inside_icon"><img src="images/navigation-icons/white_icons/009-logout-sign.png" width="35"></span> Logout</a>
        </div>
    </div>
<!-- #mySidenav -->




    <div id="main" class="main_wrapper">

        <div class="col-md-12">
            <div class="col-md-3 p-0">
                <figure class="logo">
                    <img src="images/ifunnel_logo_2.svg" alt="logo" width="250" />
                </figure>
            </div>
            <div class="col-md-6"></div>
            <div class="col-md-3 p-0">
                <ul class="list_top_icon list-inline">
                    <li class="loupe"><img src="images/navigation-icons/icons/loupe.png" class="search_icon_top" width="20" />

                        <div class="search_box animated fadeUp">
                            <input type="text" class="search_box_txt" placeholder="Search">
                        </div>

                    </li>
                    <li class="notifications"><img src="images/navigation-icons/icons/notifications-button.png" class="notifications_well" width="20" />
                        <div class="notification_inn animated flipInY">
                            <article>
                                <ul class="notification_list list-group">
                                    <li class="list-group-item">Notifications 1</li>
                                    <li class="list-group-item">Notifications 2</li>
                                    <li class="list-group-item">Notifications 3</li>
                                    <li class="list-group-item">Notifications 4</li>
                                </ul>
                            </article>
                        </div>
                    </li>
                    <li class="settingsTool notifications"><img src="images/navigation-icons/icons/settings-work-tool.png" class="notifications_setting2" width="20" />

                        <div class="notification_inn2 animated flipInY">
                            <article>
                                <ul class="notification_list list-group">
                                    <li class="list-group-item">Settings 1</li>
                                    <li class="list-group-item">Settings 2</li>
                                    <li class="list-group-item">Settings 3</li>
                                    <li class="list-group-item">Settings 4</li>
                                </ul>
                            </article>
                        </div>
                    </li>
                    <li class="userImg same_userImg notifications"><img src="https://wowsciencecamp.org/wp-content/uploads/2018/07/dummy-user-img-1.png" class="notifications_setting3" width="30" style="border-radius: 50%" /> <span class="name_mario">Mario Rossi</span>

                        <div class="notification_inn3 animated flipInY">
                            <article>
                                <ul class="notification_list list-group">
                                    <li class="list-group-item">user 1</li>
                                    <li class="list-group-item">user 2</li>
                                    <li class="list-group-item">user 3</li>
                                    <li class="list-group-item">user 4</li>
                                </ul>
                            </article>
                        </div>

                    </li>
                </ul>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 pos_top1">

                    <div class="col widget">
                        <article class="box_shadow bg_white p-15">
                            <p><img src="images/navigation-icons/plus-icon.svg" alt="" width="20" style="margin-top: -5px;" /> <span class="widget_txt">Widget</span></p>
                        </article>

                        <span class="clearfix"></span>

                        <article class="box_shadow bg_white p-15 mt-15 mb-15 inweek_pos">
                            <div class="profit_txt">
                                <span class="widget_txt">Profit</span>
                                <br>
                                <span class="profit_txt_percentage">14%</span>

                                <div class="inthisweek">
                                <span class="plus_5_percent">In this Week <span>+5%</span></span>
                            </div>

                            </div>

                            
                        </article>
                    </div>

                    <!--  <img src="images/shot_1.jpg" alt="" style="width: 100%" /> -->
                    <article class="chartCss1">
                        <div class="new_leads_pos">
                            <span class="new_leads">New Leads</span>
                            <br>
                            <span class="new_leads_txt">55</span>
                        </div>
                        <div id="chart1"></div>
                    </article>
                </div>

                <div class="col-md-6 pl-0 pr-0">

                    <div class="col mb-15">
                        <div class="col-md-6 acquisti_effettuati pl-0 pr-5">
                            <article class="box_shadow bg_white p-15 mb-15">
                                <p>Acquisti effettuati</p>
                                <p class="num_350">351
                                    <br> <span class="media_acquisti">Media acquisti giornaliera: 3,95</span></p>
                            </article>
                        </div>

                        <div class="col-md-6 acquisti_effettuati pr-0 pl-5">
                            <article class="box_shadow bg_white p-15 mb-15">
                                <p>Utenti online</p>
                                <p class="num_350">35
                                    <br> <span class="media_acquisti">Realtime 02:35:15</span></p>
                            </article>
                        </div>
                    </div>

<span class="clearfix"></span>


                    <article class="facebook_ads bg_white p-15 box_shadow mb-15">
                        <p>Facebook Ads</p>
                        <p class="euro_sign_p"><span class="euro_sign">&euro;</span> 257</p>
                    </article>
                    <!-- <img src="images/shot_2.jpg" alt="" style="width: 100%" /> -->

  <span class="clearfix"></span>


                    <div class="row">
                        <div class="col-md-12">
                            <article class="chartCss2">
                                <div class="pos_cahrt">
                                    <span class="visiter">Visiter</span>
                                    <br> <span class="visiter_num">1256</span>
                                    <br>
                                    <br>
                                    <span class="this_week">This Week</span>
                                    <br> <span class="this_week_num">+36</span>
                                </div>
                                <div id="chart2"></div>
                            </article>
                        </div>
                    </div>

                </div>

                <div class="col-md-3 pos_top2">

                    <div class="row">
                        <div class="col-md-12 utenti_online">
                            <article>
                                <a href="#" class="btn create_funnel_btn"><img src="images/navigation-icons/plus-icon.svg" alt="" width="20" /> <span class="create_funnel">Create Funnel</span></a>
                                <br>
                                <a href="#" class="btn create_funnel_btn"><img src="images/navigation-icons/plus-icon.svg" alt="" width="20" /> <span class="create_funnel">Import Funnel</span></a>
                            </article>
                        </div>
                    </div>

                    <article class="bg-black p-15 progress_bar_section">

                        <div class="plan">
                            <p class="plan_txt">Plan</p>
                        </div>

                        <span class="clearfix"></span>

                        <div class="trial">
                            <p class="trial_txt">Full suite (&euro;259/mo after 14-day Trial)</p>
                        </div>

                        <span class="clearfix"></span>

                        <div class="progress_wrap">
                            <div class="progress progress_bar">
                                <div class="progress-bar animated fadeInLeft" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width:30%">
                                    <span class="sr-only">30% Complete</span>
                                </div>
                            </div>
                            <p>
                                <span class="progress_txt1 progress_txt_similar">VISITS </span>
                                <span class="progress_txt2 progress_txt_similar">0 of 999.99999.999</span>
                            </p>
                        </div>

                        <span class="clearfix"></span>

                        <div class="progress_wrap">
                            <div class="progress progress_bar">
                                <div class="progress-bar animated fadeInLeft" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width:60%">
                                    <span class="sr-only">60% Complete</span>
                                </div>
                            </div>
                            <p>
                                <span class="progress_txt1 progress_txt_similar">PAGE </span>
                                <span class="progress_txt2 progress_txt_similar">0 of 999.99999.999</span>
                            </p>
                        </div>

                        <span class="clearfix"></span>

                        <div class="progress_wrap">
                            <div class="progress progress_bar">
                                <div class="progress-bar animated fadeInLeft" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width:90%">
                                    <span class="sr-only">90% Complete</span>
                                </div>
                            </div>
                            <p>
                                <span class="progress_txt1 progress_txt_similar">FUNNELS </span>
                                <span class="progress_txt2 progress_txt_similar">0 of 999.99999.999</span>
                            </p>
                        </div>

                        <span class="clearfix"></span>

                    </article>
                </div>
            </div>

            <!-- <br><br>

<a href="https://ianirafunnels.com/clear-cache" target="_blank">Remove Cache</a>

<br><br> -->
        </div>
        <!-- .container-fluid -->

    </div>
    <!-- .main_wrapper -->



















    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/customizer.js"></script>

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script>
        // cahrt1
        Highcharts.chart('chart1', {
            chart: {
                type: 'line'
            },
            title: {
                text: ''
            },
            // xAxis: {
            //     categories: [
            //         'Mon',
            //         'Tue',
            //         'Wed',
            //         'Thu',
            //         'Fri',
            //         'Sat',
            //         'Sun'
            //     ]
            // },
            yAxis: {
                title: {
                    text: ''
                }
            },
            tooltip: {
                shared: false,
                valueSuffix: 'day'
            },
            credits: {
                enabled: false
            },
            series: [{
                    name: '',
                    data: [0, 55, 15, 77, 30, 90, 5]
                }
                // {
                //     name: '',
                //     data: [2, 4, 6, 8, 10, 12, 14]
                // }
            ]
        });

        // chart2
        Highcharts.chart('chart2', {
            chart: {
                type: 'line'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: [
                    'Mon',
                    'Tue',
                    'Wed',
                    'Thu',
                    'Fri',
                    'Sat',
                    'Sun'
                ]
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            tooltip: {
                shared: false,
                valueSuffix: 'day'
            },
            credits: {
                enabled: false
            },
            series: [{
                name: '',
                data: [0, 55, 15, 77, 30, 90, 5]
            }]
        });
    </script>

    <style>

        body{
        -webkit-animation: fadeIn ease 2s;
        -moz-animation: fadeIn ease 2s;
        animation: fadeIn ease 2s;
        }

        @-webkit-keyframes fadeIn{
        from{opacity: 0;visibility: hidden;}
        to{opacity: 1;visibility: show;}
        }
        @-moz-keyframes fadeIn{
        from{opacity: 0;visibility: hidden;}
        to{opacity: 1;visibility: show;}
        }
        @keyframes fadeIn{
        from{opacity: 0;visibility: hidden;}
        to{opacity: 1;visibility: show;}
        }


        .loupe {
            position: relative;
        }
        
        .search_box {
            position: absolute;
            right: 50px;
            top: -8px;
            display: none;
        }
        
        .search_box .search_box_txt {
            border: 1px solid #c7c7c7;
            outline: 0;
            height: 40px;
            width: 250px;
            padding: 15px;
            font-size: 15px;
            transition: 0.5s;
            border-radius: 5px;
        }
        
        .search_box .search_box_txt:focus {
            border: 1px solid #a7a7a7;
        }
        
        .pos_cahrt {
            position: absolute;
            top: 15px;
            left: 15px;
        }
        
        .visiter {
            font-size: 24px;
            color: #333;
        }
        
        .visiter_num {
            font-size: 24px;
            color: #fbbc03;
        }
        
        .this_week {
            color: #333
        }
        
        .this_week_num {
            color: #fbbc03;
            font-size: 18px;
        }
        
        .chartCss1,
        .chartCss2 {
            background: #fff;
            padding-top: 30px;
            position: relative;
            font-family: futuraBook;
            -webkit-box-shadow: 0 0 5px 0 #ddd;
            box-shadow: 0 0 5px 0 #ddd;
        }
        
        .new_leads_pos {
            position: absolute;
            top: 15px;
            left: 15px;
        }
        
        .new_leads {
            font-size: 24px;
            color: #333;
        }
        
        .new_leads_txt {
            font-size: 36px;
            color: #fbbc03;
            margin-top: -15px;
        }
        
        .euro_sign,
        .num_350 {
            font-family: futuraBook;
        }
        
        .media_acquisti {
            font-family: sans-serif;
        }
        
        #chart1 {
            margin-top: 100px;
        }
        
        #chart1 svg {
            width: 90% !important;
            height: auto !important;
        }
        
        #chart1 rect.highcharts-button-box,
        #chart1 .highcharts-button,
        #chart1 .highcharts-legend-item {
            display: none !important;
            opacity: 0 !important;
            visibility: hidden !important;
        }
        
        #chart1 .highcharts-container {
            height: 320px !important;
            width: 100% !important;
        }
        
        #chart2 .highcharts-container {
            height: 330px !important;  
            width: 100% !important;
        }
        
        #chart2 svg {
    width: 80% !important;
    height: auto !important;
    position: absolute;
    right: 0!important;
    top: 0px!important;
}
        
        #chart2 rect.highcharts-button-box,
        #chart2 .highcharts-button,
        #chart2 .highcharts-legend-item {
            display: none !important;
            opacity: 0 !important;
            visibility: hidden !important;
        }
        
        .profit_txt_percentage {
            font-size: 36px;
            color: #fbbc03;
        }
        
        .main_wrapper {
            -webkit-transition: 0.5s;
            transition: 0.5s;
            background: #f6f6f6;
            margin-left: 85px;
            padding-bottom: 150px;
        }
        
        .first_effect_sidebar {
            position: absolute;
            top: 0;
        }
        
        .inside_icon {
            margin-right: 5px;
        }
        
        .inside_icon2 {
            margin-right: 30px;
        }
        
        .sidenav {
            height: 100%;
            width: 85px;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background-color: #111;
            overflow-x: hidden;
            -webkit-transition: 0.5s;
            transition: 0.5s;
            padding-top: 60px;
            white-space: nowrap;
        }
        
        .sidenav a {
           padding: 12px 15px 10px 20px;
            width: 100%;
            height: 60px;
            text-decoration: none;
            font-size: 16px;
            color: #a5a5a5;
            display: block;
            transition: 0.3s;
            background: rgb(17, 17, 17);
            margin-bottom: 5px;
        }
        
        .sidenav a:hover {
            color: #f1f1f1;
        }
        
        #toggleNaviagtion {
            position: absolute;
            top: 0;
            background: #000000;
            left: 0;
            right: 0;
        }
    .mt_list1{
    margin-top: 0;
}
        
        .effect_sidebar:hover {
            background: #2b2b2b;
            overflow: hidden;
        }
        
        .sidenav .closebtn {
            position: absolute;
            top: 0;
            right: 25px;
            font-size: 36px;
            margin-left: 50px;
        }
        
        .inside_icon::before {
            display: none;
        }
        
        @media screen and (max-height: 450px) {
            .sidenav {
                padding-top: 15px;
            }
            .sidenav a {
                font-size: 18px;
            }
        }
        
        .widthToggle {
            width: 250px;
            transition: 0.5s;
        }
        
        .marginLeft {
            margin-left: 250px;
            transition: 0.5s;
        }
        
        .fadeBackground {
            background: rgba(0, 0, 0, 0.4);
        }
        /*:::::::::::::::::::::::::::::Start Dashboard:::::::::::::::::::::::::::*/
        
        .brand_lg {
            width: 50px;
        }
        
        .nav_footer {
            position: relative;
            top: 70px;
            right: 0;
            width: 100%;
        }
        
        .widget_txt {
            font-size: 24px;
            font-family: futuraBook;
            color: #333;
        }
        
        .box_shadow {
            -webkit-box-shadow: 0 0 5px 0 #ddd;
            box-shadow: 0 0 5px 0 #ddd;
        }
        
        .bg_white {
            background: #fff
        }
        
        .widget article {
            background: #fff;
            padding: 15px;
            padding-bottom: 5px;
        }
        
        .userImgText {
            padding-left: 0 !important;
        }
        
        .userImg {
            padding-right: 5px;
        }
        
        .pos_top1 {
            margin-top: 0px !important;
        }
        

        /*------------------ Top Notification ---------------*/
        .notifications .notification_inn,
        .notifications .notification_inn2,
        .notifications .notification_inn3  {
            position: absolute;
            z-index: 1;
            width: 264px;
            top: 70px;
            left: 4px;
            display: none;
        }
        
        .notifications .notification_inn article,
        .notifications .notification_inn2 article,
        .notifications .notification_inn3 article {
            position: relative;
        }
        
        .notification_inn .notification_list,
        .notification_inn2 .notification_list,
        .notification_inn3 .notification_list {
            box-shadow: 0 0 5px 0 #ddd;
            border: 0;
            border-radius: 0 0 !important;
        }
        
        .notification_inn .notification_list li,
        .notification_inn2 .notification_list li,
        .notification_inn3 .notification_list li {
            border: 0;
            transition: 0.5s;
        }
        
        .notification_inn .notification_list li:hover,
        .notification_inn2 .notification_list li:hover,
        .notification_inn3 .notification_list li:hover {
            background: #eee;
        }
        
        .notifications .notification_inn article::before {
            content: "";
            position: absolute;
            left: 40px;
            top: -8px;
            border: 1px solid #ddd;
            width: 30px;
            height: 30px;
            background: #fff;
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg);
        }
        
        .notifications .notification_inn2 article::before {
            content: "";
            position: absolute;
            left: 87px;
            top: -8px;
            border: 1px solid #ddd;
            width: 30px;
            height: 30px;
            background: #fff;
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg);
        }
        .notifications .notification_inn3 article::before {
            content: "";
            position: absolute;
            left: 150px;
            top: -8px;
            border: 1px solid #ddd;
            width: 30px;
            height: 30px;
            background: #fff;
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg);
        }

.inweek_pos{position:relative}
.inthisweek {
    position: absolute;
    top: 15px;
    right: 15px;
}
.plus_5_percent span {
    font-size: 18px;
}
 

/*------------------ Top Notification ---------------*/

/*--------------------------------------Start scrollbar---------------------------------------*/
 
 
::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    border-radius: 5px;
    background-color: #F5F5F5;
}

::-webkit-scrollbar
{
    width: 7px;
    background-color: #F5F5F5;
}

::-webkit-scrollbar-thumb
{
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
    background-color: #555;
}
 


/*--------------------------------------End scrollbar---------------------------------------*/


</style>





    <script>
        $(function() {

            $(".inside_icon").addClass('inside_icon2');
            $("#toggleNaviagtion").click(function() {
                $(".inside_icon").toggleClass('inside_icon2');
                $("#mySidenav").toggleClass('widthToggle');
                $(".main_wrapper").toggleClass('marginLeft');
                $("body").toggleClass('fadeBackground');
            })

            // notification icon
            $(".search_icon_top").click(function() {
                $(".search_box").toggle();
                $(".notification_inn,.notification_inn2,.notification_inn3").hide();
            });

            $("*").on("click", ".notifications_well", function() {
                $(".notification_inn").toggle();
                $(".notification_inn2,.notification_inn3,.search_box").hide();
            });

            $("*").on("click", ".notifications_setting2", function() {
                $(".notification_inn2").toggle();
                $(".notification_inn,.notification_inn3,.search_box").hide();
            });

            $("*").on("click", ".notifications_setting3, .name_mario", function() {
                $(".notification_inn3").toggle();
                $(".notification_inn,.notification_inn2,.search_box").hide();
            });


        })
    </script>









</body>

</html>