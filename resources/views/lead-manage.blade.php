@extends('user-dashboard.user-dashboard-default')
@section('title','Select Funnel')
@section('content')
<div class="container-fluid" style="margin-top: 100px;">
    
    
    <div class="col-sm-12">
    <div class="rer"> {{$funneldata['funnel_name']}} </div>  
   
    </div>
    
    <div class="rr_bottom"> &nbsp;</div>
    

<div class="row">

    <div class="col-sm-12">
        
<div class="jumbotron">
<div class="row w-100">
    
      <div class="col-md-4 ">
          
          
          
            <div class="card border-warning mx-sm-1 p-3 newdiv">

                <div class="text-warning text-center mt-3"><h4> <img src="https://ianirafunnels.com/public/img/funnel.svg" class="svg_icon">  Type Lead</h4></div>
                <div class="text-warning text-center mt-2"><h1>{{$funneldata['type']}}</h1></div>
            </div>
          
          
          
        </div>
    
    
        <div class="col-md-4  ">
            
            <div class="card border-info mx-sm-1 p-3 ffi">
                
                <div class="text-info text-center mt-3"><h4><img src="https://ianirafunnels.com/public/img/cument.svg" class="svg_icon">Pages</h4>  </div>
                <div class="text-info text-center mt-2"><h1>{{$funneldata['page']}}</h1></div>
            </div>
        </div>
        <div class="col-md-4 ">
            <div class="card border-success mx-sm-1 p-3 fuuf">
           
                <div class="text-success text-center mt-3"><h4><img src="https://ianirafunnels.com/public/img/imer.svg" class="svg_icon">Time</h4></div>
                <div class="text-success text-center mt-2"><h1>{{$funneldata['time']}}</h1></div>
            </div>
        </div>
     
      
     </div>
</div>
        
        
        
    </div>
    
    
    
    
    
    
    
    
    
    
    
    




</div>
    
    <div class="how-section1">
    
    <!---- <div class="row">
         
         
                        <div class="col-sm-6 ">
                            <div class="how-img">
                            
                            <img src="https://image.ibb.co/dDW27U/Work_Section2_freelance_img1.png" class="rounded-circle img-fluid" alt=""/>
                      </div>
                        
                        </div>
         
         
                        <div class="col-sm-6">
                            <h4>
Where does it come from?</h4>
                                        <h4 class="subheading">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
                        <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                    </div>---->
        
        
             <div class="row">
                 
                  <div class="col-sm-6 ">
                      
                      <div class="how-img">
                            <img src="https://image.ibb.co/dDW27U/Work_Section2_freelance_img1.png" class="rounded-circle img-fluid" alt=""/>
                           </div> 
                            
                        </div>
                       
                        <div class="col-sm-6">
                           <!--- <h4>
Where does it come from?</h4>--->
                                        <h4 class="subheading">Directions</h4>
                        <p class="text-muted"> {{$funneldata['message']}}   </p>
                        </div>
                 
                 
                 
                    </div>
                
    
    
    </div>
    
     <div class="container-fluid">
              <div class="row">
                <div class="col-xs-12 ">
                  <nav>
                    <div class="nav nav-tabs nav-fill defotab" id="nav-tab" role="tablist" style=""  >
                      <a class="nav-item nav-link active" id="nav-home-tab desc" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">All</a>
                      <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Paid</a>
                      <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Free</a>
                      </div>
                   
                  </nav>

                   
                      
                      <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                    <div class="tab-pane fade  active in " id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    
                          <div class="over_get">
                        
                        
                        @foreach($template as $template_record)
                        
                      
                        <div class="col-sm-3"  >
                        <div class="border_mana">
                            
                            <div class="">
                                
                                <div class="">
                                <img src="{{asset('public/uploads/funnel/'.$template_record->image)}}" class="img-responsive">
                                </div>
                                <div class=""> 
                                
                                    @if($template_record->template_price_type=="freetemplate") 	
                                    <div class="ribbon sale">
                                        <div class="theribbon"> 
                                        Free
                                        
                                        </div>
                                     </div>
                                    @endif
                                    
                                    
                                    
                                    @if($template_record->template_price_type=="paidtemplate")
                                    
                                    
                                   
                                    <div class="ribbon sale">
                                        <div class="theribbon"> Premium
                                      
                                        </div>
                                     </div>
                                 
                                    
                                    
                                    
                                    <div class="ribbon new">
                                        <div class="theribbon">
                                       {{$template_record->template_price}}
                                        </div>
                                      <div class="ribbon-background">
                                            
                                          
                                            
                                        </div>
                                    </div>
                                    
                                    
                                       @endif
                                    
                                    
                                
                                </div>
                                
                                <div class="ogo_pop">
                                   {{$template_record->template_name}}
                                </div>
                                
                                
                                
                                
                            </div>
                            </div>
                        </div>
                      
                        
                        
                        @endforeach
                        
                        
                          </div>
                        
                 
                        
                       
                        
                        
                        
                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                      Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.
                    </div>
                    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                      Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.
                    </div>
                    <div class="tab-pane fade" id="nav-about" role="tabpanel" aria-labelledby="nav-about-tab">
                      Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.
                    </div>
                  </div>
                
                </div>
              </div>
        </div>
      </div>
</div>
    
    
    
    
    
    
</div>
</br>
</br>


<style>
    .border_mana
    {
       border: solid 1px #bcbcbc;
height: 247px;
padding-bottom: 27px;

    }
    .border_mana img
    {
         width: 100%;
   
    }
    .ogo_pop
  {
  
    font-family: "proxima-nova", sans-serif !important;
    font-size: 24px;
    font-weight: 600;
    padding-left: 12px;
    padding-top: 11px;
    padding-right: 12px;
  }
  
    
    .over_get
    {
     overflow: hidden;   
    }
    .ribbon .theribbon::after {
    left: 0px;
    top: 100%;
    border-width: 5px 10px;
    border-style: solid;
    border-color: #000000 #000000 transparent transparent;
}

    
    
    .ribbon .theribbon {
    color: #fff;
    font-family: "Roboto", Helvetica, Arial, sans-serif;
}
.ribbon .theribbon::before, .ribbon .theribbon::after {
    content: ' ';
    position: absolute;
    width: 0;
    height: 0;
}

    
    .ribbon {
    font-weight: 700;
    letter-spacing: 0.08em;
}

    .ribbon {
    position: absolute;
    top: 0px;
    padding-left: 51px;
    font-weight: 700;
    letter-spacing: 0.08em;
}
.ribbon.sale {
    top: 0;
}
.ribbon .theribbon {
    position: relative;
    width: 130px;
    padding: 6px 20px 6px 20px;
    margin: 30px 10px 10px -71px;
    color: #fff;
    background-color: red;
    font-family: "Roboto", Helvetica, Arial, sans-serif;
}

    .ribbon .ribbon-background {
    position: absolute;
    top: 0;
    right: 0;
}

    
    .hidden
    {
        overflow: hidden;
    }
  .ribbon .theribbon::before, .ribbon .theribbon::after {
    content: ' ';
    position: absolute;
    width: 0;
    height: 0;
}
.ribbon.new .theribbon::after {
    border-color: #2390b0 #2390b0 transparent transparent;
}
.ribbon .theribbon::after {
    left: 0px;
    top: 100%;
    border-width: 5px 10px;
    border-style: solid;
    border-color: #000000 #000000 transparent transparent;
}
.ribbon .theribbon::before, .ribbon .theribbon::after {
    content: ' ';
    position: absolute;
    width: 0;
    height: 0;
}





.ribbon.new .theribbon {
    background-color: #5bc0de;
    text-shadow: 0px 1px 2px #bbb;
}
.ribbon .theribbon {
    position: relative;
    width: 130px;
    padding: 6px 20px 6px 20px;
    margin: 30px 10px 10px -71px;
    color: #fff;
    background-color: #4fbfa8;
    font-family: "Roboto", Helvetica, Arial, sans-serif;
}
.ribbon .ribbon-background {
    position: absolute;
    top: 0;
    right: 0;
}


    
    .ribbon {
    position: absolute;
    top: 50px;
    padding-left: 51px;
    font-weight: 700;
    letter-spacing: 0.08em;
}

    
    nav > div a.nav-item.nav-link:hover, nav > div a.nav-item.nav-link:active {
    border: none;
    background: #f58b3c;
    color: #fff;
    border-radius: 0;
    transition: background 0.20s linear;
}

    .h_tav
    {
        overflow: hidden;
    }
    .defotab
    {
   border: solid 1px red;
padding-top: 19px;
padding-bottom: 16px;
overflow: hidden;
padding-left: 2px;
    }
   .fuuf
   {
     border: solid 1px #ffa500;
background: white;
padding-top: 36px;
padding-bottom: 31px;
border-radius: 7px;
box-shadow: 0 0 15px rgba(12, 12, 12, 0.87);  
   }
    .ffi
    {
     
    border: solid 1px #ffa500;
background: white;
padding-top: 36px;
padding-bottom: 31px;
border-radius: 7px;
box-shadow: 0 0 15px rgba(12, 12, 12, 0.87);
    }
    
    
    .mt-3 h4{
        font-weight:600
    }
   .newdiv
   {
       border: solid 1px #ffa500;
background: white;
padding-top: 36px;
padding-bottom: 31px;
border-radius: 7px;
box-shadow: 0 0 15px rgba(12, 12, 12, 0.87);


   }
    
    
    
    nav > .nav.nav-tabs{

  border: none;
    color:#fff;
    background:#272e38;
    border-radius:0;

}
nav > div a.nav-item.nav-link,
nav > div a.nav-item.nav-link.active
{
border: none;
    padding: 18px 25px;
    color: #302c2c;
    background: #fff;
    border-radius: 0;

}

nav > div a.nav-item.nav-link.active:after
 {
  content: "";
  position: relative;
  bottom: -60px;
  left: -0%;
  border: 15px solid transparent;
  border-top-color:  #f58b3c ;
}
.tab-content{
  background: #fdfdfd;
    line-height: 25px;
    border: 1px solid #ddd;
    border-top:5px solid  #f58b3c;
    border-bottom:5px solid  #f58b3c;
    padding:30px 25px;
}

nav > div a.nav-item.nav-link:hover,
nav > div a.nav-item.nav-link:focus
{
  border: none;
    background:  #f58b3c;
    color:#fff;
    border-radius:0;
    transition:background 0.20s linear;
}
 
    
    .how-section1{
   margin-bottom: 23px;
}
.how-section1 h4{
    color: #ffa500;
    font-weight: bold;
    font-size: 30px;
}
.how-section1 .subheading{
    color: #3931af;
    font-size: 20px;
}
.how-section1 .row
{
    margin-top: 1%;
}
.how-img 
{
text-align: center;
border: solid 1px #d2d2d2;

}
.how-img img{
    width: 40%;
margin: 0 auto;
    margin-top: 0px;
display: block;
margin-top: 46px;

}
    .svg_icon
    {
    height: 21px;
width: 21px;
    }

    .my-card
{
    position:absolute;
    left:40%;
    top:-20px;
    border-radius:50%;
}
    
    .gugpo
    {
        margin-top:10px;
    }
    .divses2 {
   float: right;
font-weight: 600;
padding: 8px;
border: solid 1px #f58b3c;
background: #fbfbfb;
margin-bottom: 10px;
}
.icon_with {
    width: 28px;
}

    .ff_top
    {
        margin-top:10px;
    }
    .rr_bottom
{
border-bottom: solid 1px #f58b3c;

}
    .rer
    {
text-align: center;
font-weight: 600;
font-size: 25px;
text-transform: uppercase;
border-bottom: #120f0f 2px solid;
width: 392px;
margin: 0 auto;
padding-bottom: 10px;
margin-bottom: 35px;


    }
.rru
{
    text-align: center;
}
    
    .heading_other
    {
       border: solid 1px #f58b3c;
background: #f58b3c;
color: white;
text-indent: 12px;
line-height: 38px;
font-weight: 600;

    }
   


</style>


@endsection








