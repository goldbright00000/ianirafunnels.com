@extends('layouts.default')
@section('title','Location')
@section('content')

<div class="headeryy">
<div class="container">
<div class="through_f"><h3 class="mb-0">Do you want to build a tunnel in 5 minutes? Try NOW IaniraFunnels!</h3></div>
    </div>
    </div>
<section class="top_banner py-5">
  <div class="sitecontainer">
    <div class="row align-items-center">
      <div class="col-sm-6 col-xl-5">
          <div class="baner_pera_wrp">
            <h2 class="text-white mb-md-5 mb-sm-2">
            Location <b>IaniraFunnels</b>
            </h2>
            <p class="gray_text">
          “Lorem ipsum dolor sit amet, consectetur adipiscing elit, <b>sed do eiusmod </b> tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
            </p>
          </div>
      </div>
      <div class="col-sm-6 col-xl-7">
        <div class="doubleCircle_wrp right_top">
            <div class="doubleCircle "></div>
            <figure class="mb-0">
            <img src="{{ asset('public/img/Group-7@2x-right.png')}}" alt="" class="img-fluid" >
            </figure>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="location_main py-5">
  <div class="sitecontainer">
    <div class="row">
      <div class="col-sm-6 col-xl-7">
        <div class="location_pera h-100 d-flex align-items-center">
          <p class="gray_text font-weight-light">
          “Lorem ipsum dolor sit amet, consectetur adipiscing elit, <b>sed do eiusmod </b> tempor incididunt ut labore et dolore magna aliqua.
          Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
          Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu <b>fugiat</b> nulla pariatur.
           Excepteur sint occaecat cupidatat non proident, <b>sunt</b> in culpa qui officia deserunt mollit anim id est laborum
          </p>
        </div>
      </div>
      <div class="col-sm-6 col-xl-5 map_wraper">
      <div class="doubleCircle_wrp right_top left_bottom ">
            <div class="doubleCircle doubleCircle_left"></div>
            <div class="doubleCircle "></div>
              <iframe style="width:100%;height:100%;"  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d47511.64848462647!2d12.444953818509685!3d41.904081411907256!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132f61b0ff64ce01%3A0x3a517e0bf91181e8!2sMunicipio%20I%2C%20Rome%2C%20Metropolitan%20City%20of%20Rome%2C%20Italy!5e0!3m2!1sen!2sin!4v1570619648524!5m2!1sen!2sin" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
      </div>
    </div>
  </div>
</section>







@endsection