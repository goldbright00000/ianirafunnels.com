@extends('layouts.default')
@section('title','Jason Funnel')
@section('content')
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<div class="container">
	<h2 class="text-left"> Funnel Status
</h2>
	
   	<div class="row">
      <div class="col-lg-3">
        <div class="panel panel-info">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-6">
                <i class="fa fa-users fa-5x"></i>
              </div>
              <div class="col-xs-6 text-right">
                <p class="announcement-heading">1</p>
                <p class="announcement-text">Funnel</p>
              </div>
            </div>
          </div>
          <a href="#">
            <div class="panel-footer announcement-bottom">
              <div class="row">
                <div class="col-xs-6">
                  Expand
                </div>
                <div class="col-xs-6 text-right">
                  <i class="fa fa-arrow-circle-right"></i>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="panel panel-warning">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-6">
                <i class="fa fa-barcode fa-5x"></i>
              </div>
              <div class="col-xs-6 text-right">
                <p class="announcement-heading">12</p>
                <p class="announcement-text"> User</p>
              </div>
            </div>
          </div>
          <a href="#">
            <div class="panel-footer announcement-bottom">
              <div class="row">
                <div class="col-xs-6">
                  Expand
                </div>
                <div class="col-xs-6 text-right">
                  <i class="fa fa-arrow-circle-right"></i>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="panel panel-danger">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-6">
                <i class="fa fa-users fa-5x"></i>
              </div>
              <div class="col-xs-6 text-right">
                <p class="announcement-heading">18</p>
                <p class="announcement-text">Users</p>
              </div>
            </div>
          </div>
          <a href="#">
            <div class="panel-footer announcement-bottom">
              <div class="row">
                <div class="col-xs-6">
                  Expand
                </div>
                <div class="col-xs-6 text-right">
                  <i class="fa fa-arrow-circle-right"></i>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="panel panel-success">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-6">
                <i class="fa fa-comments fa-5x"></i>
              </div>
              <div class="col-xs-6 text-right">
                <p class="announcement-heading">9000</p>
                <p class="announcement-text"> Orders!</p>
              </div>
            </div>
          </div>
          <a href="#">
            <div class="panel-footer announcement-bottom">
              <div class="row">
                <div class="col-xs-6">
                  Expand
                </div>
                <div class="col-xs-6 text-right">
                  <i class="fa fa-arrow-circle-right"></i>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div><!-- /.row -->
      
        
        
        
    
          

    </div>




<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="list-group">
                <a href="" class="list-group-item visitor">
                    <h3 class="pull-right">
                        <i class="fa fa-eye"></i>
                    </h3>
                    <h4 class="list-group-item-heading count">
                        1000</h4>
                    <p class="list-group-item-text">
                        Profile Views</p>
                </a><a href="" class="list-group-item facebook-like">
                 
                    
                    
                    
                    
                    <h4 class="list-group-item-heading count">
                        1000</h4>
                    <p class="list-group-item-text">
                        Facebook Likes</p>
                </a><a href="" class="list-group-item google-plus">
                   
                    <h4 class="list-group-item-heading count">
                        1000</h4>
                    <p class="list-group-item-text">
                        Google+</p>
                </a><a href="" class="list-group-item twitter">
                   
                    <h4 class="list-group-item-heading count">
                        1000</h4>
                    <p class="list-group-item-text">
                        Twitter Followers</p>
                </a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="list-group">
                <a href="" class="list-group-item tumblr">
                     <h3 class="pull-right">
                        <i class="fa fa-eye"></i>
                    </h3>
                    <h4 class="list-group-item-heading count">
                        1000</h4>
                    <p class="list-group-item-text">
                        Tumblr</p>
                    <a href="" class="list-group-item linkedin">
                       
                        <h4 class="list-group-item-heading count">
                            1000</h4>
                        <p class="list-group-item-text">
                            Linkedin</p>
                    </a></a><a href="" class="list-group-item youtube">
                       
                        <h4 class="list-group-item-heading count">
                            1000</h4>
                        <p class="list-group-item-text">
                            Youtub Play</p>
                    </a><a href="" class="list-group-item vimeo">
                     
                        <h4 class="list-group-item-heading count">
                            1000</h4>
                        <p class="list-group-item-text">
                            Vimeo</p>
                    </a>
            </div>
        </div>
    </div>
</div>


   
 
        


<br>
<br>



<br>
</br>
     

    <style>
        

  .mkl {
background: #fbfbfb;
height: 176px;
padding: 32px 22px;
box-shadow: 0 0 15px rgba(0, 0, 0, .1);
margin-top: 43px;
font-weight: 600;

}
a.visitor i,.visitor h4.list-group-item-heading { color:#E48A07; }
a.visitor:hover { background-color:#E48A07; }
a.visitor:hover * { color:#FFF; }
/* Facebook */
a.facebook-like i,.facebook-like h4.list-group-item-heading { color:#3b5998; }
a.facebook-like:hover { background-color:#3b5998; }
a.facebook-like:hover * { color:#FFF; }
/* Google */
a.google-plus i,.google-plus h4.list-group-item-heading { color:#dd4b39; }
a.google-plus:hover { background-color:#dd4b39; }
a.google-plus:hover * { color:#FFF; }
/* Twitter */
a.twitter i,.twitter h4.list-group-item-heading { color:#00acee; }
a.twitter:hover { background-color:#00acee; }
a.twitter:hover * { color:#FFF; }
/* Linkedin */
a.linkedin i,.linkedin h4.list-group-item-heading { color:#0e76a8; }
a.linkedin:hover { background-color:#0e76a8; }
a.linkedin:hover * { color:#FFF; }
/* Tumblr */
a.tumblr i,.tumblr h4.list-group-item-heading { color:#34526f; }
a.tumblr:hover { background-color:#34526f; }
a.tumblr:hover * { color:#FFF; }
/* Youtube */
a.youtube i,.youtube h4.list-group-item-heading { color:#c4302b; }
a.youtube:hover { background-color:#c4302b; }
a.youtube:hover * { color:#FFF; }
/* Vimeo */
a.vimeo i,.vimeo h4.list-group-item-heading { color:#44bbff; }
a.vimeo:hover { background-color:#44bbff; }
a.vimeo:hover * { color:#FFF; }




    </style>
    
    <script>
        
        $(document).ready(function(){
   $('[data-toggle="offcanvas"]').click(function(){
       $("#navigation").toggleClass("hidden-xs");
   });
});

        </script>


@endsection
