@extends('layouts.default')
@section('title','Our Team')
@section('content')



<div class="headeryy">
  <div class="container">
      <div class="through_f">
        <h3 class="mb-0">Do you want to build a tunnel in 5 minutes? Try NOW IaniraFunnels!</h3>
      </div>
  </div>
</div>
<section class="top_banner our_team_banner py-5 d-flex align-items-center">
  <div class="sitecontainer ">
    <div class="row align-items-center ">
      <div class="col-sm-6 ">
          <div class="baner_pera_wrp">
            <h2 class="text-white mb-md-5 mb-sm-2">
             <b>Team</b>
            </h2>
            <p class="gray_text">
          “Lorem ipsum dolor sit amet, consectetur adipiscing elit, <b>sed do eiusmod </b> tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
            </p>
          </div>
      </div>
		<div class="col-sm-6">
			<div class="row">
				<div class="col-6 text-center">
					<figure class="mb-0">
						<img src="{{ asset('public/img/person01.png')}}" alt="" class="img-fluid" >						
					</figure>
				</div>
				<div class="col-6 text-center">
					<figure class="mb-0">
						<img src="{{ asset('public/img/person02.png')}}" alt="" class="img-fluid" >						
					</figure>
				</div>
			</div>
		</div>
    </div>
  </div>
</section>
<section class="team_description_main mt-5">
	<div class="siteSmallContainer">
		<div class="row align-items-center mb-5">
			<div class="col-sm-7">
				<div class="team_person_text">
					<h2>
						Aldo Accardi
					</h2>
					<p class="gray_text">
					“Lorem ipsum dolor sit amet, consectetur adipiscing elit, <b>sed do eiusmod </b> tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
					</p>
				</div>
			</div>
			<div class="col-sm-5">
				<div class="doubleCircle_wrp right_top left_bottom">
					<div class="doubleCircle doubleCircle_left"></div>
					<div class="doubleCircle "></div>
					<img src="{{ asset('public/img/devloper01.jpg')}}" alt="" class="img-fluid" >						

				</div>
			</div>
		</div>
		<div class="row align-items-center reverse pt-xl-5 pt-md-4 pt-sm-3 pt-2">
		<div class="col-sm-5">
				<div class="doubleCircle_wrp right_top left_bottom">
					<div class="doubleCircle doubleCircle_left"></div>
					<div class="doubleCircle "></div>
					<img src="{{ asset('public/img/devloper02.jpg')}}" alt="" class="img-fluid" >						

				</div>
			</div>
			<div class="col-sm-7">
				<div class="team_person_text">
					<h2>
						Maria Rossi
					</h2>
					<p class="gray_text">
					“Lorem ipsum dolor sit amet, consectetur adipiscing elit, <b>sed do eiusmod </b> tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
					</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="siteMade_by my-5 py-5">
	<div class="sitecontainer">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="card_heading text-center gray_text mb-xl-5 mb-md-4 mb-sm-3 mb-2">This site is made by</h3>
			</div>
		</div>
		<div class="row mt-5">
			<div class="col-sm-6">
				<div class="developer_card">
					<div class="row">
						<div class="col-4 text-center">
							<figure>
								<img src="{{ asset('public/img/avatarTesimonial.png')}}" alt="" class="img-fluid" >						
							</figure>
						</div>
						<div class="col-8">
							<div class="card_text">
								<h2>
									Michele delle cave
								</h2>
								<h4 class="subheading gray_text">
									Designer UX/UI
								</h4>
								<p class="gray_text">
								Lorem ipsum dolor sit amet, consectetur adipiscing eli
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="developer_card">
					<div class="row">
						<div class="col-4 text-center">
							<figure>
								<img src="{{ asset('public/img/avatarTesimonial.png')}}" alt="" class="img-fluid" >						
							</figure>
						</div>
						<div class="col-8">
							<div class="card_text">
								<h2>
									Mike Lee
								</h2>
								<h4 class="subheading gray_text">
									Developer 
								</h4>
								<p class="gray_text">
								Lorem ipsum dolor sit amet, consectetur adipiscing eli
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>




@endsection
