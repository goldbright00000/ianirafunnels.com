@extends('user-dashboard.user-dashboard-default')
@section('title','Ianira Funnels Dashboard')
@section('content')

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">





<div class="container-fluid" style="margin-top: 75px;">

<div class="row">
    <div class="col-sm-12">
        <span class="alert alert-warning alert_block_top  alert-dismissible">
             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            Welcome back, <strong>Levrone30!</strong>  Today is the day! 
        </span>
    </div>
</div>




<div class="col-md-3" style="padding-left: 0;">
    <aside class="aside_left">
      
        <div class="col-md-12 top_admin_bg">
            <div class="col-xs-4" style="padding:0">
                <figure class="left_circle">
                    <img src="{{ asset('public/icon/703_profile_user.svg')}}" width="50" class="img-resposive user_img" />
                </figure>
            </div>
            <div class="col-xs-8" style="padding: 0">
                <article class="admin_top">
                <h4>Admin</h4>
                <p> Neophyte Funnel Builder </p>
                <p><span>120/388</span></p>
                </article>
            </div>
        </div>




  
<div class="col-md-12" style="padding: 0;margin-top: 15px;">

    <article class="article_accordion">

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading1">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                   <i class="glyphicon glyphicon-folder-close"></i> Ianira Funnel
                </a>
            </h4>
        </div>
        <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
            <div class="panel-body">
                <ul class="list-group">

                    <li class="list-group-item">
                         <span class="glyphicon glyphicon-pencil"></span> 
                         <a href="https://ianirafunnels.com/funnels/purchases_status"> Purchase Status </a>
                     <span class="badge">12</span>
                    </li>

                    <li class="list-group-item">
                        <span class="glyphicon glyphicon-flash"></span> 
                        <a href="https://ianirafunnels.com/funnels/contact_purchases"> Contacts Purchase</a>
                        <span class="badge">5</span>
                    </li> 

                    <li class="list-group-item">
                        <span class="glyphicon glyphicon-file"></span> <a href="#"> Sales</a>
                        <span class="badge">3</span>
                    </li> 

                    <li class="list-group-item">
                        <span class="glyphicon glyphicon-comment"></span>
                        <a href="https://ianirafunnels.com/select-funnel">Funnel Marketplace </a>
                     <span class="badge">14</span>
                 </li> 
                </ul>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading2">
            <h4 class="panel-title">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                    <i class="glyphicon glyphicon-th"></i> Modules
                </a>
            </h4>
        </div>
        <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
            <div class="panel-body">
                <ul class="list-group">
                    <li class="list-group-item"><a href="#"> Orders</a> <span class="badge">$12</span></li>
                    <li class="list-group-item"><a href="#"> Invoices</a></li> 
                    <li class="list-group-item"><a href="#"> Shipments</a></li> 
                    <li class="list-group-item"><a href="#"> Tax</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading3">
            <h4 class="panel-title">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                   <i class="glyphicon glyphicon-user"></i> Account
                </a>
            </h4>
        </div>
        <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
            <div class="panel-body">
                <ul class="list-group">
                    <li class="list-group-item"><a href="#">Change Password</a></li>
                    <li class="list-group-item"><a href="#">Notifications</a> <span class="badge">5</span></li> 
                    <li class="list-group-item"><a href="#">Import/Export</a> <span class="badge">3</span></li>
                    <li class="list-group-item">
                        <span class="glyphicon glyphicon-trash text-danger"></span>
                        <a href="#" class="text-danger"> Delete Account</a>
                    </li>  
                </ul>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading4">
            <h4 class="panel-title">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                     <i class="glyphicon glyphicon-file"></i> Reports
                </a>
            </h4>
        </div>
        <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
            <div class="panel-body">
                <ul class="list-group">
                    <li class="list-group-item"> <span class="glyphicon glyphicon-usd"></span><a href="#">Sales</a></li>
                    <li class="list-group-item"> <span class="glyphicon glyphicon-user"></span><a href="#">Customers</a></li> 
                    <li class="list-group-item"> <span class="glyphicon glyphicon-tasks"></span><a href="#">Products</a></li> 
                    <li class="list-group-item">  <span class="glyphicon glyphicon-shopping-cart"></span><a href="#">funnel</a></li> 
                </ul>
            </div>
        </div>
    </div>
</div>
        
    </article>
</div><!-- .col-md-12 -->



    </aside>
</div><!-- .col-md-3 --> 

 


<div class="col-md-9" style="padding: 0">
 
        <div class="col-md-12 section_heading1"> 
            <div class="col-md-4">
               <span class="icon_toggler">
                   <img src="https://ianirafunnels.com/public/icon/list.svg" class="icon_with">
               </span>
            </div>
            <div class="col-md-8">
                <a href="https://ianirafunnels.com/select-funnel">   
                    <div class="divses2"><img class="icon_with" src="https://ianirafunnels.com/public/icon/add_plus.svg">  Add New Funnel</div>
                </a>
            </div>
        </div>
 


     <section id="main" style="height: 500px;">
       
    <!-- first-row -->
            <div class="row">
                <div class="col-md-3">
                    <article class="single_counter single_counter_article">
                        <div class="circle_icon">
                            <i class="fa fa-phone" style="-webkit-transform: rotate(90deg);transform: rotate(90deg);"></i>
                        </div>
                        <div class="opop"><span class="statistic-counter" data-count="{{rand('1','90')}}" >0 </span> Of 999&nbsp;999&nbsp;9999</div>
                        <p class="pro">Total number of contacts associated with your account.</p>
                    </article>
                </div>
                <div class="col-md-3">
                    <article class="single_counter single_counter_article">
                        <div class="circle_icon">
                            <i class="fa fa-user"></i>
                        </div>

                      <div class="opop"> <span class="statistic-counter" data-count="{{rand('1','10')}}" >0 </span> Of 1000</div>
                        <p class="pro">Unique people who have visited your funnels this billing cycle.</p>
                    </article>
                </div>
                <div class="col-md-3">
                    <article class="single_counter single_counter_article">
                        <div class="circle_icon">
                            <i class="fa fa-clipboard"></i>
                        </div>

                       <div class="opop"> <span class="statistic-counter" data-count="{{rand('1','10')}}" >0 </span> Of 1000</div>
                        <p class="pro">The total number of pages (and variations) associated with a funnel in your account.</p>
                    </article>
                </div>
                <div class="col-md-3">
                    <article class="single_counter single_counter_article">
                        <div class="circle_icon">
                            <i class="fa fa-filter"></i>
                        </div>

                       <div class="opop"> <span class="statistic-counter" data-count="{{rand('1','5')}}" >0 </span> Of 5</div>
                        <p class="pro">The total number of funnels in your account.</p>
                    </article>
                </div>
            </div> <!-- first-row -->
           


<!-- second-row -->
<div class="row" style="margin-top: 30px;">

    <!-- .col-md-8 -->
        <div class="col-md-8">
      

<figure class="image_section">
    <span class="floating_icon dropdown">
        <img src="https://ianirafunnels.com/public/icon/more.svg" class="ufuo color-tooltip dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-placement="top">
        <ul class="dropdown-menu">
          <li><a href="#">Ianira Funnel</a></li>
          <li><a href="#">Modules</a></li>
          <li><a href="#">Account</a></li>
          <li><a href="#">Reports</a></li>
        </ul>
    </span>
    <img src="https://images.clickfunnels.com/8e/3d6390104011e983ac07fdbd5bb090/ofa-dashboard-test-4.png" class="img-resposive image_section_inner" />
</figure>


<figure class="image_section mtb_20">
    <span class="floating_icon dropdown">
        <img src="https://ianirafunnels.com/public/icon/more.svg" class="ufuo color-tooltip dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-placement="top">
        <ul class="dropdown-menu">
          <li><a href="#">Ianira Funnel</a></li>
          <li><a href="#">Modules</a></li>
          <li><a href="#">Account</a></li>
          <li><a href="#">Reports</a></li>
        </ul>
    </span>
    <img src="https://images.clickfunnels.com/63/76c130cc0511e8957f6b642db0b788/FBS_-_1_.png" class="img-resposive image_section_inner" />
</figure>


<figure class="image_section">
    <span class="floating_icon dropdown">
        <img src="https://ianirafunnels.com/public/icon/more.svg" class="ufuo color-tooltip dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-placement="top">
        <ul class="dropdown-menu">
          <li><a href="#">Ianira Funnel</a></li>
          <li><a href="#">Modules</a></li>
          <li><a href="#">Account</a></li>
          <li><a href="#">Reports</a></li>
        </ul>
    </span>
    <img src="https://images.clickfunnels.com/8e/3d6390104011e983ac07fdbd5bb090/ofa-dashboard-test-4.png" class="img-resposive image_section_inner" />
</figure>

</div><!-- .col-md-8 -->

 

<!-- .col-md-4 -->
        <div class="col-md-4">
    <div class="account_full" onclick="DstiNation();">
        <div class="user_wide">
            <img src="https://ianirafunnels.com/public/icon/user_auth.svg" class="img-resposive">
        </div>
        <div class="account_wide"> Account Wide </div>
        <div class="all_funnels"> All Funnels <span class="[ caret ] all-icon">  </span> </div>
    </div>

    <div id="Dlocation" class="funeel_drop">

        <div class="">
            <div class="define search_box_funnel">
                <input type="text" value="" class="ho_input">
            </div>

            <div class="act">
                <div class="user_wide">
                    <img src="https://ianirafunnels.com/public/icon/user_auth.svg" class="img-resposive">
                </div>
                <div class="account_wide"> Account Wide <span class="all-ic text-color">  €0.00 </span> </div>
                <div class="all_funnels"> All Funnels </div>
            </div>

            <div class="act">
                <div class="user_wide">
                    <img src="https://ianirafunnels.com/public/icon/user_auth.svg" class="img-resposive">
                </div>
                <div class="account_wide"> Account Wide <span class="all-ic text-color">  €0.00 </span> </div>
                <div class="all_funnels"> All Funnels </div>
            </div>

        </div>

    </div>

    <div class="dd_funnel">

        <div class="uu_funnel">
            Visit Funnel
        </div>

        <div class="all-button"> Last All Activity </div>

    </div>

    <div class="funnel_sugest">

        <div class="gross_revenue"> Gross Revenue</div>
        <div class="gross_price"> $0</div>
    </div>

    <div class="funnel_sugest">

        <div class="gross_revenue"> Successful Purchases</div>
        <div class="gross_price"> $0</div>
    </div>

    <div class="funnel_sugest">

        <div class="gross_revenue"> Optins</div>
        <div class="gross_price"> $0</div>
    </div>

    <div class="funnel_sugest">

        <div class="gross_revenue"> Page Views</div>
        <div class="gross_price"> $0</div>
    </div>

    </div><!-- .col-md-4 -->

</div><!-- second-row -->



 
     </section>


 





</div><!-- .col-md-9 --> 


</div><!-- .container-fluid -->

 
 



<!-- 
<div class="body-container">
   
<div class="container-fluid full-width padding-top">
    <div class="row profile">
        <center>
            <div class="alert alert-warning ogo">
            Welcome back, <strong>Levrone30!</strong>  Today is the day! 
            </div>
        </center>
        
        <div class="row">
            <center>
        <div class="col-md-6 col-md-offset-3">
            <div class="well well-sm">
                <div class="media">
                    <a class="thumbnail pull-left new_circle" href="#">
                        <img class="media-object img-circle " src="{{ asset('public/icon/703_profile_user.svg')}}">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">Admin</h4>
                        <p> Neophyte Funnel Builder </p>
                        
                		<p><span class="label label-warning">120/388</span></p>
                        <-!---<p>
                            <a href="#" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-comment"></span> Message</a>
                            <a href="#" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-heart"></span> Favorite</a>
                            <a href="#" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-ban-circle"></span> Unfollow</a>
                        </p>---=>
                    </div>
                </div>
            </div>
        </div></center>
            
        </div> 


    </div>
        
        
        
     <section id="counter" class="counter">
            <div class="main_counter_area">
                <div class="overlay p-y-3">
                    <div class="container-fluid full-width">
                        <div class="row">
                            <div class="main_counter_content text-center white-text wow fadeInUp">
                                <div class="col-md-3">
                                    <div class="single_counter p-y-2 m-t-1">
                                        <i class="fa fa-heart m-b-1"></i>
                                        <div class="opop">  <span class="statistic-counter" data-count="{{rand('1','90')}}" >0 </span> Of 999&nbsp;999&nbsp;9999</div>
                                        
                                        
                                        <p class="pro">Total number of contacts associated with your account.</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="single_counter p-y-2 m-t-1">
                                        <i class="fa fa-check m-b-1"></i>
                                      <div class="opop"> <span class="statistic-counter" data-count="{{rand('1','10')}}" >0 </span> Of 1000</div>
                                        <p class="pro">Unique people who have visited your funnels this billing cycle.</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="single_counter p-y-2 m-t-1">
                                        <i class="fa fa-refresh m-b-1"></i>
                                       <div class="opop"> <span class="statistic-counter" data-count="{{rand('1','10')}}" >0 </span> Of 1000</div>
                                        <p class="pro">The total number of pages (and variations) associated with a funnel in your account.</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="single_counter p-y-2 m-t-1">
                                        <i class="fa fa-beer m-b-1"></i>
                                       <div class="opop"> <span class="statistic-counter" data-count="{{rand('1','5')}}" >0 </span> Of 5</div>
                                        <p class="pro">The total number of funnels in your account.</p>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>  
        
        
        
        
        
           <section id="counter" class="counter">
                    <div class="container-fluid full-width">
                        <div class="row">
                            
                            <div class="col-sm-3"> 
                                
        
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-folder-close">
                            </span>Ianira Funnel</a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-pencil text-primary"></span><a href="{{ url('funnels/purchases_status') }}"> Purchase Status </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-flash text-success"></span><a href="{{ url('funnels/contact_purchases') }}"> Contacts Purchase</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-file text-info"></span><a href="#">Sales</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-comment text-success"></span><a href="{{ url('select-funnel') }}">Funnel Marketplace </a>
                                        <span class="badge">42</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                
                
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-th">
                            </span>Modules</a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <a href="#">Orders</a> <span class="label label-success">$ 320</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="#">Invoices</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="#">Shipments</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="#">Tex</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-user">
                            </span>Account</a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <a href="#">Change Password</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="#">Notifications</a> <span class="label label-info">5</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="#">Import/Export</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-trash text-danger"></span><a href="#" class="text-danger">
                                            Delete Account</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-file">
                            </span>Reports</a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-usd"></span><a href="#">Sales</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-user"></span><a href="#">Customers</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-tasks"></span><a href="#">Products</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-shopping-cart"></span><a href="#">funnel</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
      
                                
                                
                                
                                
                                
                            
                            
                            </div> 
                            
                            
                            
                              <div class="col-sm-6"> 
                                  <div class="row bore"> 
                                  <div class="divses1" data-toggle="tooltip" data-placement="top" title="Priority List"> <img src="{{asset('public/icon/list.svg')}}" class='icon_with'>   <=!---Priority List --=> </div>
                                  <a href="{{ url('select-funnel')}}">   <div class="divses2">
                                        <img class="icon_with " src="{{ asset('public/icon/add_plus.svg')}}">  Add New Funnel</div>
                              
                              </div>
                            </a>
                                  
                                  <div class="row">
                                      
                                      <div class="list_padding">
                                          <div class="floating">
  <img src="{{asset('public/icon/more.svg')}}" class="ufuo color-tooltip dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-toggle="tooltip" data-placement="top" title="More"     >  
                                          
                                          
                                                    <ul  class="dropdown-menu root_more">
                                                     <li><a class="dropdown-item" href="#"> <img src="{{asset('public/icon/multiplydelete.svg')}}" class="remove_image">  <span class="remove_ooo">Remove </span>  
                                                        <br> <div class="cardfrom"> Remove this card from your feed</div>
                                                    </a></li> 
                                                        <li><a class="dropdown-item" href="#"> <img src="{{asset('public/icon/help.svg')}}" class="remove_image">
                                                        <span class="remove_ooo">Support </span>  
                                                        <br> <div class="cardfrom"> Get help regarding this specific topic</div>
                                                    </a></li> 

                                                 </ul>  
                                              
                                              
                                          
                                          
                                          </div>
                                      
                                                <div class="list_rt">
                                                    <img src="https://images.clickfunnels.com/8e/3d6390104011e983ac07fdbd5bb090/ofa-dashboard-test-4.png" class="img-resposive">
                                                </div>
                                          
                                  
                                          
                                          
                                       </div>
                                      
                                  </div>  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                   <div class="row">
                                      
                                      <div class="list_padding">
                                          <div class="floating">
  <img src="{{asset('public/icon/more.svg')}}" class="ufuo color-tooltip dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-toggle="tooltip" data-placement="top" title="More"     >  
                                          
                                          
                                                    <ul  class="dropdown-menu root_more">
                                                     <li><a class="dropdown-item" href="#"> <img src="{{asset('public/icon/multiplydelete.svg')}}" class="remove_image">  <span class="remove_ooo">Remove </span>  
                                                        <br> <div class="cardfrom"> Remove this card from your feed</div>
                                                    </a></li> 
                                                        <li><a class="dropdown-item" href="#"> <img src="{{asset('public/icon/help.svg')}}" class="remove_image">
                                                        <span class="remove_ooo">Support </span>  
                                                        <br> <div class="cardfrom"> Get help regarding this specific topic</div>
                                                    </a></li> 

                                                 </ul>  
                                              
                                              
                                          
                                          
                                          </div>
                                      
                                                <div class="list_rt">
                                                    <img src="https://images.clickfunnels.com/63/76c130cc0511e8957f6b642db0b788/FBS_-_1_.png" class="img-resposive">
                                                </div>
                                          
                                  
                                          
                                          
                                       </div>
                                      
                                  </div>  
                                  
                                  
                                  
                                  
                                  
                                  
                                   <div class="row">
                                      
                                      <div class="list_padding">
                                          <div class="floating">
  <img src="{{asset('public/icon/more.svg')}}" class="ufuo color-tooltip dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-toggle="tooltip" data-placement="top" title="More"     >  
                                          
                                          
                                                    <ul  class="dropdown-menu root_more">
                                                     <li><a class="dropdown-item" href="#"> <img src="{{asset('public/icon/multiplydelete.svg')}}" class="remove_image">  <span class="remove_ooo">Remove </span>  
                                                        <br> <div class="cardfrom"> Remove this card from your feed</div>
                                                    </a></li> 
                                                        <li><a class="dropdown-item" href="#"> <img src="{{asset('public/icon/help.svg')}}" class="remove_image">
                                                        <span class="remove_ooo">Support </span>  
                                                        <br> <div class="cardfrom"> Get help regarding this specific topic</div>
                                                    </a></li> 

                                                 </ul>  
                                              
                                              
                                          
                                          
                                          </div>
                                      
                                                <div class="list_rt">
                                                    <img src="https://images.clickfunnels.com/8e/3d6390104011e983ac07fdbd5bb090/ofa-dashboard-test-4.png" class="img-resposive">
                                                </div>
                                          
                                  
                                          
                                          
                                       </div>
                                      
                                  </div>  
                                  
                                  
                                  
                                  
                                  
                                  
                              </div> 
                              
                           
                               <div class="col-sm-3"> 
                                   
                                   
                                   <div class="account_full" onclick="DstiNation();">
                                                <div class="user_wide">
                                                    <img src="{{ asset('public/icon/user_auth.svg')}}" class="img-resposive">
                                                </div>
                                                <div class="account_wide"> Account Wide   </div> 
                                                 <div class="all_funnels"> All Funnels <span class="[ caret ] all-icon">  </span> </div>
                                            </div> 
                                   
                                   
                                   <div id="Dlocation" class="funeel_drop">
                   
                  
                                       <div class=""> 
                                                        <div class="define search_box_funnel">
                                                          <input type="text" value="" class="ho_input" >
                                                        </div>
                                           
                                           
                                             <div class="act">
                                                <div class="user_wide">
                                                    <img src="{{ asset('public/icon/user_auth.svg')}}" class="img-resposive">
                                                </div>
                                                <div class="account_wide"> Account Wide   <span class="all-ic text-color">  €0.00 </span> </div> 
                                                 <div class="all_funnels"> All Funnels </div>
                                            </div> 
                                           
                                           
                                           <div class="act">
                                                <div class="user_wide">
                                                    <img src="{{ asset('public/icon/user_auth.svg')}}" class="img-resposive">
                                                </div>
                                                <div class="account_wide"> Account Wide   <span class="all-ic text-color">  €0.00 </span> </div> 
                                                 <div class="all_funnels"> All Funnels </div>
                                            </div> 
                                           
                                           
                                               
                                               
                                               
                                       </div>
                                          
                                   </div>
                                   
                                   <div class="dd_funnel">
                                       
                                       <div class="uu_funnel">
                                       Visit Funnel
                                        </div> 
                                       
                                       
                                          <div class="all-button"> Last All Activity </div>
                                       
                                   </div>
                                   
                                
                                   
                                   <div class="funnel_sugest">
                                       
                                       <div class="gross_revenue"> Gross Revenue</div>
                                       <div class="gross_price"> $0</div>
                                   </div>
                                   
                                   
                                   <div class="funnel_sugest">
                                       
                                       <div class="gross_revenue"> Successful Purchases</div>
                                       <div class="gross_price"> $0</div>
                                   </div>
                                   
                                   
                                   <div class="funnel_sugest">
                                       
                                       <div class="gross_revenue"> Optins</div>
                                       <div class="gross_price"> $0</div>
                                   </div>
                                     
                                   
                                   <div class="funnel_sugest">
                                       
                                       <div class="gross_revenue"> Page Views</div>
                                       <div class="gross_price"> $0</div>
                                   </div>
                                   
                                   
                                   
                   
               </div>
                                   
                                </div>
                            
                            
                            
                         </div>
                              </div>
               
               
               
               
           </section>
        
        
        
        
        
        
        
        
        
	</div>
</div></div>

 -->


 



<style type="text/css">
    

.left_circle {
    border: 2px solid #ffd3a9;
    width: 75px;
    height: 75px;
    border-radius: 50%;
    overflow: hidden;
    background: #fff;
    padding: 10px;
}
.left_circle img{
    width: 100%;
    height: 100%;
}
.admin_top h4 {
    text-transform: capitalize;
    font-size: 24px;
    margin: 0;
    margin-bottom: 3px;
}
.admin_top h4 + p {
    margin: 0;
}

.top_admin_bg{
 background: #f28f30;   
 padding-top: 10px;
 padding-bottom: 10px;
}


.single_counter_article {
    background: #fff;
    padding: 15px;
    border-radius: 4px;
    text-align: center;
    min-height: 235px;
    -webkit-box-shadow: 0 0 5px 0 #f28f2f;
    box-shadow: 0 0 5px 0 #f28f2f;
}
.single_counter_article .circle_icon {
    background: #fff;
    width: 75px;
    height: 75px;
    border: 2px solid #ffd3a9;
    border-radius: 50%;
    text-align: center;
    font-size: 40px;
    padding-top: 10px;
    margin: auto;
    margin-bottom: 15px;
    color: #f28f2f;
    line-height: 56px;
}
.single_counter_article .pro {
    margin-top: 10px;
    color: #555;
    font-size: 15px;
}
.opop {
    font-weight: 700;
    text-align: center;
    font-size: 18px;
    margin-top: 20px;
    color: #080;
}
.alert_block_top {
    width: 100%;
    display: block;
    text-align: center;
    font-size: 18px;
    color: #000;
    background: #fbd189;
    border-color: #fbd189;
}

.aside_left {
    background: #fff;
    -webkit-box-shadow: 0 5px 5px 0 #eee;
    box-shadow: 0 5px 5px 0 #eee;
    display: block;
    min-height: 1100px;
}

.article_accordion .panel-default>.panel-heading {
    color: #777;
    background-color: #fff;
    border: 0 !important;
}

.article_accordion .panel-body{
    padding: 0;
    border: 0;
    border-radius: 0;
}

.article_accordion .list-group-item{
    border-radius: 0 0 !important;
    border: 0;
}


.article_accordion .panel-default{
    border: 0;
}
.article_accordion .panel-title a{
    text-decoration: none;
}
.article_accordion .glyphicon {
    margin-right: 10px;
}
.section_heading1{
    background: #f0f0f0;
    margin-bottom: 15px;
}

.section_heading1 .icon_toggler {
    margin-top: 9px;
    display: inline-block;
    margin-left: -15px;
    cursor: pointer;
}
.divses2 {
    float: right;
    font-weight: 600;
    padding: 8px;
    margin-right: -15px;
    color: #f28f30;
}
.image_section{
    position: relative;
}

.floating_icon {
    position: absolute;
    right: 10px;
    top: 10px;
    cursor: pointer;
}
.mtb_20{
    margin-top: 20px;
    margin-bottom: 20px;
}
.article_accordion .panel-title a {
    text-decoration: none;
    display: block;
}

.image_section .image_section_inner{
    width: 100%;
}

.article_accordion .badge {
    background-color: #f28f30;
}

.main_counter_area {
    background: url(https://images.pexels.com/photos/196288/pexels-photo-196288.jpeg?w=940&h=650&auto=compress&cs=tinysrgb) no-repeat top center;
    background-size: cover;
    overflow: hidden;
}

.main_counter_area .main_counter_content .single_counter {
    background: #f58b3c;
    color: #fff;
    height: 218px;
}

.main_counter_area .main_counter_content .single_counter i {
    font-size: 36px;
}

.profileicon {
    fill: red;
}

.media-object {
    width: 100px;
}

.media-heading {
    font-weight: 600;
}

.ogo {
    font-weight: 600;
    font-size: 14px;
}

.new_circle {
    border-radius: 83px;
    height: 119px;
}

.divses1 {
    float: left;
    font-weight: 600;
    padding: 7px;
}

.icon_with {
    width: 28px;
}

.floating {
    right: 2px;
    float: right;
    top: 30px;
    position: relative;
    width: 21px;
}

.bore {
    border-bottom: 1px solid #d4d4d4;
    height: 48px;
}

.list_rt img {
    width: 100%;
    border-radius: inherit;
    height: 287px;
}

.list_padding {
    padding-top: 0px;
}

.ufuo {
    width: 24px;
    vertical-align: middle;
    transform: rotate(90deg);
}

.color-tooltip + .tooltip > .tooltip-inner {
    background-color: #f00;
    max-width: 100%;
}

.color-tooltip + .tooltip > .tooltip-arrow {
    border-top-color: #f00;
}

.user_wide {
    float: left;
    width: 28px;
    line-height: 28px;
}

.user_wide img {
    width: 23px;
    height: 27px;
}

.account_full {
    border: solid 1px #eee;
    overflow: hidden;
    padding-top: 5px;
    background: #f5f5f5;
}

.account_wide {
    float: left;
    line-height: 31px;
    font-size: 15px;
    font-weight: 600;
}

.all_funnels {
    float: right;
    line-height: 28px;
    color: #757575;
    font-size: 14px;
    padding-right: 8px;
}

.all-icon {
    float: right;
    margin-right: 5px;
    margin-top: 13px;
}

.funeel_drop {
    border: 2px solid #f58b3c;
    display: block;
    position: absolute;
    width: 93.5%;
    z-index: 1;
    box-shadow: 0 0 15px rgba(0, 0, 0, .1);
}

#Dlocation {
    display: none;
}

.search_box_funnel {
    height: 46px;
    background: #f58b3c;
    padding-top: 4px;
}

.ho_input {
    border: solid 1px white;
    width: 94%;
    background: white;
    height: 35px;
    margin-top: 2px;
    margin-left: 9px;
    border-radius: 15px;
    padding-left: 8px;
}

.act {
    border: solid 1px #e8e8e8;
    overflow: hidden;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 2px;
    background: #f9f9f9;
}

.text-color {
    position: absolute;
    right: 8px;
    color: #4d4c4c;
    margin-top: 12px;
}

.dd_funnel {
    overflow: hidden;
    background: #dbdbdb;
}

.uu_funnel {
    margin: 5px;
    margin-top: 5px;
    background: white;
    line-height: 33px;
    margin-top: 17px;
    text-align: center;
}

.all-button {
    padding: 8px;
    border: solid 1px #f58b3c;
    background: #f58b3c;
    color: wheat;
    color: white;
    text-align: center;
    font-weight: 600;
    margin-top: 12px;
}

.root_more {
    margin-top: -27px;
    padding-top: 6px;
    padding-left: 0px;
    padding-bottom: 14px;
    width: 271px;
    padding-right: 6px;
}

.root_more li {
    margin-top: 7px;
}

.remove_image {
    float: left;
    width: 21px;
    margin-top: 10px;
}

.cardfrom {
    padding-left: 28px;
    font-size: 12px;
}

.remove_ooo {
    margin-left: 7px;
}

.funnel_sugest {
    background: #ffffff;
    height: 144px;
    padding: 15px;
    margin-top: 20px;
    font-weight: 600;
    -webkit-box-shadow: 0 0 4px 0 #eee;
    box-shadow: 0 0 4px 0 #eee;
    border: 1px solid #eee;
}

.gross_revenue {
    width: 142px;
    float: left;
    margin-top: 6px;
}

.gross_price {
    float: right;
    width: 57px;
    margin-top: 6px;
    text-align: right;
}

.thumbnail {
    display: block;
    padding: 8px;
    padding-right: 8px;
    padding-right: 4px;
    margin-bottom: 30px;
    line-height: 1.42857143;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 63px;
    -webkit-transition: border .2s ease-in-out;
    -o-transition: border .2s ease-in-out;
    transition: border .2s ease-in-out;
    margin-top: 29px;
    margin-left: 20px;
}

 
.article_accordion .list-group-item a {
    font-size: 14px;
    color: #555;
    text-transform: capitalize;
    text-decoration: none;
    transition: 0.5s;
}
.article_accordion .list-group-item:hover a{
    color: #999;
}


</style>


@endsection