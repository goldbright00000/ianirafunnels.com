<!DOCTYPE html>
<html>
<head>
<title>@yield('title')</title>
        
<meta name="csrf-token" content="{!! csrf_token() !!}">
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"  href="{{ asset('public/user-style/css/custom.css') }}" type="text/css">
<script src="{{ asset('public/user-style/js/custom.js') }}" type="text/javascript"></script>

</head>
<body>



<nav class="navbar navbar-default navbar-fixed-top navigation" role="navigation" style="background: transparent !important;">
  
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
       <a class="navbar-brand" href="#"><img width="185" src="{{ asset('img/funnel_logo.png')}}" class="img-responsive"></a>
    </div>
 
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navigation-navbar-collapse-1">
      <ul class="nav navbar-nav  navbar-right">

          <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Ianira Funnels <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="{{url('funnels')}}"><i class="fa fa-star"></i> Funnels</a></li>
              <li><a href="#"><i class="fa fa-star"></i> Contact</a></li>
              <li><a href="#"><i class="fa fa-star"></i> Sale</a></li>
              <li><a href="#"><i class="fa fa-star"></i> Funnel Market Place</a></li>
              <li><a href="#"><i class="fa fa-star"></i> Build Funnel</a></li>
              <li><a href="{{url('clear-cache')}}"><i class="fa fa-star"></i> Purge</a></li>
            </ul>
        </li>

 
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Actionetics  <b class="caret"></b></a>
          <!-- <ul class="dropdown-menu">
            <li><a href="#"><i class="fa fa-envelope"></i> Registration</a></li>
            <li><a href="#"><i class="fa fa-download"></i> Thank You</a></li>
            <li><a href="#"><i class="fa fa-microphone"></i> Broadcast Room</a></li>
          </ul> -->
        </li>


        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Helps <b class="caret"></b></a>
         <!--  <ul class="dropdown-menu">
            <li><a href="#"><i class="fa fa-sign-in-alt"></i> Member Access</a></li>
            <li><a href="#"><i class="fa fa-tachometer-alt"></i> Membership Area</a></li>
          </ul> -->
        </li>

        <li><a href="#"> Backpack</a></li>

         <li class="dropdown">
         	<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
         		<img src="https://ianirafunnels.com/public/icon/703_profile_user.svg" class="img-responsive img_responsive_1" width="25" />
             {{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}} <b class="caret"></b>
            </a>
    					<ul class="dropdown-menu">
    						<li><a href="#">Page 1-1</a></li>
    						<li><a href="#">Page 1-2</a></li>
    						<li><a href="{{url('logout')}}"> Log Out</a></li>
    					</ul>
         		
         	</li>
      </ul>
    
    </div><!-- /.navbar-collapse -->
  
</nav>



<!-- <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Page 1-1</a></li>
          <li><a href="#">Page 1-2</a></li>
          <li><a href="#">Page 1-3</a></li>
        </ul>
      </li> -->




    
    @yield('content')
   
<center>
<strong>Powered by <a href="" target="_blank"> ianirafunnels  </a></strong>
	


  
    
   
 <style>
 



/*:::::::::::::::::::::::::::::::::::::::::::: Main Navigation  :::::::::::::::::::::::::::::::::::::::::::::*/
.navigation {
    padding-left: 30px;
    padding-right: 30px;
    background: #fff;
    -webkit-box-shadow: 0px 2px 2px 0 #f7f7f7;
    box-shadow: 0px 2px 2px 0 #f7f7f7;
    height: 60px;
    padding-top: 7px;
}

.navigation .nav>li>a>img {
    max-width: none;
    float: left;
    width: 20px;
    border: 1px solid #ddd;
    border-radius: 50%;
    margin-right: 10px;
}

.navigation .navbar-brand>img {
    display: block;
    width: 165px;
    margin-top: -5px;
}


.navigation .navbar-collapse, .navbar-default .navbar-form {
    border-color: transparent;
    background: #fff !important;
}
.navigation  .dropdown-menu>li>a {
    display: block;
    padding:10px 30px 10px 15px !important;
    width: 220px;
    clear: both;
    font-weight: 400;
    line-height: 1.42857143;
    color: #777;
    white-space: nowrap;
    -webkit-transition: 0.5s;
     transition: 0.5s;
}
.navigation .dropdown-menu>li>a:focus, 
.navigation .dropdown-menu>li>a:hover {
    color: #ffffff !important;
    background-color: #f28d2c!important;
}


.selectYourFunnelDone .form-control{
box-shadow: 0 0 !important;
border: 2px solid #aab6bf;
outline: 0 !important;
height:50px !important;
}

</style>
    
   
    
<!--footer starts from here-->

<script type="text/javascript">

  

 



$(function(){

  $(".dropdown").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                $(this).toggleClass('open');
                $('.caret', this).toggleClass("caret fa fa-caret-up"); 
                // alert(1)             
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                $(this).toggleClass('open');
                $('.caret', this).toggleClass("caret fa fa-caret-down");   
                // alert(2)                 
            });

 



 


})
</script>

    
    
    
    </body>
</html>
 
