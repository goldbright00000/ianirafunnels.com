@extends('crm.default')
@section('title','Admin Dashboard')
@section('content')
 <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-12">
    <div ng-show="ticketsLoader" layout-align="center center" class="text-center layout-align-center-center ng-hide" id="circular_loader" aria-hidden="true">
        <md-progress-circular md-mode="indeterminate" md-diameter="40" aria-valuemin="0" aria-valuemax="100" role="progressbar" class="ng-isolate-scope md-mode-indeterminate" style="width: 40px; height: 40px;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" style="width: 40px; height: 40px; transform-origin: 20px 20px 20px;"><path fill="none" stroke-width="4" stroke-linecap="square" d="M20,2A18,18 0 1 1 2,20" stroke-dasharray="84.82300164692441" stroke-dashoffset="175.48311660958817" transform="rotate(-180 20 20)"></path></svg></md-progress-circular>
        <p style="font-size: 15px;margin-bottom: 5%;">
         <span>
            Please wait <br>
           <small><strong>Loading Tickets...</strong></small>
         </span>
       </p>
     </div>
    <div ng-show="!ticketsLoader" class="main-content container-fluid col-xs-12 col-md-3 col-lg-3 md-pl-0" aria-hidden="false">
      <div class="panel-default panel-table borderten lead-manager-head">
        <md-content style="border-bottom: 2px dashed #e8e8e8; padding-bottom: 20px;" layout-padding="" class="layout-padding _md">
          <div class="col-md-3 col-xs-6 border-right text-uppercase">
            <div class="tasks-status-stat">
              <h4 class="text-bold ciuis-task-stat-title">
              <span class="task-stat-number ng-binding" ng-bind="(tickets | filter:{status_id:'1'}).length">11</span>
              <span class="task-stat-all ng-binding" ng-bind="'/'+' '+tickets.length+' '+'TICKET'">/ 39 TICKET</span>
              </h4>
              <span class="ciuis-task-percent-bg">
              <span class="ciuis-task-percent-fg" style="width: 28.205128205128204%;"></span>
              </span>
            </div>
            <span style="color:#989898">OPEN</span>
          </div>
          <div class="col-md-3 col-xs-6 border-right text-uppercase">
            <div class="tasks-status-stat">
              <h4 class="text-bold ciuis-task-stat-title">
              <span class="task-stat-number ng-binding" ng-bind="(tickets | filter:{status_id:'2'}).length">1</span>
              <span class="task-stat-all ng-binding" ng-bind="'/'+' '+tickets.length+' '+'TICKET'">/ 39 TICKET</span>
              </h4>
              <span class="ciuis-task-percent-bg">
              <span class="ciuis-task-percent-fg" style="width: 2.5641025641025643%;"></span>
              </span>
            </div>
            <span style="color:#989898">IN PROGRESS</span>
          </div>
          <div class="col-md-3 col-xs-6 border-right text-uppercase">
            <div class="tasks-status-stat">
              <h4 class="text-bold ciuis-task-stat-title">
              <span class="task-stat-number ng-binding" ng-bind="(tickets | filter:{status_id:'3'}).length">26</span>
              <span class="task-stat-all ng-binding" ng-bind="'/'+' '+tickets.length+' '+'TICKET'">/ 39 TICKET</span>
              </h4>
              <span class="ciuis-task-percent-bg">
              <span class="ciuis-task-percent-fg" style="width: 66.66666666666667%;"></span>
              </span>
            </div>
            <span style="color:#989898">ANSWERED</span>
          </div>
          <div class="col-md-3 col-xs-6 border-right text-uppercase">
            <div class="tasks-status-stat">
              <h4 class="text-bold ciuis-task-stat-title">
              <span class="task-stat-number ng-binding" ng-bind="(tickets | filter:{status_id:'4'}).length">1</span>
              <span class="task-stat-all ng-binding" ng-bind="'/'+' '+tickets.length+' '+'TICKET'">/ 39 TICKET</span>
              </h4>
              <span class="ciuis-task-percent-bg">
              <span class="ciuis-task-percent-fg" style="width: 2.5641025641025643%;"></span>
              </span>
            </div>
            <span style="color:#989898">CLOSED</span>
          </div>
        </md-content>
          <div class="ticket-contoller-left">
            <div id="tickets-left-column text-left">
              <div class="col-md-12 ticket-row-left text-left">
              <div class="tickets-vertical-menu">
                <a ng-click="TicketsFilter = NULL" class="highlight text-uppercase"><i class="fa fa-inbox fa-lg" aria-hidden="true"></i> All Tickets <span class="ticket-num ng-binding" ng-bind="tickets.length">39</span></a>
                <a ng-click="TicketsFilter = {status_id: 1}" class="side-tickets-menu-item">OPEN <span class="ticket-num ng-binding" ng-bind="(tickets | filter:{status_id:'1'}).length">11</span></a>
                <a ng-click="TicketsFilter = {status_id: 2}" class="side-tickets-menu-item">IN PROGRESS <span class="ticket-num ng-binding" ng-bind="(tickets | filter:{status_id:'2'}).length">1</span></a>
                <a ng-click="TicketsFilter = {status_id: 3}" class="side-tickets-menu-item">ANSWERED <span class="ticket-num ng-binding" ng-bind="(tickets | filter:{status_id:'3'}).length">26</span></a>
                <a ng-click="TicketsFilter = {status_id: 4}" class="side-tickets-menu-item">CLOSED <span class="ticket-num ng-binding" ng-bind="(tickets | filter:{status_id:'4'}).length">1</span></a>
                <h5 href="#" class="menu-icon active text-uppercase text-muted"><i class="fa fa-file-text fa-lg" aria-hidden="true"></i>Filter by Priority</h5>
                <a ng-click="TicketsFilter = {priority_id: 1}" class="side-tickets-menu-item">LOW <span class="ticket-num ng-binding" ng-bind="(tickets | filter:{priority_id:'1'}).length">2</span></a>
                <a ng-click="TicketsFilter = {priority_id: 2}" class="side-tickets-menu-item">MEDIUM <span class="ticket-num ng-binding" ng-bind="(tickets | filter:{priority_id:'2'}).length">19</span></a>
                <a ng-click="TicketsFilter = {priority_id: 3}" class="side-tickets-menu-item">HIGH <span class="ticket-num ng-binding" ng-bind="(tickets | filter:{priority_id:'3'}).length">18</span></a>
              </div>
              </div>
             </div>
          </div>
      </div>
    </div>
    <div ng-show="!ticketsLoader" class="main-content container-fluid col-xs-12 col-md-9 col-lg-9 md-p-0 lead-table" aria-hidden="false">
      <md-toolbar class="toolbar-white _md _md-toolbar-transitions">
        <div class="md-toolbar-tools">
          <h2 flex="" md-truncate="" class="text-bold md-truncate flex">Tickets<br>
            <small flex="" md-truncate="" class="md-truncate flex">Track customer supports.</small>
          </h2>
          <!-- ngIf: !KanbanBoard --><button class="md-icon-button md-button ng-scope md-ink-ripple" type="button" ng-transclude="" ng-if="!KanbanBoard" ng-click="ShowKanban()" aria-label="Show Kanban">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="mdi mdi-view-week text-muted"></i></md-icon>
          </button><!-- end ngIf: !KanbanBoard -->
          <!-- ngIf: KanbanBoard -->
          <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" ng-click="Create()" aria-label="New">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-plus-round text-muted"></i></md-icon>
          </button>
        </div>
      </md-toolbar>
      <!-- ngIf: KanbanBoard -->
      <!-- ngIf: !KanbanBoard --><md-content ng-if="!KanbanBoard" class="md-pt-0 ng-scope _md">
        <ul class="custom-ciuis-list-body" style="padding: 0px;">
          <!-- ngRepeat: ticket in tickets | filter: TicketsFilter | filter:search | filter: ticket.priority | pagination : currentPage*itemsPerPage | limitTo: 5 --><li ng-repeat="ticket in tickets | filter: TicketsFilter | filter:search | filter: ticket.priority | pagination : currentPage*itemsPerPage | limitTo: 5" class="ciuis-custom-list-item ciuis-special-list-item ng-scope">
            <a ng-href="https://demo.ciuis.com/tickets/ticket/42" href="https://demo.ciuis.com/tickets/ticket/42">
              <ul class="list-item-for-custom-list">
                <li class="ciuis-custom-list-item-item col-md-12">
                  <div data-toggle="tooltip" data-placement="bottom" data-container="body" class="assigned-staff-for-this-lead user-avatar">
                    <i class="ion-help-buoy" style="font-size: 32px"></i> 
                  </div>
                  <div class="pull-left col-md-4"> 
                    <strong ng-bind="ticket.subject" class="ng-binding">add sp II page</strong><br>
                    <small ng-bind="ticket.message" class="ng-binding">a</small> 
                  </div>
                  <div class="col-md-8">
                    <div class="col-md-5"> 
                      <span class="date-start-task">
                        <small class="text-muted text-uppercase">Contact</small><br>
                        <strong ng-bind="ticket.contactname" class="ng-binding">Sue Shei</strong> 
                      </span> 
                    </div>
                    <div class="col-md-2"> 
                      <span class="date-start-task">
                        <small class="text-muted text-uppercase">Priority</small><br>
                        <span ng-switch="ticket.priority_id">
                          <!-- ngSwitchWhen: 1 -->
                          <!-- ngSwitchWhen: 2 -->
                          <!-- ngSwitchWhen: 3 --><strong ng-switch-when="3" class="ng-scope">HIGH</strong><!-- end ngSwitchWhen: -->
                        </span>
                      </span>
                    </div>
                    <div class="col-md-2"> 
                      <span class="date-start-task">
                        <small class="text-muted text-uppercase">Status</small><br>
                        <span ng-switch="ticket.status_id">
                          <!-- ngSwitchWhen: 1 --><strong ng-switch-when="1" class="ng-scope">OPEN</strong><!-- end ngSwitchWhen: -->
                          <!-- ngSwitchWhen: 2 -->
                          <!-- ngSwitchWhen: 3 -->
                          <!-- ngSwitchWhen: 4 -->
                        </span>
                      </span></div>
                      <div class="col-md-3"> 
                        <span class="date-start-task">
                          <small class="text-muted text-uppercase">Last Reply</small><br>
                          <strong>
                            <span ng-show="ticket.lastreply == NULL" class="badge ng-hide" aria-hidden="true">N/A</span><span ng-show="ticket.lastreply != NULL" class="badge ng-binding" ng-bind="ticket.lastreply | date : 'MMM d, y h:mm:ss a'" aria-hidden="false">Jan 1, 1970 5:30:00 AM</span> 
                          </strong> 
                        </span> 
                      </div>
                    </div>
                  </li>
                </ul>
            </a>
          </li><!-- end ngRepeat: ticket in tickets | filter: TicketsFilter | filter:search | filter: ticket.priority | pagination : currentPage*itemsPerPage | limitTo: 5 --><li ng-repeat="ticket in tickets | filter: TicketsFilter | filter:search | filter: ticket.priority | pagination : currentPage*itemsPerPage | limitTo: 5" class="ciuis-custom-list-item ciuis-special-list-item ng-scope">
            <a ng-href="https://demo.ciuis.com/tickets/ticket/41" href="https://demo.ciuis.com/tickets/ticket/41">
              <ul class="list-item-for-custom-list">
                <li class="ciuis-custom-list-item-item col-md-12">
                  <div data-toggle="tooltip" data-placement="bottom" data-container="body" class="assigned-staff-for-this-lead user-avatar">
                    <i class="ion-help-buoy" style="font-size: 32px"></i> 
                  </div>
                  <div class="pull-left col-md-4"> 
                    <strong ng-bind="ticket.subject" class="ng-binding">Computer down</strong><br>
                    <small ng-bind="ticket.message" class="ng-binding">may bi hu lam on sua gium so</small> 
                  </div>
                  <div class="col-md-8">
                    <div class="col-md-5"> 
                      <span class="date-start-task">
                        <small class="text-muted text-uppercase">Contact</small><br>
                        <strong ng-bind="ticket.contactname" class="ng-binding">Sue Shei</strong> 
                      </span> 
                    </div>
                    <div class="col-md-2"> 
                      <span class="date-start-task">
                        <small class="text-muted text-uppercase">Priority</small><br>
                        <span ng-switch="ticket.priority_id">
                          <!-- ngSwitchWhen: 1 -->
                          <!-- ngSwitchWhen: 2 -->
                          <!-- ngSwitchWhen: 3 --><strong ng-switch-when="3" class="ng-scope">HIGH</strong><!-- end ngSwitchWhen: -->
                        </span>
                      </span>
                    </div>
                    <div class="col-md-2"> 
                      <span class="date-start-task">
                        <small class="text-muted text-uppercase">Status</small><br>
                        <span ng-switch="ticket.status_id">
                          <!-- ngSwitchWhen: 1 -->
                          <!-- ngSwitchWhen: 2 -->
                          <!-- ngSwitchWhen: 3 --><strong ng-switch-when="3" class="ng-scope">ANSWERED</strong><!-- end ngSwitchWhen: -->
                          <!-- ngSwitchWhen: 4 -->
                        </span>
                      </span></div>
                      <div class="col-md-3"> 
                        <span class="date-start-task">
                          <small class="text-muted text-uppercase">Last Reply</small><br>
                          <strong>
                            <span ng-show="ticket.lastreply == NULL" class="badge ng-hide" aria-hidden="true">N/A</span><span ng-show="ticket.lastreply != NULL" class="badge ng-binding" ng-bind="ticket.lastreply | date : 'MMM d, y h:mm:ss a'" aria-hidden="false">Apr 7, 2019 1:20:58 PM</span> 
                          </strong> 
                        </span> 
                      </div>
                    </div>
                  </li>
                </ul>
            </a>
          </li><!-- end ngRepeat: ticket in tickets | filter: TicketsFilter | filter:search | filter: ticket.priority | pagination : currentPage*itemsPerPage | limitTo: 5 --><li ng-repeat="ticket in tickets | filter: TicketsFilter | filter:search | filter: ticket.priority | pagination : currentPage*itemsPerPage | limitTo: 5" class="ciuis-custom-list-item ciuis-special-list-item ng-scope">
            <a ng-href="https://demo.ciuis.com/tickets/ticket/40" href="https://demo.ciuis.com/tickets/ticket/40">
              <ul class="list-item-for-custom-list">
                <li class="ciuis-custom-list-item-item col-md-12">
                  <div data-toggle="tooltip" data-placement="bottom" data-container="body" class="assigned-staff-for-this-lead user-avatar">
                    <i class="ion-help-buoy" style="font-size: 32px"></i> 
                  </div>
                  <div class="pull-left col-md-4"> 
                    <strong ng-bind="ticket.subject" class="ng-binding">crm reload lenteur</strong><br>
                    <small ng-bind="ticket.message" class="ng-binding">bellahio 3andi mochkel fi katdha dhahd</small> 
                  </div>
                  <div class="col-md-8">
                    <div class="col-md-5"> 
                      <span class="date-start-task">
                        <small class="text-muted text-uppercase">Contact</small><br>
                        <strong ng-bind="ticket.contactname" class="ng-binding">Sue Shei</strong> 
                      </span> 
                    </div>
                    <div class="col-md-2"> 
                      <span class="date-start-task">
                        <small class="text-muted text-uppercase">Priority</small><br>
                        <span ng-switch="ticket.priority_id">
                          <!-- ngSwitchWhen: 1 -->
                          <!-- ngSwitchWhen: 2 -->
                          <!-- ngSwitchWhen: 3 --><strong ng-switch-when="3" class="ng-scope">HIGH</strong><!-- end ngSwitchWhen: -->
                        </span>
                      </span>
                    </div>
                    <div class="col-md-2"> 
                      <span class="date-start-task">
                        <small class="text-muted text-uppercase">Status</small><br>
                        <span ng-switch="ticket.status_id">
                          <!-- ngSwitchWhen: 1 -->
                          <!-- ngSwitchWhen: 2 -->
                          <!-- ngSwitchWhen: 3 --><strong ng-switch-when="3" class="ng-scope">ANSWERED</strong><!-- end ngSwitchWhen: -->
                          <!-- ngSwitchWhen: 4 -->
                        </span>
                      </span></div>
                      <div class="col-md-3"> 
                        <span class="date-start-task">
                          <small class="text-muted text-uppercase">Last Reply</small><br>
                          <strong>
                            <span ng-show="ticket.lastreply == NULL" class="badge ng-hide" aria-hidden="true">N/A</span><span ng-show="ticket.lastreply != NULL" class="badge ng-binding" ng-bind="ticket.lastreply | date : 'MMM d, y h:mm:ss a'" aria-hidden="false">Apr 6, 2019 12:01:31 AM</span> 
                          </strong> 
                        </span> 
                      </div>
                    </div>
                  </li>
                </ul>
            </a>
          </li><!-- end ngRepeat: ticket in tickets | filter: TicketsFilter | filter:search | filter: ticket.priority | pagination : currentPage*itemsPerPage | limitTo: 5 --><li ng-repeat="ticket in tickets | filter: TicketsFilter | filter:search | filter: ticket.priority | pagination : currentPage*itemsPerPage | limitTo: 5" class="ciuis-custom-list-item ciuis-special-list-item ng-scope">
            <a ng-href="https://demo.ciuis.com/tickets/ticket/39" href="https://demo.ciuis.com/tickets/ticket/39">
              <ul class="list-item-for-custom-list">
                <li class="ciuis-custom-list-item-item col-md-12">
                  <div data-toggle="tooltip" data-placement="bottom" data-container="body" class="assigned-staff-for-this-lead user-avatar">
                    <i class="ion-help-buoy" style="font-size: 32px"></i> 
                  </div>
                  <div class="pull-left col-md-4"> 
                    <strong ng-bind="ticket.subject" class="ng-binding">Cpanel can not log in</strong><br>
                    <small ng-bind="ticket.message" class="ng-binding">vcbcbcb</small> 
                  </div>
                  <div class="col-md-8">
                    <div class="col-md-5"> 
                      <span class="date-start-task">
                        <small class="text-muted text-uppercase">Contact</small><br>
                        <strong ng-bind="ticket.contactname" class="ng-binding"> </strong> 
                      </span> 
                    </div>
                    <div class="col-md-2"> 
                      <span class="date-start-task">
                        <small class="text-muted text-uppercase">Priority</small><br>
                        <span ng-switch="ticket.priority_id">
                          <!-- ngSwitchWhen: 1 -->
                          <!-- ngSwitchWhen: 2 --><strong ng-switch-when="2" class="ng-scope">MEDIUM</strong><!-- end ngSwitchWhen: -->
                          <!-- ngSwitchWhen: 3 -->
                        </span>
                      </span>
                    </div>
                    <div class="col-md-2"> 
                      <span class="date-start-task">
                        <small class="text-muted text-uppercase">Status</small><br>
                        <span ng-switch="ticket.status_id">
                          <!-- ngSwitchWhen: 1 --><strong ng-switch-when="1" class="ng-scope">OPEN</strong><!-- end ngSwitchWhen: -->
                          <!-- ngSwitchWhen: 2 -->
                          <!-- ngSwitchWhen: 3 -->
                          <!-- ngSwitchWhen: 4 -->
                        </span>
                      </span></div>
                      <div class="col-md-3"> 
                        <span class="date-start-task">
                          <small class="text-muted text-uppercase">Last Reply</small><br>
                          <strong>
                            <span ng-show="ticket.lastreply == NULL" class="badge ng-hide" aria-hidden="true">N/A</span><span ng-show="ticket.lastreply != NULL" class="badge ng-binding" ng-bind="ticket.lastreply | date : 'MMM d, y h:mm:ss a'" aria-hidden="false">Jan 1, 1970 5:30:00 AM</span> 
                          </strong> 
                        </span> 
                      </div>
                    </div>
                  </li>
                </ul>
            </a>
          </li><!-- end ngRepeat: ticket in tickets | filter: TicketsFilter | filter:search | filter: ticket.priority | pagination : currentPage*itemsPerPage | limitTo: 5 --><li ng-repeat="ticket in tickets | filter: TicketsFilter | filter:search | filter: ticket.priority | pagination : currentPage*itemsPerPage | limitTo: 5" class="ciuis-custom-list-item ciuis-special-list-item ng-scope">
            <a ng-href="https://demo.ciuis.com/tickets/ticket/38" href="https://demo.ciuis.com/tickets/ticket/38">
              <ul class="list-item-for-custom-list">
                <li class="ciuis-custom-list-item-item col-md-12">
                  <div data-toggle="tooltip" data-placement="bottom" data-container="body" class="assigned-staff-for-this-lead user-avatar">
                    <i class="ion-help-buoy" style="font-size: 32px"></i> 
                  </div>
                  <div class="pull-left col-md-4"> 
                    <strong ng-bind="ticket.subject" class="ng-binding">sdsdd</strong><br>
                    <small ng-bind="ticket.message" class="ng-binding">dss</small> 
                  </div>
                  <div class="col-md-8">
                    <div class="col-md-5"> 
                      <span class="date-start-task">
                        <small class="text-muted text-uppercase">Contact</small><br>
                        <strong ng-bind="ticket.contactname" class="ng-binding">test test</strong> 
                      </span> 
                    </div>
                    <div class="col-md-2"> 
                      <span class="date-start-task">
                        <small class="text-muted text-uppercase">Priority</small><br>
                        <span ng-switch="ticket.priority_id">
                          <!-- ngSwitchWhen: 1 -->
                          <!-- ngSwitchWhen: 2 --><strong ng-switch-when="2" class="ng-scope">MEDIUM</strong><!-- end ngSwitchWhen: -->
                          <!-- ngSwitchWhen: 3 -->
                        </span>
                      </span>
                    </div>
                    <div class="col-md-2"> 
                      <span class="date-start-task">
                        <small class="text-muted text-uppercase">Status</small><br>
                        <span ng-switch="ticket.status_id">
                          <!-- ngSwitchWhen: 1 -->
                          <!-- ngSwitchWhen: 2 -->
                          <!-- ngSwitchWhen: 3 --><strong ng-switch-when="3" class="ng-scope">ANSWERED</strong><!-- end ngSwitchWhen: -->
                          <!-- ngSwitchWhen: 4 -->
                        </span>
                      </span></div>
                      <div class="col-md-3"> 
                        <span class="date-start-task">
                          <small class="text-muted text-uppercase">Last Reply</small><br>
                          <strong>
                            <span ng-show="ticket.lastreply == NULL" class="badge ng-hide" aria-hidden="true">N/A</span><span ng-show="ticket.lastreply != NULL" class="badge ng-binding" ng-bind="ticket.lastreply | date : 'MMM d, y h:mm:ss a'" aria-hidden="false">Jan 1, 1970 5:30:00 AM</span> 
                          </strong> 
                        </span> 
                      </div>
                    </div>
                  </li>
                </ul>
            </a>
          </li><!-- end ngRepeat: ticket in tickets | filter: TicketsFilter | filter:search | filter: ticket.priority | pagination : currentPage*itemsPerPage | limitTo: 5 -->
        </ul>
        <div ng-hide="tickets.length < 5" class="pagination-div" aria-hidden="false">
          <ul class="pagination">
            <li ng-class="DisablePrevPage()" class="disabled"> <a href="" ng-click="prevPage()"><i class="ion-ios-arrow-back"></i></a> </li>
            <!-- ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope active" role="button" tabindex="0"> <a href="#" ng-bind="n+1" class="ng-binding">1</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0"> <a href="#" ng-bind="n+1" class="ng-binding">2</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0"> <a href="#" ng-bind="n+1" class="ng-binding">3</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0"> <a href="#" ng-bind="n+1" class="ng-binding">4</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0"> <a href="#" ng-bind="n+1" class="ng-binding">5</a> </li><!-- end ngRepeat: n in range() -->
            <li ng-class="DisableNextPage()"> <a href="" ng-click="nextPage()"><i class="ion-ios-arrow-right"></i></a> </li>
          </ul>
        </div>
        <md-content ng-show="!tickets.length" class="md-padding no-item-data _md ng-hide" aria-hidden="true">No matching items found</md-content>
      </md-content><!-- end ngIf: !KanbanBoard -->
    </div>
  </div>
 @endsection