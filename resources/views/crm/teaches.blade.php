@extends('crm.default')
@section('title','Admin Dashboard')
@section('content')
 <div class="main-content container-fluid col-xs-12 col-md-3 col-lg-3">
    <div class="panel-heading"> <strong>Task Situation</strong> <span class="panel-subtitle">Tasks Situation by status</span> </div>
    <div class="row" style="padding: 0px 20px 0px 20px;">
      <div class="col-md-6 col-xs-6 border-right text-uppercase">
        <div class="tasks-status-stat">
          <h3 class="text-bold ciuis-task-stat-title"> <span class="task-stat-number ng-binding" ng-bind="(tasks | filter:{status_id:'1'}).length">21</span> <span class="task-stat-all ng-binding" ng-bind="'/'+' '+tasks.length+' '+'Task'">/ 50 Task</span> </h3>
          <span class="ciuis-task-percent-bg"> <span class="ciuis-task-percent-fg" style="width: 42%;"></span> </span> </div>
        <span style="color:#989898">OPEN</span> </div>
      <div class="col-md-6 col-xs-6 border-right text-uppercase">
        <div class="tasks-status-stat">
          <h3 class="text-bold ciuis-task-stat-title"> <span class="task-stat-number ng-binding" ng-bind="(tasks | filter:{status_id:'2'}).length">0</span> <span class="task-stat-all ng-binding" ng-bind="'/'+' '+tasks.length+' '+'Task'">/ 50 Task</span> </h3>
          <span class="ciuis-task-percent-bg"> <span class="ciuis-task-percent-fg" style="width: 0%;"></span> </span> </div>
        <span style="color:#989898">IN PROGRESS</span> </div>
      <div class="col-md-6 col-xs-6 border-right text-uppercase">
        <div class="tasks-status-stat">
          <h3 class="text-bold ciuis-task-stat-title"> <span class="task-stat-number ng-binding" ng-bind="(tasks | filter:{status_id:'3'}).length">0</span> <span class="task-stat-all ng-binding" ng-bind="'/'+' '+tasks.length+' '+'Task'">/ 50 Task</span> </h3>
          <span class="ciuis-task-percent-bg"> <span class="ciuis-task-percent-fg" style="width: 0%;"></span> </span> </div>
        <span style="color:#989898">Waiting</span> </div>
      <div class="col-md-6 col-xs-6 border-right text-uppercase">
        <div class="tasks-status-stat">
          <h3 class="text-bold ciuis-task-stat-title"> <span class="task-stat-number ng-binding" ng-bind="(tasks | filter:{status_id:'4'}).length">28</span> <span class="task-stat-all ng-binding" ng-bind="'/'+' '+tasks.length+' '+'Task'">/ 50 Task</span> </h3>
          <span class="ciuis-task-percent-bg"> <span class="ciuis-task-percent-fg" style="width: 56%;"></span> </span> </div>
        <span style="color:#989898">Complete</span> </div>
    </div>
  </div>


  <div class="main-content container-fluid col-xs-12 col-md-9 col-lg-9 md-p-0">
    <md-toolbar class="toolbar-trans _md _md-toolbar-transitions">
      <div class="md-toolbar-tools">
        <h2 flex="" md-truncate="" class="text-bold md-truncate flex">Tasks<br>
          <small flex="" md-truncate="" class="md-truncate flex">Organize your tasks</small></h2>
        <div class="ciuis-external-search-in-table">
          <input ng-model="search.name" class="search-table-external ng-pristine ng-untouched ng-valid ng-empty" id="search" name="search" type="text" placeholder="What're you looking for ?" aria-invalid="false">
          <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" aria-label="Search">
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-search text-muted"></i></md-icon>
          </button>
        </div>
        <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" ng-click="toggleFilter()" aria-label="Filter">
          <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-android-funnel text-muted"></i></md-icon>
        </button>
        <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" ng-click="Create()" aria-label="New">
          <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-plus-round text-muted"></i></md-icon>
        </button>
      </div>
    </md-toolbar>
    <md-content layout-padding="" class="layout-padding _md">
      <div layout-padding="" class="layout-padding">
        <ul class="custom-ciuis-list-body" style="padding: 0px;">
          <!-- ngRepeat: task in tasks | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5 --><li ng-repeat="task in tasks | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5" class="milestone-todos-list ciuis-custom-list-item ciuis-special-list-item paginationclass ng-scope" style="">
            <ul class="all-milestone-todos">
              <li class="milestone-todos-list-item col-md-12  "> <span class="pull-left col-md-5"><a href="https://demo.ciuis.com/tasks/task/54"><strong ng-bind="task.name" class="ng-binding"></strong></a><br>
                <small ng-bind="task.relationtype" class="ng-binding">Project</small></span>
                <div class="col-md-7">
                  <div class="col-md-3"><span class="date-start-task"><small class="text-muted text-uppercase">Start Date <i class="ion-ios-stopwatch-outline"></i></small><br>
                    <strong ng-bind="task.startdate" class="ng-binding">08/04/19</strong></span> </div>
                  <div class="col-md-3"><span class="date-start-task"><small class="text-muted text-uppercase">Due Date <i class="ion-ios-timer-outline"></i></small><br>
                    <strong ng-bind="task.duedate" class="ng-binding">08/04/19</strong></span> </div>
                  <div class="col-md-3"><span class="date-start-task"><small class="text-muted text-uppercase">Status <i class="ion-ios-circle-outline"></i></small><br>
                    <strong ng-bind="task.status" class="ng-binding">OPEN</strong></span> </div>
                  <div class="col-md-3 text-right"> <a ng-href="https://demo.ciuis.com/tasks/task/54" class="edit-task pull-right" href="https://demo.ciuis.com/tasks/task/54"><i class="ion-compose"></i></a> </div>
                </div>
              </li>
            </ul>
          </li><!-- end ngRepeat: task in tasks | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5 --><li ng-repeat="task in tasks | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5" class="milestone-todos-list ciuis-custom-list-item ciuis-special-list-item paginationclass ng-scope">
            <ul class="all-milestone-todos">
              <li class="milestone-todos-list-item col-md-12  "> <span class="pull-left col-md-5"><a href="https://demo.ciuis.com/tasks/task/53"><strong ng-bind="task.name" class="ng-binding">aa</strong></a><br>
                <small ng-bind="task.relationtype" class="ng-binding">Project</small></span>
                <div class="col-md-7">
                  <div class="col-md-3"><span class="date-start-task"><small class="text-muted text-uppercase">Start Date <i class="ion-ios-stopwatch-outline"></i></small><br>
                    <strong ng-bind="task.startdate" class="ng-binding">11/04/19</strong></span> </div>
                  <div class="col-md-3"><span class="date-start-task"><small class="text-muted text-uppercase">Due Date <i class="ion-ios-timer-outline"></i></small><br>
                    <strong ng-bind="task.duedate" class="ng-binding">25/04/19</strong></span> </div>
                  <div class="col-md-3"><span class="date-start-task"><small class="text-muted text-uppercase">Status <i class="ion-ios-circle-outline"></i></small><br>
                    <strong ng-bind="task.status" class="ng-binding">OPEN</strong></span> </div>
                  <div class="col-md-3 text-right"> <a ng-href="https://demo.ciuis.com/tasks/task/53" class="edit-task pull-right" href="https://demo.ciuis.com/tasks/task/53"><i class="ion-compose"></i></a> </div>
                </div>
              </li>
            </ul>
          </li><!-- end ngRepeat: task in tasks | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5 --><li ng-repeat="task in tasks | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5" class="milestone-todos-list ciuis-custom-list-item ciuis-special-list-item paginationclass ng-scope">
            <ul class="all-milestone-todos">
              <li class="milestone-todos-list-item col-md-12  done"> <span class="pull-left col-md-5"><a href="https://demo.ciuis.com/tasks/task/52"><strong ng-bind="task.name" class="ng-binding">Setup</strong></a><br>
                <small ng-bind="task.relationtype" class="ng-binding">Project</small></span>
                <div class="col-md-7">
                  <div class="col-md-3"><span class="date-start-task"><small class="text-muted text-uppercase">Start Date <i class="ion-ios-stopwatch-outline"></i></small><br>
                    <strong ng-bind="task.startdate" class="ng-binding">25/04/19</strong></span> </div>
                  <div class="col-md-3"><span class="date-start-task"><small class="text-muted text-uppercase">Due Date <i class="ion-ios-timer-outline"></i></small><br>
                    <strong ng-bind="task.duedate" class="ng-binding">27/04/19</strong></span> </div>
                  <div class="col-md-3"><span class="date-start-task"><small class="text-muted text-uppercase">Status <i class="ion-ios-circle-outline"></i></small><br>
                    <strong ng-bind="task.status" class="ng-binding">Complete</strong></span> </div>
                  <div class="col-md-3 text-right"> <a ng-href="https://demo.ciuis.com/tasks/task/52" class="edit-task pull-right" href="https://demo.ciuis.com/tasks/task/52"><i class="ion-compose"></i></a> </div>
                </div>
              </li>
            </ul>
          </li><!-- end ngRepeat: task in tasks | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5 --><li ng-repeat="task in tasks | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5" class="milestone-todos-list ciuis-custom-list-item ciuis-special-list-item paginationclass ng-scope">
            <ul class="all-milestone-todos">
              <li class="milestone-todos-list-item col-md-12  "> <span class="pull-left col-md-5"><a href="https://demo.ciuis.com/tasks/task/51"><strong ng-bind="task.name" class="ng-binding">Tak1</strong></a><br>
                <small ng-bind="task.relationtype" class="ng-binding">Project</small></span>
                <div class="col-md-7">
                  <div class="col-md-3"><span class="date-start-task"><small class="text-muted text-uppercase">Start Date <i class="ion-ios-stopwatch-outline"></i></small><br>
                    <strong ng-bind="task.startdate" class="ng-binding">03/04/19</strong></span> </div>
                  <div class="col-md-3"><span class="date-start-task"><small class="text-muted text-uppercase">Due Date <i class="ion-ios-timer-outline"></i></small><br>
                    <strong ng-bind="task.duedate" class="ng-binding">03/04/19</strong></span> </div>
                  <div class="col-md-3"><span class="date-start-task"><small class="text-muted text-uppercase">Status <i class="ion-ios-circle-outline"></i></small><br>
                    <strong ng-bind="task.status" class="ng-binding">OPEN</strong></span> </div>
                  <div class="col-md-3 text-right"> <a ng-href="https://demo.ciuis.com/tasks/task/51" class="edit-task pull-right" href="https://demo.ciuis.com/tasks/task/51"><i class="ion-compose"></i></a> </div>
                </div>
              </li>
            </ul>
          </li><!-- end ngRepeat: task in tasks | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5 --><li ng-repeat="task in tasks | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5" class="milestone-todos-list ciuis-custom-list-item ciuis-special-list-item paginationclass ng-scope">
            <ul class="all-milestone-todos">
              <li class="milestone-todos-list-item col-md-12  "> <span class="pull-left col-md-5"><a href="https://demo.ciuis.com/tasks/task/50"><strong ng-bind="task.name" class="ng-binding">TASlds</strong></a><br>
                <small ng-bind="task.relationtype" class="ng-binding">Project</small></span>
                <div class="col-md-7">
                  <div class="col-md-3"><span class="date-start-task"><small class="text-muted text-uppercase">Start Date <i class="ion-ios-stopwatch-outline"></i></small><br>
                    <strong ng-bind="task.startdate" class="ng-binding">03/04/19</strong></span> </div>
                  <div class="col-md-3"><span class="date-start-task"><small class="text-muted text-uppercase">Due Date <i class="ion-ios-timer-outline"></i></small><br>
                    <strong ng-bind="task.duedate" class="ng-binding">03/04/19</strong></span> </div>
                  <div class="col-md-3"><span class="date-start-task"><small class="text-muted text-uppercase">Status <i class="ion-ios-circle-outline"></i></small><br>
                    <strong ng-bind="task.status" class="ng-binding">OPEN</strong></span> </div>
                  <div class="col-md-3 text-right"> <a ng-href="https://demo.ciuis.com/tasks/task/50" class="edit-task pull-right" href="https://demo.ciuis.com/tasks/task/50"><i class="ion-compose"></i></a> </div>
                </div>
              </li>
            </ul>
          </li><!-- end ngRepeat: task in tasks | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5 -->
        </ul>
        <md-content ng-show="!tasks.length" class="md-padding no-item-data _md ng-hide" aria-hidden="true" style="">No matching items found</md-content>
        <div class="pagination-div">
          <ul class="pagination">
            <li ng-class="DisablePrevPage()" class="disabled" style=""> <a href="" ng-click="prevPage()"><i class="ion-ios-arrow-back"></i></a> </li>
            <!-- ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope active" role="button" tabindex="0" style=""> <a href="#" ng-bind="n+1" class="ng-binding">1</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0" style=""> <a href="#" ng-bind="n+1" class="ng-binding">2</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0"> <a href="#" ng-bind="n+1" class="ng-binding">3</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0"> <a href="#" ng-bind="n+1" class="ng-binding">4</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0"> <a href="#" ng-bind="n+1" class="ng-binding">5</a> </li><!-- end ngRepeat: n in range() -->
            <li ng-class="DisableNextPage()"> <a href="" ng-click="nextPage()"><i class="ion-ios-arrow-right"></i></a> </li>
          </ul>
        </div>
      </div>
    </md-content>
  </div>
 @endsection