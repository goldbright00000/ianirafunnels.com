@extends('crm.default')
@section('title','Invoices')
@section('content')
<div class="main-content container-fluid col-xs-12 col-md-12 col-lg-12">
    <div class="col-md-9" style="padding: 0px">
      <div class="panel-default">
        <div class="ciuis-invoice-summary"><div><div class="row"><div class="col-md-12"><div style="border-top-left-radius: 10px;" class="ciuis-right-border-b1 ciuis-invoice-summaries-b1"><div class="box-header text-uppercase text-bold">Total Expenses</div><div class="box-content"><div class="sentTotal">65</div></div><div class="box-foot"><div class="sendTime box-foot-left">Amount<br><span class="box-foot-stats"><strong>USD 4,078,009.01</strong></span></div></div></div><div class="ciuis-right-border-b1 ciuis-invoice-summaries-b1"><div class="box-header text-uppercase text-bold">Billed</div><div class="box-content invoice-percent" style="width: 130px; height: 130px;"><div class="percentage">%5</div><canvas id="0" width="130" height="130"></canvas></div><div class="box-foot"><span class="arrow arrow-up"></span><div class="box-foot-left">Amount<br><span class="box-foot-stats"><strong>USD 139.25</strong></span></div><span class="arrow arrow-down"></span><div class="box-foot-right"><br><span class="box-foot-stats" "=""><strong>3</strong> (%5)</span></div></div></div><div class="ciuis-right-border-b1 ciuis-invoice-summaries-b1"><div class="box-header text-uppercase text-bold">Not Billed</div><div class="box-content invoice-percent-2" style="width: 130px; height: 130px;"><div class="percentage">%78</div><canvas id="1" width="130" height="130"></canvas></div><div class="box-foot"><span class="arrow arrow-up"></span><div class="box-foot-left">Amount<br><span class="box-foot-stats"><strong>USD 4,038,805.00</strong></span></div><span class="arrow arrow-down"></span><div class="box-foot-right"><br><span class="box-foot-stats"><strong>51</strong> (%78)</span></div></div></div><div style="border-top-right-radius: 10px;" class="ciuis-invoice-summaries-b1"><div class="box-header text-uppercase text-bold">Internal</div><div class="box-content invoice-percent-3" style="width: 130px; height: 130px;"><div class="percentage">%17</div><canvas id="2" width="130" height="130"></canvas></div><div class="box-foot"><span class="arrow arrow-up"></span><div class="box-foot-left">Amount<br><span class="box-foot-stats"><strong>USD 39,064.76</strong></span></div><div class="box-foot-right"><br><span class="box-foot-stats"><strong>11</strong> (%17)</span></div></div></div></div></div></div></div>
      </div>
      <md-toolbar class="toolbar-white _md _md-toolbar-transitions">
        <div class="md-toolbar-tools">
          <h2 flex="" md-truncate="" class="text-bold md-truncate flex">Expenses<br>
            <small flex="" md-truncate="" class="md-truncate flex">Organize your expenses</small></h2>
          <div class="ciuis-external-search-in-table">
            <input ng-model="search.title" class="search-table-external ng-pristine ng-untouched ng-valid ng-empty" id="search" name="search" type="text" placeholder="What're you looking for ?" aria-invalid="false">
            <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" aria-label="Search">
              <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-search text-muted"></i></md-icon>
            </button>
          </div>
          <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" ng-click="toggleFilter()" aria-label="Filter">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-android-funnel text-muted"></i></md-icon>
          </button>
                      <a class="md-icon-button md-button md-ink-ripple" ng-transclude="" ng-href="https://demo.ciuis.com/expenses/create" aria-label="New" href="https://demo.ciuis.com/expenses/create">
              
              <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-plus-round text-muted"></i></md-icon>
            </a>
                  </div>
      </md-toolbar>
      <md-content style="padding-top: 0px;" class="_md">
        <div ng-show="expensesLoader" layout-align="center center" class="text-center layout-align-center-center ng-hide" id="circular_loader" aria-hidden="true" style="">
          <md-progress-circular md-mode="indeterminate" md-diameter="40" aria-valuemin="0" aria-valuemax="100" role="progressbar" class="ng-isolate-scope md-mode-indeterminate" style="width: 40px; height: 40px;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" style="width: 40px; height: 40px; transform-origin: 20px 20px 20px;"><path fill="none" stroke-width="4" stroke-linecap="square" d="M20,2A18,18 0 1 1 2,20" stroke-dasharray="84.82300164692441" stroke-dashoffset="187.6350519906457" transform="rotate(-180 20 20)"></path></svg></md-progress-circular>
          <p style="font-size: 15px;margin-bottom: 5%;">
            <span>Please wait <br>
            <small><strong>Loading Expenses...</strong></small></span>
          </p>
        </div>
        <ul ng-show="!expensesLoader" class="custom-ciuis-list-body" style="padding: 0px;" aria-hidden="false">
          <!-- ngRepeat: expense in expenses | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5 --><li ng-repeat="expense in expenses | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5" i="" class="ciuis-custom-list-item ciuis-special-list-item lead-name ng-scope" style="">
            <a class="ciuis_expense_receipt_number" href="https://demo.ciuis.com/expenses/receipt/81">
              <ul class="list-item-for-custom-list">
                <li class="ciuis-custom-list-item-item col-md-12">
                  <div data-toggle="tooltip" data-placement="bottom" data-container="body" title="" data-original-title="Added by Lance Bogrol" class="assigned-staff-for-this-lead user-avatar"><i class="ion-document" style="font-size: 32px"></i></div>
                  <div class="pull-left col-md-4"> <strong ng-bind="expense.prefix + '' + expense.longid" class="ng-binding">EXP-000081</strong> 
                    <span class="label label-warning" ng-bind="expense.billstatus">Not Billed</span> <br>
                    <small ng-bind="expense.title" class="ng-binding">ewqewr</small>
                  </div>
                  <div class="col-md-8">
                    <div class="col-md-5"> <span class="date-start-task"><small class="text-muted text-uppercase">Amount</small><br>
                      <strong ng-bind-html="expense.amount | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$1,000.00</span></strong></span> 
                    </div>
                    <div class="col-md-4"> <span class="date-start-task"><small class="text-muted text-uppercase">Category</small><br>
                      <strong ng-bind="expense.category" class="ng-binding">xxxxxx</strong> 
                    </span></div>
                    <div class="col-md-3"> <span class="date-start-task"><small class="text-muted text-uppercase">Date</small><br>
                      <strong><span class="badge ng-binding" ng-bind="expense.date">07/04/19</span></strong></span> 
                    </div>
                  </div>
                </li>
              </ul>
            </a>
          </li><!-- end ngRepeat: expense in expenses | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5 --><li ng-repeat="expense in expenses | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5" i="" class="ciuis-custom-list-item ciuis-special-list-item lead-name ng-scope">
            <a class="ciuis_expense_receipt_number" href="https://demo.ciuis.com/expenses/receipt/80">
              <ul class="list-item-for-custom-list">
                <li class="ciuis-custom-list-item-item col-md-12">
                  <div data-toggle="tooltip" data-placement="bottom" data-container="body" title="" data-original-title="Added by gh" class="assigned-staff-for-this-lead user-avatar"><i class="ion-document" style="font-size: 32px"></i></div>
                  <div class="pull-left col-md-4"> <strong ng-bind="expense.prefix + '' + expense.longid" class="ng-binding">EXP-000080</strong> 
                    <span class="label label-success" ng-bind="expense.billstatus">Internal</span> <br>
                    <small ng-bind="expense.title" class="ng-binding">Digital Test</small>
                  </div>
                  <div class="col-md-8">
                    <div class="col-md-5"> <span class="date-start-task"><small class="text-muted text-uppercase">Amount</small><br>
                      <strong ng-bind-html="expense.amount | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$230.00</span></strong></span> 
                    </div>
                    <div class="col-md-4"> <span class="date-start-task"><small class="text-muted text-uppercase">Category</small><br>
                      <strong ng-bind="expense.category" class="ng-binding">Casa</strong> 
                    </span></div>
                    <div class="col-md-3"> <span class="date-start-task"><small class="text-muted text-uppercase">Date</small><br>
                      <strong><span class="badge ng-binding" ng-bind="expense.date">17/04/19</span></strong></span> 
                    </div>
                  </div>
                </li>
              </ul>
            </a>
          </li><!-- end ngRepeat: expense in expenses | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5 --><li ng-repeat="expense in expenses | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5" i="" class="ciuis-custom-list-item ciuis-special-list-item lead-name ng-scope">
            <a class="ciuis_expense_receipt_number" href="https://demo.ciuis.com/expenses/receipt/79">
              <ul class="list-item-for-custom-list">
                <li class="ciuis-custom-list-item-item col-md-12">
                  <div data-toggle="tooltip" data-placement="bottom" data-container="body" title="" data-original-title="Added by Emma Durst" class="assigned-staff-for-this-lead user-avatar"><i class="ion-document" style="font-size: 32px"></i></div>
                  <div class="pull-left col-md-4"> <strong ng-bind="expense.prefix + '' + expense.longid" class="ng-binding">EXP-000079</strong> 
                    <span class="label label-success" ng-bind="expense.billstatus">Internal</span> <br>
                    <small ng-bind="expense.title" class="ng-binding">Utilizing Right Data for Retail</small>
                  </div>
                  <div class="col-md-8">
                    <div class="col-md-5"> <span class="date-start-task"><small class="text-muted text-uppercase">Amount</small><br>
                      <strong ng-bind-html="expense.amount | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$275.00</span></strong></span> 
                    </div>
                    <div class="col-md-4"> <span class="date-start-task"><small class="text-muted text-uppercase">Category</small><br>
                      <strong ng-bind="expense.category" class="ng-binding">Casa</strong> 
                    </span></div>
                    <div class="col-md-3"> <span class="date-start-task"><small class="text-muted text-uppercase">Date</small><br>
                      <strong><span class="badge ng-binding" ng-bind="expense.date">27/03/19</span></strong></span> 
                    </div>
                  </div>
                </li>
              </ul>
            </a>
          </li><!-- end ngRepeat: expense in expenses | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5 --><li ng-repeat="expense in expenses | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5" i="" class="ciuis-custom-list-item ciuis-special-list-item lead-name ng-scope">
            <a class="ciuis_expense_receipt_number" href="https://demo.ciuis.com/expenses/receipt/78">
              <ul class="list-item-for-custom-list">
                <li class="ciuis-custom-list-item-item col-md-12">
                  <div data-toggle="tooltip" data-placement="bottom" data-container="body" title="" data-original-title="Added by Lance Bogrol" class="assigned-staff-for-this-lead user-avatar"><i class="ion-document" style="font-size: 32px"></i></div>
                  <div class="pull-left col-md-4"> <strong ng-bind="expense.prefix + '' + expense.longid" class="ng-binding">EXP-000078</strong> 
                    <span class="label label-warning" ng-bind="expense.billstatus">Not Billed</span> <br>
                    <small ng-bind="expense.title" class="ng-binding">Menu Printing</small>
                  </div>
                  <div class="col-md-8">
                    <div class="col-md-5"> <span class="date-start-task"><small class="text-muted text-uppercase">Amount</small><br>
                      <strong ng-bind-html="expense.amount | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$8,524.00</span></strong></span> 
                    </div>
                    <div class="col-md-4"> <span class="date-start-task"><small class="text-muted text-uppercase">Category</small><br>
                      <strong ng-bind="expense.category" class="ng-binding">Offset Printing</strong> 
                    </span></div>
                    <div class="col-md-3"> <span class="date-start-task"><small class="text-muted text-uppercase">Date</small><br>
                      <strong><span class="badge ng-binding" ng-bind="expense.date">29/03/19</span></strong></span> 
                    </div>
                  </div>
                </li>
              </ul>
            </a>
          </li><!-- end ngRepeat: expense in expenses | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5 --><li ng-repeat="expense in expenses | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5" i="" class="ciuis-custom-list-item ciuis-special-list-item lead-name ng-scope">
            <a class="ciuis_expense_receipt_number" href="https://demo.ciuis.com/expenses/receipt/77">
              <ul class="list-item-for-custom-list">
                <li class="ciuis-custom-list-item-item col-md-12">
                  <div data-toggle="tooltip" data-placement="bottom" data-container="body" title="" data-original-title="Added by Deo" class="assigned-staff-for-this-lead user-avatar"><i class="ion-document" style="font-size: 32px"></i></div>
                  <div class="pull-left col-md-4"> <strong ng-bind="expense.prefix + '' + expense.longid" class="ng-binding">EXP-000077</strong> 
                    <span class="label label-success" ng-bind="expense.billstatus">Internal</span> <br>
                    <small ng-bind="expense.title" class="ng-binding">gasoslina</small>
                  </div>
                  <div class="col-md-8">
                    <div class="col-md-5"> <span class="date-start-task"><small class="text-muted text-uppercase">Amount</small><br>
                      <strong ng-bind-html="expense.amount | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$30,000.00</span></strong></span> 
                    </div>
                    <div class="col-md-4"> <span class="date-start-task"><small class="text-muted text-uppercase">Category</small><br>
                      <strong ng-bind="expense.category" class="ng-binding"></strong> 
                    </span></div>
                    <div class="col-md-3"> <span class="date-start-task"><small class="text-muted text-uppercase">Date</small><br>
                      <strong><span class="badge ng-binding" ng-bind="expense.date">27/03/19</span></strong></span> 
                    </div>
                  </div>
                </li>
              </ul>
            </a>
          </li><!-- end ngRepeat: expense in expenses | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5 -->
        </ul>
        <div class="pagination-div" ng-show="!expensesLoader" aria-hidden="false" style="">
          <ul class="pagination">
            <li ng-class="DisablePrevPage()" class="disabled" style=""> <a href="" ng-click="prevPage()"><i class="ion-ios-arrow-back"></i></a> </li>
            <!-- ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope active" role="button" tabindex="0" style=""> <a href="#" ng-bind="n+1" class="ng-binding">1</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0" style=""> <a href="#" ng-bind="n+1" class="ng-binding">2</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0"> <a href="#" ng-bind="n+1" class="ng-binding">3</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0"> <a href="#" ng-bind="n+1" class="ng-binding">4</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0"> <a href="#" ng-bind="n+1" class="ng-binding">5</a> </li><!-- end ngRepeat: n in range() -->
            <li ng-class="DisableNextPage()"> <a href="" ng-click="nextPage()"><i class="ion-ios-arrow-right"></i></a> </li>
          </ul>
        </div>
        <md-content ng-show="!expenses.length &amp;&amp; !expensesLoader" class="md-padding no-item-data _md ng-hide" aria-hidden="true" style="">No matching items found</md-content>
      </md-content>
    </div>
    <div class="col-md-3" style="padding: 0px;padding-left: 10px;">
      <md-toolbar class="toolbar-white _md _md-toolbar-transitions">
        <div class="md-toolbar-tools">
          <h2 flex="" md-truncate="" class="text-bold md-truncate flex">CATEGORIES<br>
            <small flex="" md-truncate="" class="md-truncate flex">Manage expenses categories</small></h2>
                        <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" ng-click="NewCategory()" aria-label="New Category">
              <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-plus-round text-muted"></i></md-icon>
            </button>
                    </div>
      </md-toolbar>
      <div ng-show="expensesCatLoader" layout-align="center center" class="text-center layout-align-center-center ng-hide" id="circular_loader" aria-hidden="true" style="">
        <md-progress-circular md-mode="indeterminate" md-diameter="25" aria-valuemin="0" aria-valuemax="100" role="progressbar" class="ng-isolate-scope md-mode-indeterminate" style="width: 25px; height: 25px;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25 25" style="width: 25px; height: 25px; transform-origin: 12.5px 12.5px 12.5px;"><path fill="none" stroke-width="2.5" stroke-linecap="square" d="M12.5,1.25A11.25,11.25 0 1 1 1.25,12.5" stroke-dasharray="53.014376029327764" stroke-dashoffset="125.41168871279841" transform="rotate(-180 12.5 12.5)"></path></svg></md-progress-circular>
        <p style="font-size: 15px;margin-bottom: 5%;">
          <span>Please wait <br>
          <small><strong>Loading CATEGORIES...</strong></small></span>
        </p>
      </div>
      <!-- ngRepeat: category in categories --><md-content ng-show="!expensesCatLoader" ng-repeat="category in categories" class="ng-scope _md" aria-hidden="false" style="">
        <div class="widget widget-stats widget-expenses  red-bg margin-top-0">
                    <button class="md-icon-button pull-right md-button md-ink-ripple" type="button" ng-transclude="" ng-click="Remove($index)" aria-label="Remove">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-trash-b text-muted"></i></md-icon>
          </button>
          <button class="md-icon-button pull-right md-button md-ink-ripple" type="button" ng-transclude="" ng-click="UpdateCategory($index)" aria-label="Update">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-gear-a text-muted"></i></md-icon>
          </button>
                    <div class="stats-title text-uppercase ng-binding" ng-bind="category.name">ergetert</div>
          <div class="stats-number"><span ng-bind-html="category.amountby | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$0.00</span></span></div>
          <div class="stats-progress progress">
            <div style="width: 0%;" class="progress-bar"></div>
          </div>
          <div class="stats-desc">Category Percent (<span ng-bind="category.percent+'%'" class="ng-binding">0%</span>)</div>
        </div>
      </md-content><!-- end ngRepeat: category in categories --><md-content ng-show="!expensesCatLoader" ng-repeat="category in categories" class="ng-scope _md" aria-hidden="false">
        <div class="widget widget-stats widget-expenses  red-bg margin-top-0">
                    <button class="md-icon-button pull-right md-button md-ink-ripple" type="button" ng-transclude="" ng-click="Remove($index)" aria-label="Remove">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-trash-b text-muted"></i></md-icon>
          </button>
          <button class="md-icon-button pull-right md-button md-ink-ripple" type="button" ng-transclude="" ng-click="UpdateCategory($index)" aria-label="Update">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-gear-a text-muted"></i></md-icon>
          </button>
                    <div class="stats-title text-uppercase ng-binding" ng-bind="category.name">xxxxxx</div>
          <div class="stats-number"><span ng-bind-html="category.amountby | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$1,000.00</span></span></div>
          <div class="stats-progress progress">
            <div style="width: 2%;" class="progress-bar"></div>
          </div>
          <div class="stats-desc">Category Percent (<span ng-bind="category.percent+'%'" class="ng-binding">2%</span>)</div>
        </div>
      </md-content><!-- end ngRepeat: category in categories --><md-content ng-show="!expensesCatLoader" ng-repeat="category in categories" class="ng-scope _md" aria-hidden="false">
        <div class="widget widget-stats widget-expenses  red-bg margin-top-0">
                    <button class="md-icon-button pull-right md-button md-ink-ripple" type="button" ng-transclude="" ng-click="Remove($index)" aria-label="Remove">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-trash-b text-muted"></i></md-icon>
          </button>
          <button class="md-icon-button pull-right md-button md-ink-ripple" type="button" ng-transclude="" ng-click="UpdateCategory($index)" aria-label="Update">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-gear-a text-muted"></i></md-icon>
          </button>
                    <div class="stats-title text-uppercase ng-binding" ng-bind="category.name">aasada</div>
          <div class="stats-number"><span ng-bind-html="category.amountby | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$0.00</span></span></div>
          <div class="stats-progress progress">
            <div style="width: 0%;" class="progress-bar"></div>
          </div>
          <div class="stats-desc">Category Percent (<span ng-bind="category.percent+'%'" class="ng-binding">0%</span>)</div>
        </div>
      </md-content><!-- end ngRepeat: category in categories --><md-content ng-show="!expensesCatLoader" ng-repeat="category in categories" class="ng-scope _md" aria-hidden="false">
        <div class="widget widget-stats widget-expenses  red-bg margin-top-0">
                    <button class="md-icon-button pull-right md-button md-ink-ripple" type="button" ng-transclude="" ng-click="Remove($index)" aria-label="Remove">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-trash-b text-muted"></i></md-icon>
          </button>
          <button class="md-icon-button pull-right md-button md-ink-ripple" type="button" ng-transclude="" ng-click="UpdateCategory($index)" aria-label="Update">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-gear-a text-muted"></i></md-icon>
          </button>
                    <div class="stats-title text-uppercase ng-binding" ng-bind="category.name">Offset Printing</div>
          <div class="stats-number"><span ng-bind-html="category.amountby | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$8,524.00</span></span></div>
          <div class="stats-progress progress">
            <div style="width: 2%;" class="progress-bar"></div>
          </div>
          <div class="stats-desc">Category Percent (<span ng-bind="category.percent+'%'" class="ng-binding">2%</span>)</div>
        </div>
      </md-content><!-- end ngRepeat: category in categories --><md-content ng-show="!expensesCatLoader" ng-repeat="category in categories" class="ng-scope _md" aria-hidden="false">
        <div class="widget widget-stats widget-expenses  red-bg margin-top-0">
                    <button class="md-icon-button pull-right md-button md-ink-ripple" type="button" ng-transclude="" ng-click="Remove($index)" aria-label="Remove">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-trash-b text-muted"></i></md-icon>
          </button>
          <button class="md-icon-button pull-right md-button md-ink-ripple" type="button" ng-transclude="" ng-click="UpdateCategory($index)" aria-label="Update">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-gear-a text-muted"></i></md-icon>
          </button>
                    <div class="stats-title text-uppercase ng-binding" ng-bind="category.name">Casa</div>
          <div class="stats-number"><span ng-bind-html="category.amountby | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$605.00</span></span></div>
          <div class="stats-progress progress">
            <div style="width: 5%;" class="progress-bar"></div>
          </div>
          <div class="stats-desc">Category Percent (<span ng-bind="category.percent+'%'" class="ng-binding">5%</span>)</div>
        </div>
      </md-content><!-- end ngRepeat: category in categories --><md-content ng-show="!expensesCatLoader" ng-repeat="category in categories" class="ng-scope _md" aria-hidden="false">
        <div class="widget widget-stats widget-expenses  red-bg margin-top-0">
                    <button class="md-icon-button pull-right md-button md-ink-ripple" type="button" ng-transclude="" ng-click="Remove($index)" aria-label="Remove">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-trash-b text-muted"></i></md-icon>
          </button>
          <button class="md-icon-button pull-right md-button md-ink-ripple" type="button" ng-transclude="" ng-click="UpdateCategory($index)" aria-label="Update">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-gear-a text-muted"></i></md-icon>
          </button>
                    <div class="stats-title text-uppercase ng-binding" ng-bind="category.name">otra</div>
          <div class="stats-number"><span ng-bind-html="category.amountby | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$0.00</span></span></div>
          <div class="stats-progress progress">
            <div style="width: 0%;" class="progress-bar"></div>
          </div>
          <div class="stats-desc">Category Percent (<span ng-bind="category.percent+'%'" class="ng-binding">0%</span>)</div>
        </div>
      </md-content><!-- end ngRepeat: category in categories --><md-content ng-show="!expensesCatLoader" ng-repeat="category in categories" class="ng-scope _md" aria-hidden="false">
        <div class="widget widget-stats widget-expenses  red-bg margin-top-0">
                    <button class="md-icon-button pull-right md-button md-ink-ripple" type="button" ng-transclude="" ng-click="Remove($index)" aria-label="Remove">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-trash-b text-muted"></i></md-icon>
          </button>
          <button class="md-icon-button pull-right md-button md-ink-ripple" type="button" ng-transclude="" ng-click="UpdateCategory($index)" aria-label="Update">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-gear-a text-muted"></i></md-icon>
          </button>
                    <div class="stats-title text-uppercase ng-binding" ng-bind="category.name">test</div>
          <div class="stats-number"><span ng-bind-html="category.amountby | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$6,687.76</span></span></div>
          <div class="stats-progress progress">
            <div style="width: 2%;" class="progress-bar"></div>
          </div>
          <div class="stats-desc">Category Percent (<span ng-bind="category.percent+'%'" class="ng-binding">2%</span>)</div>
        </div>
      </md-content><!-- end ngRepeat: category in categories --> 
    </div>
  </div>
 @endsection