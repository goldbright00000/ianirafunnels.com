@extends('crm.default')
@section('title','Leads')
@section('content')
 <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-12">



    
    <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-12 md-p-0 lead-table">
      <md-toolbar class="toolbar-white _md _md-toolbar-transitions">
        <div class="md-toolbar-tools">
          <h2 flex="" md-truncate="" class="text-bold md-truncate flex">Register User Activity<br>
        </h2>
          <div class="ciuis-external-search-in-table">
            <input ng-model="search.name" class="search-table-external ng-pristine ng-untouched ng-valid ng-empty" id="search" name="search" type="text" placeholder="What're you looking for ?" aria-invalid="false">
            <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" aria-label="Search">
              <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-search text-muted"></i></md-icon>
            </button>
          </div>
          <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" ng-click="LeadSettings()" aria-label="Settings">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-ios-gear text-muted"></i></md-icon>
          </button>
          <!-- ngIf: !KanbanBoard --><button class="md-icon-button md-button ng-scope md-ink-ripple" type="button" ng-transclude="" ng-if="!KanbanBoard" ng-click="ShowKanban()" aria-label="Show Kanban">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="mdi mdi-view-week text-muted"></i></md-icon>
          </button><!-- end ngIf: !KanbanBoard -->
          <!-- ngIf: KanbanBoard -->
          <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" ng-click="toggleFilter()" aria-label="Filter">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-android-funnel text-muted"></i></md-icon>
          </button>
          <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" ng-click="Create()" aria-label="New">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-android-add-circle text-success"></i></md-icon>
          </button>
          <md-menu md-position-mode="target-right target" ng-hide="ONLYADMIN != 'true'" class="md-menu ng-scope _md" aria-hidden="false">
            <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" aria-label="Open demo menu" ng-click="$mdMenu.open($event)" aria-haspopup="true" aria-expanded="false" aria-owns="menu_container_29">
              <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-android-more-vertical text-muted"></i></md-icon>
            </button>
            
          <div class="_md md-open-menu-container md-whiteframe-z2" id="menu_container_29" style="display: none;" aria-hidden="true"><md-menu-content width="4" role="menu">
              <md-menu-item>
                <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="Import()" role="menuitem">
                  <div layout="row" flex="" class="ng-scope layout-row flex">
                    <p flex="" class="flex">Import Leads</p>
                    <md-icon md-menu-align-target="" class="ion-upload material-icons" style="margin: auto 3px auto 0;" role="img" aria-hidden="true"></md-icon>
                  </div>
                </button>
              </md-menu-item>
              <form action="https://demo.ciuis.com/leads/exportdata" class="form-horizontal ng-pristine ng-valid" enctype="multipart/form-data" method="post" accept-charset="utf-8">
              <md-menu-item>
                <button class="md-button md-ink-ripple" type="submit" ng-transclude="" role="menuitem">
                  <div layout="row" flex="" class="ng-scope layout-row flex">
                    <p flex="" ng-bind="lang.exportleads" class="ng-binding flex">Export Leads</p>
                    <md-icon md-menu-align-target="" class="ion-android-download text-muted material-icons" style="margin: auto 3px auto 0;" role="img" aria-hidden="true"></md-icon>
                  </div>
                </button>
              </md-menu-item>
              </form>              <md-menu-item>
                <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="RemoveConverted()" role="menuitem">
                  <div layout="row" flex="" class="ng-scope layout-row flex">
                    <p flex="" class="flex">Delete Converted Leads</p>
                    <md-icon md-menu-align-target="" class="ion-android-remove-circle material-icons" style="margin: auto 3px auto 0;" role="img" aria-hidden="true"></md-icon>
                  </div>
                </button>
              </md-menu-item>
              <md-menu-item>
                <a class="md-button md-ink-ripple" ng-transclude="" ng-href="https://demo.ciuis.com/leads/forms" role="menuitem" href="https://demo.ciuis.com/leads/forms">
                  <div layout="row" flex="" class="ng-scope layout-row flex">
                    <p flex="" class="flex">Online Web Leads</p>
                    <md-icon md-menu-align-target="" class="ion-earth material-icons" style="margin: auto 3px auto 0;" role="img" aria-hidden="true"></md-icon>
                  </div>
                </a>
              </md-menu-item>
            </md-menu-content></div></md-menu>
        </div>
      </md-toolbar>
      <div ng-show="leadsLoader" layout-align="center center" class="text-center layout-align-center-center ng-hide" id="circular_loader" aria-hidden="true" style="">
        <md-progress-circular md-mode="indeterminate" md-diameter="40" aria-valuemin="0" aria-valuemax="100" role="progressbar" class="ng-isolate-scope md-mode-indeterminate" style="width: 40px; height: 40px;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" style="width: 40px; height: 40px; transform-origin: 20px 20px 20px;"><path fill="none" stroke-width="4" stroke-linecap="square" d="M20,2A18,18 0 1 1 2,20" stroke-dasharray="84.82300164692441" stroke-dashoffset="250.04885021641428" transform="rotate(0 20 20)"></path></svg></md-progress-circular>
        <p style="font-size: 15px;margin-bottom: 5%;">
         <span>
            Please wait <br>
           <small><strong>Loading Leads...</strong></small>
         </span>
       </p>
     </div>
      <!-- ngIf: KanbanBoard -->
      <!-- ngIf: !KanbanBoard --><md-content ng-show="!leadsLoader" ng-if="!KanbanBoard" class="md-pt-0 ng-scope _md" aria-hidden="false" style="">
        <ul class="custom-ciuis-list-body" style="padding: 0px;">
         
            
          <li ng-repeat="lead in leads | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5" class="ciuis-custom-list-item ciuis-special-list-item ng-scope" style="">
            <ul class="list-item-for-custom-list">
              <li class="ciuis-custom-list-item-item col-md-12">
                <div data-toggle="tooltip" data-placement="bottom" data-container="body" title="" data-original-title="Assigned: pippo" class="assigned-staff-for-this-lead user-avatar">
                                                                                                                                                         
                <img src="https://demo.ciuis.com/uploads/images/n-img.jpg" alt="pippo"> </div>
                <div class="pull-left col-md-6">
                    <a ng-href="https://demo.ciuis.com/leads/lead/46" href="https://demo.ciuis.com/leads/lead/46">
                        <strong ng-bind="lead.name" class="ng-binding">Name</strong></a><br>
                  <small ng-bind="lead.company" class="ng-binding"></small> </div>
                <div class="col-md-6">
                  <div class="col-md-5"><span class="date-start-task"><small class="text-muted text-uppercase">Email  <i class="ion-ios-stopwatch-outline"></i></small><br>
                    <strong ng-bind="lead.phone" class="ng-binding"></strong></span> </div>
                  <div class="col-md-4"><span class="date-start-task"><small class="text-muted text-uppercase">Activity Details</small><br>
                    <strong><span class="badge ng-binding" style="border-color: #fff;background-color: #fff3d1;" ng-bind="lead.statusname">View Details</span></strong></span>
                 </div>
               
                </div>
              </li>
            </ul>
          </li>   
          
            @foreach($fetchdata as $user_activity)
            <li ng-repeat="lead in leads | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5" class="ciuis-custom-list-item ciuis-special-list-item ng-scope" style="">
            <ul class="list-item-for-custom-list">
              <li class="ciuis-custom-list-item-item col-md-12">
                <div data-toggle="tooltip" data-placement="bottom" data-container="body" title="" data-original-title="Assigned: pippo" class="assigned-staff-for-this-lead user-avatar">
                                                                                                                                                         
                <img src="https://demo.ciuis.com/uploads/images/n-img.jpg"> </div>
                <div class="pull-left col-md-6">
                    <a ng-href="https://demo.ciuis.com/leads/lead/46" href="https://demo.ciuis.com/leads/lead/46">
                        <strong ng-bind="lead.name" class="ng-binding">{{$user_activity->name}}</strong></a><br>
                  <small ng-bind="lead.company" class="ng-binding"></small> </div>
                <div class="col-md-6">
                  <div class="col-md-5"><span class="date-start-task"><small class="text-muted text-uppercase">{{$user_activity->email}}  <i class="ion-ios-stopwatch-outline"></i></small><br>
                    <strong ng-bind="lead.phone" class="ng-binding"></strong></span> </div>
<div class="col-md-4">
    <span class="date-start-task"> 
        <a href="{{url('crm/user-details/'.$user_activity->id)}}">  <small class="text-muted text-uppercase">Activity</small>   <br>
            <strong><span class="badge ng-binding" style="border-color: #fff;background-color: #fff3d1;" ng-bind="lead.statusname">View Details</span></strong>
              </a>           
    </span>
                 </div>
               
                </div>
              </li>
            </ul>
          </li>   
          @endforeach
         
              
              
          </li><!-- end ngRepeat: lead in leads | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5 -->
        </ul>
        <div ng-hide="leads.length < 5" class="pagination-div" aria-hidden="false">
          <ul class="pagination">
            <li ng-class="DisablePrevPage()" class="disabled" style=""> <a href="" ng-click="prevPage()"><i class="ion-ios-arrow-back"></i></a> </li>
            <!-- ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope active" role="button" tabindex="0" style=""> <a href="#" ng-bind="n+1" class="ng-binding">1</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0" style=""> <a href="#" ng-bind="n+1" class="ng-binding">2</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0"> <a href="#" ng-bind="n+1" class="ng-binding">3</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0"> <a href="#" ng-bind="n+1" class="ng-binding">4</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0"> <a href="#" ng-bind="n+1" class="ng-binding">5</a> </li><!-- end ngRepeat: n in range() -->
            <li ng-class="DisableNextPage()"> <a href="" ng-click="nextPage()"><i class="ion-ios-arrow-right"></i></a> </li>
          </ul>
        </div>
        <md-content ng-show="!leads.length" class="md-padding no-item-data _md ng-hide" aria-hidden="true" style="">No matching items found</md-content>
      </md-content><!-- end ngIf: !KanbanBoard -->
    </div>
  </div>
 @endsection