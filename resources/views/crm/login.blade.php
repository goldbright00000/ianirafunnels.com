<!DOCTYPE html>
<html ng-app="Ciuis" lang="en_US">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Ciuis, CRM, Project Management tool, client management, crm, customer, expenses, invoice system, invoices, lead, project management, recurring invoices, sales, self hosted, support tickets, task manager, ticket system">
<meta name="keywords" content="Ciuis™ CRM software for customer relationship management is available for sale, so you can get more information to take advantage of your exclusive concierge.">
<meta name="author" content="">
<link rel="shortcut icon" href="assets/img/images/logo-fav.png">
<title>Login</title>
<script src="{{url('public/crm/assets/lib/angular/angular.min.js')}}"></script>
<script src="{{url('public/crm/assets/lib/angular/angular.min.js')}}"></script>
<script src="{{url('public/crm/assets/lib/angular/angular.min.js')}}"></script>
<script src="{{url('public/crm/assets/lib/angular/i18n/angular-locale_en-us.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{url('public/crm/assets/css/ciuis.css')}}" type="text/css"/>
<script>var BASE_URL = "index.html",ACTIVESTAFF = "",SHOW_ONLY_ADMIN = "false",CURRENCY = "false",LOCATE_SELECTED = "en_US",UPIMGURL = "uploads/images/index.html",IMAGESURL = "assets/img/index.html",SETFILEURL = "uploads/ciuis_settings/index.html",NTFTITLE = "Notification",EVENTADDEDMSG = "Event successfully added",TODOADDEDMSG = "Todo added!",TODODONEMSG = "Todo mark as done!",REMINDERREAD = "Reminder mark as read!",INVMARKCACELLED = "Invoice Cancelled!",TICKSTATUSCHANGE = "Ticket status changed!",LEADMARKEDAS = "Lead marked as",LEADUNMARKEDAS = "Lead unmarked as",TODAYDATE = "2019.04.01 ",LOGGEDINSTAFFID = "",LOGGEDINSTAFFNAME = "",LOGGEDINSTAFFAVATAR = "",VOICENOTIFICATIONLANG = "en-us",initialLocaleCode = "en";</script>
</head>
<body class="ciuis-body-splash-screen">
    
   @if(session()->has('fail')) 
<div id="sucessfullyMessage" class="alert alert-danger animated fadeIn sucess_message">
        <button type="button" class="close s_close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
          {{session('fail')}}
        </strong>
    </div> 
    @endif
    
<div class="ciuis-body-wrapper ciuis-body-login"> 
  <div class="ciuis-body-content">
    <div class="col-md-4 login-left hide-xs hide-sm" style="background-image: {{url('public/assets/img/images/login.jpg')}}">
      <div class="lg-content">
        <h2>ianira funnels</h2>
        <p class="text-muted">Welcome! With ianira funnels you can easily manage your customer relationships and save time on your business.</p> 
        <!-- <md-button href="area/login/auth.html" class="btn btn-warning md-raised md-warn p-l-20 p-r-20">Client Area</md-button> -->
      </div>
    </div>
    <div class="main-content container-fluid col-md-8 login_page_right_block">
      <div class="splash-container">
        <md-card flex-xs flex-gt-xs="100" layout="column">
          <div class="panel panel-default"> 
            <div class="panel-heading"><img src="{{url('public/crm/images/brand.png')}}" alt="logo" class="logo-img nav-logo"> ianira funnels<span class="splash-description">Please login with your user information.</span>
            </div>
            <div class="panel-body">
              <form action="{{url('auth/check')}}" method="POST" >
                    {{ csrf_field() }}
              <div class="form-group">
                <input id="email" type="email" placeholder="E-Mail" name="username" autocomplete="off" class="form-control">
              </div>
              <div class="form-group"> 
                <input id="password"  type="password" name="password" placeholder="Login Password" class="form-control">
              </div>
              <div class="form-group row login-tools">
                <div class="col-xs-6 login-remember">
                  <div class="ciuis-body-checkbox">
                    <input type="checkbox" name="remember" id="remember">
                    <label for="remember">Remember me</label>
                  </div>
                </div>
                <div class="col-xs-6 login-forgot-password"><a href="login/forgot.html">Forgot Password?</a> </div>
              </div>
              <div class="form-group">
                <label><b>Select a Language:</b></label>
                <select class="form-control" name="language" required="" style="padding: 4px;height: 30px;">
                                      <option value="german" >
                      Deutsche                    </option>
                                      <option value="english" selected="selected">
                      English                    </option>
                                      <option value="spanish" >
                      Español                    </option>
                                      <option value="french" >
                      Français                    </option>
                                      <option value="portuguese_pt" >
                      Português                    </option>
                                      <option value="portuguese_br" >
                      Português Brasil                    </option>
                                      <option value="turkish" >
                      Türk                    </option>
                                      <option value="russian" >
                      Pусский                    </option>
                                      <option value="swedish" >
                      Svenska                    </option>
                                      <option value="italian" >
                      Italiano                    </option>
                                  </select>
              </div>
              <div class="form-group login-submit">
                  <button data-dismiss="modal" type="submit" value="submit" name="submit"   class="login-button btn btn-ciuis btn-xl">Login</button>
              </div>
              <label class="text-danger"></label>           
            
              </form>
            </div>
          </div>
        </md-card>
      </div>
    </div>
  </div>
</div>
 <style>
    .sucess_message {
   margin: 0;
    margin-bottom: 0px;
margin-bottom: 0px;
margin-bottom: 0px;
position: absolute !important;
top: 12%;
left: 50%;
transform: translate(-50%, -50%);
position: absolute;
width: 24%;
height: 46px;
line-height: 11px;
text-align: left;
z-index: 999999;
color: white;

}
.s_close
{
   line-height: 15px; 
}

margin-right: 1.25rem;
width: 16px;
line-height: 1;
font-size: 18px;
color: #979797;

    </style>

 
    
<script src="{{url('assets/lib/jquery/jquery.min.js')}}" type="text/javascript"></script> 
<script src="{{url('assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}" type="text/javascript"></script> 
<script src="{{url('assets/lib/hoverIntent/hoverIntent.js')}}"></script> 
<script src="{{url('assets/js/Ciuis.js')}}" type="text/javascript"></script> 
<script src="{{url('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script> 
<script src="{{url('assets/lib/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script> 
<script src="{{url('assets/lib/jquery.gritter/js/jquery.gritter.js')}}" type="text/javascript"></script> 
<script src="{{url('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script> 
<script src="{{url('assets/lib/angular-datepicker/src/js/angular-datepicker.js')}}" type="text/javascript"></script> 
<script src="{{url('assets/lib/select2/js/select2.min.js')}}" type="text/javascript"></script> 
<script src="{{url('assets/lib/select2/js/select2.full.min.js')}}" type="text/javascript"></script> 
<script src="{{url('assets/lib/material/angular-material.min.js')}}"></script> 
<script src="{{url('assets/lib/currency-format/currency-format.min.js')}}"></script> 
<script src="{{url('assets/lib/angular-datetimepicker/angular-material-datetimepicker.min.js')}}"></script> 
<script src="{{url('assets/lib/scheduler/scheduler.min.js')}}"></script> 
<script src="{{url('assets/js/CiuisAngular.js')}}"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    setTimeout(function() {
    $('#sucessfullyMessage').fadeOut('fast');
}, 2700);

 $('.close').click(function(){
     
    $('sucessfullyMessage').hide('fast');
    });
    </script>   
</body>

</html>
