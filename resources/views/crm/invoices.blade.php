@extends('crm.default')
@section('title','Invoices')
@section('content')
<div class="main-content container-fluid col-xs-12 col-md-12 col-lg-12">
        <div ng-hide="ONLYADMIN != 'true'" class="panel-default" aria-hidden="false">
            <div class="ciuis-invoice-summary"><div><div class="row"><div class="col-md-12"><div style="border-top-left-radius: 10px;" class="ciuis-right-border-b1 ciuis-invoice-summaries-b1"><div class="box-header text-uppercase text-bold">Total Invoices</div><div class="box-content"><div class="sentTotal">84</div></div><div class="box-foot"><div class="sendTime box-foot-left">Amount<br><span class="box-foot-stats"><strong>USD 2,665,378.14</strong></span></div></div></div><div class="ciuis-right-border-b1 ciuis-invoice-summaries-b1"><div class="box-header text-uppercase text-bold">PAID</div><div class="box-content invoice-percent" style="width: 130px; height: 130px;"><div class="percentage">%49</div><canvas id="0" width="130" height="130"></canvas></div><div class="box-foot"><span class="arrow arrow-up"></span><div class="box-foot-left">Amount<br><span class="box-foot-stats"><strong>USD 2,068,843.87</strong></span></div><span class="arrow arrow-down"></span><div class="box-foot-right"><br><span class="box-foot-stats" "=""><strong>41</strong> (%49)</span></div></div></div><div class="ciuis-right-border-b1 ciuis-invoice-summaries-b1"><div class="box-header text-uppercase text-bold">Unpaid</div><div class="box-content invoice-percent-2" style="width: 130px; height: 130px;"><div class="percentage">%6</div><canvas id="1" width="130" height="130"></canvas></div><div class="box-foot"><span class="arrow arrow-up"></span><div class="box-foot-left">Amount<br><span class="box-foot-stats"><strong>USD 55,540.00</strong></span></div><span class="arrow arrow-down"></span><div class="box-foot-right"><br><span class="box-foot-stats"><strong>5</strong> (%6)</span></div></div></div><div style="border-top-right-radius: 10px;" class="ciuis-invoice-summaries-b1"><div class="box-header text-uppercase text-bold">Over Due</div><div class="box-content invoice-percent-3" style="width: 130px; height: 130px;"><div class="percentage">%31</div><canvas id="2" width="130" height="130"></canvas></div><div class="box-foot"><span class="arrow arrow-up"></span><div class="box-foot-left">Amount<br><span class="box-foot-stats"><strong>USD 531,283.02</strong></span></div><div class="box-foot-right"><br><span class="box-foot-stats"><strong>26</strong> (%31)</span></div></div></div></div></div></div></div>
        </div>
                <div ng-show="invoiceLoader" layout-align="center center" class="text-center layout-align-center-center ng-hide" id="circular_loader" aria-hidden="true" style="">
            <md-progress-circular md-mode="indeterminate" md-diameter="40" aria-valuemin="0" aria-valuemax="100" role="progressbar" class="ng-isolate-scope md-mode-indeterminate" style="width: 40px; height: 40px;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" style="width: 40px; height: 40px; transform-origin: 20px 20px 20px;"><path fill="none" stroke-width="4" stroke-linecap="square" d="M20,2A18,18 0 1 1 2,20" stroke-dasharray="84.82300164692441" stroke-dashoffset="190.32765653743087" transform="rotate(0 20 20)"></path></svg></md-progress-circular>
              <p style="font-size: 15px;margin-bottom: 5%;">
               <span>
                  Please wait <br>
                 <small><strong>Loading Invoices...</strong></small>
               </span>
             </p>
           </div>
        <md-toolbar ng-show="!invoiceLoader" class="toolbar-white _md _md-toolbar-transitions" aria-hidden="false" style="">
            <div class="md-toolbar-tools">
                <h2 flex="" md-truncate="" class="text-bold md-truncate flex">Invoices<br><small flex="" md-truncate="" class="md-truncate flex">Organize your invoices</small></h2>
                <div class="ciuis-external-search-in-table">
                    <input ng-model="search.customer" class="search-table-external ng-pristine ng-untouched ng-valid ng-empty" id="search" name="search" type="text" placeholder="Search by Customer" aria-invalid="false"> 
                    <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" aria-label="Search">
                        <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-search text-muted"></i></md-icon>
                    </button>
                </div>
                <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" ng-click="toggleFilter()" aria-label="Filter">
                    <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-android-funnel text-muted"></i></md-icon>
                </button>
                                    <a class="md-icon-button md-button md-ink-ripple" ng-transclude="" ng-href="#" aria-label="New" href="#">
                        
                        <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-android-add-circle text-success"></i></md-icon>
                    </a>
                            </div>
        </md-toolbar>
        <md-content ng-show="!invoiceLoader" class="_md" aria-hidden="false" style="">
            <ul class="custom-ciuis-list-body" style="padding: 0px;">
                <!-- ngRepeat: invoice in invoices | filter: FilteredData |  filter:search | pagination : currentPage*itemsPerPage | limitTo: 5 --><li ng-repeat="invoice in invoices | filter: FilteredData |  filter:search | pagination : currentPage*itemsPerPage | limitTo: 5" class="ciuis-custom-list-item ciuis-special-list-item ng-scope" style="">
                    <a class="ciuis_expense_receipt_number" href="https://demo.ciuis.com/invoices/invoice/99">
                        <ul class="list-item-for-custom-list">
                            <li class="ciuis-custom-list-item-item col-md-12">
                            <div class="assigned-staff-for-this-lead user-avatar"><i class="ico-ciuis-invoices" style="font-size: 32px"></i></div>
                                <div class="pull-left col-md-3">
                                <strong>
                                <span ng-bind="invoice.prefix + '' + invoice.longid" class="ng-binding">INV-000099</span>
                                </strong><br><small ng-bind="invoice.customer" class="ng-binding">DHARMA Initiative</small>
                                </div>
                                <div class="col-md-9">
                                    <div class="col-md-3">
                                    <span class="date-start-task"><small class="text-muted text-uppercase">Billed Date</small><br><strong><span class="badge ng-binding" ng-bind="invoice.created">03/04/19</span></strong></span>
                                    </div>
                                    <div class="col-md-3">
                                    <span class="date-start-task"><small class="text-muted text-uppercase">Due Date</small><br><strong><span class="badge ng-binding" ng-bind="invoice.duedate">No Due Date</span></strong></span>
                                    </div>
                                    <div class="col-md-3">
                                    <span class="date-start-task"><small class="text-muted text-uppercase">Status</small><br><strong class="text-uppercase text-success" ng-bind="invoice.status">PAID</strong>
                                    </span></div>
                                    <div class="col-md-3 text-right">
                                    <span class="date-start-task"><small class="text-muted text-uppercase">Amount</small><br><strong ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$122.00</span></strong></span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </a>
                </li><!-- end ngRepeat: invoice in invoices | filter: FilteredData |  filter:search | pagination : currentPage*itemsPerPage | limitTo: 5 --><li ng-repeat="invoice in invoices | filter: FilteredData |  filter:search | pagination : currentPage*itemsPerPage | limitTo: 5" class="ciuis-custom-list-item ciuis-special-list-item ng-scope">
                    <a class="ciuis_expense_receipt_number" href="https://demo.ciuis.com/invoices/invoice/98">
                        <ul class="list-item-for-custom-list">
                            <li class="ciuis-custom-list-item-item col-md-12">
                            <div class="assigned-staff-for-this-lead user-avatar"><i class="ico-ciuis-invoices" style="font-size: 32px"></i></div>
                                <div class="pull-left col-md-3">
                                <strong>
                                <span ng-bind="invoice.prefix + '' + invoice.longid" class="ng-binding">INV-000098</span>
                                </strong><br><small ng-bind="invoice.customer" class="ng-binding">DHARMA Initiative</small>
                                </div>
                                <div class="col-md-9">
                                    <div class="col-md-3">
                                    <span class="date-start-task"><small class="text-muted text-uppercase">Billed Date</small><br><strong><span class="badge ng-binding" ng-bind="invoice.created">03/04/19</span></strong></span>
                                    </div>
                                    <div class="col-md-3">
                                    <span class="date-start-task"><small class="text-muted text-uppercase">Due Date</small><br><strong><span class="badge ng-binding" ng-bind="invoice.duedate">No Due Date</span></strong></span>
                                    </div>
                                    <div class="col-md-3">
                                    <span class="date-start-task"><small class="text-muted text-uppercase">Status</small><br><strong class="text-uppercase text-success" ng-bind="invoice.status">PAID</strong>
                                    </span></div>
                                    <div class="col-md-3 text-right">
                                    <span class="date-start-task"><small class="text-muted text-uppercase">Amount</small><br><strong ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$297.00</span></strong></span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </a>
                </li><!-- end ngRepeat: invoice in invoices | filter: FilteredData |  filter:search | pagination : currentPage*itemsPerPage | limitTo: 5 --><li ng-repeat="invoice in invoices | filter: FilteredData |  filter:search | pagination : currentPage*itemsPerPage | limitTo: 5" class="ciuis-custom-list-item ciuis-special-list-item ng-scope">
                    <a class="ciuis_expense_receipt_number" href="https://demo.ciuis.com/invoices/invoice/97">
                        <ul class="list-item-for-custom-list">
                            <li class="ciuis-custom-list-item-item col-md-12">
                            <div class="assigned-staff-for-this-lead user-avatar"><i class="ico-ciuis-invoices" style="font-size: 32px"></i></div>
                                <div class="pull-left col-md-3">
                                <strong>
                                <span ng-bind="invoice.prefix + '' + invoice.longid" class="ng-binding">INV-000097</span>
                                </strong><br><small ng-bind="invoice.customer" class="ng-binding">GTG GROUP</small>
                                </div>
                                <div class="col-md-9">
                                    <div class="col-md-3">
                                    <span class="date-start-task"><small class="text-muted text-uppercase">Billed Date</small><br><strong><span class="badge ng-binding" ng-bind="invoice.created">02/04/19</span></strong></span>
                                    </div>
                                    <div class="col-md-3">
                                    <span class="date-start-task"><small class="text-muted text-uppercase">Due Date</small><br><strong><span class="badge ng-binding" ng-bind="invoice.duedate">No Due Date</span></strong></span>
                                    </div>
                                    <div class="col-md-3">
                                    <span class="date-start-task"><small class="text-muted text-uppercase">Status</small><br><strong class="text-uppercase text-success" ng-bind="invoice.status">PAID</strong>
                                    </span></div>
                                    <div class="col-md-3 text-right">
                                    <span class="date-start-task"><small class="text-muted text-uppercase">Amount</small><br><strong ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$100.00</span></strong></span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </a>
                </li><!-- end ngRepeat: invoice in invoices | filter: FilteredData |  filter:search | pagination : currentPage*itemsPerPage | limitTo: 5 --><li ng-repeat="invoice in invoices | filter: FilteredData |  filter:search | pagination : currentPage*itemsPerPage | limitTo: 5" class="ciuis-custom-list-item ciuis-special-list-item ng-scope">
                    <a class="ciuis_expense_receipt_number" href="https://demo.ciuis.com/invoices/invoice/96">
                        <ul class="list-item-for-custom-list">
                            <li class="ciuis-custom-list-item-item col-md-12">
                            <div class="assigned-staff-for-this-lead user-avatar"><i class="ico-ciuis-invoices" style="font-size: 32px"></i></div>
                                <div class="pull-left col-md-3">
                                <strong>
                                <span ng-bind="invoice.prefix + '' + invoice.longid" class="ng-binding">INV-000096</span>
                                </strong><br><small ng-bind="invoice.customer" class="ng-binding">test company 1</small>
                                </div>
                                <div class="col-md-9">
                                    <div class="col-md-3">
                                    <span class="date-start-task"><small class="text-muted text-uppercase">Billed Date</small><br><strong><span class="badge ng-binding" ng-bind="invoice.created">02/04/19</span></strong></span>
                                    </div>
                                    <div class="col-md-3">
                                    <span class="date-start-task"><small class="text-muted text-uppercase">Due Date</small><br><strong><span class="badge ng-binding" ng-bind="invoice.duedate">No Due Date</span></strong></span>
                                    </div>
                                    <div class="col-md-3">
                                    <span class="date-start-task"><small class="text-muted text-uppercase">Status</small><br><strong class="text-uppercase text-success" ng-bind="invoice.status">PAID</strong>
                                    </span></div>
                                    <div class="col-md-3 text-right">
                                    <span class="date-start-task"><small class="text-muted text-uppercase">Amount</small><br><strong ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$100.00</span></strong></span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </a>
                </li><!-- end ngRepeat: invoice in invoices | filter: FilteredData |  filter:search | pagination : currentPage*itemsPerPage | limitTo: 5 --><li ng-repeat="invoice in invoices | filter: FilteredData |  filter:search | pagination : currentPage*itemsPerPage | limitTo: 5" class="ciuis-custom-list-item ciuis-special-list-item ng-scope">
                    <a class="ciuis_expense_receipt_number" href="https://demo.ciuis.com/invoices/invoice/95">
                        <ul class="list-item-for-custom-list">
                            <li class="ciuis-custom-list-item-item col-md-12">
                            <div class="assigned-staff-for-this-lead user-avatar"><i class="ico-ciuis-invoices" style="font-size: 32px"></i></div>
                                <div class="pull-left col-md-3">
                                <strong>
                                <span ng-bind="invoice.prefix + '' + invoice.longid" class="ng-binding">INV-000095</span>
                                </strong><br><small ng-bind="invoice.customer" class="ng-binding">MyTest</small>
                                </div>
                                <div class="col-md-9">
                                    <div class="col-md-3">
                                    <span class="date-start-task"><small class="text-muted text-uppercase">Billed Date</small><br><strong><span class="badge ng-binding" ng-bind="invoice.created">01/04/19</span></strong></span>
                                    </div>
                                    <div class="col-md-3">
                                    <span class="date-start-task"><small class="text-muted text-uppercase">Due Date</small><br><strong><span class="badge ng-binding" ng-bind="invoice.duedate">No Due Date</span></strong></span>
                                    </div>
                                    <div class="col-md-3">
                                    <span class="date-start-task"><small class="text-muted text-uppercase">Status</small><br><strong class="text-uppercase text-success" ng-bind="invoice.status">PAID</strong>
                                    </span></div>
                                    <div class="col-md-3 text-right">
                                    <span class="date-start-task"><small class="text-muted text-uppercase">Amount</small><br><strong ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$1,711,740.99</span></strong></span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </a>
                </li><!-- end ngRepeat: invoice in invoices | filter: FilteredData |  filter:search | pagination : currentPage*itemsPerPage | limitTo: 5 -->
            </ul>
            <div class="pagination-div text-center" ng-show="invoices.length > 5" aria-hidden="false" style="">
                <ul class="pagination">
                    <li ng-class="DisablePrevPage()" class="disabled" style=""> <a href="" ng-click="prevPage()"><i class="ion-ios-arrow-back"></i></a> </li>
                    <!-- ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope active" role="button" tabindex="0" style=""> <a href="#" ng-bind="n+1" class="ng-binding">1</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0" style=""> <a href="#" ng-bind="n+1" class="ng-binding">2</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0"> <a href="#" ng-bind="n+1" class="ng-binding">3</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0"> <a href="#" ng-bind="n+1" class="ng-binding">4</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0"> <a href="#" ng-bind="n+1" class="ng-binding">5</a> </li><!-- end ngRepeat: n in range() -->
                    <li ng-class="DisableNextPage()"> <a href="" ng-click="nextPage()"><i class="ion-ios-arrow-right"></i></a> </li>
                </ul>
            </div>
            <md-content ng-show="!invoices.length" class="md-padding no-item-data _md ng-hide" aria-hidden="true" style="">No matching items found</md-content> 
        </md-content>
    </div>
 @endsection