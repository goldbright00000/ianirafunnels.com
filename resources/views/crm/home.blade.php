@extends('crm.default')
@section('title','Admin Dashboard')
@section('content')
 @if(session()->has('sucess')) 
<div id="sucessfullyMessage" class="alert alert-success animated fadeIn sucess_message">
        <button type="button" class="close s_close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
          {{session('sucess')}}
        </strong>
    </div>
@endif
 <md-content class="bg-white ciuis-home-summary-top _md">
                    <div class="col-md-3 col-sm-3 col-lg-3 nopadding">
                        <md-toolbar class="toolbar-white _md _md-toolbar-transitions" style="border-right:1px solid #e0e0e0">
                            <div class="md-toolbar-tools">
                                <h4 class="text-muted md-truncate flex" flex="" md-truncate=""><strong class="ng-binding"> Summary</strong></h4>
                                <button class="md-icon-button md-button md-ink-ripple" type="button" aria-label="Actions">
                                    <i class="fas fa-flag"></i>
                                </button>
                            </div>
                        </md-toolbar>
                        <md-content class="bg-white ciuis-summary-two _md">
                            <div class="ciuis-dashboard-box-b1-xs-ca-body">
                                <div class="ciuis-dashboard-box-stats ciuis-dashboard-box-stats-main">
                                    <div class="ciuis-dashboard-box-stats-amount">1256</div>
                                    <div class="ciuis-dashboard-box-stats-caption">Visitors </div>
                                    <div class="ciuis-dashboard-box-stats-change">
                                        <div class="ciuis-dashboard-box-stats-value ciuis-dashboard-box-stats-value--positive ng-binding">+34</div>
                                        <div class="ciuis-dashboard-box-stats-period ng-binding">in This Week</div>
                                    </div>
                                </div>
                                <div class="ciuis-dashboard-box-stats">
                                    <div class="ciuis-dashboard-box-stats-amount ng-binding">25</div>
                                    <div class="ciuis-dashboard-box-stats-caption ng-binding">New Leads</div>
                                   <!--  <div class="ciuis-dashboard-box-stats-change">
                                        <div class="ciuis-dashboard-box-stats-value ciuis-dashboard-box-stats-value-negative">
                                            <span class="fas fa-redo"></span>
                                        </div>
                                    </div> -->
                                </div>

                                   <div class="ciuis-dashboard-box-stats">
                                    <div class="ciuis-dashboard-box-stats-amount ng-binding">12</div>
                                    <div class="ciuis-dashboard-box-stats-caption ng-binding">New Sales </div>
                                   <!--  <div class="ciuis-dashboard-box-stats-change">
                                        <div class="ciuis-dashboard-box-stats-value ciuis-dashboard-box-stats-value-negative">
                                            <span class="fas fa-redo"></span>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <div class="hidden-xs"  aria-hidden="false">
                                <a href="#" class="btn btn-default btn_default_new"> <i class="fa fa-plus"></i> &nbsp; Add New Funnel</a>
                            </div>
                             
                        </md-content>


                    </div>
                    <div class="col-sm-9 xs-p-0">
                        <md-toolbar class="toolbar-white _md _md-toolbar-transitions">
                            <div class="md-toolbar-tools">
                                <h4 class="text-muted md-truncate flex" flex="" md-truncate="">
                                    <strong class="ng-binding"> Welcome LANCE BOGROL</strong>
                                </h4>
                               <!--  <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude=""  aria-label="Event">

                                    <i class="fa fa-calendar-week"></i>
                                </button> -->

                                 <div class="select_box">
                                    <span>All funnel</span> <span class="arow1 pull-right"><i class="fa fa-chevron-down"></i></span>
                                    <ul class="list-unstyled  new_select_form1">
                                        <li>All funnel</li>
                                        <li>All funnel</li>
                                    </ul>
                                 </div>

                              <!--   <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" ng-click="SetOnsiteVisit()" aria-label="Onsite Visits" ng-hide="ONLYADMIN != 'true'" aria-hidden="false">

                               
                                     <i class="fa fa-boxes"></i>
                                </button> -->
                            </div>
                        </md-toolbar>
                        <md-content layout-padding="" class="bg-white ciuis-summary-two layout-padding _md" style="overflow: hidden;">
                            <div class="brr-5 trr-5" ng-hide="ONLYADMIN != 'true'" aria-hidden="false">
                                <div class="col-sm-4 nopadding">
                                    <div class="col-md-12 nopadding">
                                        <div class="hpanel stats">
                                            <div style="padding-top: 0px;line-height: 0.99;margin-right: 10px;" class="panel-body h-200 xs-p-0">
                                                <div class="col-md-12 xs-mb-20 md-pl-0" style="margin-top: -14px;">
                                                    <h3 style="font-size:27px;line-height: 0.8;font-weight: 500;" class="no-margins font-extra-bold text-warning">
                      <span class="ng-binding"> $0.00</span>
                      </h3>
                                                    <small><strong class="ng-binding">Today's Sales</strong></small>
                                                    <div class="pull-right text-danger"><strong>-100%</strong></div>
                                                </div>
                                                <div class="col-md-12 nopadding md-pt-10 xs-pt-20" style="border-top: 1px solid #e0e0e0;">
                                                    <div class="stats-title">
                                                        <h4 style="font-weight: 500;color: #c7cbd5;">Net Cashflow</h4></div>
                                                    <h3 style="font-weight: 500;" class="m-b-xs"><span>$1,714,027.99</span></h3>
                                                    <div style="height: 10px" class="progress">
                                                        <div style="font-size: 7px;line-height: 10px;width:100%" class="progress-bar progress-bar-success progress-bar-striped ng-binding">100%</div>
                                                        <div style="font-size: 7px;line-height: 10px;width:10%" class="progress-bar progress-bar-danger progress-bar-striped ng-binding">10%</div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <small class="stats-label text-uppercase text-bold text-success"> Incomings</small>
                                                            <h4 class="ng-binding">$1,714,027.99</h4>
                                                        </div>

                                                        <div class="col-xs-6">
                                                            <small class="stats-label text-uppercase text-bold text-danger">$1,714,027.99</small>
                                                            <h4 class="ng-binding">$0.00</h4>
                                                        </div>
                                                    </div>
                                                    <span ng-bind-html="lang.cashflowdetail|trustAsHtml" class="ng-binding"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="hidden-sm hidden-md hidden-lg">
                                <div class="col-sm-8 nopadding">
                                    <div class="widget widget-fullwidth ciuis-body-loading">
                                        <div class="widget-chart-container">
                                            <div class="widget-counter-group widget-counter-group-right">
                                                <div class="pull-left">
                                                    <div class="pull-left text-left">
                                                        <h4 style="padding: 0px;margin: 0px;"><b class="ng-binding">Weekly Gain</b></h4>
                                                        <small class="ng-binding">This information includes only this week sales.</small>
                                                    </div>
                                                </div>
                                                <div class="counter counter-big">
                                                    <div class="text-warning value">
                                                        <span class="ng-binding">$1,711,740.99</span>
                                                    </div>
                                                    <div class="desc ng-binding">in This Week</div>
                                                </div>
                                                <div class="counter counter-big">
                                                    <div class="value"><span class="text-">N/A%</span></div>
                                                    <div class="desc ng-binding">Increase</div>
                                                </div>
                                            </div>
                                            <div class="my-2 ng-scope" ng-controller="Chart_Controller">
                                                <div class="chart-wrapper" style="height:235px;">
                                                    <canvas id="main-chart" height="235px"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ciuis-body-spinner">
                                            <svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                                                <circle fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30" class="circle"></circle>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="brr-5 trr-5 ng-hide" ng-hide="ONLYADMIN != 'false'" aria-hidden="true">
                                <div class="col-sm-4 nopadding">
                                    <div class="col-md-12 nopadding">
                                        <div class="hpanel stats">
                                            <div style="padding-top: 0px;line-height: 0.99;margin-right: 10px;" class="panel-body h-200 xs-p-0">
                                                <div class="col-md-12 xs-mb-20 md-pl-0" style="line-height: 28px;">
                                                    <div class="col-md-12 text-center">
                                                        <p><img ng-src="https://demo.ciuis.com/assets/img/" on-error-src="https://demo.ciuis.com/assets/img/placeholder.png" src="https://demo.ciuis.com/assets/img/placeholder.png"></p>
                                                        <h4 ng-bind="stats.daymessage" class="ng-binding"></h4>
                                                        <span ng-bind="user.staffname" class="ng-binding"></span>
                                                    </div>
                                                    <div class="col-md-12  md-pt-10 xs-pt-20 text-center ng-binding" style="border-top: 1px solid #e0e0e0;" ng-bind="lang.haveaniceday"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="hidden-sm hidden-md hidden-lg">
                                <div class="col-sm-8 nopadding">
                                    <div class="panel panel-default">
                                        <div class="panel-body" style="height: 400px; overflow: scroll; zoom: 0.8; margin-top: 25px; box-shadow: inset 0px 17px 50px 10px #ffffff; overflow-y: scroll;overflow-x: hidden;">
                                            <ul class="user-timeline user-timeline-compact">
                                          

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </md-content>
                    </div>
                </md-content>

<script>
setTimeout(function() {
    $('#sucessfullyMessage').fadeOut('fast');
}, 2700);

</script>


<style>
    .sucess_message {
    margin: 0;
        margin-bottom: 0px;
    margin-bottom: 0px;
    position: absolute !important;
    top: 12%;
    left: 50%;
    transform: translate(-50%, -50%);
    position: absolute;
    width: 24%;
    height: 41px;
    line-height: 15px;
    text-align: left;
    z-index: 999999;
    color:white;
    
}

.btn_default_new {
      background: #FFC107!important;
    display: inline-block;
    margin-top: 20px;
    margin-left: 15px;
    border: 2px solid #f7d87b !important;
    color: #000 !important;
    border-radius: 8px !important;
    width: 200px;
    height: 40px;
    line-height: 40px;
    font-size: 14px;
}
.new_select_form1{
    background: #ffffff;
    margin-left: -8px;
    margin-right: -8px;
    /*padding: 8px;*/
}
.new_select_form1 li{
    transition: 0.5s;
        padding: 8px;
         border-bottom: 1px solid #eee;
}
.new_select_form1 li:hover{
    background: #eee;
}
/*.new_select_form1*/
 .select_box{
    height: 35px;
    width: 220px;
    color: #999;
    border: 1px solid #ddd;
    border-radius: 4px;
    font-size: 14px;
    padding: 8px;
    cursor: pointer;
}
.select_box .new_select_form1 {
    display: none;
}
.select_box:hover .new_select_form1{
    display: block;
}

.btn_default_new .fa{
    color: #000 !important;
    font-size: 14px;
}
.s_close
{
    
   border: solid 1px red;
line-height: 0px;
color: white;
opacity: 1;

}
.sucess_message strong{

line-height: 6px;
font-size: 15px;
}


    </style>

<script>
    setTimeout(function() {
    $('#sucessfullyMessage').fadeOut('fast');
}, 2700);

 $('.close').click(function(){
     
    $('sucessfullyMessage').hide('fast');
    });
    </script> 
 @endsection
 