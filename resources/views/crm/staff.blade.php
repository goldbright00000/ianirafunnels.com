@extends('crm.default')
@section('title','Admin Dashboard')
@section('content')
 <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-9">
    <!-- ngRepeat: member in staff --><div class="col-md-4 ng-scope" ng-repeat="member in staff" style="padding: 0px; margin: 0px;">
      <md-card md-theme-watch="" style="margin-top:0px" class="_md">
        <md-card-title>
          <md-card-title-media style="width:100px;height:100px"> 
            <img style="border-radius: 5px;" src="https://demo.ciuis.com/uploads/images/n-img.jpg" alt="Avatar"></md-card-title-media>
            <md-card-title-text class="md-ml-20 xs-ml-20" style="margin-top: -10px;"> 
              <a class="md-headline cursor ng-binding" ng-click="ViewStaff(member.id)" ng-bind="member.name">pippo</a> 
              <span class="md-subhead ng-binding" ng-bind="member.department">test</span> 
              <span class="md-subhead"><a href="tel:" class="ng-binding"></a></span> 
              <span class="md-subhead ng-binding" ng-bind="member.email">pippo@ccj.com</span> 
            </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="end center" class="layout-align-end-center layout-row">
          <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="ViewStaff(member.id)">View</button>
        </md-card-actions>
      </md-card>
    </div><!-- end ngRepeat: member in staff --><div class="col-md-4 ng-scope" ng-repeat="member in staff" style="padding: 0px;margin: 0px">
      <md-card md-theme-watch="" style="margin-top:0px" class="_md">
        <md-card-title>
          <md-card-title-media style="width:100px;height:100px"> 
            <img style="border-radius: 5px;" src="https://demo.ciuis.com/uploads/images/pm2.jpg" alt="Avatar"></md-card-title-media>
            <md-card-title-text class="md-ml-20 xs-ml-20" style="margin-top: -10px;"> 
              <a class="md-headline cursor ng-binding" ng-click="ViewStaff(member.id)" ng-bind="member.name">Lance Bogrol</a> 
              <span class="md-subhead ng-binding" ng-bind="member.department"></span> 
              <span class="md-subhead"><a href="tel:+1-202-555-0160" class="ng-binding">+1-202-555-0160</a></span> 
              <span class="md-subhead ng-binding" ng-bind="member.email">lance@example.com</span> 
            </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="end center" class="layout-align-end-center layout-row">
          <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="ViewStaff(member.id)">View</button>
        </md-card-actions>
      </md-card>
    </div><!-- end ngRepeat: member in staff --><div class="col-md-4 ng-scope" ng-repeat="member in staff" style="padding: 0px;margin: 0px">
      <md-card md-theme-watch="" style="margin-top:0px" class="_md">
        <md-card-title>
          <md-card-title-media style="width:100px;height:100px"> 
            <img style="border-radius: 5px;" src="https://demo.ciuis.com/uploads/images/tttttttt.jpg" alt="Avatar"></md-card-title-media>
            <md-card-title-text class="md-ml-20 xs-ml-20" style="margin-top: -10px;"> 
              <a class="md-headline cursor ng-binding" ng-click="ViewStaff(member.id)" ng-bind="member.name">Emma Durst</a> 
              <span class="md-subhead ng-binding" ng-bind="member.department"></span> 
              <span class="md-subhead"><a href="tel:+1-202-555-0158" class="ng-binding">+1-202-555-0158</a></span> 
              <span class="md-subhead ng-binding" ng-bind="member.email">emma@example.com</span> 
            </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="end center" class="layout-align-end-center layout-row">
          <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="ViewStaff(member.id)">View</button>
        </md-card-actions>
      </md-card>
    </div><!-- end ngRepeat: member in staff --><div class="col-md-4 ng-scope" ng-repeat="member in staff" style="padding: 0px;margin: 0px">
      <md-card md-theme-watch="" style="margin-top:0px" class="_md">
        <md-card-title>
          <md-card-title-media style="width:100px;height:100px"> 
            <img style="border-radius: 5px;" src="https://demo.ciuis.com/uploads/images/guy.jpg" alt="Avatar"></md-card-title-media>
            <md-card-title-text class="md-ml-20 xs-ml-20" style="margin-top: -10px;"> 
              <a class="md-headline cursor ng-binding" ng-click="ViewStaff(member.id)" ng-bind="member.name">Guy Mann</a> 
              <span class="md-subhead ng-binding" ng-bind="member.department"></span> 
              <span class="md-subhead"><a href="tel:+1-202-555-0129" class="ng-binding">+1-202-555-0129</a></span> 
              <span class="md-subhead ng-binding" ng-bind="member.email">guy@example.com</span> 
            </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="end center" class="layout-align-end-center layout-row">
          <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="ViewStaff(member.id)">View</button>
        </md-card-actions>
      </md-card>
    </div><!-- end ngRepeat: member in staff --><div class="col-md-4 ng-scope" ng-repeat="member in staff" style="padding: 0px;margin: 0px">
      <md-card md-theme-watch="" style="margin-top:0px" class="_md">
        <md-card-title>
          <md-card-title-media style="width:100px;height:100px"> 
            <img style="border-radius: 5px;" src="https://demo.ciuis.com/uploads/images/ruby.jpg" alt="Avatar"></md-card-title-media>
            <md-card-title-text class="md-ml-20 xs-ml-20" style="margin-top: -10px;"> 
              <a class="md-headline cursor ng-binding" ng-click="ViewStaff(member.id)" ng-bind="member.name">Ruby Von Rails</a> 
              <span class="md-subhead ng-binding" ng-bind="member.department"></span> 
              <span class="md-subhead"><a href="tel:+1-202-555-0143" class="ng-binding">+1-202-555-0143</a></span> 
              <span class="md-subhead ng-binding" ng-bind="member.email">ruby@example.com</span> 
            </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="end center" class="layout-align-end-center layout-row">
          <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="ViewStaff(member.id)">View</button>
        </md-card-actions>
      </md-card>
    </div><!-- end ngRepeat: member in staff --><div class="col-md-4 ng-scope" ng-repeat="member in staff" style="padding: 0px;margin: 0px">
      <md-card md-theme-watch="" style="margin-top:0px" class="_md">
        <md-card-title>
          <md-card-title-media style="width:100px;height:100px"> 
            <img style="border-radius: 5px;" src="https://demo.ciuis.com/uploads/images/n-img.jpg" alt="Avatar"></md-card-title-media>
            <md-card-title-text class="md-ml-20 xs-ml-20" style="margin-top: -10px;"> 
              <a class="md-headline cursor ng-binding" ng-click="ViewStaff(member.id)" ng-bind="member.name">asd</a> 
              <span class="md-subhead ng-binding" ng-bind="member.department"></span> 
              <span class="md-subhead"><a href="tel:asd" class="ng-binding">asd</a></span> 
              <span class="md-subhead ng-binding" ng-bind="member.email">asd@asd.com</span> 
            </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="end center" class="layout-align-end-center layout-row">
          <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="ViewStaff(member.id)">View</button>
        </md-card-actions>
      </md-card>
    </div><!-- end ngRepeat: member in staff --><div class="col-md-4 ng-scope" ng-repeat="member in staff" style="padding: 0px;margin: 0px">
      <md-card md-theme-watch="" style="margin-top:0px" class="_md">
        <md-card-title>
          <md-card-title-media style="width:100px;height:100px"> 
            <img style="border-radius: 5px;" src="https://demo.ciuis.com/uploads/images/n-img.jpg" alt="Avatar"></md-card-title-media>
            <md-card-title-text class="md-ml-20 xs-ml-20" style="margin-top: -10px;"> 
              <a class="md-headline cursor ng-binding" ng-click="ViewStaff(member.id)" ng-bind="member.name">GUNDA GHAN</a> 
              <span class="md-subhead ng-binding" ng-bind="member.department"></span> 
              <span class="md-subhead"><a href="tel:123451345" class="ng-binding">123451345</a></span> 
              <span class="md-subhead ng-binding" ng-bind="member.email">gunda@ghan.com</span> 
            </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="end center" class="layout-align-end-center layout-row">
          <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="ViewStaff(member.id)">View</button>
        </md-card-actions>
      </md-card>
    </div><!-- end ngRepeat: member in staff --><div class="col-md-4 ng-scope" ng-repeat="member in staff" style="padding: 0px;margin: 0px">
      <md-card md-theme-watch="" style="margin-top:0px" class="_md">
        <md-card-title>
          <md-card-title-media style="width:100px;height:100px"> 
            <img style="border-radius: 5px;" src="https://demo.ciuis.com/uploads/images/n-img.jpg" alt="Avatar"></md-card-title-media>
            <md-card-title-text class="md-ml-20 xs-ml-20" style="margin-top: -10px;"> 
              <a class="md-headline cursor ng-binding" ng-click="ViewStaff(member.id)" ng-bind="member.name">Sharjeel</a> 
              <span class="md-subhead ng-binding" ng-bind="member.department"></span> 
              <span class="md-subhead"><a href="tel:923028293278" class="ng-binding">923028293278</a></span> 
              <span class="md-subhead ng-binding" ng-bind="member.email">sharjeel@interlinkpakistan.com</span> 
            </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="end center" class="layout-align-end-center layout-row">
          <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="ViewStaff(member.id)">View</button>
        </md-card-actions>
      </md-card>
    </div><!-- end ngRepeat: member in staff --><div class="col-md-4 ng-scope" ng-repeat="member in staff" style="padding: 0px;margin: 0px">
      <md-card md-theme-watch="" style="margin-top:0px" class="_md">
        <md-card-title>
          <md-card-title-media style="width:100px;height:100px"> 
            <img style="border-radius: 5px;" src="https://demo.ciuis.com/uploads/images/n-img.jpg" alt="Avatar"></md-card-title-media>
            <md-card-title-text class="md-ml-20 xs-ml-20" style="margin-top: -10px;"> 
              <a class="md-headline cursor ng-binding" ng-click="ViewStaff(member.id)" ng-bind="member.name">Hamid</a> 
              <span class="md-subhead ng-binding" ng-bind="member.department"></span> 
              <span class="md-subhead"><a href="tel:4925011" class="ng-binding">4925011</a></span> 
              <span class="md-subhead ng-binding" ng-bind="member.email">hamid@interlinkpakistan.com</span> 
            </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="end center" class="layout-align-end-center layout-row">
          <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="ViewStaff(member.id)">View</button>
        </md-card-actions>
      </md-card>
    </div><!-- end ngRepeat: member in staff --><div class="col-md-4 ng-scope" ng-repeat="member in staff" style="padding: 0px;margin: 0px">
      <md-card md-theme-watch="" style="margin-top:0px" class="_md">
        <md-card-title>
          <md-card-title-media style="width:100px;height:100px"> 
            <img style="border-radius: 5px;" src="https://demo.ciuis.com/uploads/images/n-img.jpg" alt="Avatar"></md-card-title-media>
            <md-card-title-text class="md-ml-20 xs-ml-20" style="margin-top: -10px;"> 
              <a class="md-headline cursor ng-binding" ng-click="ViewStaff(member.id)" ng-bind="member.name">Gonzalo Gonzales</a> 
              <span class="md-subhead ng-binding" ng-bind="member.department"></span> 
              <span class="md-subhead"><a href="tel:59858562541" class="ng-binding">59858562541</a></span> 
              <span class="md-subhead ng-binding" ng-bind="member.email">g.gonzales@fabrica.com</span> 
            </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="end center" class="layout-align-end-center layout-row">
          <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="ViewStaff(member.id)">View</button>
        </md-card-actions>
      </md-card>
    </div><!-- end ngRepeat: member in staff --><div class="col-md-4 ng-scope" ng-repeat="member in staff" style="padding: 0px;margin: 0px">
      <md-card md-theme-watch="" style="margin-top:0px" class="_md">
        <md-card-title>
          <md-card-title-media style="width:100px;height:100px"> 
            <img style="border-radius: 5px;" src="https://demo.ciuis.com/uploads/images/n-img.jpg" alt="Avatar"></md-card-title-media>
            <md-card-title-text class="md-ml-20 xs-ml-20" style="margin-top: -10px;"> 
              <a class="md-headline cursor ng-binding" ng-click="ViewStaff(member.id)" ng-bind="member.name"></a> 
              <span class="md-subhead ng-binding" ng-bind="member.department"></span> 
              <span class="md-subhead"><a href="tel:" class="ng-binding"></a></span> 
              <span class="md-subhead ng-binding" ng-bind="member.email"></span> 
            </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="end center" class="layout-align-end-center layout-row">
          <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="ViewStaff(member.id)">View</button>
        </md-card-actions>
      </md-card>
    </div><!-- end ngRepeat: member in staff --><div class="col-md-4 ng-scope" ng-repeat="member in staff" style="padding: 0px;margin: 0px">
      <md-card md-theme-watch="" style="margin-top:0px" class="_md">
        <md-card-title>
          <md-card-title-media style="width:100px;height:100px"> 
            <img style="border-radius: 5px;" src="https://demo.ciuis.com/uploads/images/n-img.jpg" alt="Avatar"></md-card-title-media>
            <md-card-title-text class="md-ml-20 xs-ml-20" style="margin-top: -10px;"> 
              <a class="md-headline cursor ng-binding" ng-click="ViewStaff(member.id)" ng-bind="member.name">joaquim</a> 
              <span class="md-subhead ng-binding" ng-bind="member.department"></span> 
              <span class="md-subhead"><a href="tel:34323423" class="ng-binding">34323423</a></span> 
              <span class="md-subhead ng-binding" ng-bind="member.email">savesgospel@gmail.com</span> 
            </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="end center" class="layout-align-end-center layout-row">
          <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="ViewStaff(member.id)">View</button>
        </md-card-actions>
      </md-card>
    </div><!-- end ngRepeat: member in staff --><div class="col-md-4 ng-scope" ng-repeat="member in staff" style="padding: 0px;margin: 0px">
      <md-card md-theme-watch="" style="margin-top:0px" class="_md">
        <md-card-title>
          <md-card-title-media style="width:100px;height:100px"> 
            <img style="border-radius: 5px;" src="https://demo.ciuis.com/uploads/images/n-img.jpg" alt="Avatar"></md-card-title-media>
            <md-card-title-text class="md-ml-20 xs-ml-20" style="margin-top: -10px;"> 
              <a class="md-headline cursor ng-binding" ng-click="ViewStaff(member.id)" ng-bind="member.name">test</a> 
              <span class="md-subhead ng-binding" ng-bind="member.department"></span> 
              <span class="md-subhead"><a href="tel:123456789" class="ng-binding">123456789</a></span> 
              <span class="md-subhead ng-binding" ng-bind="member.email">test@tst.com</span> 
            </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="end center" class="layout-align-end-center layout-row">
          <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="ViewStaff(member.id)">View</button>
        </md-card-actions>
      </md-card>
    </div><!-- end ngRepeat: member in staff --><div class="col-md-4 ng-scope" ng-repeat="member in staff" style="padding: 0px;margin: 0px">
      <md-card md-theme-watch="" style="margin-top:0px" class="_md">
        <md-card-title>
          <md-card-title-media style="width:100px;height:100px"> 
            <img style="border-radius: 5px;" src="https://demo.ciuis.com/uploads/images/n-img.jpg" alt="Avatar"></md-card-title-media>
            <md-card-title-text class="md-ml-20 xs-ml-20" style="margin-top: -10px;"> 
              <a class="md-headline cursor ng-binding" ng-click="ViewStaff(member.id)" ng-bind="member.name">Manish chawda</a> 
              <span class="md-subhead ng-binding" ng-bind="member.department"></span> 
              <span class="md-subhead"><a href="tel:9899899890" class="ng-binding">9899899890</a></span> 
              <span class="md-subhead ng-binding" ng-bind="member.email">manish3chawda@gmail.com</span> 
            </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="end center" class="layout-align-end-center layout-row">
          <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="ViewStaff(member.id)">View</button>
        </md-card-actions>
      </md-card>
    </div><!-- end ngRepeat: member in staff --><div class="col-md-4 ng-scope" ng-repeat="member in staff" style="padding: 0px;margin: 0px">
      <md-card md-theme-watch="" style="margin-top:0px" class="_md">
        <md-card-title>
          <md-card-title-media style="width:100px;height:100px"> 
            <img style="border-radius: 5px;" src="https://demo.ciuis.com/uploads/images/n-img.jpg" alt="Avatar"></md-card-title-media>
            <md-card-title-text class="md-ml-20 xs-ml-20" style="margin-top: -10px;"> 
              <a class="md-headline cursor ng-binding" ng-click="ViewStaff(member.id)" ng-bind="member.name">Ulisses</a> 
              <span class="md-subhead ng-binding" ng-bind="member.department"></span> 
              <span class="md-subhead"><a href="tel:3592540066" class="ng-binding">3592540066</a></span> 
              <span class="md-subhead ng-binding" ng-bind="member.email">wesjacob29@gmail.com</span> 
            </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="end center" class="layout-align-end-center layout-row">
          <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="ViewStaff(member.id)">View</button>
        </md-card-actions>
      </md-card>
    </div><!-- end ngRepeat: member in staff --><div class="col-md-4 ng-scope" ng-repeat="member in staff" style="padding: 0px;margin: 0px">
      <md-card md-theme-watch="" style="margin-top:0px" class="_md">
        <md-card-title>
          <md-card-title-media style="width:100px;height:100px"> 
            <img style="border-radius: 5px;" src="https://demo.ciuis.com/uploads/images/n-img.jpg" alt="Avatar"></md-card-title-media>
            <md-card-title-text class="md-ml-20 xs-ml-20" style="margin-top: -10px;"> 
              <a class="md-headline cursor ng-binding" ng-click="ViewStaff(member.id)" ng-bind="member.name">Deo</a> 
              <span class="md-subhead ng-binding" ng-bind="member.department"></span> 
              <span class="md-subhead"><a href="tel:+243970755800" class="ng-binding">+243970755800</a></span> 
              <span class="md-subhead ng-binding" ng-bind="member.email">deobaiise@gmail.com</span> 
            </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="end center" class="layout-align-end-center layout-row">
          <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="ViewStaff(member.id)">View</button>
        </md-card-actions>
      </md-card>
    </div><!-- end ngRepeat: member in staff --><div class="col-md-4 ng-scope" ng-repeat="member in staff" style="padding: 0px;margin: 0px">
      <md-card md-theme-watch="" style="margin-top:0px" class="_md">
        <md-card-title>
          <md-card-title-media style="width:100px;height:100px"> 
            <img style="border-radius: 5px;" src="https://demo.ciuis.com/uploads/images/n-img.jpg" alt="Avatar"></md-card-title-media>
            <md-card-title-text class="md-ml-20 xs-ml-20" style="margin-top: -10px;"> 
              <a class="md-headline cursor ng-binding" ng-click="ViewStaff(member.id)" ng-bind="member.name">roberto</a> 
              <span class="md-subhead ng-binding" ng-bind="member.department"></span> 
              <span class="md-subhead"><a href="tel:1145382947" class="ng-binding">1145382947</a></span> 
              <span class="md-subhead ng-binding" ng-bind="member.email">mrobertofernandez@gmail.com</span> 
            </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="end center" class="layout-align-end-center layout-row">
          <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="ViewStaff(member.id)">View</button>
        </md-card-actions>
      </md-card>
    </div><!-- end ngRepeat: member in staff --><div class="col-md-4 ng-scope" ng-repeat="member in staff" style="padding: 0px;margin: 0px">
      <md-card md-theme-watch="" style="margin-top:0px" class="_md">
        <md-card-title>
          <md-card-title-media style="width:100px;height:100px"> 
            <img style="border-radius: 5px;" src="https://demo.ciuis.com/uploads/images/n-img.jpg" alt="Avatar"></md-card-title-media>
            <md-card-title-text class="md-ml-20 xs-ml-20" style="margin-top: -10px;"> 
              <a class="md-headline cursor ng-binding" ng-click="ViewStaff(member.id)" ng-bind="member.name">mauricio</a> 
              <span class="md-subhead ng-binding" ng-bind="member.department"></span> 
              <span class="md-subhead"><a href="tel:1149382947" class="ng-binding">1149382947</a></span> 
              <span class="md-subhead ng-binding" ng-bind="member.email">mrobertopaz@gmail.com</span> 
            </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="end center" class="layout-align-end-center layout-row">
          <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="ViewStaff(member.id)">View</button>
        </md-card-actions>
      </md-card>
    </div><!-- end ngRepeat: member in staff --><div class="col-md-4 ng-scope" ng-repeat="member in staff" style="padding: 0px;margin: 0px">
      <md-card md-theme-watch="" style="margin-top:0px" class="_md">
        <md-card-title>
          <md-card-title-media style="width:100px;height:100px"> 
            <img style="border-radius: 5px;" src="https://demo.ciuis.com/uploads/images/n-img.jpg" alt="Avatar"></md-card-title-media>
            <md-card-title-text class="md-ml-20 xs-ml-20" style="margin-top: -10px;"> 
              <a class="md-headline cursor ng-binding" ng-click="ViewStaff(member.id)" ng-bind="member.name">testee</a> 
              <span class="md-subhead ng-binding" ng-bind="member.department"></span> 
              <span class="md-subhead"><a href="tel:" class="ng-binding"></a></span> 
              <span class="md-subhead ng-binding" ng-bind="member.email">teste@teste.com.br</span> 
            </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="end center" class="layout-align-end-center layout-row">
          <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="ViewStaff(member.id)">View</button>
        </md-card-actions>
      </md-card>
    </div><!-- end ngRepeat: member in staff --><div class="col-md-4 ng-scope" ng-repeat="member in staff" style="padding: 0px;margin: 0px">
      <md-card md-theme-watch="" style="margin-top:0px" class="_md">
        <md-card-title>
          <md-card-title-media style="width:100px;height:100px"> 
            <img style="border-radius: 5px;" src="https://demo.ciuis.com/uploads/images/n-img.jpg" alt="Avatar"></md-card-title-media>
            <md-card-title-text class="md-ml-20 xs-ml-20" style="margin-top: -10px;"> 
              <a class="md-headline cursor ng-binding" ng-click="ViewStaff(member.id)" ng-bind="member.name">prova</a> 
              <span class="md-subhead ng-binding" ng-bind="member.department"></span> 
              <span class="md-subhead"><a href="tel:" class="ng-binding"></a></span> 
              <span class="md-subhead ng-binding" ng-bind="member.email">prova@pova.it</span> 
            </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="end center" class="layout-align-end-center layout-row">
          <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="ViewStaff(member.id)">View</button>
        </md-card-actions>
      </md-card>
    </div><!-- end ngRepeat: member in staff --><div class="col-md-4 ng-scope" ng-repeat="member in staff" style="padding: 0px;margin: 0px">
      <md-card md-theme-watch="" style="margin-top:0px" class="_md">
        <md-card-title>
          <md-card-title-media style="width:100px;height:100px"> 
            <img style="border-radius: 5px;" src="https://demo.ciuis.com/uploads/images/n-img.jpg" alt="Avatar"></md-card-title-media>
            <md-card-title-text class="md-ml-20 xs-ml-20" style="margin-top: -10px;"> 
              <a class="md-headline cursor ng-binding" ng-click="ViewStaff(member.id)" ng-bind="member.name">gh</a> 
              <span class="md-subhead ng-binding" ng-bind="member.department"></span> 
              <span class="md-subhead"><a href="tel:" class="ng-binding"></a></span> 
              <span class="md-subhead ng-binding" ng-bind="member.email">gh@qawqw.com</span> 
            </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="end center" class="layout-align-end-center layout-row">
          <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="ViewStaff(member.id)">View</button>
        </md-card-actions>
      </md-card>
    </div><!-- end ngRepeat: member in staff -->
  </div>

  <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-3 md-pl-0">
    <md-toolbar class="toolbar-white _md _md-toolbar-transitions">
      <div class="md-toolbar-tools">
        <h2 class="md-pl-10 md-truncate flex" flex="" md-truncate="">Departments</h2>
        <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" ng-click="NewDepartment()" aria-label="Department">
          
          <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-android-add text-muted"></i></md-icon>
        </button>
        <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" ng-click="Create()" aria-label="Create">
          
          <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-person-add text-muted"></i></md-icon>
        </button>
      </div>
    </md-toolbar>
    <md-content class="bg-white _md">
      <md-list flex="" class="md-p-0 sm-p-0 lg-p-0 flex" role="list">
        <!-- ngRepeat: department in departments --><md-list-item ng-repeat="department in departments" role="listitem" class="_md-button-wrap ng-scope _md md-clickable" tabindex="-1"><div class="md-button md-no-style"><button class="md-no-style md-button md-ink-ripple" type="button" ng-transclude="" ng-click="EditDepartment($index)" aria-label="Project"></button>   <div class="md-list-item-inner">
          <p><strong ng-bind="department.name" class="ng-binding">test</strong></p>
          <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" ng-click="DeleteDepartment($index)" aria-label="Create">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-trash-b text-muted"></i></md-icon>
          </button>
          <md-divider></md-divider>
        </div><div class="md-secondary-container"></div></div></md-list-item><!-- end ngRepeat: department in departments -->
      </md-list>
      <md-content ng-show="!departments.length" class="md-padding bg-white no-item-data _md ng-hide" aria-hidden="true">No matching items found</md-content>
    </md-content>
  </div>
 @endsection