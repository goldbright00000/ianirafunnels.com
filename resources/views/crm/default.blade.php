<html ng-app="Ciuis" lang="en_US" class="ng-scope">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <title>@yield('title')</title>


    <script src="https://ianirafunnels.com/public/crm/assets/js/angular.min.js"></script>
    <script src="https://ianirafunnels.com/public/crm/assets/js/angular-animate.min.js"></script>
    <script src="https://ianirafunnels.com/public/crm/assets/js/angular-aria.min.js"></script>
    <script src="https://ianirafunnels.com/public/crm/assets/js/angular-locale_en-us.js"></script>
    <link rel="stylesheet" type="text/css" href="https://ianirafunnels.com/public/crm/assets/css/ciuis.css">


   <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
   <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="{{ asset('public/crm/css/ciuis-app.css')}}"/>
   <link rel="stylesheet" type="text/css" href="{{ asset('public/crm/css/ionicons.min.css')}}" />
   <link rel="stylesheet" type="text/css" href="{{ asset('public/style/style1.css')}}" />

<link rel="shortcut icon" href="{{asset('public/crm/images/fab_icon.ico')}}" />

<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">


   <style type="text/css">.ico-ciuis-panel:before {padding-right: 44px;
    margin-left: -8px;
    content: "";
    background-image: url(/public/crm/images/panel.png) !important;
    width: 25px;
    height: 25px;
    position: relative;
    padding-bottom: 34px;
    background-repeat: no-repeat;}  


.ico-ciuis-cutomers:before{padding-right: 44px;
    margin-left: -8px;
    content: "";
    background-image: url(/public/crm/images/customers.png);
    width: 25px;
    height: 25px;
    position: relative;
   
    padding-bottom: 34px;
    background-repeat: no-repeat;}  
.ico-ciuis-leads:before{padding-right: 44px;
    margin-left: -8px;
    content: "";
    background-image: url(/public/crm/images/leads.png);
    width: 25px;
    height: 25px;
    position: relative;
    padding-bottom: 34px;
    background-repeat: no-repeat;}  
    
.ico-ciuis-projects:before{padding-right: 44px;
    margin-left: -8px;
    content: "";
    background-image: url(/public/crm/images/project.png);
    width: 25px;
    height: 25px;
    position: relative;
    padding-bottom: 34px;
    background-repeat: no-repeat;}  
    
    
.ico-ciuis-invoices:before{padding-right: 44px;
    margin-left: -8px;
    content: "";
    background-image: url(/public/crm/images/invoice.png);
    width: 25px;
    height: 25px;
    position: relative;
    padding-bottom: 34px;
    background-repeat: no-repeat;}  
    
    
    .ico-ciuis-proposals:before{padding-right: 44px;
    margin-left: -8px;
    content: "";
    background-image: url(/public/crm/images/proposal.png);
    width: 25px;
    height: 25px;
    position: relative;
    padding-bottom: 34px;
    background-repeat: no-repeat;}  
    
    
    .ico-ciuis-expenses:before{padding-right: 44px;
    margin-left: -8px;
    content: "";
    background-image: url(/public/crm/images/expenses.png);
    width: 25px;
    height: 25px;
    position: relative;
    padding-bottom: 34px;
    background-repeat: no-repeat;}  
    
        
    .ico-ciuis-staff:before{padding-right: 44px;
    margin-left: -8px;
    content: "";
    background-image: url(/public/crm/images/staff.png);
    width: 25px;
    height: 25px;
    position: relative;
    padding-bottom: 34px;
    background-repeat: no-repeat;}  
    
    .ico-ciuis-tickets:before{padding-right: 44px !important;
    margin-left: -8px!important;
    content: ""!important;
    background-image: url(../public/crm/images/tickets.png)!important;
    width: 25px!important;
    height: 25px!important;
    position: relative!important;
    padding-bottom: 34px!important;
    background-repeat: no-repeat!important;}  
    
    
    
    .ico-ciuis-teaches:before{padding-right: 44px;
    margin-left: -8px;
    content: "";
    background-image: url(/public/crm/images/teaches.png);
    width: 25px;
    height: 25px;
    position: relative;
    padding-bottom: 34px;
    background-repeat: no-repeat;} 




     </style>
    <script>
        var BASE_URL = "https://demo.ciuis.com/",
            update_error = "Error occured!, please contact support.",
            email_error = "Something wrong with your email SMTP settings",
            ACTIVESTAFF = "2",
            SHOW_ONLY_ADMIN = "true",
            CURRENCY = "USD",
            LOCATE_SELECTED = "en_US",
            UPIMGURL = "https://demo.ciuis.com/uploads/images/",
            NTFTITLE = "Notification",
            INVMARKCACELLED = "Invoice Cancelled!",
            TICKSTATUSCHANGE = "Ticket status changed!",
            LEADMARKEDAS = "Lead marked as",
            LEADUNMARKEDAS = "Lead unmarked as",
            TODAYDATE = "2019.04.01 ",
            LOGGEDINSTAFFID = "2",
            LOGGEDINSTAFFNAME = "Emma Durst",
            LOGGEDINSTAFFAVATAR = "tttttttt.jpg",
            VOICENOTIFICATIONLANG = "en-us",
            initialLocaleCode = "en";
        var new_item = "New";
        var item_unit = "Unit";
    </script>
 

</head>

<body ng-controller="Ciuis_Controller" class="ng-scope">
    <div id="ciuisloader" style="display: none;"></div>
    <md-toolbar class="toolbar-ciuis-top _md _md-toolbar-transitions">
        <div class="md-toolbar-tools">
        
            <div md-truncate="" class="crm-name crm-nm md-truncate"><span> <img src="/public/crm/images/ifunnel_logo_2.svg" width="200" /> </span></div>
            <button class="md-icon-button hidden-lg hidden-md md-button md-ink-ripple" type="button" ng-transclude="" ng-click="OpenMenu()" aria-label="Menu">
                <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><span class="ion-flag text-muted"></span></md-icon>
            </button>
          


            <ul flex="" class="ciuis-v3-menu hidden-xs flex">
 
    <li class="ng-scope" style="float: left !important;">
        <a href="#" class="seacrh_pos" aria-hidden="false"> 
            <input class="searchbar form-control" placeholder="Search Funnels, Contacts, Email and more...">
            <span class="searc_bar_icon"><i class="fa fa-search"></i></span>
         </a>
    </li>
  
    <li class="ng-scope">
        <a href="#" class="ng-binding" aria-hidden="false">IANIRA FUNNELS </a>
        <ul ng-show="nav.sub_menu.length" aria-hidden="false" class="">
            <li class="ng-scope">
                <a href="#"> 
                    <i class="icon ico-ciuis-invoices"></i> 
                    <span class="title ng-binding">Invoices</span> 
                    <span class="descr ng-binding">Manage Invoices</span> 
                </a>
            </li>
      
            <li>
                <a href="#"> <i class="icon ico-ciuis-proposals"></i> 
                    <span class="title ng-binding">Proposals</span> 
                    <span class="descr ng-binding">Manage Proposals</span> 
                </a>
            </li>
            <li ng-repeat="submenu in nav.sub_menu | orderBy:'order_id'" class="ng-scope">
                <a ng-href="https://demo.ciuis.com/expenses" href="https://demo.ciuis.com/expenses"> <i class="icon ico-ciuis-expenses"></i> <span class="title ng-binding" ng-bind="submenu.name">Expenses</span> <span class="descr ng-binding" ng-bind="submenu.description">Manage Expenses</span> </a>
            </li>
    
            <li ng-repeat="submenu in nav.sub_menu | orderBy:'order_id'" class="ng-scope">
                <a ng-href="https://demo.ciuis.com/accounts" href="https://demo.ciuis.com/accounts"> <i class="icon ico-ciuis-proposals"></i> <span class="title ng-binding" ng-bind="submenu.name">Accounts</span> <span class="descr ng-binding" ng-bind="submenu.description">Manage Accounts</span> </a>
            </li>
         
            <li ng-repeat="submenu in nav.sub_menu | orderBy:'order_id'" class="ng-scope">
                <a ng-href="https://demo.ciuis.com/orders" href="https://demo.ciuis.com/orders"> <i class="icon ico-ciuis-proposals"></i> <span class="title ng-binding" ng-bind="submenu.name">Orders</span> <span class="descr ng-binding" ng-bind="submenu.description">Manage Orders</span> </a>
            </li>
          
        </ul>
    </li>
  
    <li ng-repeat="nav in navbar  | orderBy:'order_id'" class="ng-scope"><a ng-show="(nav.url != '#') || (nav.sub_menu.length > 0)" href="#" ng-bind="nav.name" class="ng-binding" aria-hidden="false">  ACCOUNT DETAILS</a>
        <ul ng-show="nav.sub_menu.length" aria-hidden="false" class="">
         
            <li ng-repeat="submenu in nav.sub_menu | orderBy:'order_id'" class="ng-scope">
                <a ng-href="https://demo.ciuis.com/customers" href="https://demo.ciuis.com/customers"> <i class="icon ico-ciuis-proposals"></i> <span class="title ng-binding" ng-bind="submenu.name">Customers</span> <span class="descr ng-binding" ng-bind="submenu.description">Manage Customers</span> </a>
            </li>

            <li ng-repeat="submenu in nav.sub_menu | orderBy:'order_id'" class="ng-scope">
                <a ng-href="https://demo.ciuis.com/leads" href="https://demo.ciuis.com/leads"> <i class="icon ico-ciuis-leads"></i> <span class="title ng-binding" ng-bind="submenu.name">Leads</span> <span class="descr ng-binding" ng-bind="submenu.description">Manage Leads</span> </a>
            </li>
   
        </ul>
    </li>

   <!--  <li ng-repeat="nav in navbar  | orderBy:'order_id'" class="ng-scope"><a ng-show="(nav.url != '#') || (nav.sub_menu.length > 0)" href="#" ng-bind="nav.name" class="ng-binding" aria-hidden="false">Track</a>
        <ul ng-show="nav.sub_menu.length" aria-hidden="false" class="">
        
            <li ng-repeat="submenu in nav.sub_menu | orderBy:'order_id'" class="ng-scope">
                <a ng-href="https://demo.ciuis.com/projects" href="https://demo.ciuis.com/projects"><i class="far fa-file"></i> <span class="title ng-binding" ng-bind="submenu.name">Projects</span> <span class="descr ng-binding" ng-bind="submenu.description">Manage Projects</span> </a>
            </li>
        
            <li ng-repeat="submenu in nav.sub_menu | orderBy:'order_id'" class="ng-scope">
                <a ng-href="https://demo.ciuis.com/tasks" href="https://demo.ciuis.com/tasks"> <i class="far fa-file"></i> <span class="title ng-binding" ng-bind="submenu.name">Tasks</span> <span class="descr ng-binding" ng-bind="submenu.description">Manage Tasks</span> </a>
            </li>
         
            <li ng-repeat="submenu in nav.sub_menu | orderBy:'order_id'" class="ng-scope">
                <a ng-href="https://demo.ciuis.com/tickets" href="https://demo.ciuis.com/tickets"> <i class="fas fa-ticket-alt"></i><span class="title ng-binding" ng-bind="submenu.name">Tickets</span> <span class="descr ng-binding" ng-bind="submenu.description">Manage Tickets</span> </a>
            </li>
       
            <li ng-repeat="submenu in nav.sub_menu | orderBy:'order_id'" class="ng-scope">
                <a ng-href="https://demo.ciuis.com/products" href="https://demo.ciuis.com/products"> <i class="fa fa-cube"></i><span class="title ng-binding" ng-bind="submenu.name">Products</span> <span class="descr ng-binding" ng-bind="submenu.description">Manage Products</span> </a>
            </li>
            
        </ul>
    </li> -->
  
<!--     <li ng-repeat="nav in navbar  | orderBy:'order_id'" class="ng-scope"><a ng-show="(nav.url != '#') || (nav.sub_menu.length > 0)" href="#" ng-bind="nav.name" class="ng-binding" aria-hidden="false">Others</a>
        <ul ng-show="nav.sub_menu.length" aria-hidden="false" class="">
        
            <li ng-repeat="submenu in nav.sub_menu | orderBy:'order_id'" class="ng-scope">
                <a ng-href="https://demo.ciuis.com/staff" href="https://demo.ciuis.com/staff"><i class="fa fa-users"></i> <span class="title ng-binding" ng-bind="submenu.name">Staff</span> <span class="descr ng-binding" ng-bind="submenu.description">Manage Staff</span> </a>
            </li>
         
            <li ng-repeat="submenu in nav.sub_menu | orderBy:'order_id'" class="ng-scope">
                <a ng-href="https://demo.ciuis.com/report" href="https://demo.ciuis.com/report"> <i class="fas fa-chart-line"></i><span class="title ng-binding" ng-bind="submenu.name">Reports</span> <span class="descr ng-binding" ng-bind="submenu.description">Manage Reports</span> </a>
            </li>
           
            <li ng-repeat="submenu in nav.sub_menu | orderBy:'order_id'" class="ng-scope">
                <a ng-href="https://demo.ciuis.com/emails" href="https://demo.ciuis.com/emails"> <i class="fas fa-envelope-open"></i> <span class="title ng-binding" ng-bind="submenu.name">Email Templates</span> <span class="descr ng-binding" ng-bind="submenu.description">Manage Email Templates</span> </a>
            </li>
      
            <li class="ng-scope">
                <a href="#"> <i class="fas fa-clock"></i> <span class="title ng-binding" ng-bind="submenu.name">Timesheets</span> <span class="descr ng-binding" ng-bind="submenu.description">Manage Timesheets</span> </a>
            </li>
      
        </ul>
    </li> -->

  <!--   <li class="ng-scope"><a href="#" class="ng-binding" aria-hidden="false">Calendar</a>
        <ul aria-hidden="true" class="ng-hide">
         
        </ul>
    </li> -->
 
</ul>
         
           <!--  <button class="md-icon-button md-button md-ink-ripple" type="button" ng-click="searchNav()" aria-label="search">
                <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"> <i class="fas fa-search text-muted"></i></md-icon>
            </button> -->
           <!--  <div class="dropdown-container timer">
                <button class="md-icon-button dropdown-toggle md-button md-ink-ripple" type="button" ng-click="getTimer()" id="getTimer" data-toggle="dropdown" aria-label="search">
                    <md-icon class="ng-scope material-icons" role="img" aria-hidden="true">
                        <i class="fas fa-clock text-muted" id="timerStart"  aria-hidden="true"></i>
                        <i id="timerStarted" ng-hide="timer.start" class="ion-ios-clock text-success" aria-hidden="false"></i>
                    </md-icon>
                </button>
                <div class="dropdown-menu dropdown-menu-center layout-padding" role="menu" layout-padding="">
                 
                    <p ng-show="timer.found" class="text-muted bottom-border ng-hide" aria-hidden="true">No Timer Found!</p>
                    <p ng-show="timer.start" aria-hidden="true" class="ng-hide">
                        <button class="start md-button md-ink-ripple" type="button" ng-transclude="" ng-click="startTimer('start')">
                            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="fas fa-clock"></i></md-icon> Start</button>
                    </p>
                    <p ng-show="timer.stop" aria-hidden="true" class="ng-hide">
                        <a ng-show="timer.task_id" href="https://demo.ciuis.com/tasks/task/" class="assigned ng-hide" aria-hidden="true"><strong ng-bind="timer.task" class="ng-binding"></strong></a>
                        <a ng-show="!timer.task_id" class="label label-info assign" ng-click="stopTimerWithTask('assign')" aria-hidden="false">Assign Task &nbsp;&nbsp;<i class="ion-compose"></i></a>
                        <br>
                        <span class="text-muted">Started at: <span ng-bind="timer.started" class="ng-binding"></span></span>
                        <br>
                        <span class="time-captured">Total Time Captured: <span ng-bind="timer.total" class="ng-binding"></span></span>
                        <br>
                    </p>
                    <p ng-show="timer.stop" aria-hidden="true" class="ng-hide">
                        <button class="complete md-button md-ink-ripple" type="button" ng-transclude="" ng-click="startTimer('stop')">
                            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="mdi mdi-timer-off"></i></md-icon> Stop</button>
                    </p>
                    <p class="top-border">
                        <a class="cursor" title="View all timesheets" href="#">View all timesheets</a>
                    </p>
                </div>
            </div> -->

            <a class="md-icon-button md-button md-ink-ripple" href="#" aria-hidden="false">
                <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="fas fa-cog text-muted"></i></md-icon>
            </a>
            <button class="md-icon-button md-button md-ink-ripple" type="button" ng-click="Todo()" aria-label="Todo">
                <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="fas  fa-clipboard text-muted"></i></md-icon>
            </button>
            <button class="md-icon-button md-button md-ink-ripple" type="button" ng-click="Notifications()" aria-label="Notifications">

                <div ng-show="stats.newnotification == true" class="notify ng-scope ng-hide" aria-hidden="true"> <span class="heartbit"></span> <span class="point"></span> </div>
                <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="fas fa-bell text-muted"></i></md-icon>
            </button>
            <button class="md-icon-button avatar-button-ciuis md-button md-ink-ripple" type="button" ng-transclude="" ng-click="Profile()" aria-label="User Profile"> <img height="100%" class="md-avatar ng-scope" alt="Lance Bogrol" ng-src="https://demo.ciuis.com/uploads/images/pm2.jpg" src="https://demo.ciuis.com/uploads/images/pm2.jpg"> </button>
            <div ng-click="Profile()" md-truncate="" class="user-informations hidden-xs md-truncate" role="button" tabindex="0"> <span class="user-name-in ng-binding" ng-bind="user.name"></span>
                <br>
                <span class="user-email-in">ianira@example.com</span> </div>
        </div>
    </md-toolbar>
    <md-content id="mobile-menu" class="_md" style="left: 0px; opacity: 1; display: none">
        <md-toolbar class="toolbar-white _md _md-toolbar-transitions">
            <div class="md-toolbar-tools">
                <div flex="" md-truncate="" class="crm-name md-truncate flex"><span ng-bind="settings.crm_name" class="ng-binding"></span></div>
                <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" ng-click="close()" aria-label="Close">
                    <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-close-circled text-muted"></i></md-icon>
                </button>
            </div>
        </md-toolbar>
        <md-content class="mobile-menu-box bg-white _md">
            <div class="mobile-menu-wrapper-inner">
                <div class="mobile-menu-wrapper">
                    <div class="mobile-menu-slider" style="left: 0px;">
                        <div class="mobile-menu">
                      
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </md-content>
    </md-content>
    <header id="mainHeader" role="banner" class="hidden-xs">
        <nav role="navigation">
            <div class="top-header">
                <div class="navBurger">
                    <a href="#"><img class="transform_logo animated rotateIn" width="34px" height="34px"  src="/public/crm/images/brand.png"></a>
                </div>
            </div>
            <ul id="menu-vertical-menu icon" class="nav">
               
                 <li class="material-icons ico-ciuis-panel">
                    <a href="{{url('crm/home')}}" class="ng-binding">Panel</a>
                </li>

                <li class="material-icons ico-ciuis-customers">
                    <a href="{{url('crm/customers')}}" class="ng-binding">Customers</a>
                </li>
                <li class="material-icons ico-ciuis-leads">
                    <a href="{{url('crm/leads')}}" class="ng-binding">User Activity </a>
                </li>

                <li class="material-icons ico-ciuis-leads">
                    <a href="{{url('crm/leads')}}" class="ng-binding">Leads</a>
                </li>
                 <li class="material-icons ico-ciuis-projects">
                    <a href="{{url('crm/projects')}}" class="ng-binding">Projects</a>
                </li>
                
                <li class="material-icons ico-ciuis-invoices">
                    <a href="{{url('crm/invoices')}}" class="ng-binding">Invoices</a>
                </li>
                <li class="material-icons ico-ciuis-proposals">
                    <a href="{{url('crm/proposals')}}" class="ng-binding">Proposals</a>
                </li>
                 <li class="material-icons ico-ciuis-expenses">
                    <a href="{{url('crm/expenses')}}" class="ng-binding">Expenses</a>
                </li>
                
                 <li class="material-icons ico-ciuis-staff">
                    <a href="{{url('crm/staff')}}" class="ng-binding">Staff</a>
                </li>
               <li class="material-icons ico-ciuis-tickets">
                    <a href="{{url('crm/tickets')}}" class="ng-binding">Tickets</a>
                </li>
            <li class="material-icons ico-ciuis-teaches">
                    <a href="{{url('crm/task')}}" class="ng-binding">Task</a>
                </li>
                
            </ul>
        </nav>
    </header>


    <md-sidenav class="md-sidenav-left md-whiteframe-4dp md-closed ng-isolate-scope _md" md-component-id="PickUpTo" tabindex="-1"></md-sidenav>
    <md-sidenav class="md-sidenav-right md-whiteframe-4dp md-closed ng-isolate-scope _md" md-component-id="SetOnsiteVisit" tabindex="-1">
        <md-toolbar class="md-theme-light _md _md-toolbar-transitions" style="background:#262626">
            <div class="md-toolbar-tools">
                <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" ng-click="close()" aria-label="Close"> <i class="ion-android-arrow-forward ng-scope"></i></button>
                <md-truncate ng-bind="lang.set_onsite_visit" class="ng-binding md-truncate"></md-truncate>
            </div>
        </md-toolbar>
        <md-content layout-padding="" class="layout-padding _md">
            <md-content layout-padding="" class="layout-padding _md">
                <md-input-container class="md-block">
                    <label ng-bind="lang.title" class="ng-binding" for="input_7"></label>
                    <input ng-model="onsite_visit.title" class="ng-pristine ng-untouched ng-valid md-input ng-empty" id="input_7" aria-invalid="false">
                    <div class="md-errors-spacer"></div>
                </md-input-container>
                <md-input-container class="md-block md-input-has-placeholder flex-gt-xs" flex-gt-xs="">
                    <label ng-bind="lang.customer" class="ng-binding md-required md-placeholder" for="select_8"></label>
                    <md-select required="" placeholder="" ng-model="onsite_visit.customer_id" style="min-width: 200px;" aria-label="Customer" class="ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required" tabindex="0" aria-disabled="false" role="listbox" aria-expanded="false" aria-multiselectable="false" id="select_8" aria-required="true" aria-invalid="true">
                        <md-select-value class="md-select-value md-select-placeholder" id="select_value_label_0"><span></span><span class="md-select-icon" aria-hidden="true"></span></md-select-value>
                        <div class="md-select-menu-container" aria-hidden="true" role="presentation" id="select_container_9">
                            <md-select-menu role="presentation" class="_md">
                                <md-content class="_md">
                           
                                </md-content>
                            </md-select-menu>
                        </div>
                    </md-select>
                </md-input-container>
                <md-input-container class="md-block md-input-has-placeholder">
                    <label ng-bind="lang.assigned" class="ng-binding md-placeholder" for="select_10"></label>
                    <md-select placeholder="" ng-model="onsite_visit.staff_id" style="min-width: 200px;" aria-label="Staff" class="ng-pristine ng-untouched ng-valid ng-empty" tabindex="0" aria-disabled="false" role="listbox" aria-expanded="false" aria-multiselectable="false" id="select_10" aria-invalid="false">
                        <md-select-value class="md-select-value md-select-placeholder" id="select_value_label_1"><span></span><span class="md-select-icon" aria-hidden="true"></span></md-select-value>
                        <div class="md-select-menu-container" aria-hidden="true" role="presentation" id="select_container_11">
                            <md-select-menu role="presentation" class="_md">
                                <md-content class="_md">
                              
                                </md-content>
                            </md-select-menu>
                        </div>
                    </md-select>
                </md-input-container>
                <br>
                <md-input-container class="md-block md-input-has-placeholder">
                    <label ng-bind="lang.start" class="ng-binding" for="datetime"></label>
                    <input mdc-datetime-picker="" date="true" time="true" type="text" id="datetime" placeholder="" show-todays-date="" minutes="true" min-date="date" show-icon="true" ng-model="onsite_visit.start" class="dtp-no-msclear dtp-input md-input ng-pristine ng-untouched ng-valid ng-isolate-scope ng-empty" aria-invalid="false">
                    <div class="md-errors-spacer"></div>
                    <button class="dtp-btn-calendar md-icon-button md-button ng-scope md-ink-ripple" type="button" ng-transclude="" tabindex="-1" aria-hidden="true" ng-click="openCalendarDiag($event)">
                        <md-icon aria-label="md-calendar" md-svg-src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCIgdmlld0JveD0iMCAwIDI0IDI0Ij48cGF0aCBkPSJNMTkgM2gtMVYxaC0ydjJIOFYxSDZ2Mkg1Yy0xLjExIDAtMS45OS45LTEuOTkgMkwzIDE5YzAgMS4xLjg5IDIgMiAyaDE0YzEuMSAwIDItLjkgMi0yVjVjMC0xLjEtLjktMi0yLTJ6bTAgMTZINVY4aDE0djExek03IDEwaDV2NUg3eiIvPjwvc3ZnPg==" class="ng-scope" role="img">
                            <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 24 24" fit="" preserveAspectRatio="xMidYMid meet" focusable="false">
                                <path d="M19 3h-1V1h-2v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 16H5V8h14v11zM7 10h5v5H7z"></path>
                            </svg>
                        </md-icon>
                    </button>
                    <button class="md-icon-button dtp-clear md-button ng-scope md-ink-ripple ng-hide" type="button" ng-transclude="" ng-show="currentDate" aria-hidden="true" ng-click="clear()">✕</button>
                </md-input-container>
                <md-input-container class="md-block md-input-has-placeholder">
                    <label ng-bind="lang.end" class="ng-binding" for="datetime"></label>
                    <input mdc-datetime-picker="" date="true" time="true" type="text" id="datetime" placeholder="" show-todays-date="" minutes="true" min-date="date" show-icon="true" ng-model="onsite_visit.end" class="dtp-no-msclear dtp-input md-input ng-pristine ng-untouched ng-valid ng-isolate-scope ng-empty" aria-invalid="false">
                    <div class="md-errors-spacer"></div>
                    <button class="dtp-btn-calendar md-icon-button md-button ng-scope md-ink-ripple" type="button" ng-transclude="" tabindex="-1" aria-hidden="true" ng-click="openCalendarDiag($event)">
                        <md-icon aria-label="md-calendar" md-svg-src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCIgdmlld0JveD0iMCAwIDI0IDI0Ij48cGF0aCBkPSJNMTkgM2gtMVYxaC0ydjJIOFYxSDZ2Mkg1Yy0xLjExIDAtMS45OS45LTEuOTkgMkwzIDE5YzAgMS4xLjg5IDIgMiAyaDE0YzEuMSAwIDItLjkgMi0yVjVjMC0xLjEtLjktMi0yLTJ6bTAgMTZINVY4aDE0djExek03IDEwaDV2NUg3eiIvPjwvc3ZnPg==" class="ng-scope" role="img">
                            <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 24 24" fit="" preserveAspectRatio="xMidYMid meet" focusable="false">
                                <path d="M19 3h-1V1h-2v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 16H5V8h14v11zM7 10h5v5H7z"></path>
                            </svg>
                        </md-icon>
                    </button>
                    <button class="md-icon-button dtp-clear md-button ng-scope md-ink-ripple ng-hide" type="button" ng-transclude="" ng-show="currentDate" aria-hidden="true" ng-click="clear()">✕</button>
                </md-input-container>
                <md-input-container class="md-block md-input-has-placeholder">
                    <label ng-bind="lang.description" class="ng-binding md-required" for="input_12"></label>
                    <div class="md-resize-wrapper">
                        <textarea required="" ng-model="onsite_visit.description" placeholder="Type Something..." class="form-control note-description ng-pristine ng-untouched md-input ng-empty ng-invalid ng-invalid-required" id="input_12" rows="1" aria-invalid="true" style="height: 0px;"></textarea>
                        <div class="md-resize-handle" style="touch-action: pan-x;"></div>
                    </div>
                    <div class="md-errors-spacer"></div>
                </md-input-container>
                <div class="pull-right">
                    <button class="md-button ng-binding md-ink-ripple" type="button" ng-transclude="" ng-click="AddOnsiteVisit()" ng-bind="lang.set" aria-label="Add Onsite Visit"></button>
                </div>
            </md-content>
        </md-content>
    </md-sidenav>

    <md-sidenav class="md-sidenav-right md-whiteframe-5dp md-closed ng-isolate-scope _md" md-component-id="searchNav" md-disable-close-events="" style="width: 650px;" tabindex="-1">
        <md-toolbar class="md-theme-light _md _md-toolbar-transitions" style="background:#262626">
            <div class="md-toolbar-tools">
                <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" ng-click="close()" aria-label="Close"> <i class="ion-android-arrow-forward ng-scope"></i></button>
                <md-truncate ng-bind="lang.search" class="ng-binding md-truncate"></md-truncate>
            </div>
        </md-toolbar>
        <md-content class="_md">
            <br>
            <md-input-container class="md-block" style="margin-bottom: unset;">
                <label for="input_13">Search Here...</label>
                <input ng-submit="searchInput(search_input)" name="search" ng-model="search_input" ng-keyup="searchInput(search_input)" class="ng-pristine ng-untouched ng-valid md-input ng-empty" id="input_13" aria-invalid="false">
                <div class="md-errors-spacer"></div>
            </md-input-container>
            <p class="text-center text-muted ng-hide" ng-show="searchResult == 1" aria-hidden="true">No result found!</p>
            <p class="text-center text-muted ng-hide" ng-show="searchInputMsg == 1" aria-hidden="true">Type something to search...</p>
            <div ng-show="searchLoader == 1" aria-hidden="true" class="ng-hide">
                <md-progress-circular md-mode="indeterminate" md-diameter="20" style="margin-left: auto; margin-right: auto; width: 20px; height: 20px;" aria-valuemin="0" aria-valuemax="100" role="progressbar" class="ng-isolate-scope md-mode-indeterminate">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" style="width: 20px; height: 20px; transform-origin: 10px 10px 10px;">
                        <path fill="none" stroke-width="2" stroke-linecap="square" d="M10,1A9,9 0 1 1 1,10" stroke-dasharray="42.411500823462205" stroke-dashoffset="120.47991806201448" transform="rotate(-270 10 10)"></path>
                    </svg>
                </md-progress-circular>
                <p class="text-center">
                    <strong>Searching</strong>
                </p>
            </div>
            <section ng-show="searchStaff.length > 0" aria-hidden="true" class="ng-hide">
                <div class="md-accent md-subheader _md" role="heading" aria-level="2" style="position: sticky; top: 0px; z-index: 2;">
                    <div class="md-subheader-inner">
                        <div class="md-subheader-content"><span class="material-icons ico-ciuis-staff search-icon pull-right ng-scope"></span> Staff Members</div>
                    </div>
                </div>
                <md-list layout-padding="" class="layout-padding" role="list">
                    <!-- ngRepeat: staff in searchStaff -->
                </md-list>
            </section>
            <section ng-show="searchProjects.length > 0" aria-hidden="true" class="ng-hide">
                <div class="md-accent md-subheader _md" role="heading" aria-level="2" style="position: sticky; top: 0px; z-index: 2;">
                    <div class="md-subheader-inner">
                        <div class="md-subheader-content">
                            <span class="material-icons ico-ciuis-projects search-icon pull-right ng-scope"></span> Projects </div>
                    </div>
                </div>
                <md-list layout-padding="" class="layout-padding" role="list">
             
                </md-list>
            </section>
            <section ng-show="searchInvoices.length > 0" aria-hidden="true" class="ng-hide">
                <div class="md-accent md-subheader _md" role="heading" aria-level="2" style="position: sticky; top: 0px; z-index: 2;">
                    <div class="md-subheader-inner">
                        <div class="md-subheader-content"><span class="material-icons ico-ciuis-invoices search-icon pull-right ng-scope"></span> Invoices</div>
                    </div>
                </div>
                <md-list layout-padding="" class="layout-padding" role="list">
    
                </md-list>
            </section>
            <section ng-show="searchProposals.length > 0" aria-hidden="true" class="ng-hide">
                <div class="md-accent md-subheader _md" role="heading" aria-level="2" style="position: sticky; top: 0px; z-index: 2;">
                    <div class="md-subheader-inner">
                        <div class="md-subheader-content"><span class="material-icons ico-ciuis-proposals search-icon pull-right ng-scope"></span> Proposals</div>
                    </div>
                </div>
                <md-list layout-padding="" class="layout-padding" role="list">
            
                </md-list>
            </section>
            <section ng-show="searchCustomers.length > 0" aria-hidden="true" class="ng-hide">
                <div class="md-accent md-subheader _md" role="heading" aria-level="2" style="position: sticky; top: 0px; z-index: 2;">
                    <div class="md-subheader-inner">
                        <div class="md-subheader-content"><span class="material-icons ico-ciuis-customers search-icon pull-right ng-scope"></span> Customers</div>
                    </div>
                </div>
                <md-list layout-padding="" class="layout-padding" role="list">
      
                </md-list>
            </section>
            <section ng-show="searchLeads.length > 0" aria-hidden="true" class="ng-hide">
                <div class="md-accent md-subheader _md" role="heading" aria-level="2" style="position: sticky; top: 0px; z-index: 2;">
                    <div class="md-subheader-inner">
                        <div class="md-subheader-content"><span class="material-icons ico-ciuis-leads search-icon pull-right ng-scope"></span> Leads</div>
                    </div>
                </div>
                <md-list layout-padding="" class="layout-padding" role="list">
                   
                </md-list>
            </section>
            <section ng-show="searchExpenses.length > 0" aria-hidden="true" class="ng-hide">
                <div class="md-accent md-subheader _md" role="heading" aria-level="2" style="position: sticky; top: 0px; z-index: 2;">
                    <div class="md-subheader-inner">
                        <div class="md-subheader-content"><span class="material-icons ico-ciuis-expenses search-icon pull-right ng-scope"></span> Expenses</div>
                    </div>
                </div>
                <md-list layout-padding="" class="layout-padding" role="list">
                   
                </md-list>
            </section>
            <section ng-show="searchProducts.length > 0" aria-hidden="true" class="ng-hide">
                <div class="md-accent md-subheader _md" role="heading" aria-level="2" style="position: sticky; top: 0px; z-index: 2;">
                    <div class="md-subheader-inner">
                        <div class="md-subheader-content"><span class="material-icons ico-ciuis-products search-icon pull-right ng-scope"></span> Products</div>
                    </div>
                </div>
                <md-list layout-padding="" class="layout-padding" role="list">
       
                </md-list>
            </section>
            <section ng-show="searchTickets.length > 0" aria-hidden="true" class="ng-hide">
                <div class="md-accent md-subheader _md" role="heading" aria-level="2" style="position: sticky; top: 0px; z-index: 2;">
                    <div class="md-subheader-inner">
                        <div class="md-subheader-content"><span class="material-icons ico-ciuis-supports search-icon pull-right ng-scope"></span> Tickets</div>
                    </div>
                </div>
                <md-list layout-padding="" class="layout-padding" role="list">
            
                </md-list>
            </section>
            <section ng-show="searchTasks.length > 0" aria-hidden="true" class="ng-hide">
                <div class="md-accent md-subheader _md" role="heading" aria-level="2" style="position: sticky; top: 0px; z-index: 2;">
                    <div class="md-subheader-inner">
                        <div class="md-subheader-content"><span class="material-icons ico-ciuis-tasks search-icon pull-right ng-scope"></span> Tasks</div>
                    </div>
                </div>
                <md-list layout-padding="" class="layout-padding" role="list">
           
                </md-list>
            </section>
            <section ng-show="searchOrders.length > 0" aria-hidden="true" class="ng-hide">
                <div class="md-accent md-subheader _md" role="heading" aria-level="2" style="position: sticky; top: 0px; z-index: 2;">
                    <div class="md-subheader-inner">
                        <div class="md-subheader-content"><span class="material-icons ico-ciuis-orders search-icon pull-right ng-scope"></span> Orders</div>
                    </div>
                </div>
                <md-list layout-padding="" class="layout-padding" role="list">
  
                </md-list>
            </section>
        </md-content>
    </md-sidenav>
    <md-sidenav class="md-sidenav-right md-whiteframe-4dp md-closed ng-isolate-scope _md" md-component-id="EventForm" tabindex="-1">
        <md-toolbar class="md-theme-light _md _md-toolbar-transitions" style="background:#262626">
            <div class="md-toolbar-tools">
                <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" ng-click="close()" aria-label="Close"> <i class="ion-android-arrow-forward ng-scope"></i> </button>
                <md-truncate ng-bind="lang.addevent" class="ng-binding md-truncate"></md-truncate>
            </div>
        </md-toolbar>
        <md-content layout-padding="" class="layout-padding _md">
            <md-content layout-padding="" class="layout-padding _md">
                <md-input-container class="md-block">
                    <label ng-bind="lang.title" class="ng-binding" for="input_14"></label>
                    <input ng-model="event_title" class="ng-pristine ng-untouched ng-valid md-input ng-empty" id="input_14" aria-invalid="false">
                    <div class="md-errors-spacer"></div>
                </md-input-container>
                <md-input-container class="md-block md-input-has-placeholder">
                    <label ng-bind="lang.start" class="ng-binding" for="datetime"></label>
                    <input mdc-datetime-picker="" date="true" time="true" type="text" id="datetime" placeholder="" show-todays-date="" minutes="true" min-date="date" show-icon="true" ng-model="event_start" class="dtp-no-msclear dtp-input md-input ng-pristine ng-untouched ng-valid ng-isolate-scope ng-empty" aria-invalid="false">
                    <div class="md-errors-spacer"></div>
                    <button class="dtp-btn-calendar md-icon-button md-button ng-scope md-ink-ripple" type="button" ng-transclude="" tabindex="-1" aria-hidden="true" ng-click="openCalendarDiag($event)">
                        <md-icon aria-label="md-calendar" md-svg-src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCIgdmlld0JveD0iMCAwIDI0IDI0Ij48cGF0aCBkPSJNMTkgM2gtMVYxaC0ydjJIOFYxSDZ2Mkg1Yy0xLjExIDAtMS45OS45LTEuOTkgMkwzIDE5YzAgMS4xLjg5IDIgMiAyaDE0YzEuMSAwIDItLjkgMi0yVjVjMC0xLjEtLjktMi0yLTJ6bTAgMTZINVY4aDE0djExek03IDEwaDV2NUg3eiIvPjwvc3ZnPg==" class="ng-scope" role="img">
                            <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 24 24" fit="" preserveAspectRatio="xMidYMid meet" focusable="false">
                                <path d="M19 3h-1V1h-2v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 16H5V8h14v11zM7 10h5v5H7z"></path>
                            </svg>
                        </md-icon>
                    </button>
                    <button class="md-icon-button dtp-clear md-button ng-scope md-ink-ripple ng-hide" type="button" ng-transclude="" ng-show="currentDate" aria-hidden="true" ng-click="clear()">✕</button>
                </md-input-container>
                <md-input-container class="md-block">
                    <label ng-bind="lang.end" class="ng-binding" for="datetime"></label>
                    <input mdc-datetime-picker="" date="true" time="true" type="text" id="datetime" minutes="true" min-date="date" show-icon="true" ng-model="event_end" class="dtp-no-msclear dtp-input md-input ng-pristine ng-untouched ng-valid ng-isolate-scope ng-empty" aria-invalid="false">
                    <div class="md-errors-spacer"></div>
                    <button class="dtp-btn-calendar md-icon-button md-button ng-scope md-ink-ripple" type="button" ng-transclude="" tabindex="-1" aria-hidden="true" ng-click="openCalendarDiag($event)">
                        <md-icon aria-label="md-calendar" md-svg-src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCIgdmlld0JveD0iMCAwIDI0IDI0Ij48cGF0aCBkPSJNMTkgM2gtMVYxaC0ydjJIOFYxSDZ2Mkg1Yy0xLjExIDAtMS45OS45LTEuOTkgMkwzIDE5YzAgMS4xLjg5IDIgMiAyaDE0YzEuMSAwIDItLjkgMi0yVjVjMC0xLjEtLjktMi0yLTJ6bTAgMTZINVY4aDE0djExek03IDEwaDV2NUg3eiIvPjwvc3ZnPg==" class="ng-scope" role="img">
                            <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 24 24" fit="" preserveAspectRatio="xMidYMid meet" focusable="false">
                                <path d="M19 3h-1V1h-2v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 16H5V8h14v11zM7 10h5v5H7z"></path>
                            </svg>
                        </md-icon>
                    </button>
                    <button class="md-icon-button dtp-clear md-button ng-scope md-ink-ripple ng-hide" type="button" ng-transclude="" ng-show="currentDate" aria-hidden="true" ng-click="clear()">✕</button>
                </md-input-container>
                <md-input-container class="md-block md-input-has-placeholder">
                    <label ng-bind="lang.description" class="ng-binding md-required" for="input_15"></label>
                    <div class="md-resize-wrapper">
                        <textarea required="" name="detail" ng-model="event_detail" placeholder="Type Something..." class="form-control note-description ng-pristine ng-untouched md-input ng-empty ng-invalid ng-invalid-required" id="input_15" rows="1" aria-invalid="true" style="height: 0px;"></textarea>
                        <div class="md-resize-handle" style="touch-action: pan-x;"></div>
                    </div>
                    <div class="md-errors-spacer"></div>
                </md-input-container>
                <div class="ciuis-body-checkbox has-primary pull-left">
                    <input ng-model="event_public" name="public" class="ci-public-check ng-pristine ng-untouched ng-valid ng-empty" id="public" type="checkbox" value="1" aria-invalid="false">
                    <label for="public" ng-bind="lang.publicevent" class="ng-binding"></label>
                </div>
                <div class="pull-right">
                    <button class="md-button ng-binding md-ink-ripple" type="button" ng-transclude="" ng-click="AddEvent()" ng-bind="lang.addevent" aria-label="Add Event"></button>
                </div>
            </md-content>
        </md-content>
    </md-sidenav>
    <md-sidenav class="md-sidenav-right md-whiteframe-4dp md-closed ng-isolate-scope _md" md-component-id="Todo" tabindex="-1">
        <md-content layout-padding="" class="layout-padding _md">
            <md-content layout-padding="" class="layout-padding _md">
                <md-input-container class="md-icon-float md-icon-right md-block">
                    <label ng-click="delegateClick()" tabindex="-1" class="ng-scope" role="button" for="input_16">Type an to do!</label>
                    <input ng-model="tododetail" class="tododetail ng-pristine ng-untouched ng-valid md-input ng-empty" id="input_16" aria-invalid="false">
                    <div class="md-errors-spacer"></div>
                    <md-icon ng-show="addingTodo == true" class="material-icons ng-hide" aria-label="Add Todo" aria-hidden="true">
                        <md-progress-circular md-mode="indeterminate" md-diameter="18" aria-valuemin="0" aria-valuemax="100" role="progressbar" class="ng-isolate-scope md-mode-indeterminate" style="width: 18px; height: 18px;">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18" style="width: 18px; height: 18px; transform-origin: 9px 9px 9px;">
                                <path fill="none" stroke-width="1.8" stroke-linecap="square" d="M9,0.9A8.1,8.1 0 1 1 0.9,9" stroke-dasharray="38.17035074111598" stroke-dashoffset="112.89066118663722" transform="rotate(-270 9 9)"></path>
                            </svg>
                        </md-progress-circular>
                    </md-icon>
                    <md-icon ng-hide="addingTodo == true" aria-label="Add Todo" ng-click="AddTodo()" class="ion-ios-checkmark text-success material-icons" role="button" tabindex="0" aria-hidden="false"></md-icon>
                </md-input-container>
                <h4 md-truncate="" class="text-muted text-uppercase md-truncate"><strong ng-bind="lang.new" class="ng-binding"></strong></h4>
                <md-content layout-padding="" class="layout-padding _md">
                    <ul class="todo-item">
       
                    </ul>
                </md-content>
                <h4 md-truncate="" class="text-success md-truncate"><strong ng-bind="lang.donetodo" class="ng-binding"></strong></h4>
                <md-content layout-padding="" class="layout-padding _md">
                    <ul class="todo-item-done">
            
                    </ul>
                </md-content>
            </md-content>
        </md-content>
    </md-sidenav>
    <md-sidenav class="md-sidenav-right md-whiteframe-4dp md-closed ng-isolate-scope _md" md-component-id="Notifications" tabindex="-1">
        <md-toolbar class="toolbar-white _md _md-toolbar-transitions">
            <div class="md-toolbar-tools">
                <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" ng-click="close()" aria-label="Close"><i class="ion-android-arrow-forward ng-scope"></i></button>
                <md-truncate ng-bind="lang.notifications" class="ng-binding md-truncate"></md-truncate>
                <button class="md-mini md-button md-ink-ripple" type="button" ng-transclude="" aria-label="Undread Notifications"><span ng-bind="stats.tbs" class="ng-binding ng-scope"></span></button>
            </div>
        </md-toolbar>
        <md-content class="_md">
            <md-list flex="" role="list" class="flex">
            
            </md-list>
        </md-content>
    </md-sidenav>
    <md-sidenav class="md-sidenav-right md-whiteframe-4dp md-closed ng-isolate-scope _md" md-component-id="Profile" tabindex="-1">
        <md-content class="_md">
            <md-tabs md-dynamic-height="" md-border-bottom="" class="ng-isolate-scope md-dynamic-height">
                <md-tabs-wrapper>
                    <md-tab-data>
                        <md-tab label="Profile" class="ng-scope ng-isolate-scope"></md-tab>
                        <md-tab label="Onsite Visits" class="ng-scope ng-isolate-scope"></md-tab>
                    </md-tab-data>
              
                    <md-tabs-canvas ng-focus="$mdTabsCtrl.redirectFocus()" ng-class="{ 'md-paginated': $mdTabsCtrl.shouldPaginate, 'md-center-tabs': $mdTabsCtrl.shouldCenterTabs }" ng-keydown="$mdTabsCtrl.keydown($event)">
                        <md-pagination-wrapper ng-class="{ 'md-center-tabs': $mdTabsCtrl.shouldCenterTabs }" md-tab-scroll="$mdTabsCtrl.scroll($event)" role="tablist" style="transform: translate3d(0px, 0px, 0px);">
           
                            <md-tab-item tabindex="0" class="md-tab ng-scope ng-isolate-scope md-ink-ripple md-active" ng-repeat="tab in $mdTabsCtrl.tabs" role="tab" id="tab-item-17" md-tab-id="17" aria-selected="true" aria-disabled="false" ng-click="$mdTabsCtrl.select(tab.getIndex())" ng-focus="$mdTabsCtrl.hasFocus = true" ng-blur="$mdTabsCtrl.hasFocus = false" ng-class="{ 'md-active':    tab.isActive(), 'md-focused':   tab.hasFocus(), 'md-disabled':  tab.scope.disabled }" ng-disabled="tab.scope.disabled" md-swipe-left="$mdTabsCtrl.nextPage()" md-swipe-right="$mdTabsCtrl.previousPage()" md-tabs-template="::tab.label" md-scope="::tab.parent" aria-controls="tab-content-17">Profile</md-tab-item>
                   
                            <md-tab-item tabindex="-1" class="md-tab ng-scope ng-isolate-scope md-ink-ripple" ng-repeat="tab in $mdTabsCtrl.tabs" role="tab" id="tab-item-18" md-tab-id="18" aria-selected="false" aria-disabled="false" ng-click="$mdTabsCtrl.select(tab.getIndex())" ng-focus="$mdTabsCtrl.hasFocus = true" ng-blur="$mdTabsCtrl.hasFocus = false" ng-class="{ 'md-active':    tab.isActive(), 'md-focused':   tab.hasFocus(), 'md-disabled':  tab.scope.disabled }" ng-disabled="tab.scope.disabled" md-swipe-left="$mdTabsCtrl.nextPage()" md-swipe-right="$mdTabsCtrl.previousPage()" md-tabs-template="::tab.label" md-scope="::tab.parent" aria-controls="tab-content-18">Onsite Visits</md-tab-item>
          
                            <md-ink-bar></md-ink-bar>
                        </md-pagination-wrapper>
                        <md-tabs-dummy-wrapper aria-hidden="true" class="md-visually-hidden md-dummy-wrapper">
          
                            <md-dummy-tab class="md-tab ng-scope ng-isolate-scope" tabindex="-1" ng-repeat="tab in $mdTabsCtrl.tabs" md-tabs-template="::tab.label" md-scope="::tab.parent">Profile</md-dummy-tab>

                            <md-dummy-tab class="md-tab ng-scope ng-isolate-scope" tabindex="-1" ng-repeat="tab in $mdTabsCtrl.tabs" md-tabs-template="::tab.label" md-scope="::tab.parent">Onsite Visits</md-dummy-tab>

                        </md-tabs-dummy-wrapper>
                    </md-tabs-canvas>
                </md-tabs-wrapper>
                <md-tabs-content-wrapper ng-show="$mdTabsCtrl.hasContent &amp;&amp; $mdTabsCtrl.selectedIndex >= 0" class="_md" aria-hidden="false">
           
                    <md-tab-content id="tab-content-17" class="_md ng-scope md-active md-no-scroll" role="tabpanel" aria-labelledby="tab-item-17" md-swipe-left="$mdTabsCtrl.swipeContent &amp;&amp; $mdTabsCtrl.incrementIndex(1)" md-swipe-right="$mdTabsCtrl.swipeContent &amp;&amp; $mdTabsCtrl.incrementIndex(-1)" ng-if="tab.hasContent" ng-repeat="(index, tab) in $mdTabsCtrl.tabs" ng-class="{ 'md-no-transition': $mdTabsCtrl.lastSelectedIndex == null, 'md-active':        tab.isActive(), 'md-left':          tab.isLeft(), 'md-right':         tab.isRight(), 'md-no-scroll':     $mdTabsCtrl.dynamicHeight }" style="">
                

                        <div md-tabs-template="::tab.template" md-connected-if="tab.isActive()" md-scope="::tab.parent" ng-if="$mdTabsCtrl.enableDisconnect || tab.shouldRender()" class="ng-scope ng-isolate-scope">
                            <md-content layout-padding="" class="md-mt-10 text-center layout-padding ng-scope _md" style="line-height: 0px;height:200px"> <img style="border-radius: 50%; box-shadow: 0 0 20px 0px #00000014;" height="100px" width="auto" ng-src="https://demo.ciuis.com/uploads/images/" class="md-avatar" alt="" src="https://demo.ciuis.com/uploads/images/">
                                <h3><strong ng-bind="user.name" class="ng-binding"></strong></h3>
                                <br>
                                <span ng-bind="user.email" class="ng-binding"></span></md-content>
                            <md-content class="md-mt-30 text-center ng-scope _md">
                                <a class="md-raised md-button ng-binding md-ink-ripple ng-hide" ng-transclude="" ng-show="ONLYADMIN != 'true'" ng-href="https://demo.ciuis.com/staff/profile" ng-bind="lang.profile" aria-label="Profile" href="https://demo.ciuis.com/staff/profile" aria-hidden="true"></a>
                                <a class="md-raised md-button ng-binding md-ink-ripple" ng-transclude="" ng-show="ONLYADMIN == 'true'" ng-href="https://demo.ciuis.com/staff/staffmember/2" ng-bind="lang.profile" aria-label="Profile" href="https://demo.ciuis.com/staff/staffmember/2" aria-hidden="false"></a>
                                <a class="md-raised md-button ng-binding md-ink-ripple" ng-transclude="" ng-href="https://demo.ciuis.com/login/logout" ng-bind="lang.logout" aria-label="LogOut" href="https://demo.ciuis.com/login/logout"></a>
                            </md-content>
                            <md-content layout-padding="" class="layout-padding ng-scope _md">
                                <md-switch ng-model="appointment_availability" ng-change="ChangeAppointmentAvailability(user.id,appointment_availability)" aria-label="Status" tabindex="0" type="checkbox" role="checkbox" class="ng-pristine ng-untouched ng-valid ng-empty" aria-checked="false" aria-invalid="false">
                                    <div class="md-container" style="touch-action: pan-x;">
                                        <div class="md-bar"></div>
                                        <div class="md-thumb-container">
                                            <div class="md-thumb md-ink-ripple" md-ink-ripple="" md-ink-ripple-checkbox=""></div>
                                        </div>
                                    </div>
                                    <div ng-transclude="" class="md-label"><strong class="text-muted ng-binding ng-scope" ng-bind="lang.appointment_availability"></strong></div>
                                </md-switch>
                            </md-content>
                            <md-content class="ng-scope _md">
                                <md-list class="md-dense flex" flex="" role="list">
                                    <div class="md-no-sticky text-uppercase md-subheader _md" role="heading" aria-level="2">
                                        <div class="md-subheader-inner">
                                            <div class="md-subheader-content"><span ng-bind="lang.appointments" class="ng-binding ng-scope"></span></div>
                                        </div>
                                    </div>
                     

                                </md-list>
                            </md-content>
                        </div>
   

                    </md-tab-content>
               

                    <md-tab-content id="tab-content-18" class="_md ng-scope md-right md-no-scroll" role="tabpanel" aria-labelledby="tab-item-18" md-swipe-left="$mdTabsCtrl.swipeContent &amp;&amp; $mdTabsCtrl.incrementIndex(1)" md-swipe-right="$mdTabsCtrl.swipeContent &amp;&amp; $mdTabsCtrl.incrementIndex(-1)" ng-if="tab.hasContent" ng-repeat="(index, tab) in $mdTabsCtrl.tabs" ng-class="{ 'md-no-transition': $mdTabsCtrl.lastSelectedIndex == null, 'md-active':        tab.isActive(), 'md-left':          tab.isLeft(), 'md-right':         tab.isRight(), 'md-no-scroll':     $mdTabsCtrl.dynamicHeight }" style="">
                  

                        <div md-tabs-template="::tab.template" md-connected-if="tab.isActive()" md-scope="::tab.parent" ng-if="$mdTabsCtrl.enableDisconnect || tab.shouldRender()" class="ng-scope ng-isolate-scope">
                            <md-list class="md-dense ng-scope flex" flex="" role="list">
                                <div class="text-uppercase md-subheader _md" role="heading" aria-level="2" style="position: sticky; top: 0px; z-index: 2;">
                                    <div class="md-subheader-inner">
                                        <div class="md-subheader-content"><span ng-bind="lang.onsite_visits" class="ng-binding ng-scope"></span></div>
                                    </div>
                                </div>
                            

                            </md-list>
                        </div>
       

                    </md-tab-content>
               

                </md-tabs-content-wrapper>
            </md-tabs>
        </md-content>
    </md-sidenav>
    <md-content class="ciuis-body-wrapper ciuis-body-fixed-sidebar _md" ciuis-ready="">
        <div class="ciuis-body-content">
            <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-9 hidden-sm hidden-md hidden-lg" ng-hide="ONLYADMIN != 'true'" aria-hidden="false">
                <div class="ciuis-an-x">
                    <div class="ciuis-panel-summary-bg col-md-3 col-xs-6 text-center blr-5 tlr-5 md-pt-15 md-mr-10"> <span>Outstanding Invoices:</span>
                        <h1 class="txt-scale-xs no-margin-top text-danger xs-28px figures"><span class="ng-binding">$1,711,740.99</span></h1>
                        <p class="secondary-text"> <span class="label label-danger ng-binding"></span> <span>Invoice Not Paid</span> </p>
                        <div class="col-md-12">
                            <div class="progress">
                                <div style="width:55%" class="progress-bar progress-bar-danger progress-bar-striped active">55%</div>
                            </div>
                        </div> 
                    </div>
                    <div class="ciuis-panel-summary-bg col-md-3 col-xs-6 text-center brr-5 trr-5 md-pt-15 md-mr-10"> Monthly Return:
                        <h1 class="txt-scale-xs no-margin-top xs-28px figures"><span class="ng-binding">$0.00</span></h1>
                        <p class="secondary-text"> Last Month:
                            <br>
                            <strong><span ng-bind-html="stats.oak | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"></span></strong> <span class="text-"><i class=""></i> ( <span ng-bind="stats.monmessage+'%'" class="ng-binding">$1,711,740.99</span> )</span>
                        </p>
                        <br>
                    </div>
                    <div class="ciuis-panel-summary-bg col-md-3 col-xs-6 text-center rad-5 md-pt-15 pull-right"> Monthly Expense:
                        <h1 class="txt-scale-xs no-margin-top xs-28px figures"><span ng-bind-html="stats.mex | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"></span></h1>
                        <p class="secondary-text"> Last Month:
                            <br>
                            <strong><span ng-bind-html="stats.pme | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"></span></strong> <span class="text-"><i class=""></i> ( <span ng-bind="stats.expensestatus+'%'" class="ng-binding">%</span> )</span>
                        </p>
                        <br>
                    </div>
                    <div class="ciuis-panel-summary-bg col-md-3 col-xs-6 text-center rad-5 md-pt-15"> Annual Return:
                        <h1 class="txt-scale-xs no-margin-top xs-28px figures"><span ng-bind-html="stats.ycr | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"></span></h1>
                        <p class="secondary-text"> Last Year:
                            <br>
                            <strong><span ng-bind-html="stats.oyc | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"></span></strong> <span class="text-"><i class=""></i> ( <span ng-bind="stats.yearmessage+'%'" class="ng-binding">%</span> )</span>
                        </p>
                        <br>
                    </div>
                </div>
            </div>
            <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-9 hidden-sm hidden-md hidden-lg ng-hide" ng-hide="ONLYADMIN != 'false'" aria-hidden="true">
                <div class="ciuis-an-x">
                    <div class="ciuis-panel-summary-bg col-md-3 col-xs-6 text-center blr-5 tlr-5 md-pt-15 md-mr-10"> <span>YOUR LEADS</span>
                        <h1 class="txt-scale-xs no-margin-top text-success xs-28px figures"><span ng-bind="stats.mlc" class="ng-binding"></span></h1>
                        <p class="secondary-text"><span class="label label-success ng-binding" ng-bind="stats.clc"></span> Leads Converted</p>
                        <div class="col-md-12">
                            <div class="progress">
                                <div style="width:%" class="progress-bar progress-bar-success progress-bar-striped active ng-binding" ng-bind="stats.clp+'%'">%</div>
                            </div>
                        </div>
                    </div>
                    <div class="ciuis-panel-summary-bg col-md-3 col-xs-6 text-center brr-5 trr-5 md-pt-15 md-mr-10"> <span>YOUR TICKETS</span>
                        <h1 class="txt-scale-xs no-margin-top text-muted xs-28px figures"><span ng-bind="stats.mct" class="ng-binding"></span></h1>
                        <p class="secondary-text"> <span class="label label-default ng-binding" ng-bind="stats.mct"></span> Ticket Closed </p>
                        <div class="col-md-12">
                            <div class="progress">
                                <div style="width:%" class="progress-bar progress-bar-success progress-bar-striped active ng-binding" ng-bind="stats.mtp+'%'">%</div>
                            </div>
                        </div>
                    </div>
                    <div class="ciuis-panel-summary-bg col-md-3 col-xs-6 text-center rad-5 md-pt-15 pull-right"> <span>UPCOMING EVENTS</span>
                        <h1 class="txt-scale-xs no-margin-top text-muted xs-28px figures"><span ng-bind="stats.ues" class="ng-binding"></span></h1>
                        <div class="col-md-12">
                            <div> <a href="https://demo.ciuis.com/calendar" class="btn btn-space btn-default btn-big"><i class="icon mdi ion-ios-calendar-outline"></i> Calendar</a> </div>
                        </div>
                    </div>
                    <div class="ciuis-panel-summary-bg col-md-3 col-xs-6 text-center rad-5 md-pt-15"> <span>YOUR CUSTOMERS</span>
                        <h1 class="txt-scale-xs no-margin-top text-muted xs-28px figures"><span ng-bind="stats.myc" class="ng-binding"></span></h1>
                        <div class="col-md-12">
                            <button class="btn btn-space btn-default btn-big"> <i class="icon mdi ion-ios-people-outline"></i> Customers </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-9">
            @yield('content')
                
                
               
            </div>
           <!--  <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-9 hidden-xs">
                <div class="ciuis-an-x">
                    <div class="ciuis-panel-summary-bg col-md-3 col-xs-6 text-center blr-5 tlr-5 md-pt-15 md-mr-10"> <span>Outstanding Invoices:</span>
                        <h1 class="txt-scale-xs no-margin-top text-danger xs-28px figures">
                            <span class="ng-binding">$55,540.00</span></h1>
                        <p class="secondary-text"> <span class="label label-danger">4</span> <span>Invoice Not Paid</span> </p>
                        <div class="col-md-12">
                            <div class="progress">
                                <div style="width:15%" class="progress-bar progress-bar-danger progress-bar-striped active">15%</div>
                            </div>
                        </div>
                    </div>
                    <div class="ciuis-panel-summary-bg col-md-3 col-xs-6 text-center brr-5 trr-5 md-pt-15 md-mr-10"> Monthly Return:
                        <h1 class="txt-scale-xs no-margin-top xs-28px figures">
                          <span class="ng-binding">$1,21,14.00</span></h1>
                        <p class="secondary-text"> Last Month:
                            <br>
                            <strong><span>$0.00</span></strong> <span class="text-success"><i class="fas fa-arrow-up small"></i> ( <span class="ng-binding">Not yet% </span> )</span>
                        </p>
                        <br>
                    </div>
                    <div class="ciuis-panel-summary-bg col-md-3 col-xs-6 text-center rad-5 md-pt-15 pull-right"> Monthly Expense:
                        <h1 class="txt-scale-xs no-margin-top xs-28px figures"><span class="ng-binding">$256.00</span></h1>
                        <p class="secondary-text"> Last Month:
                            <br>
                            <strong><span>$0.00</span></strong> <span class="text-success"><i class="fas fa-arrow-up small"></i> ( <span class="ng-binding">Not yet% </span> )</span>
                        </p>
                        <br>
                    </div>
                    <div class="ciuis-panel-summary-bg col-md-3 col-xs-6 text-center rad-5 md-pt-15"> Annual Return:
                        <h1 class="txt-scale-xs no-margin-top xs-28px figures">
                            <span class="ng-binding">$1,150,71.11</span></h1>
                        <p class="secondary-text"> Last Year:
                            <br>
                            <strong><span>$0.00</span></strong> <span class="text-success"><i class="fas fa-arrow-up small"></i> ( <span class="ng-binding">Not yet% </span> )</span>
                        </p>
                        <br>
                    </div>
                </div>
            </div>
             -->
            <!-- <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-9 hidden-xs ng-hide" ng-hide="ONLYADMIN != 'false'" aria-hidden="true">
                <div class="ciuis-an-x">
                    <div class="ciuis-panel-summary-bg col-md-3 col-xs-6 text-center blr-5 tlr-5 md-pt-15 md-mr-10"> <span>YOUR LEADS</span>
                        <h1 class="txt-scale-xs no-margin-top text-success xs-28px figures"><span ng-bind="stats.mlc" class="ng-binding"></span></h1>
                        <p class="secondary-text"><span class="label label-success ng-binding" ng-bind="stats.clc"></span> Leads Converted</p>
                        <div class="col-md-12">
                            <div class="progress">
                                <div style="width:%" class="progress-bar progress-bar-success progress-bar-striped active ng-binding" ng-bind="stats.clp+'%'">%</div>
                            </div>
                        </div>
                    </div>
                    <div class="ciuis-panel-summary-bg col-md-3 col-xs-6 text-center brr-5 trr-5 md-pt-15 md-mr-10"> <span>YOUR TICKETS</span>
                        <h1 class="txt-scale-xs no-margin-top text-muted xs-28px figures"><span ng-bind="stats.mct" class="ng-binding"></span></h1>
                        <p class="secondary-text"> <span class="label label-default ng-binding" ng-bind="stats.mct"></span> Ticket Closed </p>
                        <div class="col-md-12">
                            <div class="progress">
                                <div style="width:%" class="progress-bar progress-bar-success progress-bar-striped active ng-binding" ng-bind="stats.mtp+'%'">%</div>
                            </div>
                        </div>
                    </div>
                    <div class="ciuis-panel-summary-bg col-md-3 col-xs-6 text-center rad-5 md-pt-15 pull-right"> <span>UPCOMING EVENTS</span>
                        <h1 class="txt-scale-xs no-margin-top text-muted xs-28px figures"><span ng-bind="stats.ues" class="ng-binding"></span></h1>
                        <div class="col-md-12">
                            <div> <a href="https://demo.ciuis.com/calendar" class="btn btn-space btn-default btn-big"><i class="icon mdi ion-ios-calendar-outline"></i> Calendar</a> </div>
                        </div>
                    </div>
                    <div class="ciuis-panel-summary-bg col-md-3 col-xs-6 text-center rad-5 md-pt-15"> <span>YOUR CUSTOMERS</span>
                        <h1 class="txt-scale-xs no-margin-top text-muted xs-28px figures"><span ng-bind="stats.myc" class="ng-binding"></span></h1>
                        <div class="col-md-12">
                            <button class="btn btn-space btn-default btn-big"> <i class="icon mdi ion-ios-people-outline"></i> Customers </button>
                        </div>
                    </div>
                </div>
            </div> -->

             @section('sidebar')
             <ciuis-sidebar>
               <aside class="page-aside">


 <div class="row subscribtion" style="padding-left: 30px;">
    <div class="col-md-12" style="background: #fff;">
            <h4>Subscribtion</h4>
    </div>
</div>

                 
<div class="row">
                 <div class="col-md-12">
                <div class="row" style="padding-left: 30px;">
                    <div class="col-md-12 border_top">
                        <div class="col-md-4">
                            <img class="img-responsive circle_img2" src="https://ianirafunnels.com/public/crm/images/brand_grey1.png">
                        </div>
                        <div class="col-md-8 new_right1">
                            <h4>Silver Packag</h4>
                            <span>valid until 10/09/2019</span>
                        </div>

                        
                    </div>
                </div>


                <div class="row" style="padding-left: 30px;">
                    <div class="col-md-12 border_top2">
                        <div class="col-md-12 new_right2">
                            <ul class="list-unstyled plan_list1">
                                <li style="font-size: 22px;text-align: center;">LANCE BOGROL</li>
                                <li style="font-size: 14px;text-align: center;margin-bottom: 20px">Analytical data of 1 month </li>
                                <li><span class="pull-left">CONTACTS</span> <span class="pull-right">147 </span> </li>
                                <div class="boder_bottom_only"><span style="width:85%"></span></div>
                                <li><span class="pull-left">VISITORS </span> <span class="pull-right">2463 </span></li>
                                <div class="boder_bottom_only"><span style="width: 75%"></span></div>
                                <li><span class="pull-left">FUNNELS</span> <span class="pull-right"> 9</span></li>
                                <div class="boder_bottom_only"><span style="width: 9%"></span></div>
                                
                            </ul>
                        </div>

                    </div>
                </div>

          <!--       <div class="row" style="padding-left: 30px;margin-top: 30px;">
                    <div class="col-md-12 border_top3">
                        <div class="col-md-4">
                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRaZUVuCVDWg32Auq1TSkfZm4YrFIgJBRe6BQ7TOPbaLAXUDiwh" width="85">
                        </div>
                        <div class="col-md-8">
                            <span class="get_advance">Get Advanced Training:</span>
                            <h4 class="funnel_heading"> FUNNEL HACKS</h4>

                        </div>

                        <div class="col-md-12" style="margin-top: 20px;">
                            <a class="btn btn-block new_watch_btn" href="#"><i class="fa fa-microphone"></i> Watch Webinar Now</a>
                        </div>
                    </div>
                </div> -->

            </div>

</div>


<style type="text/css">
.border_top {
    border: 0;
    border-top: 2px solid #dcdcdc;
    padding: 30px;
    padding-bottom: 25px;
    background: #fff;
}
.searchbar {
      width: 311px;
    height: 40px;
    margin-top: -15px;
    border-radius: 30px;  
}
.searchbar::-webkit-input-placeholder{
    font-size: 12px;
}
.ciuis-v3-menu > li {
    float: right !important;
}

.new_right1 h4 {
    margin: 0;
}

.new_right1 span {
    font-size: 12px;
    color: #777;
}
/*.ciuis-1 {
    width: 100%!important;
    margin-top: 15px !important;
}*/
.circle_img2 {
    height: 52px;
    border: 0;
    border-radius: 50%;
    outline: 0;
    box-shadow: 0 0;
}
.border_top2 {
    padding: 30px;
    background: #f3f3f3;
}

.plan_list1 li {
    color: #000;
    font-weight: 400;
    font-size: 14px;
    margin-bottom: 5px;
    overflow: hidden;
}


h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6, p,span, div {
    font-family: 'Roboto', sans-serif;
}



.boder_bottom_only {
    border-bottom: 2px solid #fff;
    margin-bottom: 20px;
    margin-top: 10px;
    width: 100%;
    display: block;
    position: relative;
    border-radius: 20px;
}
.boder_bottom_only > span {
    position: absolute;
    top: -1px;
    left: 0;
    height: 4px;
    background: #1a171b;
    display: block;
    border-radius: 8px;
}

.border_top3 {
    background: #ffffff;
    box-shadow: 0 0 2px 0 #ddd;
    padding: 30px 0;
}


.new_watch_btn, .new_watch_btn:active, .new_watch_btn:link {
    display: block;
    clear: both;
    font-weight: bold;
    text-transform: capitalize;
    color: #fff !important;
    background: #f58b3c;
    border: 2px solid #f58b3c !important;
    height: 40px;
    line-height: 40px;
    transition: 0.5s;
    box-shadow: 0 0 !important;
    outline: 0 !important;
    border-radius: 30px;
    outline: 0 !important;
}

.new_watch_btn:hover {
    background: #fff!important;
    color: #f58b3c!important;
}
#events .ciuis-1 {
    position: absolute !important;
    z-index: 999;
    width: 100% !important;
}
._md-toolbar-transitions .material-icons{
font-size: 18px !important; 
}
</style>






                    <div id="events">
                        <div class="ciuis-1 col-xs-12 nopadding">
                            <div class="col-xs-6 nopadding date-and-time-ciuis">
                                <i class="fas fa-clock"></i>
                                <span id="time-ciuis">18:12</span>
                            </div>
                            <div class="col-xs-6 date-a">01, April 2019 Monday</div>
                        </div>
                        <div class="row">
                            <div class="events events_xs col-md-12">
                                <ul>
      
      <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date">  <i class="fa fa-exclamation"></i> </label>
                        <a href="https://demo.ciuis.com/invoices/invoice/96"><i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i></a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer">test company 1</span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$100.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b  class="ng-binding">-2 days</b></span></span>
                        </p>
                    </li>
                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date"> <i class="fa fa-exclamation"></i> </label>
                        <a href="https://demo.ciuis.com/invoices/invoice/94"><i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i> </a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer">Bada Restaurant</span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$1,000.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-737550 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date">  <i class="fa fa-exclamation"></i> </label>
                        <a href="https://demo.ciuis.com/invoices/invoice/89"> <i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i> </a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer">kesavan</span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$61.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-9 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date">  <i class="fa fa-exclamation"></i> </label>
                        <a href="https://demo.ciuis.com/invoices/invoice/88"> <i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i></a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer">Cirrus Contractors</span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$18,010.02</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-9 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date">  <i class="fa fa-exclamation"></i> </label>
                        <a href="https://demo.ciuis.com/invoices/invoice/86"> <i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i> </a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer">golbb</span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$6,000.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-9 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date">  <i class="fa fa-exclamation"></i> </label>
                        <a href="https://demo.ciuis.com/invoices/invoice/85"><i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i></a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer">dsfdgfdf</span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$57,500.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-7 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date"> <i class="fa fa-exclamation"></i></label>
                        <a href="https://demo.ciuis.com/invoices/invoice/83"> <i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i></a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer"></span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$0.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-9 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date"><i class="fa fa-exclamation"></i></label>
                        <a href="https://demo.ciuis.com/invoices/invoice/82"> <i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i> </a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer">golbb</span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$18,000.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-9 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date"> <i class="fa fa-exclamation"></i> </label>
                        <a href="https://demo.ciuis.com/invoices/invoice/81"> <i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i></a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer"></span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$1,200.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-9 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date"> <i class="fa fa-exclamation"></i></label>
                        <a href="https://demo.ciuis.com/invoices/invoice/70"><i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i></a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer">DEMO FİRMA</span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$0.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-4 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date"> <i class="fa fa-exclamation"></i> </label>
                        <a href="https://demo.ciuis.com/invoices/invoice/69"> <i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i></a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer"></span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$0.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-14 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date"> <i class="fa fa-exclamation"></i> </label>
                        <a href="https://demo.ciuis.com/invoices/invoice/68"> <i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i></a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer">cwecwe</span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$168.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-8 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date"> <i class="fa fa-exclamation"></i> </label>
                        <a href="https://demo.ciuis.com/invoices/invoice/65"> <i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i></a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer">test update</span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$0.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-737550 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date"> <i class="fa fa-exclamation"></i> </label>
                        <a href="https://demo.ciuis.com/invoices/invoice/61"> <i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i></a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer">leo</span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$1,500.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-8 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date"> <i class="fa fa-exclamation"></i></label>
                        <a href="https://demo.ciuis.com/invoices/invoice/60"> <span class="class=&quot;text-white&quot; ion-ios-arrow-forward"></span> </a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer"></span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$50.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-9 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date"> <i class="fa fa-exclamation"></i> </label>
                        <a href="https://demo.ciuis.com/invoices/invoice/55"> <i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i></a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer">deneme</span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$0.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-17 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date"> <i class="fa fa-exclamation"></i> </label>
                        <a href="https://demo.ciuis.com/invoices/invoice/53"> <i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i></a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer">test</span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$50.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-17 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date"> <i class="fa fa-exclamation"></i> </label>
                        <a href="https://demo.ciuis.com/invoices/invoice/52"> <i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i> </a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer">SEARLE</span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$461,287.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-18 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date"><i class="fa fa-exclamation"></i> </label>
                        <a href="https://demo.ciuis.com/invoices/invoice/50"><i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i> </a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer">AGP</span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$0.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-19 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date">  <i class="fa fa-exclamation"></i>  </label>
                        <a href="https://demo.ciuis.com/invoices/invoice/48"> <i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i></a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer"></span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$1,387.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-19 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date"> <i class="fa fa-exclamation"></i> </label>
                        <a href="https://demo.ciuis.com/invoices/invoice/39"> <i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i></a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer">ECOBOL</span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$120.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-19 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date"> <i class="fa fa-exclamation"></i></label>
                        <a href="https://demo.ciuis.com/invoices/invoice/30"> <i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i></a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer">Parallax Corporation</span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$210.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-20 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date"> <i class="fa fa-exclamation"></i></label>
                        <a href="https://demo.ciuis.com/invoices/invoice/29"> <i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i></a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer"></span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$0.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-27 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date"> <i class="fa fa-exclamation"></i></label>
                        <a href="https://demo.ciuis.com/invoices/invoice/23"> <i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i> </a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer">Parallax Corporation</span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$0.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-91 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date"> <i class="fa fa-exclamation"></i></label>
                        <a href="https://demo.ciuis.com/invoices/invoice/22"> <span class="class=&quot;text-white&quot; ion-ios-arrow-forward"></span> </a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer"></span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$0.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-143 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date"><i class="fa fa-exclamation"></i></label>
                        <a href="https://demo.ciuis.com/invoices/invoice/13"><i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i> </a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer">Parallax Corporation</span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$210.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-156 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="invoice in overdueinvoices" class="next overdueinvoices ng-scope">
                        <label class="date"> <i class="fa fa-exclamation"></i></label>
                        <a href="https://demo.ciuis.com/invoices/invoice/7"> <i class="fa fa-angle-right" style="color: #919FAF; font-size: 16px;"></i></a>
                        <h3 class="text-bold overduetext ng-binding" ng-bind="lang.overdueinvoices">OVER DUE INVOICES</h3>
                        <p>
                            <span class="duration text-bold ng-binding" ng-bind="invoice.customer">DHARMA Initiative</span>
                            <span class="location text-bold"><span ng-bind-html="invoice.total | currencyFormat:cur_code:null:true:cur_lct" class="ng-binding"><span dir="ltr">$200.00</span></span></span><br>
                            <span style="color: #ee7a6b;" class="text-bold"><span class="text-default mdi mdi-timer-off"></span> <span class="text-default"><b ng-bind="invoice.status" class="ng-binding">-113 days</b></span></span>
                        </p>
                    </li>

                    <li ng-repeat="newticket in newtickets" class="next newticketsidebar ng-scope">
                        <label class="date"> <i class="mdi mdi-ticket-star"></i> </label>
                        <a href="https://demo.ciuis.com/tickets/ticket/5"> <span class="ion-ios-arrow-forward"></span> </a>
                        <h3 class="text-bold ng-binding" ng-bind="newticket.subject">New projet</h3>
                        <p>
                            <span style="color:black;" class="duration text-bold ng-binding" ng-bind="newticket.contactname +' '+ newticket.contactsurname">Sue Shei</span>
                            <span style="color:black;" class="location text-bold ng-binding" ng-bind="newticket.priority">MEDIUM</span>
                        </p>
                    </li>

                    <li ng-repeat="newticket in newtickets" class="next newticketsidebar ng-scope">
                        <label class="date"> <i class="mdi mdi-ticket-star"></i> </label>
                        <a href="https://demo.ciuis.com/tickets/ticket/8"> <span class="ion-ios-arrow-forward"></span> </a>
                        <h3 class="text-bold ng-binding" ng-bind="newticket.subject">6rtterqwqer</h3>
                        <p>
                            <span style="color:black;" class="duration text-bold ng-binding" ng-bind="newticket.contactname +' '+ newticket.contactsurname">null null</span>
                            <span style="color:black;" class="location text-bold ng-binding" ng-bind="newticket.priority">MEDIUM</span>
                        </p>
                    </li>

                    <li ng-repeat="newticket in newtickets" class="next newticketsidebar ng-scope">
                        <label class="date"> <i class="mdi mdi-ticket-star"></i> </label>
                        <a href="https://demo.ciuis.com/tickets/ticket/11"> <span class="ion-ios-arrow-forward"></span> </a>
                        <h3 class="text-bold ng-binding" ng-bind="newticket.subject">teting</h3>
                        <p>
                            <span style="color:black;" class="duration text-bold ng-binding" ng-bind="newticket.contactname +' '+ newticket.contactsurname">Sue Shei</span>
                            <span style="color:black;" class="location text-bold ng-binding" ng-bind="newticket.priority">HIGH</span>
                        </p>
                    </li>

                    <li ng-repeat="newticket in newtickets" class="next newticketsidebar ng-scope">
                        <label class="date"> <i class="mdi mdi-ticket-star"></i> </label>
                        <a href="https://demo.ciuis.com/tickets/ticket/12"> <span class="ion-ios-arrow-forward"></span> </a>
                        <h3 class="text-bold ng-binding" ng-bind="newticket.subject">ok</h3>
                        <p>
                            <span style="color:black;" class="duration text-bold ng-binding" ng-bind="newticket.contactname +' '+ newticket.contactsurname">Sue Shei</span>
                            <span style="color:black;" class="location text-bold ng-binding" ng-bind="newticket.priority">MEDIUM</span>
                        </p>
                    </li>
                    <li ng-repeat="newticket in newtickets" class="next newticketsidebar ng-scope">
                        <label class="date"> <i class="mdi mdi-ticket-star"></i> </label>
                        <a href="https://demo.ciuis.com/tickets/ticket/15"> <span class="ion-ios-arrow-forward"></span> </a>
                        <h3 class="text-bold ng-binding" ng-bind="newticket.subject">it doesn't work</h3>
                        <p>
                            <span style="color:black;" class="duration text-bold ng-binding" ng-bind="newticket.contactname +' '+ newticket.contactsurname">Sue Shei</span>
                            <span style="color:black;" class="location text-bold ng-binding" ng-bind="newticket.priority">MEDIUM</span>
                        </p>
                    </li>

                    <li ng-repeat="newticket in newtickets" class="next newticketsidebar ng-scope">
                        <label class="date"> <i class="mdi mdi-ticket-star"></i> </label>
                        <a href="https://demo.ciuis.com/tickets/ticket/16"> <span class="ion-ios-arrow-forward"></span> </a>
                        <h3 class="text-bold ng-binding" ng-bind="newticket.subject">10</h3>
                        <p>
                            <span style="color:black;" class="duration text-bold ng-binding" ng-bind="newticket.contactname +' '+ newticket.contactsurname">Sue Shei</span>
                            <span style="color:black;" class="location text-bold ng-binding" ng-bind="newticket.priority">HIGH</span>
                        </p>
                    </li>

                    <li ng-repeat="newticket in newtickets" class="next newticketsidebar ng-scope">
                        <label class="date"> <i class="mdi mdi-ticket-star"></i> </label>
                        <a href="https://demo.ciuis.com/tickets/ticket/19"> <span class="ion-ios-arrow-forward"></span> </a>
                        <h3 class="text-bold ng-binding" ng-bind="newticket.subject">Prueba</h3>
                        <p>
                            <span style="color:black;" class="duration text-bold ng-binding" ng-bind="newticket.contactname +' '+ newticket.contactsurname">test test</span>
                            <span style="color:black;" class="location text-bold ng-binding" ng-bind="newticket.priority">MEDIUM</span>
                        </p>
                    
                  <li ng-repeat="reminder in reminders" class="next reminder ng-scope" aria-label="This reminder created by Lance Bogrol" md-labeled-by-tooltip="md-tooltip-42">
                    
                    <label class="detail"><i class="ion-ios-bell"></i></label>
                        <a ng-click="ReminderRead($index)" class="mark-read-reminder ion-checkmark-round" style="cursor: pointer"></a>
                        <h3 class="text-bold ng-binding" style="margin-bottom: 5px">Lead Reminder</h3>
                        <span class="reminder-detail ng-binding" style="display: table-cell;" ng-bind="reminder.description">sadasd</span>
                        <p style="display: table-footer-group;"><span style="color:black;" class="duration text-bold"><span ng-bind="reminder.date | date : 'MMM d, y h:mm:ss a'" class="ng-binding">Mar 19, 2019 10:20:00 AM</span></span></p>
                    </li>

                    <li ng-repeat="reminder in reminders" class="next reminder ng-scope" aria-label="This reminder created by Lance Bogrol" md-labeled-by-tooltip="md-tooltip-43">
                    
                    <label class="detail"><i class="ion-ios-bell"></i></label>
                        <a ng-click="ReminderRead($index)" class="mark-read-reminder ion-checkmark-round" style="cursor: pointer"></a>
                        <h3 class="text-bold ng-binding" style="margin-bottom: 5px" ng-bind="reminder.title">Lead Reminder</h3>
                        <span class="reminder-detail ng-binding" style="display: table-cell;" ng-bind="reminder.description">sdadadadadadad</span>
                        <p style="display: table-footer-group;"><span style="color:black;" class="duration text-bold"><span ng-bind="reminder.date | date : 'MMM d, y h:mm:ss a'" class="ng-binding">Mar 27, 2019 8:45:00 PM</span></span></p>
                    </li>
                </ul>
                            
                            </div>
                            <div class="ciuis-activity-line col-md-12">
                                <ul class="ciuis-activity-timeline">
                          
                                    <load-more><a ng-click="loadMore()" id="loadButton" class="activity_tumu"><i style="font-size:22px;" class="icon ion-android-arrow-down"></i></a></load-more>
                                </ul>
                            </div>
                        </div>
                    </div>
                </aside>
            </ciuis-sidebar>
          
            @show
            
            
            
            
            
            
        </div>
    </md-content>




<script src="https://ianirafunnels.com/public/crm/assets/js/Ciuis.js"></script>
<script src="https://ianirafunnels.com/public/crm/assets/js/moment.min.js"></script>
<script src="https://ianirafunnels.com/public/crm/assets/js/Chart.bundle.js"></script>
<script src="https://ianirafunnels.com/public/crm/assets/js/2.7.0/Chart.js"></script>
<script src="https://ianirafunnels.com/public/crm/assets/js/highcharts.js"></script>
<script src="https://ianirafunnels.com/public/crm/assets/js/angular-material.min.js"></script>
<script src="https://ianirafunnels.com/public/crm/assets/js/currency-format.min.js"></script>
<script src="https://ianirafunnels.com/public/crm/assets/js/angular-material-datetimepicker.min.js"></script>
<script src="https://ianirafunnels.com/public/crm/assets/js/scheduler.min.js"></script>
<script src="https://ianirafunnels.com/public/crm/assets/js/jquery.min.js"></script>
<script src="https://ianirafunnels.com/public/crm/assets/js/bootstrap.min.js"></script>
<script src="https://ianirafunnels.com/public/crm/assets/js/jquery.gritter.js"></script>
<script src="https://ianirafunnels.com/public/crm/assets/js/CiuisAngular.js"></script>



<script type="text/ng-template" id="ciuis-sidebar.html">





        <aside class="page-aside hidden-sm hidden-xs ciuis-sag-sidebar-xs">
            <div id="events">
                <div class="ciuis-1 col-xs-12 nopadding">
                    <div class="col-xs-6 nopadding date-and-time-ciuis">
                        <i class="fas fa-clock"></i>
                        <span id="time-ciuis" ng-bind="clock | date:'HH:mm'"></span>
                    </div>
                    <div class="col-xs-6 date-a" ng-bind="date | date:'dd, MMMM yyyy EEEE'"></div>
                </div>
				
				
                <div class="row">
                    <div class="events events_xs col-md-12">
					
       
	  
                    </div>
                    <div class="ciuis-activity-line col-md-12">
                        <ul class="ciuis-activity-timeline">
                            <li ng-repeat="log in logs | limitTo: LogLimit" class="ciuis-activity-detail">
                                <div class="ciuis-activity-title" ng-bind="log.date"></div>
                                <div class="ciuis-activity-detail-body">
                                    <div ng-bind-html="log.detail|trustAsHtml"></div>
                                    <div style="margin-right: 15px; border-radius: 3px; background: transparent; color: #2f3239; font-weight: 400;" class="pull-right label label-default">
                                        <small class="log-date"><i class="ion-android-time"></i> <span ng-bind="log.logdate | date : 'MMM d, y h:mm:ss a'"></span></small>
                                    </div>
                                </div>
                            </li>
                            <load-more></load-more>
                        </ul>
                    </div>
                </div>
            </div>
        </aside>
    </script>
    
 
  
    <script type="text/javascript">
        function speak(CiuisVoiceNotification) {
            var s = new SpeechSynthesisUtterance();
            s.volume = 0.5;
            s.rate = 1;
            s.pitch = 1;
            s.lang = VOICENOTIFICATIONLANG;
            s.text = CiuisVoiceNotification;
            window.speechSynthesis.speak(s);
        }
        var voice = document.querySelectorAll('body');
        var reminder = 'You have  reminders. Please check it because I will not remember it again, but you can see it in the timeline.';
        var oepnticket = 'There are  new tickets.';
    </script>





<style type="text/css">
    
.md-sidenav-right.md-whiteframe-4dp.ng-isolate-scope._md{ display: block !important; }

.searchbar {
       padding-right: 40px; 
}
.seacrh_pos{
    position: relative;
}
.searc_bar_icon{
    position: absolute;
    right: 25px;
    font-size: 18px;
    top: 18px;
}


</style>


</body>

</html>