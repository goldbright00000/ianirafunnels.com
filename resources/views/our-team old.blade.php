@extends('layouts.default')
@section('title','Our Team')
@section('content')



<div class="headeryy">
<div class="container">
<div class="through_f"><h3><i class="fa fa-caret-square-o-right" aria-hidden="true"></i>

 <span> Lorem Ipsum</span>  is simply dummy text of the printing and typesetting industry.</h3></div>
    </div>
    </div>





<div class="container section-ourTeam">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12 ourTeam-hedding text-center">
			<h1>Meet Our Team</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="row section-success ourTeam-box text-center">
				<div class="col-md-12 section1">
					<i class="fas fa-user"></i>
				</div>
				<div class="col-md-12 section2">
					<p>LISA MAHETA</p><br>
					<h1>MARKETING</h1><br>
				</div>
				<div class="col-md-12 section3">
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
					</p>
				</div>
				<div class="col-md-12 section4">
					<i class="fa fa-facebook-official" aria-hidden="true"></i>
					<i class="fa fa-twitter" aria-hidden="true"></i>
					<i class="fa fa-google-plus" aria-hidden="true"></i>
					<i class="fa fa-envelope" aria-hidden="true"></i>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="row section-info ourTeam-box text-center">
				<div class="col-md-12 section22 ">
					<i class="fas fa-user"></i>
				</div>
				<div class="col-md-12 section2">
					<p>JOHN MEKER</p><br>
					<h1>DESIGNER</h1><br>
				</div>
				<div class="col-md-12 section3">
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
					</p>
				</div>
				<div class="col-md-12 section4">
					<i class="fa fa-facebook-official" aria-hidden="true"></i>
					<i class="fa fa-twitter" aria-hidden="true"></i>
					<i class="fa fa-google-plus" aria-hidden="true"></i>
					<i class="fa fa-envelope" aria-hidden="true"></i>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="row section-danger ourTeam-box text-center">
				<div class="col-md-12 section33">
				<i class="fas fa-user"></i>
				</div>
				<div class="col-md-12 section2">
					<p>VIN DEASEL</p><br>
					<h1>PRODUCTER</h1>
				</div>
				<div class="col-md-12 section3">
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
					</p>
				</div>
				<div class="col-md-12 section4">
					<i class="fa fa-facebook-official" aria-hidden="true"></i>
					<i class="fa fa-twitter" aria-hidden="true"></i>
					<i class="fa fa-google-plus" aria-hidden="true"></i>
					<i class="fa fa-envelope" aria-hidden="true"></i>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection
