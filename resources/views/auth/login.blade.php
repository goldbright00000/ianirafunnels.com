<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link type="text/css" rel="stylesheet" href="css/style.css" />
    <link rel="icon" href="images/brand/risorsa-lg.png" type="image/png" />
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <meta name="google-signin-client_id"
        content="859033187105-lngvjlsn3bk69fbfe21akilvdeuopu43.apps.googleusercontent.com">

    <script>
        function onSignIn(googleUser) {
            var profile = googleUser.getBasicProfile();
            $.ajax({
                url: 'userdata',
                type: 'post',
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    id: profile.getId(),
                    name: profile.getName(),
                    email: profile.getEmail(),
                    image: profile.getImageUrl()
                },
                success: function (data1) {

                    window.location.href = data1;
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    document.write(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }

            });
        }
    </script>


</head>

<body>

    @if(session('passwordsucess'))

    <div id="sucessfullyMessage" class="alert alert-success animated fadeIn email-animation sucess_message">
        <button type="button" class="close s_close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
            {{session('passwordsucess')}}
        </strong>
    </div>
    @endif
    <div class="container">
        <div class="back_icon use_left">
            <a href="javascipt:void(0)" onclick="window.history.go(-1); return false;"><img
                    src="images/icons/arrow-left.png" alt="arrow-left" /></a>
        </div>
        <section class="login_page">
            <header class="forgot_password_header">
                <figure>
                    <img src="images/brand/logo-ianira-DEF.png" alt="brand" class="img-responsive" />

                    <div class="heading_txt">
                        <p class="mp_sale">Map your sales funnel, the easy way</p>
                        <p class="create_strategy">Create strategy, build a template,start implementation</p>
                    </div>
                </figure>
            </header>

            <article class="login_article">
                <form method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}   ">
                            <label>Email or Nickname:</label>
                            <input type="email" class="form-control" id="email" placeholder="prova@gmail.com"
                                name="email" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label>Password:</label>
                            <input type="password" class="form-control" id="password" placeholder="******************"
                                name="password" required>
                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <span class="clearfix"></span>
                        <button type="submit" class="btn btn_login">LOGIN</button>
                        <a href="forget-password" class="foget_txt">Forget your password<span
                                class="question_mark">?</span></a>
                        <span class="clearfix"></span>

                        <span class="clearfix"></span>
                        <div class="login_forget_account">
                            <p>Don't have an account<span class="question_mark">?</span>&nbsp;<span
                                    class="clearfix visible-sm"></span> <a href="{{ route('register') }}"
                                    class="registration" style="padding-top:0">Registration</a></p>
                        </div>
                </form>
            </article>
        </section>
    </div>
    <!-- //container -->

    <div class="container">
        <footer class="footer">
            <div class="row">
                <div class="col-sm-6"></div>
                <div class="col-sm-3"><a href="#" class="faq_support">Faq Support</a></div>
                <div class="col-sm-3"></div>
            </div>
        </footer>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $(".email-animation").fadeIn();
            setInterval(function () {
                $(".email-animation").fadeOut();
            }, 12000);
        })
    </script>


</body>

</html>