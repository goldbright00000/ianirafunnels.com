<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Registration</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link type="text/css" rel="stylesheet" href="./css/style.css" />
    <link rel="icon" href="images/brand/risorsa-lg.png" type="image/png" /> 
       <script src="https://apis.google.com/js/platform.js" async defer></script>
<meta name="csrf-token" content="{!! csrf_token() !!}">

	<meta name="google-signin-client_id" content="859033187105-lngvjlsn3bk69fbfe21akilvdeuopu43.apps.googleusercontent.com" >
        
       <script>
           function onSignIn(googleUser) {

	  var profile = googleUser.getBasicProfile();
	
	
      if(profile){
      }
    }
           </script>
</head>
<body>

    <div class="container">
        <div class="back_icon use_left">
            <a href="javascipt:void(0)" onclick="window.history.go(-1); return false;"><img src="images/icons/arrow-left.png" alt="arrow-left" /></a>
        </div>
        <section class="login_page">
            <header class="forgot_password_header">
                <figure>
                    <img src="images/brand/logo-ianira-DEF.png" alt="brand" class="img-responsive" />

                    <div class="heading_txt">
                        <p class="mp_sale">Map your sales funnel, the easy way</p>
                        <p class="create_strategy">Create strategy, build a template,start implementation</p>
                        <p class="header_registration_txt">Registration</p>
                    </div>
                </figure>
            </header>

            <article class="login_article">
                <form  method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="use_google_account">
                            <!--<span>Use <span class="google ">Google</span> Account</span>-->
                            <div> <span class="google g-signin2 new_g_signin2"></span> <span class="use_account"> Use <span class="google ">Google</span> Account</span></div>
                        </div>
                    </div>
                    
                    
<style>
.new_g_signin2 {
position: relative;
top: 3px;
height: 53px;
}
.new_g_signin2 .abcRioButtonLightBlue{
height: 48px !important;
width: 100% !important;
border: 1px solid #d73636;
margin-top: -3px !important;
opacity: 0;
}
.use_google_account .use_account {
    position: relative!important;
    top: -46px!important;
}
</style>       
                    
                    
                    
                    

                    <div class="line">
                        <span>or</span>
                    </div>
                    
                    
                    

                        <input type="hidden" name="name" value="user">
                     <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} ">
                        <label>Email:</label>
                        <input type="email" id="email"  class="form-control" placeholder="prova@gmail.com" name="email" value="{{ old('email') }}" required>
                         @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                    </div>
                     <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label>Password:</label>
                        <input type="password" id="password"  class="form-control"  placeholder="******************" name="password" required>
                        @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                    </div>

                     <div class="form-group">
                        <label>Confirm Password:</label>
                        <input   id="password-confirm" type="password" class="form-control"  placeholder="******************" name="password_confirmation" required>


                    </div>

                    <button type="submit" class="btn btn_login rgistration_btn1">Registration</button>
                    <!-- <a href="forgot_password" class="foget_txt">Forget your password<span class="question_mark">?</span></a><span class="clearfix"></span> -->
               
<span class="clearfix"></span>
                <div class="login_forget_account">
                    <p>Have an account<span class="question_mark">?</span>&nbsp;<span class="clearfix visible-sm"></span> <a href="{{ route('login') }}" class="registration" style="padding: 0">Log In</a></p>
                </div>

                 </form>
            </article>
</section>
    </div>
    <!-- //container -->
    
<div class="container">
<footer class="footer">
    <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-3"><a href="#" class="faq_support">Faq Support</a></div>
        <div class="col-sm-3"></div>
    </div>
</footer>
</div>

 
    

 


<style>
        
.faq_support{
    text-align: right;
    float: right;
    color: #FDBB17!important;
    margin-bottom: 30px;
    margin-top: 20px;
}
.login_page {
    padding-bottom: 30px;
}

 /*-------------------Start Registration ----------------------*/
.header_registration_txt{
    font-size: 24px !important;
    margin-top: 40px;
    color: #FDBB17 !important;
    letter-spacing: 1px;
    font-family: futuraBook;
    font-weight: 700;  
}   
/*-------------------End Registration ----------------------*/



/*-----------Apply this only not use another css file-----------*/
.login_page {
    background-position: 90px 290px !important;
}
/*-----------Apply this only not use another css file-----------*/
 
.line > span {
    background: #fff;
    display: block;
    width: 30px;
    height: 30px;
    left: 48%;
    top: 7px;
    border-radius: 15px;
    text-transform: uppercase;
    line-height: 32px;
    box-shadow: 0 0 5px 0 #ddd;
    font-size: 12px;
}




@media (max-width: 767px) {

.login_page {
    background-position: 15px 480px !important;
}
span::before {
    background: #ffffff;
}
.faq_support{padding-right: 15px}
  

} 


</style>



</body>
</html>