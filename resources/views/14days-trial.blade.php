@extends('layouts.default')
@section('title','14 Days Trial')
@section('content')
<style>   
</style>
 @if (session()->has('success'))
    <div id="sucessfullyMessage" class="alert alert-success animated fadeIn sucess_message">
        <button type="button" class="close s_close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
            {!! session()->get('success') !!}
        </strong>
    </div>
@endif
    
@if (session()->has('errors'))
    <div id="ErrorMessage"   class="alert alert-danger animated fadeIn sucess_message">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
            {!! session()->get('errors') !!}
        </strong>
    </div>
@endif

<div class="headeryy">
  <div class="container">
      <div class="through_f">
        <h3 class="mb-0">Do you want to build a tunnel in 5 minutes? Try NOW IaniraFunnels!</h3>
      </div>
  </div>
</div>
<section class="top_banner py-5 d-flex align-items-center">
  <div class="sitecontainer ">
    <div class="row align-items-center ">
      <div class="col-sm-6 col-xl-5">
          <div class="baner_pera_wrp">
            <h2 class="text-white mb-md-5 mb-sm-2">
            <b> Start trial 14</b>
            <br>
            <span>Day Trial Now</span>
            </h2>
            <p class="gray_text">
            “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore             </p>
          </div>
      </div>
      <div class="col-sm-6 col-xl-7">
        <div class="doubleCircle_wrp right_top">
            <div class="doubleCircle "></div>
            <figure class="mb-0">
            <img src="{{ asset('public/img/Group-7@2x-right.png')}}" alt="" class="img-fluid" >
            </figure>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="startTrialForm_main mt-5">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
      <div class="doubleCircle_wrp right_top left_bottom">
            <div class="doubleCircle doubleCircle_left"></div>
            <div class="doubleCircle "></div>
            <div class="trail_form_inner bg-light">
              <div class="trailForm_header shadow py-sm-4 px-sm-3 py-3 px-2">
                <h2 class="text-center mb-0">
                  Start trail 14
                </h2>
              </div>
              <div class="trailForm_body p-xl-5 p-sm-4 p-3">
                  <form action="" method="post" class="mt-xl-5 mt-md-2 mt-sm-2 mt-0">
                    <div class="form-group mb-md-3 mb-sm-3">
                      <label for="exampleInputEmail1" class="gray_text">Email</label>
                      <input type="email" class="form-control text-center" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Prova@gmail.com">
                    </div>
                    <div class="form-group mb-md-3 mb-sm-3">
                      <label for="inputPassword" class="gray_text">Password</label>
                        <input type="password" class="form-control text-center" id="inputPassword" placeholder="Password">
                      </div>
                      <div class="form-group mb-md-3 mb-sm-3 ">
                        <label for="inputCPassword" class="gray_text">Confirm Password</label>
                        <input type="password" class="form-control text-center" id="inputCPassword" placeholder="Password">
                      </div>
                      <div class="form-group pt-md-5 pt-3 ">
                        <button type="submit" class="btn trailForm_btn btn-lg btn-block">Start 14-day trial</button>
                      </div>
                  </form>
              </div>
              <div class="trailForm_footer text-center py-xl-5 py-md-4 py-sm-3 py-2">
                <span>Have an account? <a href="javaScript:void(0)" class="text-warning">Log In</a></span>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="bootom_white_space">

</div>
@endsection