@extends('layouts.default')
@section('title','Home Page')
@section('content')
<style>   

</style>
 @if (session()->has('success'))
    <div id="sucessfullyMessage" class="alert alert-success animated fadeIn sucess_message">
        <button type="button" class="close s_close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
            {!! session()->get('success') !!}
        </strong>
    </div>
@endif
    
@if (session()->has('errors'))
    <div id="ErrorMessage"   class="alert alert-danger animated fadeIn sucess_message">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
            {!! session()->get('errors') !!}
        </strong>
    </div>
@endif
<div class="headeryy">
      <div class="container">
          <div class="through_f">
            <h3><i class="fa fa-user" aria-hidden="true"></i>
              Do you want to build a tunnel in 5 minutes? Try NOW IaniraFunnels!
            </h3>
          </div>
      </div>
</div>
<div class="container-fluid all-1">
  <div class="sales-part">
      <div class ="row topage">
        <div class ="col-lg-5 col-md-6 ">
            <div class ="row">
                <div class ="col-md-2 d-xxl-none"></div>
                <div class ="col-md-10 banner_trial_text_wrp">
                    <div class ="titel-letter">
                        <span class="quickly-letter">"Quickly Create Beautiful</span>
                        <br>
                        <span class = "sales-letter font-weight-bold">Sales Funnels</span><br>
                        <span class ="letter-2">That <span id="convert-letter">Convert Your Visitors</span> Into <br>
                        Leads And Then Customers..."<span>
                    </div>
                    <div id = "btn-set"><button type="button" class="btn-coustomize">Start trial 14 Day Trial Now
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing</span>
                    </button></div>
                </div>
            </div>
        </div>
        <div class ="col-lg-7 col-md-6">
        <div class="groupimage"></div>
            <div class="thumbnail-1">
                <div class ="background-images">
                    <img src="{{asset('images/Risorsa 6.png')}}" alt="Lights">
                    <div class="embed-responsive embed-responsive-16by9 video-top"> 
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/7kkSC7jdnf8" 
                            frameborder="0" allowfullscreen></iframe>
                    </div> 
                </div>
              <!-- <img src="{{asset('images/Risorsa 6.png')}}" alt="Lights"> -->
               
            </div>
        </div>
      </div>
  </div>
  <div class="demo_main">
    <div class="sitecontainer">
        <div class="row">
            <div class="col-sm-5">
                <div class="watch_demo">
                <h2 class="text-white font-weight-light mb-3" >Watch the demo</h2>
                    <!-- <p><a href="https://www.clickfunnels.com?wvideo=2cob1ndeyg">
                    <img src="https://embedwistia-a.akamaihd.net/deliveries/8b5d009d651e771cb917a2e8ea29bbd4.jpg?wistia-2cob1ndeyg-1-2cob1ndeyg-video-thumbnail=1&amp;image_play_button_size=2x&amp;image_crop_resized=960x540&amp;image_play_button=1&amp;image_play_button_color=54bbffe0" width="400" height="225" style="width: 400px; height: 225px;"></a></p>
                    <p><a href="https://www.clickfunnels.com?wvideo=2cob1ndeyg"></a></p>
                    <div class="groupimage"></div>
                -->
                </div> 
                <div class="video_wrp">
                    <div class="doubleCircle"></div>
                    <iframe width="560" height="315" src="https://fast.wistia.net/embed/iframe/ox19hzybqo?autoplay=0&amp;wmode=transparent" allowtransparency="true" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" webkitallowfullscreen="webkitallowfullscreen" oallowfullscreen="oallowfullscreen" msallowfullscreen="msallowfullscreen" wmode="opaque" id="fitvid357679" frameborder="0"></iframe>
                </div>
            </div>
            <div class="col-sm-7">
            <h2 class="" style="min-height:32px;"></h2>
                <div class="video_description_text bg-light text-center px-3 py-5">
                <h2 class="text-warning">
                    “Lorem ipsum dolor sit amet, consectetur adipiscing
                    </h2>
                    <p class="text-center font-weight-light">
                    elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam”
                    </p>
                    <div class="total_user_wrp">
                    <ul class="list-inline d-flex justify-content-center">

                            <li class="list-inline-item mx-3 text-center px-2">
                                <h2 class="mb-0">15.5K</h2>
                                <p class="mb-0">Users</p>
                            </li>
                            <li class="list-inline-item mx-3 text-center px-2">
                                <h2 class="mb-0">15.5K</h2>
                                <p class="mb-0">Funnels</p>
                            </li>
                            <li class="list-inline-item mx-3 text-center px-2">
                                <h2 class="mb-0">15.5K</h2>
                                <p class="mb-0">Processed</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
  <section class="wht_funnel_main py-5">
      <div class="sitecontainer">
          <div class="row">
              <div class="col-sm-12 text-center">
                    <figure>
                        <img  src="{{ asset('public/img/Risorsa 6@2x.png')}}" alt="" class="img-fluid" >
                    </figure>
                    <h2 class="funnel_heading font-weight-bold">
                    What’s a Funnel…?
                    </h2>
                    <p>
                    What Exactly is the <b>Difference</b> between a <b>website and a sales funnel ?</b> 
                    </p>
              </div>
          </div>
      </div>
  </section>
  <section>
    <div class="sitecontainer">
    <div class="row">
          <div class="col-sm-7">
              <div class="row">
                  <div class="col-sm-6">
                    <figure>
                        <img  src="{{ asset('public/img/Risorsa 6new.png')}}" alt="" class="img-fluid" >
                    </figure>
                  </div>
                  <div class="col-sm-6">
                    <figure>
                        <img  src="{{ asset('public/img/Artboard - 2.png')}}" alt="" class="img-fluid" >
                    </figure>
                  </div>
              </div>
          </div>
          <div class="col-sm-5">
              <p class="funnel_pera font-weight-light gray_text">
              “Lorem ipsum dolor sit amet, consectetur adipiscing elit, <b>sed do eiusmod</b> tempor incididunt ut labore et 
              dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip 
              ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit .”
              </p>
          </div>
      </div>
    </div>
  </section>
  <section class="multiple_like text-center py-5">
      <div class="sitecontainer">
          <div class="row">
              <div class="col-sm-12">
                  <h2 class="dark_heading font-weight-bold">
                  In Every Funnel, There are Multiple
                <br> List Building Opportunities, Like:
                  </h2>
              </div>
          </div>
      </div>
  </section>
  <section class="pages_main py-5">
    <div class="sitecontainer">
        <div class="row">
            <div class="col-xl-7 col-md-6">
                <div class="figure_wrp position-relative" >
                        <figure>
                            <img src="{{ asset('public/img/Group 7@2x.png')}}" alt="" class="img-fluid" >
                        </figure>
                        <figure class="absolute_figure position-absolute">
                            <img src="{{ asset('public/img/Group 7@2x.png')}}" alt="" class="img-fluid" >
                        </figure>
                    </div>
            </div>
            <div class="col-xl-5 col-md-6">
                <div class="page_text_col">
                    <h2 class="blue_heading text-center font-weight-bold">
                    Optin Pages
                    </h2>
                    <h6 class="dark_subHeading text-center mb-5">
                    To generate leads and build a list…
                    </h6>
                    <p class="gray_text mb-3 font-weight-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit, <b>sed do eiusmod</b> tempor incididunt ut labore et dolore magna aliqua.</p>
                    <p class="gray_text font-weight-light">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit 
                    </p>
                </div>
            </div>
        </div>
    </div>
  </section>
  <section class="pages_main pages_main_reverse py-5">
    <div class="sitecontainer">
        <div class="row">
            <div class="col-xl-5 col-md-6">
                <div class="page_text_col">
                    <h2 class="blue_heading text-center font-weight-bold">
                    Sales Pages
                    </h2>
                    <h6 class="dark_subHeading text-center mb-5">
                    To sell your products……
                    </h6>
                    <p class="gray_text mb-3 font-weight-light">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <p class="gray_text font-weight-light">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
                    eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.”
                    </p>
                </div>
            </div>
            <div class="col-xl-7 col-md-6">
                <div class="figure_wrp position-relative" >
                    <figure>
                        <img src="{{ asset('public/img/Group 7@2x.png')}}" alt="" class="img-fluid" >
                    </figure>
                    <figure class="absolute_figure position-absolute">
                        <img src="{{ asset('public/img/Group 7@2x.png')}}" alt="" class="img-fluid" >
                    </figure>
                </div>
            </div>
        </div>
    </div>
  </section>
  <section class="pages_main py-5">
    <div class="sitecontainer">
        <div class="row">
            <div class="col-xl-7 col-md-6">
                <div class="figure_wrp position-relative" >
                        <figure>
                            <img src="{{ asset('public/img/Group 7@2x.png')}}" alt="" class="img-fluid" >
                        </figure>
                        <figure class="absolute_figure position-absolute">
                            <img src="{{ asset('public/img/Group 7@2x.png')}}" alt="" class="img-fluid" >
                        </figure>
                    </div>
            </div>
            <div class="col-xl-5 col-md-6">
                <div class="page_text_col">
                    <h2 class="blue_heading text-center font-weight-bold">
                    Order From Pages
                    </h2>
                    <h6 class="dark_subHeading text-center mb-5">
                    To collect payments
                    </h6>
                    <p class="gray_text mb-3 font-weight-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit, <b>sed do eiusmod</b> tempor incididunt ut labore et dolore magna aliqua.</p>
                    <p class="gray_text font-weight-light">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit 
                    </p>
                </div>
            </div>
        </div>
    </div>
  </section>
  <section class="pages_main pages_main_reverse py-5">
    <div class="sitecontainer">
        <div class="row">
            <div class="col-xl-5 col-md-6">
                <div class="page_text_col">
                    <h2 class="blue_heading text-center font-weight-bold">
                    Upsell pages
                    </h2>
                    <h6 class="dark_subHeading text-center mb-5">
                    So you can do 1-click upsets…
                    </h6>
                    <p class="gray_text mb-3 font-weight-light">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <p class="gray_text font-weight-light">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
                    eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.”
                    </p>
                </div>
            </div>
            <div class="col-xl-7 col-md-6">
                <div class="figure_wrp position-relative" >
                    <figure>
                        <img src="{{ asset('public/img/Group 7@2x.png')}}" alt="" class="img-fluid" >
                    </figure>
                    <figure class="absolute_figure position-absolute">
                        <img src="{{ asset('public/img/Group 7@2x.png')}}" alt="" class="img-fluid" >
                    </figure>
                </div>
            </div>
        </div>
    </div>
  </section>
  <section class="pages_main py-5">
    <div class="sitecontainer">
        <div class="row">
            <div class="col-xl-7 col-md-6">
                <div class="figure_wrp position-relative" >
                        <figure>
                            <img src="{{ asset('public/img/Group 7@2x.png')}}" alt="" class="img-fluid" >
                        </figure>
                        <figure class="absolute_figure position-absolute">
                            <img src="{{ asset('public/img/Group 7@2x.png')}}" alt="" class="img-fluid" >
                        </figure>
                    </div>
            </div>
            <div class="col-xl-5 col-md-6">
                <div class="page_text_col">
                    <h2 class="blue_heading text-center font-weight-bold">
                    Membership Sites
                    </h2>
                    <h6 class="dark_subHeading text-center mb-5">
                    So you can store and sell your content…
                    </h6>
                    <p class="gray_text mb-3 font-weight-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit, <b>sed do eiusmod</b> tempor incididunt ut labore et dolore magna aliqua.</p>
                    <p class="gray_text font-weight-light">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit 
                    </p>
                </div>
            </div>
        </div>
    </div>
  </section>
  <section class=" text-center py-5">
      <div class="sitecontainer andSomuch">
          <div class="row">
              <div class="col-sm-12">
                  <h2 class="dark_heading ">
                  And so much more…
                  </h2>
                  <p>
                    <span class="text-warning"> “Lorem ipsum dolor sit amet, consectetur adipiscing</span> elit, sed do eiusmod tempor incididunt ut labore et”
                    </p>
              </div>
          </div>
      </div>
  </section>
  <section class="start_trial_main ">
      <div class="siteSmallContainer">
          <div class="row">
            <div class="col-sm-6 d-flex align-items-center justify-content-center ">
                    <div class="trial_col_text_wrp ">
                  <h2 class="text-warning text-center">Start generating funnels</h2>
                    <p class="gray_text font-weight-light">
                    “Lorem ipsum dolor sit amet, consectetur adipiscing elit, <b>sed do eiusmod</b> tempor incididunt ut labore et
                    </p>
                    <button class="start_trial_btn btn btn-warning btn-lg text-white">
                    <b>Start trial 14 Day Trial Now</b>
                    <span class="gray_text">Lorem ipsum dolor sit amet, consectetur adipiscing</span>
                    </button>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="doubleCircle_wrp right_top">
                    <div class="doubleCircle "></div>
                    <figure class="mb-0">
                    <img src="{{ asset('public/img/Imagestyle@2x.png')}}" alt="" class="img-fluid" >
                    </figure>
                </div>
            </div>
          </div>
      </div>
  </section>
  <section class="start_trial_main start_trial_reverse  ">
      <div class="siteSmallContainer">
          <div class="row">
          <div class="col-sm-6">
              <div class="doubleCircle_wrp">
                <div class="doubleCircle"></div>
                <figure class="mb-0">
                <img src="{{ asset('public/img/Imagestyle@2x.png')}}" alt="" class="img-fluid" >
                </figure>
              </div>
              
            </div>
            <div class="col-sm-6 d-flex align-items-center justify-content-center ">
                    <div class="trial_col_text_wrp ">
                  <h2 class="text-warning text-center">Sell your products</h2>
                    <p class="gray_text font-weight-light">
                    “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et 
                    </p>
                    <button class="start_trial_btn btn btn-warning btn-lg text-white">
                    <b>Start trial 14 Day Trial Now</b>
                    <span class="gray_text">Lorem ipsum dolor sit amet, consectetur adipiscing</span>
                    </button>
                </div>
            </div>

          </div>
      </div>
  </section>
  <section class="faq_main mt-5 pt-5">
    <div class="siteSmallContainer">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="text-center faq_heading mb-5">
                    FAQ
                </h2>
            </div>
            <div class="col-sm-6 mb-5">
                <div class="faq_col_inner font-weight-bold">
                    <h3 class="text-center mb-4">
                    Lorem ipsum?
                    </h3>
                    <p>
                    “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                    </p>
                </div>
            </div>
            <div class="col-sm-6 mb-5">
                <div class="faq_col_inner font-weight-bold">
                    <h3 class="text-center mb-4">
                    Lorem ipsum?
                    </h3>
                    <p>
                    “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                    </p>
                </div>
            </div>
            <div class="col-sm-6 mb-5">
                <div class="faq_col_inner font-weight-bold">
                    <h3 class="text-center mb-4">
                    Lorem ipsum?
                    </h3>
                    <p>
                    “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                    </p>
                </div>
            </div>
            <div class="col-sm-6 mb-5">
                <div class="faq_col_inner font-weight-bold">
                    <h3 class="text-center mb-4">
                    Lorem ipsum?
                    </h3>
                    <p>
                    “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                    </p>
                </div>
            </div>
            
            
        </div>
    </div>
  </section>
  <section class="get_freel_trial_main mt-5 pt-5 pt-sm-0">
        <div class="siteSmallContainer">
            <div class="getblue_trial_inner ">
                <div class="dark_head_wrp">
                    <h2 class="font-weight-bold">
                    Everything you get with
                    <br> Your <a href="javascript:boid(0)">FREE TRIAL</a> today!
                    </h2>
                </div>
                <div class="list_list_wrp p-5 ">
                    <div class="list_wrp_inner">
                        <ul>
                            <li>
                                <span>
                                    Lorem ipsum dolor sit amet, consectetur
                                </span> 
                            </li>
                            <li>
                                <span>
                                    Lorem ipsum dolor sit amet, consectetur
                                </span> 
                            </li>
                        </ul>
                        <h3 class="light_center_heading text-center font-weight-bold mb-5 mb-sm-3">
                        Lorem ipsum dolor sit amet
                        </h3>
                        <div class="Free_text_wrp text-center">
                            <p class="gray_text">Get started today for: </p>
                            <h2  class="text-primary font-weight-bold mb-5 mb-sm-2">
                                FREE!
                            </h2>
                        </div>
                        <button class="start_trial_btn btn btn-primary btn-lg text-white">
                        <b>Start trial 14 Day Trial Now</b>
                        <span class="gray_text">Lorem ipsum dolor sit amet, consectetur adipiscing</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
  </section>
  <section class="bootom_white_space"></section>
</div>
@endsection