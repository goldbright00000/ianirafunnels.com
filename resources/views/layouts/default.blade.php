<!DOCTYPE html>
<html>

    <head>
        <title>@yield('title')</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{ asset('public/style/style.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('public/style/bootstrap.min.css') }}" type="text/css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="{{ asset('public/style/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('public/style/js/custom.js') }}"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
            integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    </head>

    <body>
        <div class="header header_one">

            <div class="container">
                <div class="row">
                    <div class="col-sm-9 col-6">
                        <div class="logo" style="color:red;"> <a href="home">
                            <img  src="{{ asset('images/funel_logo_header.png')}}"></a></div>
                    </div>
                    <div class="col-sm-3 mem_login col-6">
                        @if (Auth::guest())
                        <a href="funnel-registration">
                            <div class="memeberlogin_user">
                                <div class="member_login"> <i class="fa_prepended fa fa-user"></i>&nbsp Member Login </div>
                            </div>
                        </a>
                        @else
                        <a href="{{ route('logout') }}">
                            <div class="memeberlogin_user">

                                <div class="member_login"> <i class="fa fa-power-off" aria-hidden="true"></i> Sign Out
                                </div>

                            </div>
                        </a>
                        @endif
                    </div>
                </div>
            </div>

        </div>
        @yield('content')

        <!--footer starts from here-->
        <footer class="footer">
            <div class="container bottom_border">
                <div class="row">
                    <div class=" col-sm-4 col-md  col-6 col">
                        <h5 class="headin5_amrc col_white_amrc pt2">ABOUT</h5>
                        <!--headin5_amrc-->
                        <ul class="footer_ul_amrc">

                            <li><a href="{{url('location')}}">Locations </a></li>
                            <li><a href="{{url('our-team')}}">Meet The Team</a></li>
                            <li><a href="{{url('origin-story')}}">Origin Story</a></li>
                            <li><a href="{{url('career')}}">Careers</a></li>

                        </ul>
                        <!--footer_ul_amrc ends here-->
                    </div>
                    <div class=" col-sm-4 col-md  col-6 col">
                        <h5 class="headin5_amrc col_white_amrc pt2">PRODUCTS</h5>
                        <!--headin5_amrc-->
                        <ul class="footer_ul_amrc">
                            <li><a href="#">What is ClickFunnels?</a></li>
                            <li><a href="#">What is Etison Editor?</a></li>
                            <li><a href="#">What is Actionetics?</a></li>
                            <li><a href="#">What is Backpack?</a></li>
                        </ul>
                        <!--footer_ul_amrc ends here-->
                    </div>
                    <div class=" col-sm-4 col-md  col-12 col">
                        <h5 class="headin5_amrc col_white_amrc pt2">HELP</h5>
                        <ul class="footer_ul_amrc">
                            <li><a href="#">ClickFunnels Blog</a> </li>
                            <li><a href="#">Documentation</a></li>
                            <li><a href="#">Official Facebook Group</a></li>
                            <li><a href="#">Support Chat</a></li>
                        </ul>
                    </div>
                    <div class=" col-sm-4 col-md  col-12 col">
                        <div class="headin5_amrc col_white_amrc pt2">
                            <a href="home" href="home">
                                <img  src="{{ asset('images/logo-black.png')}}" width="200px;">
                            </a>
                        </div>
                    </div>
                    <!--footer_ul2_amrc ends here-->
                </div>
            </div>
            <div class="container">
                <!-- <ul class="foote_bottom_ul_amrc">
                    <li><a href="home">Home</a></li>
                    <li><a href="location">Location</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Pricing</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Contact</a></li>
                </ul> -->
                <!--foote_bottom_ul_amrc ends here-->
                <p class="text-center">Copyright @2019 | Designed With by <a href="#">Ianira Funnels</a></p>

                <ul class="social_footer_ul">
                    <li><a href="#"><i class="fab fa-facebook-f"></i>
                        </a></li>
                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                    <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                </ul>
                <!--social_footer_ul ends here-->
            </div>
        </footer>
    </body>
</html>