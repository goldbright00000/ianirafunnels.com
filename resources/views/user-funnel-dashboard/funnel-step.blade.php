@extends('user-dashboard.user-dashboard-default') @section('title','Jason Funnel') @section('content')
 <!-- css -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  
  
  
<main id="main" class="main_dashboard">
<div class="container-fluid container_fluid">


<div class="row first">
    <div class="col-md-6">
    <div class="col-sm-1">
         <span class="fa_cog_icon"><i class="fa fa-cog"></i></span>
    </div>
    <div class="col-sm-4">
            <h4 class="heading_test">jhjkhj</h4>
    </div>

    <div class="col-sm-7">


<div class="btn-group btn-group-justified top_col-md-7">
    <a href="#" class="btn btn-warning"><i class="fa fa-link"></i></a>
    <a href="#" class="btn btn-warning"><i class="fa fa-copy"></i></a>
    <a href="#" class="btn btn-warning"><i class="fa fa-external-link-alt"></i></a>
    <a href="#" class="btn btn-warning"><i class="fa fa-copy"></i></a>
    <a href="#" class="btn btn-warning"><i class="fa fa-question-circle"></i></a>
</div>



    </div>

    </div><!-- .col-md-6 -->


<div class="col-md-6">


<ul class="btn-group top_col_md_6">
    <a href="#" class="btn btn-warning active_nav"><i class="fa fa fa-bars"></i> Steps</a>
    <a href="#" class="btn btn-warning"><i class="fa fa-chart-bar"></i> Stats</a>
    <a href="#" class="btn btn-warning"><i class="fa fa-users"></i> Contacts</a>
    <a href="#" class="btn btn-warning"><i class="fa fa-money-bill-alt"></i> Sales</a>
    <a href="#" class="btn btn-warning"><i class="fa fa-cog"></i> Settings</a>
</ul>


</div><!-- .col-md-6 -->



</div><!-- .row first -->



<div class="row second"> 
<div class="col-sm-3 left-sidebar">
    <article>
    <ul class="list-group">
        <li class="list-group-item"><a href="#"><span class="left_box"><i class="fa fa-check-square"></i></span> Launch Checklist</a><i class="fa fa-times pull-right fa_times"></i></li>
        <li class="list-group-item"><a href="#"><span class="left_box"><i class="fa fa-list-ol"></i> </span>  Funnel Steps </a> <i class="fa fa-times pull-right fa_times"></i></li> 
        <li class="list-group-item"><a href="#"><span class="left_box"> <i class="fa fa-envelope"></i></span>  Optin </a> <i class="fa fa-times pull-right fa_times"></i></li> 
        <li class="list-group-item"><a href="#"><span class="left_box"> <i class="fa fa-rocket"></i> </span>  Launch Page 1 </a> <i class="fa fa-times pull-right fa_times"></i></li> 
        <li class="list-group-item"><a href="#"><span class="left_box"> <i class="fa fa-rocket"></i></span>  Launch Page 2 </a> <i class="fa fa-times pull-right fa_times"></i></li> 
        <li class="list-group-item"><a href="#"><span class="left_box"> <i class="fa fa-rocket"></i></span>  Launch Page 3 </a> <i class="fa fa-times pull-right fa_times"></i></li> 
    </ul>
    </article> 

    <div class="button_handler_left">
        <button class="btn" data-toggle="modal" data-target="#button_handler_left_modal"> <span><i class="fa fa-plus"></i></span> &nbsp; Add new steps</button>
    </div>
</div><!-- .left-sidebar -->

<div class="col-sm-9 main_col_9_container">




    <div class="row">
        <div class="col-md-12 col_md_12_pl_0">
             <nav class="navbar navbar-default navigation_top2" role="navigation">
  
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <!-- <li class="active"><a href="#">Optin</a></li> -->
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> Optin <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#"><i class="fa fa-envelope"></i> Email Optin</a></li>
            <li><a href="#"><i class="fa fa-download"></i> Thank You</a></li>
          </ul>
        </li>


          <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-shopping-cart"></i> Sales <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#"><i class="fa fa-dollar-sign"></i> Sales Page</a></li>
            <li><a href="#"><i class="fa fa-rocket"></i> Product Launch</a></li>
            <li><a href="#"><i class="fa fa-shopping-cart"></i> Order Form</a></li>
            <li><a href="#"><i class="fa fa-arrow-up"></i> One Click Upsell (OTO)</a></li>
            <li><a href="#"><i class="fa fa-arrow-down"></i> One Click Downsell</a></li>
            <li><a href="#"><i class="fa fa-check-square"></i> Order Confirmation</a></li>
          </ul>
        </li>
 

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-microphone"></i> Webinar <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#"><i class="fa fa-envelope"></i> Registration</a></li>
            <li><a href="#"><i class="fa fa-download"></i> Thank You</a></li>
            <li><a href="#"><i class="fa fa-microphone"></i> Broadcast Room</a></li>
          </ul>
        </li>


        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-graduation-cap"></i> Membership <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#"><i class="fa fa-sign-in-alt"></i> Member Access</a></li>
            <li><a href="#"><i class="fa fa-tachometer-alt"></i> Membership Area</a></li>
          </ul>
        </li>

        <li><a href="#"><i class="fa fa-external-link-square-alt"></i> ClickPop</a></li>
         <li><a href="#"><i class="fa fa-bars"></i> Misc</a></li>
      </ul>
    
    </div><!-- /.navbar-collapse -->
  
</nav>
        </div>
    </div>


 <div class="row categorySubNav categorySubNav1">
        <div class="col-md-12">
            <ul class="nav navbar-nav navbar-right categorySubNav1_navbar_right">
                <li><a href="#" class="active"><i class="fa fa-th-large"></i> Overview</a></li>
                <li><a href="#"><i class="fa fa-radiation-alt"></i> AutomationMenu</a></li>
                <li><a href="#"><i class="fa fa-shopping-cart"></i> ProductsMenu</a></li>
                <li><a href="#"><i class="fa fa-wrench"></i> PublishingMenu</a></li>
            </ul>     
        </div>        
        </div>
   

<style type="text/css">
  .categorySubNav1{
    padding: 0 !important;
  }
  #bs-example-navbar-collapse-1 .navbar-nav>li>a{
     font-size: 12pt;
      color: #777;
    text-transform: capitalize;
  }

   #bs-example-navbar-collapse-1 {
    background: #fff !important;
    border: 0 !important;
    box-shadow: 0px 3px 2px -3px #ddd !important;
}



  .categorySubNav1 .navbar-nav>li>a{
      font-size: 12pt;
      color: #777;
      text-transform: capitalize;
  }

.categorySubNav1 .navbar-nav>li>a.active{
    background: #f28d2c;
    color: #fff;
}

.categorySubNav1{
  background: #ffffff !important;
}
.categorySubNav1_navbar_right >li>a{
  -webkit-transition: 0.3s;
  transition: 0.3s;
}
.categorySubNav1_navbar_right >li>a:hover{
  background: #f28d2c;
    color: #fff;
}






</style>


    <div class="row categorySubNav">
        <div class="col-md-9">
            <a class="showlink" href="#"><i class="fa fa-file-o"></i> Show Templates</a>         
        </div>

        <div class="col-md-3">
            <div class="search_small">
                <input class="select2-choices" />
                <span class="clearfix"></span>
                  <ul class="list-group show_focus_list_group">
                    <a class="list-group-item">First item</a>
                    <a class="list-group-item">Second item</a>
                    <a class="list-group-item">Third item</a>
                  </ul> 
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-4">
            <h3>Optin-Overview</h3>
        </div>
        <div class="col-sm-8">

            <div class="btn-group btn-group-sm btn_group_btn1 pull-right">
                <button type="button" class="btn btn-primary"><i class="fa fa-level-down-alt"></i></button>
                <button type="button" class="btn btn-primary"><i class="fa fa-link"></i></button>
                <button type="button" class="btn btn-primary"><i class="fa fa-globe"></i></button>
                <button type="button" class="btn btn-primary"><i class="fa fa-download"></i></button>
                <button type="button" class="btn btn-primary"><i class="fa fa-file"></i></button>
                <button type="button" class="btn btn-primary"><i class="fa fa-clipboard"></i></button>
                <button type="button" class="btn btn-primary"><i class="fa fa-trash"></i></button>
            </div>



        </div>
        <span class="clearfix"></span>
        <hr class="line_hr" />
    </div>


    <div class="row" style="margin-top: 20px">
        <div class="col-md-12 input_group_addon_1">  
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-cog"></i></span>
                    <input id="" type="text" class="form-control" name="" placeholder="" value="https://ianirafunnels.com/funnel-step">
                </div>
        </div>
    </div><!-- .row -->

 

 <div class="row  pannel_box">
    <div class="col-sm-4">
        <article class="pannel_box_article">
            <p> <i class="fa fa-heart"></i> Control - 1 </p>

            <!-- <button class="black_btn1" data-toggle="modal" data-target="#navigation_modal_popup1"><i class="fa fa-share-square"></i> Change</button> -->
            <div class="row">
              <div class="group_btn2">
                  <div class="btn-group btn-group-sm">
                    <button type="button" class="btn"><i class="fa fa-pencil-alt"></i></button>
                    <button type="button" class="btn"><i class="fa fa-trash-alt"></i></button>
                    <button type="button" class="btn"><i class="fa fa-cog"></i></button>
                    <button type="button" class="btn"><i class="fa fa-map-signs"></i></button>
              </div>
          </div>
        </article>
    </div> 

     <div class="col-sm-4">
        <article class="pannel_box_article pannel_box_article_mute">
            <p> <i class="fa fa-heart"></i> Control - 2 </p>

            <button class="black_btn1" style="width: 175px;margin-left: 15%;"><i class="fa fa-plus"></i> Create Variation</button>
        </article>
    </div> 
</div>




</div><!-- .main_col_9_container -->

</div><!-- .row second -->


 

</div><!-- .container-fluid container_fluid -->
</main><!-- .main_dashboard -->





<!-- ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: Modal-1 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->

<!-- Modal -->
  <div class="modal fade" id="button_handler_left_modal" role="dialog">

  <form method="POST" action="{{url('add/funnel-step')}}">

{{ csrf_field()}}

   
    </form>
  </div>

 

<!-- ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: Modal-2 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->
 

<!-- navigation_modal_popup1  style="display: block-->
<div id="navigation_modal_popup1" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg modal_lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
         
        <div class="row">
            <div class="col-sm-12">
               


<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#tab_1">All</a></li>
  <li><a data-toggle="tab" href="#tab_2">Optin</a></li>
  <li><a data-toggle="tab" href="#tab_3">Thank You</a></li>
   <li><a data-toggle="tab" href="#tab_4">Order Form</a></li>
   <li><a data-toggle="tab" href="#tab_5">Launch Page</a></li> 
   <li><a data-toggle="tab" href="#tab_6">Membership Area</a></li>
  <li><a data-toggle="tab" href="#tab_7">Webinar Registration</a></li>
   <li><a data-toggle="tab" href="#tab_8">Auto Webinar Registration</a></li>
  <li><a data-toggle="tab" href="#tab_9">Webinar Countdown</a></li>

  <li><a data-toggle="tab" href="#tab_2">Optin</a></li>
  <li><a data-toggle="tab" href="#tab_3">Thank You</a></li>
   <li><a data-toggle="tab" href="#tab_4">Order Form</a></li>
   <li><a data-toggle="tab" href="#tab_5">Launch Page</a></li> 
   <li><a data-toggle="tab" href="#tab_6">Membership Area</a></li>
  <li><a data-toggle="tab" href="#tab_7">Webinar Registration</a></li>
   <li><a data-toggle="tab" href="#tab_8">Auto Webinar Registration</a></li>
  <li><a data-toggle="tab" href="#tab_9">Webinar Countdown</a></li>
</ul>





            </div>
        </div>

      </div>
      <div class="modal-body">
        

   
<div class="tab-content">
  <div id="tab_1" class="tab-pane fade in active">

    <div class="row main_article">

        <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c5aaedaaaaa563f2d8297/5c3c5aaedaaaaa563f2d8297.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->


 <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c3f1edaaaaa563f2d80d5/5c3c3f1edaaaaa563f2d80d5.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->



         <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c6073daaaaa563f2d82f2/5c3c6073daaaaa563f2d82f2.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->



        <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c5aaedaaaaa563f2d8297/5c3c5aaedaaaaa563f2d8297.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->


 <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c3f1edaaaaa563f2d80d5/5c3c3f1edaaaaa563f2d80d5.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->



         <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c6073daaaaa563f2d82f2/5c3c6073daaaaa563f2d82f2.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->

        <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c5aaedaaaaa563f2d8297/5c3c5aaedaaaaa563f2d8297.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->


 <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c3f1edaaaaa563f2d80d5/5c3c3f1edaaaaa563f2d80d5.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->



         <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c6073daaaaa563f2d82f2/5c3c6073daaaaa563f2d82f2.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->


    </div><!-- .main_article -->
     
  </div><!-- #tab_1 --> 







  <div id="tab_2" class="tab-pane fade">
    <div class="row main_article">

        <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c5aaedaaaaa563f2d8297/5c3c5aaedaaaaa563f2d8297.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->


 <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c3f1edaaaaa563f2d80d5/5c3c3f1edaaaaa563f2d80d5.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->



         <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c6073daaaaa563f2d82f2/5c3c6073daaaaa563f2d82f2.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->



        <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c5aaedaaaaa563f2d8297/5c3c5aaedaaaaa563f2d8297.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->

 


    </div><!-- .main_article -->
  </div><!-- #tab_2 -->



  <div id="tab_3" class="tab-pane fade">
  <div class="row main_article">

        <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c5aaedaaaaa563f2d8297/5c3c5aaedaaaaa563f2d8297.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->


 <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c3f1edaaaaa563f2d80d5/5c3c3f1edaaaaa563f2d80d5.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->



         <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c6073daaaaa563f2d82f2/5c3c6073daaaaa563f2d82f2.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->



        <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c5aaedaaaaa563f2d8297/5c3c5aaedaaaaa563f2d8297.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->


 <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c3f1edaaaaa563f2d80d5/5c3c3f1edaaaaa563f2d80d5.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->


    </div><!-- .main_article -->
  </div><!-- #tab_3 -->




<div id="tab_4" class="tab-pane fade">
    <div class="row main_article">

        <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c5aaedaaaaa563f2d8297/5c3c5aaedaaaaa563f2d8297.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->


 <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c3f1edaaaaa563f2d80d5/5c3c3f1edaaaaa563f2d80d5.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->

 <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c5aaedaaaaa563f2d8297/5c3c5aaedaaaaa563f2d8297.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->


         <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c5aaedaaaaa563f2d8297/5c3c5aaedaaaaa563f2d8297.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->


         <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c6073daaaaa563f2d82f2/5c3c6073daaaaa563f2d82f2.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->



        <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c5aaedaaaaa563f2d8297/5c3c5aaedaaaaa563f2d8297.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->

    </div><!-- .main_article -->
  </div><!-- #tab_4 -->



<div id="tab_5" class="tab-pane fade">
    <div class="row main_article">

       


 <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c3f1edaaaaa563f2d80d5/5c3c3f1edaaaaa563f2d80d5.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->



         <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c6073daaaaa563f2d82f2/5c3c6073daaaaa563f2d82f2.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->



        <div class="col-md-3">
            <article class="inner_article">
                <figure>
                    <img src="https://s3-us-west-2.amazonaws.com/xfunnels-builder-mint/admin-templates/5c3c5aaedaaaaa563f2d8297/5c3c5aaedaaaaa563f2d8297.png" class="img-responsive">
                </figure>
            
                <div class="inner_article_hover">
                    <div class="btn_div1">
                        <p><a href="#" class="btn one_1"> <i class="fa fa-plus"></i>  Select Theme</a></p>
                        <p><a href="#" class="btn two_2"><i class="fa fa-eye"></i> Preview</a></p>
                    </div>
                </div>
            </article>
        </div> <!-- .col-md-3 -->
    </div><!-- .main_article -->
  </div><!-- #tab_5-->









</div>








      </div>
    </div>
  </div>
</div>
<!-- #navigation_modal_popup1 -->
 





<style>
    body{
    font-family: 'Roboto', sans-serif;
    font-size: 14px;
    background: #fff;
}


h1,h2,h3,h4,h5,h6,p,ol,ul li,ul li a, span,a,div{
    font-family: 'Roboto', sans-serif;
}

.container_fluid {
    margin-left: 30px;
    margin-right: 30px;
    margin-top: 75px;
}
.first_heading  h3{
    margin: 0
}
.heading_test {
    font-size: 25px;
    font-weight: 600;
    color: #333;
    margin-top: 18px;
}

.top_col-md-7 a, .top_col_md_6 a {
    padding: 15px;
    background: transparent;
    border: 0;
    font-size: 18px;
    padding-top: 17px;
}
.top_col_md_6{
    margin: 0;
    padding: 0
}
.top_col_md_6 {
        font-size: 18px;
}
.btn-warning {
    color: #333;
}
.first{
    min-height: 57px;
        background: #f28d2c;
}

.navbar-default {
    background-color: #f8f8f8;
    border-color: transparent !important;
    border-radius: 0 0 !important;
}

.col_md_12_pl_0{
    padding: 0;
}


.navigation_top2 .dropdown-menu>li>a {
    padding: 10px!important;
}




.navigation_top2 .navbar-default .navbar-nav>.open>a:hover {
    color: #f28d2c;
    background-color: transparent;
}


 


.left-sidebar {
    background: #f8f8f8;
    min-height: 625px;
    padding: 0;
}
 
.left-sidebar .list-group {
    margin-bottom: 0;
    margin-top: 0;
}
.panel-default {
    border-color: #f9f9f9;
}
.panel_default_wrap .panel-footer {
    padding: 15px 15px;
    background-color: #ffffff;
    border-top: 0;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 0;
    color: #808080;
}

.left-sidebar .list-group-item {
    background: transparent;
    display: block;
    width: 100%;
    color: #ddd;
    font-size: 20px;
    border: 0;
    border-left: 3px solid #535d65;
    margin-top: 7px;
    border-radius: 0 0;
}
.left-sidebar .list-group-item a {
    color: #929ba2;
    text-decoration: none;
    padding: 0;
    padding-left: 0;
    display: inline-block;
    font-size: 18px;
}
.left-sidebar .list-group-item a .fa{
    color: #535d65;
}

.left-sidebar .left_box {
    /*background: #f5f5f5;*/
    width: 50px;
    line-height: 45px;
    display: inline-block;
    text-align: center;
    margin-right: 15px;
}
.left-sidebar .list-group-item:hover{
     background: #ffffff;
         border-left: 3px solid #f28d2c;
}
 
.left-sidebar .list-group-item:hover a .left_box{
 color: #777 !important;
}

.panel_default_wrap .panel-body{
    padding: 0;
}

.left-sidebar .fa_times {
    padding-top: 15px;
    padding-right: 15px;
    display: inline-block;
    cursor: pointer;
}
.left-sidebar .fa_times:hover{
  color: #777;
}  

.fa_cog_icon {
    font-size: 30px;
    margin-top: 10px;
    display: inline-block;
    margin-left: -15px;
}

.panel_default_wrap img{
    width: 100%
}


.showlink {
    color: #748193;
    display: inline-block;
    padding-top: 7px;
    text-decoration: none;
}
.categorySubNav {
    padding: 8px 5px;
    padding-right: 5px;
    padding-bottom: 5px;
    border-radius: 5px;
    background: #f8f8f8;
    border: 0;
    margin-left: 0;
    margin-bottom: 20px;
}
.select2-choices {
    border: none !important;
    background: #fff url(./images/search.png) no-repeat left !important;
    background-position: 98% 50% !important;
    padding: 2px 5px !important;
    padding-right: 15px !important;
    font-size: 13px !important;
    border-radius: 4px !important;
    height: 35px;
    width: 100%;
}
.search_small {
    width: 340px;
    border-radius: 4px !important;
    float: right;
    margin-top: -4px !important;
    border: 2px solid #EAEAEA !important;
    font-size: 14px;
    position: relative;
}
.top_col-md-7 .btn-warning,
.top_col_md_6 .btn-warning{
     box-shadow: 0 0 !important;
    border-radius: 0 0;
}
.top_col-md-7 .btn-warning:hover,
.top_col_md_6 .btn-warning:hover {
    color: #f28d2c!important;
    background-color: #333333 !important;
    border-color: #ffffff!important;
}

.panel_default_relative{
    position: relative;
    overflow: hidden;
}
.hover_effect {
    position: absolute;
    width: 100%;
    height: 100%;
    background: rgba(242, 141, 44, 0.5);
    text-align: center;
    top: 0;
    left: 0;
    right: 0;
    padding: 15px;
    -webkit-transition: 0.5s;
    -webkit-transform: skew(45deg) translateY(-500px);
    transition: 0.5s;
    transform: skew(45deg) translateY(-500px);
}
.panel_default_relative:hover .hover_effect{
    -webkit-transform: none;
    transform: none;
}

.hover_effect_inner {
    padding-top: 100px;
}
.hover_effect_inner .btn{
        width: 100px;
    font-size: 14px;
    border: 0;
    border-radius: 0 0;
    text-transform: uppercase;
}
    
.caret-up {
    width: 0; 
    height: 0; 
    border-left: 4px solid rgba(0, 0, 0, 0);
    border-right: 4px solid rgba(0, 0, 0, 0);
    border-bottom: 4px solid;
    
    display: inline-block;
    margin-left: 2px;
    vertical-align: middle;
}

.active_nav {
    color: #f28d2c!important;
    background-color: #f8f8f8 !important;
    border-color: #ffffff!important;
    box-shadow: 0 0 !important;
    border-radius: 0 0;
}

.button_handler_left > .btn:active,
.button_handler_left > .btn:focus,
.button_handler_left > .btn {
    border: 1px solid #f28d2c;
    background: #f28d2c !important;
    border-radius: 0 !important;
    padding: 10px 20px;
    font-size: 16px;
    color: #ffffff;
    outline: 0!important;
    box-shadow: 0 0 !important;
    text-transform: uppercase;
    display: block;
    margin: 20px;
    margin-top: 50px;
    width: 285px;
    -webkit-transition: 0.5s;
    transition: 0.5s;
}
.button_handler_left > .btn:hover{
  background: #ffffff !important;
  color: #f28d2c;
}

#button_handler_left_modal .modal-header {
    padding: 15px;
    border-bottom: 1px solid #f28d2c;
    background: #f28d2c !important;
    color: #fff;
}


#button_handler_left_modal .form-control {
    border-radius: 0 !important;
    box-shadow: 0 0 !important;
    border-color: #f0f0f0;
    background: #f9f9f9;
    height: 45px;
}

#button_handler_left_modal .btn {
    outline: 0 !important;
display: block;
margin: 0 auto 20px auto;
font-size: 18px;
background: #fff !important;
box-shadow: 0 0 3px 0 #f28d2c !important;
color: #f28d2c !important;
transition: 0.5s;
width: 286px;
height: 60px;

}
#button_handler_left_modal .btn:hover{
    background: #f28d2c !important;
    color: #fff !important;
}

#button_handler_left_modal .modal-body{
    padding: 40px 40px 20px 40px!important;
}

 #button_handler_left_modal .fa_arrow_right1 {
    margin-left: 10px;
margin-top: 0px;
color: #f28d2c;
display: inline-block;
padding-top: 4px;

}

/*----------------------------Checkbox css--------------------------*/
.checkbox {
    left: 50px;
    transform: translate(-50%,-50%);
    width: 100px;
    height: 45px;
    border-radius: 25px;
    background: linear-gradient(0deg, #d8d8d8, #cccccc);
    border-top: 0.01em solid #ececec;
    border-bottom: 0.01em solid #ececec;
}
.checkbox .inner {
    position: absolute;
    top: 10px;
    left: 10px;
    right: 10px;
    bottom: 10px;
    /*background: linear-gradient(0deg, #a5a5a5, #717171);*/
    border-radius: 20px;
    /*box-shadow: inset 0 0 2px rgba(0,0,0,.5);*/
    background: #fff;
}
.checkbox {
    position: relative;
    display: block;
    margin-top: 15px !important;
    margin-bottom: 0 !important;
}

.checkbox .inner .toggle 
{
    position:absolute;
    top:-6px;
    left:-3px;
    width:36px;
    height:36px;
    border-radius:50%;
    background:linear-gradient(0deg, #ccc, #e4e4e4);
    box-shadow: 0 4px 6px rgba(0,0,0,0.2);
    box-sizing:border-box;
    border-top:0.04em solid #ececec;
    border-bottom:0.04em solid #ececec;
    transition:0.5s;
}
.checkbox .inner.active .toggle:before {
    content:'ON';
    color:green;
}
.checkbox .inner .toggle:before {
    content:'OFF';
    position:absolute;
    left: 11px;
    top: 13px;
    color:#a5a5a5;
    font-size:10px;
    line-height:10px;
    text-align:center;
} 
.checkbox .inner.active .toggle 
{
    left:47px;
}
/*----------------------------Checkbox css--------------------------*/

.btn_group_btn1 {
    float: right!important;
    margin-top: 15px;
}

.btn_group_btn1 > .btn:focus,
.btn_group_btn1 > .btn:active,
.btn_group_btn1 > .btn {
    background: #2196F3 !important;
    border: 1px solid #2196f3 !important;
    margin-left: 5px !important;
    box-shadow: 0 0 !important;
    border-radius: 0 0 !important;
    outline: 0 !important;
    transition: 0.5s;
}

.btn_group_btn1 > .btn:hover{
 background: #ffff !important;
 color: #2196F3 !important;   
}

.btn_group_btn1 > .btn:last-child{
      background: #e00 !important;
    border: 1px solid #e00 !important;
     color: #fff !important; 
}
.btn_group_btn1 > .btn:last-child:hover{
    background: #fff !important;
     color: #e00 !important; 
}

hr {
    margin-top: 20px;
    margin-bottom: 20px;
    border: 0;
    border-top: 1px solid #eee;
    margin-left: 20px;
}

.input_group_addon_1 .input-group {
    height: 45px;
}
.input_group_addon_1 .input-group-addon {
    font-size: 24px!important;
    color: #fff!important;;
    background-color: #f28d2c!important;;
    border: 1px solid #f28d2c!important;;
}

.input_group_addon_1 .form-control {
    height: 45px;
    box-shadow: 0 0 !important;
    outline: 0!important;
    border: 2px solid #f28d2c!important;
    background: #fff!important;
    color: #777 !important;
}


.pannel_box{
    margin-top: 30px;
    margin-bottom: 100px;
}


.pannel_box .pannel_box_article {
    background: #fff;
    -webkit-box-shadow: 0 0 5px 0 #ddd;
    box-shadow: 0 0 5px 0 #ddd;
    padding: 15px;
    background: url(https://ianirafunnels.com/images/change_bg1.png) no-repeat;
    background-position: center 50px;
    min-height: 310px;
    position: relative;
}

.pannel_box .pannel_box_article .black_btn1{
    position: absolute;
    bottom: 15px;
    display: inline-block;
    margin-left: 25%;
    width: 100px;
    height: 35px;
    outline: 0 !important;
    border: 0 !important;
    color: #848484!important;
}

 
.pannel_box_article_mute{
   box-shadow: 0 0 1px 0 #eee !important;
    opacity: 0.5;
}




@media (min-width: 992px) {

.modal_lg {
    width: 100%;
}

}


/*-----------------------popup------design-------------------*/

 #navigation_modal_popup1{
        padding-left: 30px;
        padding-right: 30px;
            overflow: hidden;
    overflow-y: auto !important;
    background: rgba(0,0,0,0.5) !important;
    }
 
.inner_article{
    background: #f1f1f1;
    position: relative;
    height: 200px;
    overflow: hidden;
    margin-bottom: 15px;
    border-radius: 4px;
}
.inner_article figure img {
    height: 200px;
    width: 100%;
}
.inner_article .inner_article_hover {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    display: block;
    background: rgba(0, 0, 0, 0.56);
    width: 100%;
    height: 100%;
     -webkit-transition: 0.5s;
    -moz-transition: 0.5s;
    -webkit-transform: skew(10deg) scale(2) rotate(-2deg) translateY(250px);
    -moz-transform: skew(10deg) scale(2) rotate(-2deg) translateY(250px);
    transition: 0.5s;
    transform: skew(10deg) scale(2) rotate(-2deg) translateY(250px);
}

.inner_article:hover .inner_article_hover{
-webkit-transform: none;
-moz-transform: none;
transform: none;
}

.inner_article_hover .btn_div1 {
    width: 150px;
    display: block;
    margin: auto;
    margin-top: 40px;
}

.inner_article_hover .btn_div1 a {
    margin-top: 10px;
    width: 100%;
    box-shadow: 0 0 !important;
    border: 1px solid rgb(255, 255, 255);
    background: rgb(255, 255, 255);
    height: 40px;
    color: #495326;
    line-height: 28px;
    border-radius: 8px !important;
    transition: 0.5s;
}

.inner_article_hover .btn_div1 .one_1:hover {
    background: #FF9800;
    border: 1px solid #fff;
    color: #fff;
}
.inner_article_hover .btn_div1 .two_2:hover {
    background: #333;
    border: 1px solid #fff;
    color: #fff;
}
/*-----------------------popup------design-------------------*/


#navigation_modal_popup1 .modal-header {
    padding: 15px;
    border-bottom: 1px solid #ff9800 !important;
    background: #ff9800 !important;
}

#navigation_modal_popup1 .nav-tabs {
    border-bottom: 0 !important;
}

#navigation_modal_popup1 .nav-tabs>li {
    margin-bottom: 0!important;
}

#navigation_modal_popup1 .nav-tabs>li>a {
    margin-right: 5px!important;
    border-radius: 8px!important;
    border: 1px solid #fff !important;
    background: #ff9800;
    margin-bottom: 15px;
    color: #fff !important;
    padding: 10px 20px!important;
}
#navigation_modal_popup1 .nav-tabs>li.active>a, #navigation_modal_popup1 .nav-tabs>li.active>a:focus, 
#navigation_modal_popup1 .nav-tabs>li.active>a:hover {
    color: #ff9800 !important;
    cursor: default;
    background-color: #ffffff !important;
    border: 1px solid #ffffff !important;
    border-bottom-color: #ffffff !important;
    transition: 0.5s;
}


#navigation_modal_popup1 .nav>li>a:focus, 
#navigation_modal_popup1 .nav>li>a:hover {
    background-color: #fff !important;
}

#navigation_modal_popup1 .nav-tabs>li>a:hover {
    color: #ff9800!important;
}
#navigation_modal_popup1 .modal-header .close {
    margin-top: -2px;
    margin-right: 15px;
}

#navigation_modal_popup1 .modal-content {
    border: 2px solid #ff9800;
}
.group_btn2 {
    position: absolute;
    bottom: 5px;
    left: 20%;
}
.group_btn2 .btn {
background: #848484!important;
    color: #fff !important;
    border: 2px solid #848484;
    margin: 5px;
    border-radius: 50% !important;
    box-shadow: 0 0 !important;
    height: 35px;
    width: 35px;
    outline: 0 !important;
    margin-right: 10px;
    transition: 0.5s;
}
.group_btn2 .btn:hover:nth-child(1) {
    color: #4CAF50 !important;
    border: 2px solid #4CAF50;
    background: #fff !important;
}
.group_btn2 .btn:hover:nth-child(2) {
    color: #F44336 !important;
    border: 2px solid #F44336;
    background: #fff !important;
}
.group_btn2 .btn:hover:nth-child(3) {
    color: #97a5b0 !important;
    border: 2px solid #97a5b0;
    background: #fff !important;
}
.group_btn2 .btn:hover:nth-child(4) {
    color: #FF9800 !important;
    border: 2px solid #FF9800;
    background: #fff !important;
}
.show_focus_list_group a{
  cursor: pointer;
}
.show_focus_list_group {
    display: none;
    margin-bottom: 0;
    position: absolute;
    width: 100%;
    z-index: 1;
}

</style>

 

<script type="text/javascript">
    
$(function(){

// show_focus_list_group
$(".select2-choices").focusin(function(){
$('.show_focus_list_group').fadeIn();
  }).focusout(function(){
    $('.show_focus_list_group').fadeOut();
})

// show_focus_list_group
$(".show_focus_list_group a").on("click", function(){
  $(".select2-choices").val($(this).text())
})
 
/*----------------------------Checkbox css--------------------------*/
 $(".toggle").click(function(){
       $(".inner").toggleClass('active'); 
    });
/*----------------------------Checkbox css--------------------------*/


     $(".fa_times").click(function(){
        alert($(this).closest());
     })
 
    // $(".dropdown").hover(            
    //         function() {
    //             $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
    //             $(this).toggleClass('open');
    //             $('b', this).toggleClass("caret caret-up");                
    //         },
    //         function() {
    //             $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
    //             $(this).toggleClass('open');
    //             $('b', this).toggleClass("caret caret-up");                
    //         });
    });


</script>

@endsection