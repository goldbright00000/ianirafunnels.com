@extends('user-dashboard.user-dashboard-default') @section('title','Jason Funnel') @section('content')
 <!-- css -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  
 
  
<main id="main" class="main_dashboard">
<div class="container-fluid container_fluid">


<div class="row first">
	<div class="col-md-6">
	<div class="col-sm-1">
		 <span class="fa_cog_icon"><i class="fa fa-cog"></i></span>
	</div>
	<div class="col-sm-4">
            <h4 class="heading_test">{{$funnel_name}}</h4>
	</div>

	<div class="col-sm-7">


<div class="btn-group btn-group-justified top_col-md-7">
	<a href="#" class="btn btn-warning"><i class="fa fa-link"></i></a>
	<a href="#" class="btn btn-warning"><i class="fa fa-copy"></i></a>
	<a href="#" class="btn btn-warning"><i class="fa fa-external-link-alt"></i></a>
	<a href="#" class="btn btn-warning"><i class="fa fa-copy"></i></a>
	<a href="#" class="btn btn-warning"><i class="fa fa-question-circle"></i></a>
</div>



	</div>

	</div><!-- .col-md-6 -->


<div class="col-md-6">


<ul class="btn-group top_col_md_6">
	<a href="#" class="btn btn-warning active_nav"><i class="fa fa fa-bars"></i> Steps</a>
	<a href="#" class="btn btn-warning"><i class="fa fa-chart-bar"></i> Stats</a>
	<a href="#" class="btn btn-warning"><i class="fa fa-users"></i> Contacts</a>
	<a href="#" class="btn btn-warning"><i class="fa fa-money-bill-alt"></i> Sales</a>
	<a href="#" class="btn btn-warning"><i class="fa fa-cog"></i> Settings</a>
</ul>


</div><!-- .col-md-6 -->



</div><!-- .row first -->

<?php
//echo $id;die;
?>

<div class="row second"> 
<div class="col-sm-3 left-sidebar">
	<article>
	<ul class="list-group">
		<li class="list-group-item"><a href="#"><span class="left_box"><i class="fa fa-check-square"></i></span> Launch Checklist</a><i class="fa fa-times pull-right fa_times"></i></li>
		<li class="list-group-item"><a href="#"><span class="left_box"><i class="fa fa-list-ol"></i> </span>  Funnel Steps </a> <i class="fa fa-times pull-right fa_times"></i></li> 


  @foreach($funnel_step as $funnel_step_row)

<li class="list-group-item" id="deleteId{{$funnel_step_row['id']}}"><a href="#"><span class="left_box"> <i class="fa fa-rocket"></i></span>    {{$funnel_step_row['funnel_step_name']}}
 </a> <i class="fa fa-times pull-right fa_times" data-id="{{$funnel_step_row['id']}}"></i></li> 
 @endforeach
		



	</ul> 
	</article> 

    <div class="button_handler_left">
        <button class="btn" data-toggle="modal" data-target="#button_handler_left_modal"> <span><i class="fa fa-plus"></i></span> &nbsp; Add new steps</button>
    </div>
</div><!-- .left-sidebar -->


 


<div class="col-sm-9 main_col_9_container">




	<div class="row">
		<div class="col-md-12 col_md_12_pl_0">
		 	 <nav class="navbar navbar-default navigation_top2" role="navigation">
  
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <!-- <li class="active"><a href="#">Optin</a></li> -->
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> Optin <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#"><i class="fa fa-envelope"></i> Email Optin</a></li>
            <li><a href="#"><i class="fa fa-download"></i> Thank You</a></li>
          </ul>
        </li>


          <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-shopping-cart"></i> Sales <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#"><i class="fa fa-dollar-sign"></i> Sales Page</a></li>
            <li><a href="#"><i class="fa fa-rocket"></i> Product Launch</a></li>
            <li><a href="#"><i class="fa fa-shopping-cart"></i> Order Form</a></li>
            <li><a href="#"><i class="fa fa-arrow-up"></i> One Click Upsell (OTO)</a></li>
            <li><a href="#"><i class="fa fa-arrow-down"></i> One Click Downsell</a></li>
            <li><a href="#"><i class="fa fa-check-square"></i> Order Confirmation</a></li>
          </ul>
        </li>
 

   		<li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-microphone"></i> Webinar <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#"><i class="fa fa-envelope"></i> Registration</a></li>
            <li><a href="#"><i class="fa fa-download"></i> Thank You</a></li>
            <li><a href="#"><i class="fa fa-microphone"></i> Broadcast Room</a></li>
          </ul>
        </li>


		<li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-graduation-cap"></i> Membership <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#"><i class="fa fa-sign-in-alt"></i> Member Access</a></li>
            <li><a href="#"><i class="fa fa-tachometer-alt"></i> Membership Area</a></li>
          </ul>
        </li>

		<li><a href="#"><i class="fa fa-external-link-square-alt"></i> ClickPop</a></li>
         <li><a href="#"><i class="fa fa-bars"></i> Misc</a></li>
      </ul>
    
    </div><!-- /.navbar-collapse -->
  
</nav>
		</div>
	</div>







	<div class="row categorySubNav">
		<div class="col-md-9">
		 	<a class="showlink" href="#"><i class="fa fa-file-o"></i> Show Templates</a>		 
		</div>

		<div class="col-md-3">
			<div class="search_small">
				<input class="select2-choices" /> 				 
			</div>
		</div>
		
	</div>


	<div class="row">
		<div class="col-md-12">
			<p> <i class="fa fa-envelope"></i> Email Optin Templates</p>
		</div>
	</div>


	<div class="row" style="margin-top: 20px">
		<div class="col-md-4">	
			<article class="panel_default_wrap">
					<div class="panel panel-default panel_default_relative">
						  <div class="panel-body">
						  		<figure>
						  			<img src="{{url('public/images/thumb1.png')}}" class="img-responsive" />
						  		</figure>
						  		
						  </div>
						  <div class="panel-footer">Mother Funnel Squeeze Page - 1</div>

						  <div class="hover_effect">
				  			<div class="hover_effect_inner">
					  			<a href="#" class="btn btn-default btn-sm"> <i class="fa fa-hand-pointer"></i> Select </a>
					  			<a href="#" class="btn btn-info btn-sm"> <i class="fa fa-eye"></i> Preview </a>
				  			</div>
						  </div>
					</div>
			</article>
		</div>


		<div class="col-md-4">	
			<article class="panel_default_wrap">
					<div class="panel panel-default panel_default_relative">
						  <div class="panel-body">
						  		<figure>
						  			<img src="{{url('public/images/thumb2.png')}}" class="img-responsive" />
						  		</figure>
						  </div>
						  <div class="panel-footer">Mother Funnel Squeeze Page - 2</div>

						   <div class="hover_effect">
				  			<div class="hover_effect_inner">
					  			<a href="#" class="btn btn-default btn-sm"> <i class="fa fa-hand-pointer"></i> Select </a>
					  			<a href="#" class="btn btn-info btn-sm"> <i class="fa fa-eye"></i> Preview </a>
				  			</div>
						  </div>
					</div>
			</article>
		</div>


		<div class="col-md-4">	
			<article class="panel_default_wrap">
					<div class="panel panel-default panel_default_relative">
						  <div class="panel-body">
						  		<figure>
						  			<img src=" {{url('public/images/thumb3.png')}}" class="img-responsive" />
						  		</figure>
						  </div>
						  <div class="panel-footer">Mother Funnel Squeeze Page - 3</div>

						   <div class="hover_effect">
				  			<div class="hover_effect_inner">
					  			<a href="#" class="btn btn-default btn-sm"> <i class="fa fa-hand-pointer"></i> Select </a>
					  			<a href="#" class="btn btn-info btn-sm"> <i class="fa fa-eye"></i> Preview </a>
				  			</div>
						  </div>
					</div>
			</article>
		</div>
	</div><!-- .row -->





<div class="row" style="margin-top: 20px">
		<div class="col-md-4">	
			<article class="panel_default_wrap">
					<div class="panel panel-default panel_default_relative">
						  <div class="panel-body">
						  		<figure>
						  			<img src="{{url('public/images/thumb3.png')}}" class="img-responsive" />
						  		</figure>
						  		
						  </div>
						  <div class="panel-footer">Mother Funnel Squeeze Page - 1</div>

						  <div class="hover_effect">
				  			<div class="hover_effect_inner">
					  			<a href="#" class="btn btn-default btn-sm"> <i class="fa fa-hand-pointer"></i> Select </a>
					  			<a href="#" class="btn btn-info btn-sm"> <i class="fa fa-eye"></i> Preview </a>
				  			</div>
						  </div>
					</div>
			</article>
		</div>


		<div class="col-md-4">	
			<article class="panel_default_wrap">
					<div class="panel panel-default panel_default_relative">
						  <div class="panel-body">
						  		<figure>
						  			<img src="{{url('public/images/thumb2.png')}}" class="img-responsive" />
						  		</figure>
						  </div>
						  <div class="panel-footer">Mother Funnel Squeeze Page - 2</div>

						   <div class="hover_effect">
				  			<div class="hover_effect_inner">
					  			<a href="#" class="btn btn-default btn-sm"> <i class="fa fa-hand-pointer"></i> Select </a>
					  			<a href="#" class="btn btn-info btn-sm"> <i class="fa fa-eye"></i> Preview </a>
				  			</div>
						  </div>
					</div>
			</article>
		</div>


		<div class="col-md-4">	
			<article class="panel_default_wrap">
					<div class="panel panel-default panel_default_relative">
						  <div class="panel-body">
						  		<figure>
						  			<img src=" {{url('public/images/thumb1.png')}}" class="img-responsive" />
						  		</figure>
						  </div>
						  <div class="panel-footer">Mother Funnel Squeeze Page - 3</div>

						   <div class="hover_effect">
				  			<div class="hover_effect_inner">
					  			<a href="#" class="btn btn-default btn-sm"> <i class="fa fa-hand-pointer"></i> Select </a>
					  			<a href="#" class="btn btn-info btn-sm"> <i class="fa fa-eye"></i> Preview </a>
				  			</div>
						  </div>
					</div>
			</article>
		</div>
	</div><!-- .row -->


</div><!-- .main_col_9_container -->

</div><!-- .row second -->


 

</div><!-- .container-fluid container_fluid -->
</main><!-- .main_dashboard -->







<!-- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::Modal::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->

<!-- Modal -->
  <div class="modal fade" id="button_handler_left_modal" role="dialog">


                            <form method="POST" action="{{url('funnel/added-new/step')}}">    


                        {{csrf_field()}}   

    <input type="hidden" name="funnel_id"  value="<?php echo $id; ?>">

    
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">NEW STEP IN FUNNEL</h4>
        </div>
        <div class="modal-body">
          <div class="row">
              <div class="col-sm-12">
                        <div class="form-group">
                            <label>Name of Funnel Step</label>
                            <input class="form-control"  type="text" name="funnel_step_name"/>
                        </div>
                    </div>
            </div>
                 
                  <br>
                  <div class="row">
                    <div class="col-sm-12">
                          <div class="form-group">
                              <label>The path for this funnel step <span class="fa_question_bg"><i class="fa fa-question"></i></span></label>
                              <input class="form-control" name="funnel_url" type="text"/>
                          </div>
                      </div>
                  </div>
                  <br>
                  <span class="clearfix"></span>

                    <div class="row">
                    <div class="col-sm-12">
                        <div class="checkbox">
                            <div class="inner">
                                 <div class="toggle"></div>
                            </div>
                        </div>
                    </div>
                </div>

                 <br> 
                 <span class="clearfix"></span>

                  <div class="row">
                    <div class="col-sm-12">
                        <button class="btn">Create funnel step <span class="fa_arrow_right1"> <i class="fa fa-arrow-right"></i></span></button>
                    </div>
                </div>
          </div><!-- .modal-body -->
        </div>
      </div>
      
    </div>
                                  </form>                 

      
      
      
      
      
      
    </div>
  </div>


  
<div class="alert alert-success sucessDelete" id="sucessDelete">
 
</div>



@if(session()->has('insert')) 
<div id="sucessfullyMessage" class="alert alert-success animated fadeIn insersucess">
        <button type="button" class="close s_close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
          {{session('insert')}}
        </strong>
    </div>
@endif


 


<style>

    .insersucess {
    position: fixed;
    right: 17px;
    bottom: 1px;
    width: 225px;
    z-index: 9999;
    line-height: 25px;
}





.sucessDelete
{
  position: fixed;

right: 17px;

bottom: 1px;

width: 200px;
z-index: 9999;
line-height: 10px;
}


    body{
    font-family: 'Roboto', sans-serif;
    font-size: 14px;
    background: #fff;
}


h1,h2,h3,h4,h5,h6,p,ol,ul li,ul li a, span,a,div{
	font-family: 'Roboto', sans-serif;
}

.container_fluid {
    margin-left: 30px;
    margin-right: 30px;
    margin-top: 75px;
}
.first_heading  h3{
    margin: 0
}
.heading_test {
    font-size: 25px;
    font-weight: 600;
    color: #333;
    margin-top: 18px;
}

.top_col-md-7 a, .top_col_md_6 a {
    padding: 15px;
    background: transparent;
    border: 0;
    font-size: 18px;
    padding-top: 17px;
}
.top_col_md_6{
    margin: 0;
    padding: 0
}
.top_col_md_6 {
	    font-size: 18px;
}
.btn-warning {
    color: #333;
}
.first{
    min-height: 57px;
        background: #f28d2c;
}

.navbar-default {
    background-color: #f8f8f8;
    border-color: transparent !important;
    border-radius: 0 0 !important;
}

.col_md_12_pl_0{
	padding: 0;
}


.navigation_top2 .dropdown-menu>li>a {
    padding: 10px!important;
}




.navigation_top2 .navbar-default .navbar-nav>.open>a:hover {
    color: #f28d2c;
    background-color: transparent;
}


 


.left-sidebar {
    background: #f8f8f8;
    min-height: 625px;
    padding: 0;
}
 
.left-sidebar .list-group {
    margin-bottom: 0;
    margin-top: 0;
}
.panel-default {
    border-color: #f9f9f9;
}
.panel_default_wrap .panel-footer {
    padding: 15px 15px;
    background-color: #ffffff;
    border-top: 0;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 0;
    color: #808080;
}

.left-sidebar .list-group-item {
    background: transparent;
    display: block;
    width: 100%;
    color: #acb1b5;
    font-size: 20px;
    border: 0;
    border-left: 3px solid #535d65;
    margin-top: 15px;
    border-radius: 0 0;
}
.left-sidebar .list-group-item a {
    color: #929ba2;
    text-decoration: none;
    padding: 0;
    padding-left: 0;
    display: inline-block;
    font-size: 16px;
}
.left-sidebar .list-group-item a .fa{
    color: #535d65;
}

.left-sidebar .left_box {
    /*background: #f5f5f5;*/
    width: 50px;
    line-height: 45px;
    display: inline-block;
    text-align: center;
    margin-right: 15px;
}
.left-sidebar .list-group-item:hover{
     background: #ffffff;
         border-left: 3px solid #f28d2c;
}
 
.left-sidebar .list-group-item:hover a .left_box{
 color: #f28d2c !important;
}

.panel_default_wrap .panel-body{
	padding: 0;
}

.left-sidebar .fa_times {
    padding-top: 15px;
    padding-right: 15px;
    display: inline-block;
    cursor: pointer;
}

.fa_cog_icon {
    font-size: 30px;
    margin-top: 10px;
    display: inline-block;
    margin-left: -15px;
}

.panel_default_wrap img{
    width: 100%
}


.showlink {
    color: #748193;
    display: inline-block;
    padding-top: 7px;
    text-decoration: none;
}
.categorySubNav {
    padding: 8px 5px;
    padding-right: 5px;
    padding-bottom: 5px;
    border-radius: 5px;
    background: #f8f8f8;
    border: 0;
    margin-left: 0;
    margin-bottom: 20px;
}
.select2-choices {
    border: none !important;
    background: #fff url(./images/search.png) no-repeat left !important;
    background-position: 98% 50% !important;
    padding: 2px 5px !important;
    padding-right: 15px !important;
    font-size: 13px !important;
    border-radius: 4px !important;
    height: 35px;
    width: 100%;
}
.search_small {
    width: 340px;
    border-radius: 4px !important;
    float: right;
    margin-top: -4px !important;
    border: 2px solid #EAEAEA !important;
    font-size: 14px;
}
.top_col-md-7 .btn-warning,
.top_col_md_6 .btn-warning{
	 box-shadow: 0 0 !important;
    border-radius: 0 0;
}
.top_col-md-7 .btn-warning:hover,
.top_col_md_6 .btn-warning:hover {
    color: #f28d2c!important;
    background-color: #333333 !important;
    border-color: #ffffff!important;
}

.panel_default_relative{
	position: relative;
	overflow: hidden;
}
.hover_effect {
    position: absolute;
    width: 100%;
    height: 100%;
    background: rgba(242, 141, 44, 0.5);
    text-align: center;
    top: 0;
    left: 0;
    right: 0;
    padding: 15px;
    -webkit-transition: 0.5s;
    -webkit-transform: skew(45deg) translateY(-500px);
    transition: 0.5s;
    transform: skew(45deg) translateY(-500px);
}
.panel_default_relative:hover .hover_effect{
	-webkit-transform: none;
	transform: none;
}

.hover_effect_inner {
    padding-top: 100px;
}
.hover_effect_inner .btn{
	    width: 100px;
    font-size: 14px;
    border: 0;
    border-radius: 0 0;
    text-transform: uppercase;
}
	
.caret-up {
    width: 0; 
    height: 0; 
    border-left: 4px solid rgba(0, 0, 0, 0);
    border-right: 4px solid rgba(0, 0, 0, 0);
    border-bottom: 4px solid;
    
    display: inline-block;
    margin-left: 2px;
    vertical-align: middle;
}

.active_nav {
    color: #f28d2c!important;
    background-color: #f8f8f8 !important;
    border-color: #ffffff!important;
    box-shadow: 0 0 !important;
    border-radius: 0 0;
}

.button_handler_left > .btn:active,
.button_handler_left > .btn:focus,
.button_handler_left > .btn {
    border: 1px solid #f28d2c;
    background: #f28d2c !important;
    border-radius: 0 !important;
    padding: 10px 20px;
    font-size: 16px;
    color: #ffffff;
    outline: 0!important;
    box-shadow: 0 0 !important;
    text-transform: uppercase;
    display: block;
    margin: 20px;
    margin-top: 50px;
    width: 285px;
    -webkit-transition: 0.5s;
    transition: 0.5s;
}
.button_handler_left > .btn:hover{
  background: #ffffff !important;
  color: #f28d2c;
}

#button_handler_left_modal .modal-header {
    padding: 15px;
    border-bottom: 1px solid #f28d2c;
    background: #f28d2c !important;
    color: #fff;
}


#button_handler_left_modal .form-control {
    border-radius: 0 !important;
    box-shadow: 0 0 !important;
    border-color: #f0f0f0;
    background: #f9f9f9;
    height: 45px;
}

#button_handler_left_modal .btn {
    outline: 0 !important;
    display: block;
    margin: 0 auto 20px auto;
    font-size: 18px;
    background: #fff !important;
    box-shadow: 0 0 3px 0 #f28d2c !important;
    color: #f28d2c !important;
    transition: 0.5s;
    width: 265px;
    height: 50px;
    line-height: 40px;
}



#button_handler_left_modal .modal-body{
    padding: 40px 40px 20px 40px!important;
}

 #button_handler_left_modal .fa_arrow_right1 {
    margin-left: 3px;
margin-top: 4px;
color: #f28d2c;
transition: 0.5s;
}
 
#button_handler_left_modal .btn:hover{
    background: #f28d2c !important;
    color: #fff !important;
}
#button_handler_left_modal .btn:hover .fa_arrow_right1{
    color: #fff !important;
    margin-left: 15px;
}
/*----------------------------Checkbox css--------------------------*/
.checkbox {
    left: 50px;
    transform: translate(-50%,-50%);
    width: 100px;
    height: 45px;
    border-radius: 25px;
    background: linear-gradient(0deg, #d8d8d8, #cccccc);
    border-top: 0.01em solid #ececec;
    border-bottom: 0.01em solid #ececec;
}
.checkbox .inner {
    position: absolute;
    top: 10px;
    left: 10px;
    right: 10px;
    bottom: 10px;
    /*background: linear-gradient(0deg, #a5a5a5, #717171);*/
    border-radius: 20px;
    /*box-shadow: inset 0 0 2px rgba(0,0,0,.5);*/
    background: #fff;
}
.checkbox {
    position: relative;
    display: block;
    margin-top: 15px !important;
    margin-bottom: 0 !important;
}

.checkbox .inner .toggle 
{
    position:absolute;
    top:-6px;
    left:-3px;
    width:36px;
    height:36px;
    border-radius:50%;
    background:linear-gradient(0deg, #ccc, #e4e4e4);
    box-shadow: 0 4px 6px rgba(0,0,0,0.2);
    box-sizing:border-box;
    border-top:0.04em solid #ececec;
    border-bottom:0.04em solid #ececec;
    transition:0.5s;
}
.checkbox .inner.active .toggle:before {
    content:'ON';
    color:green;
}
.checkbox .inner .toggle:before {
    content:'OFF';
    position:absolute;
    left: 11px;
    top: 13px;
    color:#a5a5a5;
    font-size:10px;
    line-height:10px;
    text-align:center;
} 
.checkbox .inner.active .toggle 
{
    left:47px;
}
/*----------------------------Checkbox css--------------------------*/
.sucessDelete{
	display: none;
};
 

</style>



<script type="text/javascript">





setTimeout(function() {
    $('#sucessfullyMessage').fadeOut('fast');
}, 2700);

 
// <!-- data-id -->
$(".left-sidebar .fa_times").on("click", function(){
	var getId = $(this).attr('data-id');
	var deleteId = "deleteId"+getId;
$("#"+deleteId).hide();


// Ajax....
$.ajax({

url:'/stepdelete',
type: 'post',
data: {_token:$('meta[name="csrf-token"]').attr('content'),stepid:getId},
success: function(data1) {  

$('.sucessDelete').slideDown().html(data1).slideUp(5000);


},
error: function(xhr, ajaxOptions, thrownError) {
	document.write(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
}

});





});






$(function(){

    $('.select2-choices').keyup(function(){

        $dd=$(this).val();
        alert($dd);
    })





/*----------------------------Checkbox css--------------------------*/
 $(".toggle").click(function(){
       $(".inner").toggleClass('active'); 
    });
/*----------------------------Checkbox css--------------------------*/




	 
 
    $(".dropdown").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            });
    });


</script>

@endsection