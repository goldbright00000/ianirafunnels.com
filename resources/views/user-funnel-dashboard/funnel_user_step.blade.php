@extends('crm.default')
@section('title','Leads')
@section('content')



 <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-12">
    
    <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-12 md-p-0 lead-table">
      <md-toolbar class="toolbar-white _md _md-toolbar-transitions">
        <div class="md-toolbar-tools">
          <h2 flex="" md-truncate="" class="text-bold md-truncate flex">Funnel <br>
            <small flex="" md-truncate="" class="md-truncate flex">User Activity</small></h2>
          <div class="ciuis-external-search-in-table">
            <input ng-model="search.name" class="search-table-external ng-pristine ng-untouched ng-valid ng-empty" id="search" name="search" type="text" placeholder="What're you looking for ?" aria-invalid="false">
            <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" aria-label="Search">
              <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-search text-muted"></i></md-icon>
            </button>
          </div>
         
          
          
          
          <!-- end ngIf: !KanbanBoard -->
          <!-- ngIf: KanbanBoard -->
          <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" ng-click="toggleFilter()" aria-label="Filter">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-android-funnel text-muted"></i></md-icon>
          </button>
          <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" ng-click="Create()" aria-label="New">
            
            <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-android-add-circle text-success"></i></md-icon>
          </button>
          <md-menu md-position-mode="target-right target" ng-hide="ONLYADMIN != 'true'" class="md-menu ng-scope _md" aria-hidden="false">
            <button class="md-icon-button md-button md-ink-ripple" type="button" ng-transclude="" aria-label="Open demo menu" ng-click="$mdMenu.open($event)" aria-haspopup="true" aria-expanded="false" aria-owns="menu_container_29">
              <md-icon class="ng-scope material-icons" role="img" aria-hidden="true"><i class="ion-android-more-vertical text-muted"></i></md-icon>
            </button>
            
          <div class="_md md-open-menu-container md-whiteframe-z2" id="menu_container_29" style="display: none;" aria-hidden="true"><md-menu-content width="4" role="menu">
              <md-menu-item>
                <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="Import()" role="menuitem">
                  <div layout="row" flex="" class="ng-scope layout-row flex">
                    <p flex="" class="flex">Import Leads</p>
                    <md-icon md-menu-align-target="" class="ion-upload material-icons" style="margin: auto 3px auto 0;" role="img" aria-hidden="true"></md-icon>
                  </div>
                </button>
              </md-menu-item>
              <form action="https://demo.ciuis.com/leads/exportdata" class="form-horizontal ng-pristine ng-valid" enctype="multipart/form-data" method="post" accept-charset="utf-8">
              <md-menu-item>
                <button class="md-button md-ink-ripple" type="submit" ng-transclude="" role="menuitem">
                  <div layout="row" flex="" class="ng-scope layout-row flex">
                    <p flex="" ng-bind="lang.exportleads" class="ng-binding flex">Export Leads</p>
                    <md-icon md-menu-align-target="" class="ion-android-download text-muted material-icons" style="margin: auto 3px auto 0;" role="img" aria-hidden="true"></md-icon>
                  </div>
                </button>
              </md-menu-item>
              </form>              <md-menu-item>
                <button class="md-button md-ink-ripple" type="button" ng-transclude="" ng-click="RemoveConverted()" role="menuitem">
                  <div layout="row" flex="" class="ng-scope layout-row flex">
                    <p flex="" class="flex">Delete Converted Leads</p>
                    <md-icon md-menu-align-target="" class="ion-android-remove-circle material-icons" style="margin: auto 3px auto 0;" role="img" aria-hidden="true"></md-icon>
                  </div>
                </button>
              </md-menu-item>
              <md-menu-item>
                <a class="md-button md-ink-ripple" ng-transclude="" ng-href="https://demo.ciuis.com/leads/forms" role="menuitem" href="https://demo.ciuis.com/leads/forms">
                  <div layout="row" flex="" class="ng-scope layout-row flex">
                    <p flex="" class="flex">Online Web Leads</p>
                    <md-icon md-menu-align-target="" class="ion-earth material-icons" style="margin: auto 3px auto 0;" role="img" aria-hidden="true"></md-icon>
                  </div>
                </a>
              </md-menu-item>
            </md-menu-content></div></md-menu>
        </div>
      </md-toolbar>
      <div ng-show="leadsLoader" layout-align="center center" class="text-center layout-align-center-center ng-hide" id="circular_loader" aria-hidden="true" style="">
        <md-progress-circular md-mode="indeterminate" md-diameter="40" aria-valuemin="0" aria-valuemax="100" role="progressbar" class="ng-isolate-scope md-mode-indeterminate" style="width: 40px; height: 40px;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" style="width: 40px; height: 40px; transform-origin: 20px 20px 20px;"><path fill="none" stroke-width="4" stroke-linecap="square" d="M20,2A18,18 0 1 1 2,20" stroke-dasharray="84.82300164692441" stroke-dashoffset="250.04885021641428" transform="rotate(0 20 20)"></path></svg></md-progress-circular>
        <p style="font-size: 15px;margin-bottom: 5%;">
         <span>
            Please wait <br>
           <small><strong>Loading Leads...</strong></small>
         </span>
       </p>
     </div>
      <!-- ngIf: KanbanBoard -->
      <!-- ngIf: !KanbanBoard --><md-content ng-show="!leadsLoader" ng-if="!KanbanBoard" class="md-pt-0 ng-scope _md" aria-hidden="false" style="">
        <ul class="custom-ciuis-list-body user-funnel-step" style="padding: 0px;">
         
         @if(count($data) > 0)
            
         
          <li ng-repeat="lead in leads | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5" class="ciuis-custom-list-item active_tab1 ciuis-special-list-item ng-scope" style="">
            <ul class="list-item-for-custom-list">
              <li class="ciuis-custom-list-item-item col-md-12">
                <div data-toggle="tooltip" data-placement="bottom" data-container="body" title="" data-original-title="Assigned: pippo" class="assigned-staff-for-this-lead user-avatar">
                                                                                                                                                         
                <img src="{{url('public/crm/images/funnel_border.png')}}" alt="funnel_logo" style="border-radius: 0 !important;"> </div>
                <div class="pull-left col-md-2">
                    <a ng-href="https://demo.ciuis.com/leads/lead/46" href="https://demo.ciuis.com/leads/lead/46">
                        <strong ng-bind="lead.name" class="ng-binding">Funnel Name</strong></a><br>
                  <small ng-bind="lead.company" class="ng-binding"></small>
                </div>
                  
                 <div class="pull-left col-md-2">
                    <a ng-href="https://demo.ciuis.com/leads/lead/46" href="https://demo.ciuis.com/leads/lead/46">
                        <strong ng-bind="lead.name" class="ng-binding">Funnel Step</strong></a><br>
                  <small ng-bind="lead.company" class="ng-binding"></small>
                </div> 
                  
                  
                <div class="col-md-8">
                    
                    
                    
                    
                    
                    
                  <div class="col-md-4">
                  <a ng-href="https://demo.ciuis.com/leads/lead/46" href="https://demo.ciuis.com/leads/lead/46">
                        <strong ng-bind="lead.name" class="ng-binding">Funnel Type</strong></a><br>
                  <small ng-bind="lead.company" class="ng-binding"></small>


                  </div>
                    
                    
                  <div class="col-md-4">
                      <span class="date-start-task">
                          <small class="text-muted text-uppercase">Funnel Sub Domains</small><br>
                          <strong><span class="badge ng-binding" style="border-color: #fff;background-color: #fff3d1;" ng-bind="lead.statusname">View Details</span></strong>
                        
                      </span>
                 </div>
                    
                 <div class="col-md-4">
                      <span class="date-start-task">
                          <small class="text-muted text-uppercase">Step  Details</small><br>
                          <strong><span class="badge ng-binding" style="border-color: #fff;background-color: #fff3d1;" ng-bind="lead.statusname">View Details</span></strong>
                        
                      </span>
                 </div> 
                    
                    
               
                </div>
              </li>
            </ul>
          </li>   
         
            
      @foreach($data as $funneldata)      
            
            
          <li ng-repeat=" lead in leads | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5" class="funnel_list   ciuis-custom-list-item ciuis-special-list-item ng-scope" style="">
            <ul class="list-item-for-custom-list">
              <li class="ciuis-custom-list-item-item col-md-12">
                <div data-toggle="tooltip" data-placement="bottom" data-container="body" title="" data-original-title="Assigned: pippo" class="assigned-staff-for-this-lead user-avatar">
                                                                                                                                                         
                <img src="{{url('public/crm/images/funnel_border.png')}}" alt="funnel_logo" style="border-radius: 0 !important;"> </div>
                <div class="pull-left col-md-2">
                    <a ng-href="https://demo.ciuis.com/leads/lead/46" href="https://demo.ciuis.com/leads/lead/46">
                        <strong ng-bind="lead.name" class="ng-binding">{{$funneldata->funnel_name}}</strong></a><br>
                  <small ng-bind="lead.company" class="ng-binding"></small>
                </div>
                  
                 <div class="pull-left col-md-2">
                    <a ng-href="https://demo.ciuis.com/leads/lead/46" href="https://demo.ciuis.com/leads/lead/46">
                        <strong ng-bind="lead.name" class="ng-binding">{{$funneldata->funnel_step}} </strong></a><br>
                  <small ng-bind="lead.company" class="ng-binding"></small>
                </div> 
                  
                  
                <div class="col-md-8">
                    
                    
                    
                <div class="col-md-4">
                      <span class="date-start-task">
                  
                          <strong> {{ ucwords(strtolower($funneldata->funnel_type)) }} </strong>
                        
                      </span>
                 </div>


                    
                 <div class="col-md-4">
                      <span class="date-start-task" style="color:blue;">
                  
                          <strong> {{ strtolower($funneldata->funnel_alter_url) }} </strong>
                        
                      </span>
                 </div>
                    
 
                    
                    
                  


                 <div class="col-md-4">
                      <span class="date-start-task">
                  <strong> Funnel Step  Details </strong>
                        
                      </span>
                 </div>
                    



                    
                    
               
                </div>
              </li>
            </ul>
          </li>   
          
        @endforeach  
          @else
          
         
          
          <li ng-repeat="lead in leads | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5" class="ciuis-custom-list-item ciuis-special-list-item ng-scope" style="">
            <ul class="list-item-for-custom-list">
              <li class="ciuis-custom-list-item-item col-md-12">
            
                  
                  
                  <div class="col-md-12"  >
                   
                   <span style="font-weight:600;">
                       Data Not Found </span>
                    
                </div>
              </li>
            </ul>
          </li>   
          
          
          
           @endif
          
        
         
              
              
          </li><!-- end ngRepeat: lead in leads | filter: FilteredData | filter:search | pagination : currentPage*itemsPerPage | limitTo: 5 -->
        </ul>
        <div ng-hide="leads.length < 5" class="pagination-div" aria-hidden="false">
          <ul class="pagination">
            <li ng-class="DisablePrevPage()" class="disabled" style=""> <a href="" ng-click="prevPage()"><i class="ion-ios-arrow-back"></i></a> </li>
            <!-- ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope active" role="button" tabindex="0" style=""> <a href="#" ng-bind="n+1" class="ng-binding">1</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0" style=""> <a href="#" ng-bind="n+1" class="ng-binding">2</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0"> <a href="#" ng-bind="n+1" class="ng-binding">3</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0"> <a href="#" ng-bind="n+1" class="ng-binding">4</a> </li><!-- end ngRepeat: n in range() --><li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)" class="ng-scope" role="button" tabindex="0"> <a href="#" ng-bind="n+1" class="ng-binding">5</a> </li><!-- end ngRepeat: n in range() -->
            <li ng-class="DisableNextPage()"> <a href="" ng-click="nextPage()"><i class="ion-ios-arrow-right"></i></a> </li>
          </ul>
        </div>
        <md-content ng-show="!leads.length" class="md-padding no-item-data _md ng-hide" aria-hidden="true" style="">No matching items found</md-content>
      </md-content><!-- end ngIf: !KanbanBoard -->
    </div>
  </div>


<style>
.active_tab1{
background: #ddd !important;
}
</style>




<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
 $(function(){

 jQuery('#search').on("keyup input", function(){
                    var value = jQuery(this).val().toLowerCase();
   
                   
                                    $(".user-funnel-step li.funnel_list").filter(function () {
                                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                                    });
         
         
         
          
      
      });



 });

// function Create()
// {
//   jQuery('.md-sidenav-right').css('display','none');
//   alert('ok');
// }


</script>
 @endsection