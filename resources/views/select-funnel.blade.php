@extends('layouts.default')
@section('title','Select Funnel')
@section('content')
<div class="container">
    
    
    <div class="col-sm-12">
    <div class="rer"> Step 1 of 2 </div>  
       <div class="rru"> Select a funnel type. </div> 
    </div>
    
    <div class="rr_bottom"> &nbsp;</div>
    
    
    <div class="ff_top"> &nbsp;</div>
<div class="row">
<div class="col-sm-3">
    
    
    
    
    
    
    
    
    
    
    
@foreach ($a as $avalue)
    
    <div class="heading_other"> {{ $avalue['funnal']->funnel_name }} </div>
    
    
   		<form>
				<div class="checkside">
                                    
                                 
                                    
                                    

    
@foreach ($avalue['funnaldata'] as $funnalvalue)
                                    
				<div class="form-check">
					<label>
						<input type="checkbox" name="check"> <span class="label-text">{{ $funnalvalue->funnel_name }} </span>
					</label>
				</div>

@endforeach
			
				
                                   
                                    
                                    
                                    
                                    
                                  
                                    
                                    
                                  
                                    
                                    
                                    
				
                                    </div>
			</form>

    
    @endforeach

   
    
    
</div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    

<div class="col-sm-9">
 <div class="row">
     <div class="col-md-12"> 
         
    
    <div class="divses2">
        
        <img src="https://ianirafunnels.com/public/icon/add_plus.svg" class="icon_with">   Classic funnel builder
    </div>



 </div>
     
     
     
    
    </div>  
    

    
 
    
    <div class="row">
        
        
        
       
     
        
        
    
        
        
        
        
        
  @foreach($allfunnels as $funnel)
        
        
        
        
         <div class="col-sm-4">
            
             <div class="">
            <div class="card">
                <a class="img-card" href="{{url('view-funnel/'.$funnel->id)}}">
                    <img src="{{asset('public/uploads/funnel/'.$funnel->image)}}"/>
                </a>
               
                <div class="card-content">
                    <h4 class="card-title">
                        <a href="{{url('view-funnel/'.$funnel->id)}}">
                       {{$funnel->funnel_name}}                      </a>
                    </h4>
            
                    <p>{{substr($funnel->message,0,64)}} </p>
                </div>
              
            </div>
        </div>
            
        </div>
        @endforeach
          
          
          
        
        
     
        
        
   
    </div>
</div>



</div>
</div>


<style>.gugpo
    {
        margin-top:10px;
    }
    .divses2 {
   float: right;
font-weight: 600;
padding: 8px;
border: solid 1px #f58b3c;
background: #fbfbfb;
margin-bottom: 10px;
}
.icon_with {
    width: 28px;
}

    .ff_top
    {
        margin-top:10px;
    }
    .rr_bottom
{
border-bottom: solid 1px #f58b3c;

}
    .rer
    {
text-align: center;
font-weight: 600;
font-size: 19px;

    }
.rru
{
    text-align: center;
}
    
    .heading_other
    {
       border: solid 1px #f58b3c;
background: #f58b3c;
color: white;
text-indent: 12px;
line-height: 38px;
font-weight: 600;

    }
    .checkside
    {
        border: solid 1px #f58b3c;
padding-top: 13px;
margin-bottom: 10px;


    }
input[type="checkbox"], input[type="radio"]{
	position: absolute;
	right: 9000px;
}

/*Check box*/
input[type="checkbox"] + .label-text:before{
	content: "\f096";
	font-family: "FontAwesome";
	speak: none;
	font-style: normal;
	font-weight: normal;
	font-variant: normal;
	text-transform: none;
	line-height: 1;
	-webkit-font-smoothing:antialiased;
	width: 1em;
	display: inline-block;
	margin-right: 5px;
}

input[type="checkbox"]:checked + .label-text:before{
	content: "\f14a";
	color: #2980b9;
	animation: effect 250ms ease-in;
}

input[type="checkbox"]:disabled + .label-text{
	color: #aaa;
}

input[type="checkbox"]:disabled + .label-text:before{
	content: "\f0c8";
	color: #ccc;
}

/*Radio box*/

input[type="radio"] + .label-text:before{
	content: "\f10c";
	font-family: "FontAwesome";
	speak: none;
	font-style: normal;
	font-weight: normal;
	font-variant: normal;
	text-transform: none;
	line-height: 1;
	-webkit-font-smoothing:antialiased;
	width: 1em;
	display: inline-block;
	margin-right: 5px;
}

input[type="radio"]:checked + .label-text:before{
	content: "\f192";
	color: #8e44ad;
	animation: effect 250ms ease-in;
}

input[type="radio"]:disabled + .label-text{
	color: #aaa;
}

input[type="radio"]:disabled + .label-text:before{
	content: "\f111";
	color: #ccc;
}

/*Radio Toggle*/

.toggle input[type="radio"] + .label-text:before{
	content: "\f204";
	font-family: "FontAwesome";
	speak: none;
	font-style: normal;
	font-weight: normal;
	font-variant: normal;
	text-transform: none;
	line-height: 1;
	-webkit-font-smoothing:antialiased;
	width: 1em;
	display: inline-block;
	margin-right: 10px;
}

.toggle input[type="radio"]:checked + .label-text:before{
	content: "\f205";
	color: #16a085;
	animation: effect 250ms ease-in;
}

.toggle input[type="radio"]:disabled + .label-text{
	color: #aaa;
}

.toggle input[type="radio"]:disabled + .label-text:before{
	content: "\f204";
	color: #ccc;
}


@keyframes effect{
	0%{transform: scale(0);}
	25%{transform: scale(1.3);}
	75%{transform: scale(1.4);}
	100%{transform: scale(1);}
}
card:hover {
  box-shadow: 0 8px 17px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
}
.img-card {
  width: 100%;
height: 275px;
  border-top-left-radius:2px;
  border-top-right-radius:2px;
  display:block;
    overflow: hidden;
    background: #000c;
    border-radius: 4px;
}
.img-card img{
 width: 100%;
height: 275px;
object-fit: cover;
transition: all .25s ease;
opacity: 0.5;
border-radius: 4px;
}
.card-content {
  padding: 15px;
text-align: left;
position: absolute;
bottom: 0px;
color: white;

}

.card-title {
margin-bottom: 1px;
margin-top: 0px;
font-weight: 700;
font-size: 18px;
height: 45px;
line-height: 20px;
}
.card-title a {
color: #fff;
text-decoration: none !important;

}
.card-read-more {
  border-top: 1px solid #D4D4D4;
}
.card-read-more a {
  text-decoration: none !important;
  padding:10px;
  font-weight:600;
  text-transform: uppercase
}
 .card
 {
  border-radius: 4px;  
  width: 262px;
 }




</style>


@endsection