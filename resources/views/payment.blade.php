@extends('layouts.default')
@section('title','payment')
@section('content')



<div class="headeryy">
  <div class="container">
      <div class="through_f">
        <h3 class="mb-0">Do you want to build a tunnel in 5 minutes? Try NOW IaniraFunnels!</h3>
      </div>
  </div>
</div>
<section class="top_banner py-5 d-flex align-items-center">
  <div class="sitecontainer ">
    <div class="row align-items-center ">
      <div class="col-sm-6 col-xl-5">
          <div class="baner_pera_wrp">
            <h2 class="text-white mb-md-5 mb-sm-2">
            <b> Start trial 14</b>
            <br>
            <span>Day Trial Now</span>
            </h2>
            <p class="gray_text">
            “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore             </p>
          </div>
      </div>
      <div class="col-sm-6 col-xl-7">
        <div class="doubleCircle_wrp right_top">
            <div class="doubleCircle "></div>
            <figure class="mb-0">
            <img src="{{ asset('public/img/Group-7@2x-right.png')}}" alt="" class="img-fluid" >
            </figure>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="startTrialForm_main mt-5">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
      <div class="doubleCircle_wrp right_top left_bottom">
            <div class="doubleCircle doubleCircle_left"></div>
            <div class="doubleCircle "></div>
            <div class="trail_form_inner bg-light">
              <div class="trailForm_header shadow py-sm-4 px-sm-3 py-3 px-2">
                <h2 class="text-center mb-0">
                  Start trail 14
                </h2>
              </div>
              <div class="paymentForm_body p-xl-5 p-sm-4 p-3 ">
                  <form action="" method="post" class="p-sm-5 px-3 py-5 rounded border">

                    <div class="form-group text-center mb-xl-5 mb-md-3 mb-sm-3 card_image">
                      <img src="{{ asset('public/img/paymentCard.png')}}" alt="" class="img-fluid" >
                    </div>
                    <div class="form-group mb-md-5 mb-sm-3 ">
                      <label for="name" class="gray_text">Name</label>
                      <input type="text" class="form-control " id="name" placeholder="Prova">
                    </div>
                    <div class="form-group mb-md-5 mb-sm-3">
                      <label for="cardnumber" class="gray_text">Card Number</label>
                        <input type="number" class="form-control " id="cardNumber" placeholder="0000 0000 0000 0000">
                      </div>
                      <div class="row">
                        <div class="col-6">
                          <div class="form-group mb-md-5 mb-sm-3">
                            <label for="date" class="gray_text">Date</label>
                            <input type="number" class="form-control " id="Date" placeholder="09/12">
                          </div>
                        </div>
                        <div class="col-6">
                          <div class="form-group mb-md-3 mb-sm-3">
                            <label for="cvc" class="gray_text">CVC</label>
                            <input type="number" class="form-control " id="cvc" placeholder="123">
                          </div>
                        </div>
                      </div>
                      <div class="form-check saveCard">
                        <label class="form-check-label">
                          <input type="checkbox" class="form-check-input" value=""> <span>Save this card</span>
                        </label>
                      </div>
                  </form>
              </div>
              <div class="trailForm_footer text-center p-xl-5 py-md-4 px-md-2 py-sm-3 py-2">
                <div class="col-sm-12">
                  <div class="row">
                    <div class="col-sm-6 text-left col-6">
                      <a href="paypal" class="text-warning"> Pay with paypal <i class="fa fa-angle-right" aria-hidden="true"></i> </a>
                    </div>
                    <div class="col-sm-6 text-right confirm_btn col-6">
                      <button type="submit" class="btn btn-lg btn-warning btn-outline-dark">Confirm </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="bootom_white_space">

</div>


@endsection