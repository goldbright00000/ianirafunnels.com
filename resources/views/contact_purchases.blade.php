@extends('user-dashboard.user-dashboard-default') @section('title','Jason Funnel') @section('content')



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>

 <script>
$(document).ready(function(){

$("#searchfunnelName").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".article_ungrouped_header h5").filter(function() {
        $(this).parents('.similar_cls').toggle($(this).text().toLowerCase().indexOf(value) > -1);
    });

  });
    

});
</script>

<div class="container">
    <div class="funnel_search">

        <div class="row">
            <div class="col-md-8" id="myFunnelr">

                <div class="row ">
                    <div class="input_border new_search">
                        <input type="text" placeholder="Search Funnel..." class="input_fild_funnel" id="searchfunnelName">
                        <div class="addnew_ffu new_search_btn" data-toggle="modal" data-target="#modal_id1">
                            <img src="https://ianirafunnels.com/public/icon/addnew.svg" class="addnewsvg"> Add New Funnel
                        </div>
                    </div>

                    <div class="rrr_ttt">
                        <ul>
                            <li> <a href="{{url('funnels')}}" class="active">All Funnels </a></li>
                            <li> <a href="{{url('funnel/recent')}}">Recent </a> </li>
                            <li> <a href="#"> Archived </a></li>
                            <li> <a href="#"> Marketplace </a> </li>
                        </ul>
                    </div>  
                </div>

                <div class="row article_ungrouped1">
                    <div class="col-md-12">
                        <article class="title_cookbook">
                            <h4><span class="pull-left"> UNGROUPED </span> <span class="pull-right"><i class="fa fa-caret-down"></i></span>
                            <!-- Powered by ianirafunnels --> </h4>
                        </article>
                    </div>
                </div>

                <!-- .article_ungrouped -->
                <div class="hideToggler">
                    
                    
                    @foreach($ungrouprecord as $ungrouprecordrow)
                    <div class="row article_ungrouped similar_cls">
                        <div class="col-sm-2" style="width: 85px">
                            <img src="https://app.clickfunnels.com/assets/optin-18098518cec4dccfff795991e0137f31d3541c1d51b5d735d4c7f72391b75ea3.png" class="img-responsive circle_img">
                        </div>

                        <div class="col-sm-7">
                            <article class="article_ungrouped_header">
                                <h5>{{$ungrouprecordrow->funnel_name}} </h5>
                                <span>last updated about {{$ungrouprecordrow->funnel_created_time}}</span>
                                        <input type="hidden"  value="{{$ungrouprecordrow->id}}" id="funnelId">
                                        <input type="hidden" value="{{$ungrouprecordrow->funnel_url}}" id="funnelUrl">
                                        <input type="hidden" value="ungrouped" id="funneltype">
                            </article>
                        </div>
                        <div class="col-sm-3">
                            <article class="three_step">
                                <ul class="list-inline main_btn_edit_delete">
                                    <li><span class="number_3">{{$ungrouprecordrow->funnel_step_times}}</span>
                                        <br><span>STEPS</span></li>
                                    <li class="btn_edit_delete">
                                        <a href="#">
                                            <span class="fa fa-pencil"></span>
                                        </a>
                                    </li>
                                   <li class="btn_edit_delete">
                                        <a href="#">
                                            <span class="fa fa-trash"></span>
                                        </a>
                                    </li>
                                </ul>
                            </article>
                        </div>

                    </div>
                     @endforeach
                    
                    
                    <!-- .article_ungrouped -->
                </div>
                <!-- .hideToggler -->

                <div class="row article_ungrouped1">
                    <div class="col-md-12">
                        <article class="title_cookbook">
                            <h4><span class="pull-left"> COOKBOOK </span> <span class="pull-right"><i class="fa fa-caret-down"></i></span></h4>
                        </article>
                    </div>
                </div>

                <div class="hideToggler">
                    
                      @foreach($cookbookrecord as $cookbookrecord_data)
                    <div class="row article_ungrouped">
                        <!-- .article_ungrouped -->
                        <div class="col-sm-2" style="width: 85px">
                            <img src="https://app.clickfunnels.com/assets/optin-18098518cec4dccfff795991e0137f31d3541c1d51b5d735d4c7f72391b75ea3.png" class="img-responsive circle_img">
                        </div>

                        <div class="col-sm-7">
                            <article class="article_ungrouped_header">
                                <h5>{{$cookbookrecord_data->funnel_name}}</h5>
                                <span>last updated about {{$cookbookrecord_data->funnel_created_time}}</span>

                                <input type="hidden"  value="{{$cookbookrecord_data->id}}" id="funnelId">
                                        <input type="hidden" value="{{$cookbookrecord_data->funnel_url}}" id="funnelUrl">
                                        <input type="hidden" value="cookbook" id="funneltype">
                            </article>
                        </div>
                        <div class="col-sm-3">
                            <article class="three_step">
                                <ul class="list-inline main_btn_edit_delete">
                                    <li><span class="number_3">{{$cookbookrecord_data->funnel_step_times}}</span>
                                        <br><span>STEPS</span></li>
                                     <li class="btn_edit_delete">
                                        <a href="#">
                                            <span class="fa fa-pencil"></span>
                                        </a>
                                    </li>
                                   <li class="btn_edit_delete">
                                        <a href="#">
                                            <span class="fa fa-trash"></span>
                                        </a>
                                    </li>
                                </ul>
                            </article>
                        </div>

                    </div>
                   @endforeach
                    
                    
                    
                </div>
                <!-- <div class="hideToggler"> -->








                <div class="row article_ungrouped1">
                    <div class="col-md-12">
                        <article class="title_cookbook">
                            <h4><span class="pull-left"> SHARED FUNNELS</span> <span class="pull-right"><i class="fa fa-caret-down"></i></span></h4>
                        </article>
                    </div>
                </div>

                <div class="hideToggler">
                    
                    
                    @foreach($sharefunnels as $sharefunnelsrow)
                    <div class="row article_ungrouped similar_cls">
                        <div class="col-sm-2" style="width: 85px">
                            <img src="https://app.clickfunnels.com/assets/optin-18098518cec4dccfff795991e0137f31d3541c1d51b5d735d4c7f72391b75ea3.png" class="img-responsive circle_img">
                        </div>

                        <div class="col-sm-7">
                            <article class="article_ungrouped_header">
                                
                                <h5>{{$sharefunnelsrow->funnel_name}}</h5>
                                
                                <span>last updated about {{$sharefunnelsrow->funnel_created_time}}</span>

                                <input type="hidden"  value="{{$sharefunnelsrow->id}}" id="funnelId">
                                        <input type="hidden" value="{{$sharefunnelsrow->funnel_url}}" id="funnelUrl">
                                    
                                        <input type="hidden" value="sharedfunnels" id="funneltype">

                            </article>
                        </div>
                        <div class="col-sm-3">
                            <article class="three_step">
                                <ul class="list-inline main_btn_edit_delete">
                                    <li><span class="number_3">{{$sharefunnelsrow->funnel_step_times}}</span>
                                        <br><span>STEPS</span></li>
                                    <li class="btn_edit_delete">
                                        <a href="#">
                                            <span class="fa fa-pencil"></span>
                                        </a>
                                    </li>
                                   <li class="btn_edit_delete">
                                        <a href="#">
                                            <span class="fa fa-trash"></span>
                                        </a>
                                    </li>
                                </ul>
                            </article>
                        </div>

                    </div>
                 @endforeach
                </div>
                <!-- <div class="hideToggler"> -->

            </div>
            <!-- .col-md-8 -->


 



            <div class="col-md-4">
                <div class="row" style="padding-left: 30px;">
                    <div class="col-md-12 border_top">
                        <div class="col-md-8 new_right1">
                            <h4>Levrone30</h4>
                            <span>file.sw24@gmail.com</span>
                        </div>

                        <div class="col-md-4">
                            <img class="img-responsive circle_img2" src="https://gravatar.com/avatar/79472a6a34c5fc2add8ded0af589f773.jpg?d=mm&s=120">
                        </div>
                    </div>
                </div>

                <div class="row" style="padding-left: 30px;">
                    <div class="col-md-12 border_top2">
                        <div class="col-md-12 new_right2">
                            <ul class="list-unstyled plan_list1">
                                <li>PLAN</li>
                                <li>Full Suite ($297/mo after 14-day trial)</li>
                                <div class="boder_bottom_only"><span style="width:85%"></span></div>
                                <li><span class="pull-left">VISITS</span> <span class="pull-right">0 OF 9999999</span></li>
                                <div class="boder_bottom_only"><span style="width: 75%"></span></div>
                                <li><span class="pull-left">PAGES</span> <span class="pull-right">11 OF 9999</span></li>
                                <div class="boder_bottom_only"><span style="width: 55%"></span></div>
                                <li><span class="pull-left">FUNNELS</span> <span class="pull-right">6 OF 999</span></li>
                                <div class="boder_bottom_only"><span style="width: 28%"></span></div>
                            </ul>
                        </div>

                    </div>
                </div>

                <div class="row" style="padding-left: 30px;margin-top: 30px;">
                    <div class="col-md-12 border_top3">
                        <div class="col-md-4">
                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRaZUVuCVDWg32Auq1TSkfZm4YrFIgJBRe6BQ7TOPbaLAXUDiwh" width="85">
                        </div>
                        <div class="col-md-8">
                            <span class="get_advance">Get Advanced Training:</span>
                            <h4 class="funnel_heading"> FUNNEL HACKS</h4>

                        </div>

                        <div class="col-md-12" style="margin-top: 20px;">
                            <a class="btn btn-block new_watch_btn" href="#"><i class="fa fa-microphone"></i> Watch Webinar Now</a>
                        </div>
                    </div>
                </div>

            </div>
            <!-- col-md-4 -->

        </div>

    </div>

</div>

<div id="modal_id1"  class="modal fade" role="dialog">
    <span tabindex="0"></span>
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="/*overflow: hidden;*/">
            <div class="modal-header"><span class="close">×</span>
                <span class="loader"></span>
                <h4>Build a Funnel</h4></div>
            <div class="modal-body opened">
                <div class="funnelViewBlock2 decide_builder_form">
                    <!-- <h1>Build a Funnel</h1> -->
                    <div class="row">
                        <div class="col-md-6" style="text-align: center;">
                            <h3>Cookbook Builder Process</h3>
                            <div class="decide_builder_image_cookbook"></div>
                            <!-- <div class="description">Explore different types of funnels and pre-done Funnels in this "Cookbook" style process and find the right Funnel for your business.</div> -->
                            <a class="btn btn-block btn-warning" data-popup-title="Add New Funnel" href="/cookbook" id="cookbook_link" title="New Funnel">
                                <i class="fa fa-book"></i> Start Cookbook
                            </a>
                        </div>
                        <div class="col-md-6" style="text-align: center; border-left: 1px solid #eee">
                            <h3>Classic Funnel Builder</h3>
                            <div class="decide_builder_image_classic open-dialog" data-popup-title="Add New Funnel" href="/funnels/new" title=" New Funnel"></div>
                            <!-- <div class="description">Our original step by step builder that's super simple and giving a basic funnel scaffolding fast. Select from Lead, Sales, or Event Funnels!</div> -->
                            <a class="btn btn-block btn-warning open-dialog" data-popup-title="Add New Funnel" href="#" id="classic_link" title=" New Funnel">
                                <i class="fa fa-plus"></i> Create New Funnel
                            </a>
                        </div>
                    </div>
                </div>
                <style>
                    .new_funnel_form {
                        display: none;
                    }

                </style>
                <div  class="simple_form form-horizontal marginInputFix createNewFunnelForm new_funnel_form">

                   
                    <div class="funnelViewBlock2">
                        <input class="new-funnel" type="hidden" name="funnel[funnel_template_id]" id="" value="">
                        <div class="col-sm-12 selectFunnelTypeBox clearfix">
                            <div class="addNewFunnelProgressStepsDiv">
                                <div class="addNewFunnelProgressBar"></div>
                                <div class="col-sm-2 f_step1 currentlySelected">
                                    <div class="addNewFunnelProgressBox"></div>
                                    <i class="fa fa-check"></i> Choose Goal
                                </div>
                                <div class="col-sm-3">
                                    <div class="f_step2" style="display: none">
                                        <div class="addNewFunnelProgressBox smallBoxProgress"></div>
                                        <span class="step2_text_f">
Collect Email Leads
</span>
                                    </div> 
                                </div>
                                <div class="col-sm-2 f_step3">
                                    <div class="addNewFunnelProgressBox"></div>
                                    <i class="fa fa-check"></i> Choose Type
                                </div>
                                <div class="col-sm-3">
                                    <div class="f_step4" style="display: none">
                                        <div class="addNewFunnelProgressBox smallBoxProgress"></div>
                                        <span class="step4_text_f">
Email Optin Funnel
</span>
                                    </div>
                                </div>
                                <div class="col-sm-2 f_step5">
                                    <div class="addNewFunnelProgressBox"></div>
                                    <i class="fa fa-check"></i> Build Funnel
                                </div>
                            </div>
                            <div class="secondOneforAddNewFunnel" style="display: block">
                                <!--  <strong class="createFunnelStepStrong">
<small>
Choose what you want to do.
</small> 
</strong> -->
                                <div class="col-sm-4">
                                    <div class="funnelStartSplitTestBlock innerFunnel_block chooseOptinEmailFunnel selection_optinfunnel" data-tabtype="1" style="padding-top: 20px;margin-top: -30px;">
                                        <!--   <div class="funnelStartSplitTestIcon">
                                            <img src="#"  width="50" alt="Mail">
                                        </div> -->
                                        <div class="funnelStartSplitTestHeadline">
                                            <img src="https://californiapsychics.zendesk.com/hc/article_attachments/360001335208/DM_Icon_1.5.png" width="30" alt="Mail"> Collect Emails
                                        </div>
                                        <div class="funnelStartSplitTestText">
                                            Build a new email list and start building an engaging email list.
                                        </div>
                                        <div class="funnelStartSplitTestButton2">
                                            <a class="btn btn-default" href="#">
                                                <i class="fa fa-plus"></i> Choose
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="funnelStartSplitTestBlock innerFunnel_block selection_choosesalesfunnel" data-tabtype="1" style="padding-top: 20px;margin-top: -30px;">
                                        <!--     <div class="funnelStartSplitTestIcon">
                                            <img src="#" width="50" alt="Sales">
                                        </div> -->
                                        <div class="funnelStartSplitTestHeadline">
                                            <img src="http://poutios-digi-land.cloudaccess.host/images/webinar/webinar-icon-2.png" width="30" alt="Sales"> Sell Your Product
                                        </div>
                                        <div class="funnelStartSplitTestText">
                                            Sell your products or services with a variety of funnels.
                                        </div>
                                        <div class="funnelStartSplitTestButton2">
                                            <a class="btn btn-default" href="#">
                                                <i class="fa fa-plus"></i> Choose
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="funnelStartSplitTestBlock innerFunnel_block chooseWebinarFunnel" data-tabtype="1" style="padding-top: 20px;margin-top: -30px;">
                                        <!--  <div class="funnelStartSplitTestIcon">
                                            <img src="#" width="50" alt="Webinar">
                                        </div> -->
                                        <div class="funnelStartSplitTestHeadline">
                                            <img src="https://i0.wp.com/xtalks.com/wp-content/uploads/2016/12/webinar-effectivness-pie-chart-1.png?fit=966%2C895&ssl=1" width="30" alt="Webinar"> Host Webinar
                                        </div>
                                        <div class="funnelStartSplitTestText">
                                            Get an audience at your next webinar whether it is live or automated replay.
                                        </div>
                                        <div class="funnelStartSplitTestButton2">
                                            <a class="btn btn-default" href="#">
                                                <i class="fa fa-plus"></i> Choose
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4" style="padding-top: 20px">
                                    <a class="btn btn-default selection_customfunnel innerFunnelSecond" data-tabtype="31">Create a Custom Funnel</a>
                                </div>
                                <div class="col-sm-4 col-sm-offset-4" style="padding-top: 30px; text-align:right;">
                                    <a href="javascript:void(0)" target="_blank">Browse Funnel Marketplace</a>
                                </div>
                            </div>

                            <div class="secondStepforAddNewFunnel">
                                <div class="choseSales" style="display: none">
                                    <div class="col-sm-4">
                                        <div class="funnelStartSplitTestBlock innerFunnel_block innerFunnelSecond selection_salesfunnel" data-tabtype="101" style="padding-top: 20px;margin-top: -30px;">
                                            <!--  <div class="funnelStartSplitTestIcon">
                                                <img src="/images/createfunnels/sales.png" width="50" alt="Sales">
                                            </div> -->
                                            <div class="funnelStartSplitTestHeadline">
                                                <img src="https://cdn4.iconfinder.com/data/icons/seo-and-business-glyph-1/64/seo-and-business-glyph-1-20-512.png" alt="Sales"> Sales Funnel
                                            </div>
                                            <div class="funnelStartSplitTestText">
                                                Sell your products after collecting leads with a squeeze page.
                                            </div>
                                            <div class="funnelStartSplitTestButton2" style="margin-bottom: 10px">
                                                <a class="btn btn-default" href="#">
                                                    <i class="fa fa-plus"></i> Choose
                                                </a>
                                            </div>
                                            <div class="col-sm-12 smallFunnelImage" style="display: none;">
                                                <span class="showInfoOnTheFunnel" data-infotypefunnel="membership_salesfunnel">
Show Info
</span>
                                                <i class="fa fa-envelope" data-toggle="tooltip" title="Squeeze Page"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-usd" data-toggle="tooltip" title="Sales Page"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-check-square-o" data-toggle="tooltip" title="Order Confirmation"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-download" data-toggle="tooltip" title="Download Page"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="funnelStartSplitTestBlock innerFunnel_block innerFunnelSecond selection_launchfunnel" data-tabtype="61" style="padding-top: 20px;margin-top: -30px;">
                                            <!--  <div class="funnelStartSplitTestIcon">
                                                <img src="/images/createfunnels/launch.png" alt="Launch">
                                            </div> -->
                                            <div class="funnelStartSplitTestHeadline">
                                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTEqpK6T8wnXkA4fCTpQdS9HjWO3qOPborRFfkdLdmQ2t9l3KJZ" alt="Launch"> Product Launch
                                            </div>
                                            <div class="funnelStartSplitTestText">
                                                Sell your product in a launch sequence with video pages.
                                            </div>
                                            <div class="funnelStartSplitTestButton2" style="margin-bottom: 10px">
                                                <a class="btn btn-default" href="#">
                                                    <i class="fa fa-plus"></i> Choose
                                                </a>
                                            </div>
                                            <div class="col-sm-12 smallFunnelImage" style="display: none;">
                                                <span class="showInfoOnTheFunnel" data-infotypefunnel="launch_salesfunnel">
Show Info
</span>
                                                <i class="fa fa-envelope" data-toggle="tooltip" title="Squeeze Page"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-youtube-play" data-toggle="tooltip" title="Video Page"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-shopping-cart" data-toggle="tooltip" title="Order Page"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-check-square-o" data-toggle="tooltip" title="Confirmation Page"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-download" data-toggle="tooltip" title="Download Page"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4" style="margin-bottom: 0">
                                        <div class="funnelStartSplitTestBlock innerFunnel_block innerFunnelSecond selection_membershipfunnel" data-tabtype="71" style="    margin-top: -29px;padding-top: 20px;margin-bottom: 0">
                                            <!--  <div class="funnelStartSplitTestIcon">
                                                <img src="/images/createfunnels/membership.png" alt="Membership">
                                            </div> -->
                                            <div class="funnelStartSplitTestHeadline">
                                                <img src="https://www.bwf.com/wp-content/uploads/2015/09/services-Board-Engagement.png" alt="Membership"> Membership
                                            </div>
                                            <div class="funnelStartSplitTestText">
                                                Sell your content with a membership that you can charge for.
                                            </div>
                                            <div class="funnelStartSplitTestButton2" style="margin-bottom: 10px">
                                                <a class="btn btn-default" href="#">
                                                    <i class="fa fa-plus"></i> Choose
                                                </a>
                                            </div>
                                            <div class="col-sm-12 smallFunnelImage" style="display: none;">
                                                <span class="showInfoOnTheFunnel" data-infotypefunnel="membership_salesfunnel">
Show Info
</span>
                                                <i class="fa fa-envelope" data-toggle="tooltip" title="Member Access Page"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-graduation-cap" data-toggle="tooltip" title="Members Area"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="choseWebinar" style="display: none;margin-top: -30px">
                                    <div class="col-sm-4">
                                        <div class="funnelStartSplitTestBlock innerFunnel_block innerFunnelSecond selection_webinarfunnel" data-tabtype="11" style="padding-top: 20px;margin-bottom: 0">
                                            <!--   <div class="funnelStartSplitTestIcon">
                                                <img src="/images/createfunnels/webinar.png" alt="Webinar">
                                            </div> -->
                                            <div class="funnelStartSplitTestHeadline">
                                                <img src="https://cdn4.iconfinder.com/data/icons/webinars-online-education-web-conferences/52/webinar_glyph-14-512.png" alt="Webinar"> Live Webinar
                                            </div>
                                            <div class="funnelStartSplitTestText">
                                                Sell your products after collecting leads with a squeeze page.
                                            </div>
                                            <div class="funnelStartSplitTestButton2" style="margin-bottom: 10px">
                                                <a class="btn btn-default" href="#">
                                                    <i class="fa fa-plus"></i> Choose
                                                </a>
                                            </div>
                                            <div class="col-sm-12 smallFunnelImage" style="display: none;">
                                                <span class="showInfoOnTheFunnel" data-infotypefunnel="membership_salesfunnel">
Show Info
</span>
                                                <i class="fa fa-envelope" data-toggle="tooltip" title="Webinar Registration"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-check-square-o" data-toggle="tooltip" title="Webinar Countdown"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-microphone" data-toggle="tooltip" title="Webinar Broadcast Room"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="funnelStartSplitTestBlock innerFunnel_block innerFunnelSecond selection_autowebinarfunnel" data-tabtype="21" style="padding-top: 20px;margin-bottom: 0">
                                            <!--   <div class="funnelStartSplitTestIcon">
                                                <img src="/images/createfunnels/autowebinar.png" alt="Autowebinar">
                                            </div> -->
                                            <div class="funnelStartSplitTestHeadline">
                                                <img src="https://uwm.edu/studentinvolvement/wp-content/uploads/sites/260/2019/01/Upcomingn-Events-Grid-Photo.png" alt="Autowebinar"> Webinar Replay
                                            </div>
                                            <div class="funnelStartSplitTestText">
                                                Showcase a replay of a webinar for evergreen lead generation.
                                            </div>
                                            <div class="funnelStartSplitTestButton2" style="margin-bottom: 10px">
                                                <a class="btn btn-default" href="#">
                                                    <i class="fa fa-plus"></i> Choose
                                                </a>
                                            </div>
                                            <div class="col-sm-12 smallFunnelImage" style="display: none;">
                                                <span class="showInfoOnTheFunnel" data-infotypefunnel="membership_salesfunnel">
Show Info
</span>
                                                <i class="fa fa-envelope" data-toggle="tooltip" title="Webinar Registration"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-check-square-o" data-toggle="tooltip" title="Webinar Countdown"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-microphone" data-toggle="tooltip" title="Webinar Broadcast Room"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-youtube-play" data-toggle="tooltip" title="Webinar Replay Room"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 goback" style="display: none">
                                    <a class="btn btn-sm btn-default pull-left goBacktoGoal2" href="#" style="color: rgb(134, 132, 132);margin-top: 15px;margin-left: 20px;float: left">
                                        <i class="fa fa-caret-left"></i> Go Back
                                    </a>
                                </div>
                            </div>
                            <div class="thirdStepforAddNewFunnel" style="display: none;padding-top: 30px;margin-top: -50px">
                                <div class="savedFunnelViewHeader" style="margin-top: -30px;border-radius: 5px;"></div>
                                <div class="col-sm-7" style="clear: both;padding-bottom: 20px;padding-right: 5px;    margin-top: 20px;padding-left: 0;">
                                    
                                    <form method="POST" action="{{url('funnel/added/step')}}">
                                    {{ csrf_field() }}


                                       <input type="hidden" value="" name="funnel_type" id="funnelactivitytype">
                                            <input type="hidden" value="" name="funnel_step_times" id="funnel_step_times">
                                            
                                    <div class="selectYourFunnelDone clearfix" style="background: none;border-bottom-left-radius: 5px;border-bottom-right-radius: 5px;margin-left: -4px;padding: 20px 20px;padding-right: 10px;padding-top:0px;padding-bottom: 0">
                                        <div class="col-sm-12" style="padding-top: 0px; padding: 0px 0px">
                                            <strong style="font-weight: bold">Name:</strong>
                                            <div class="form-group string required funnel_name">
                                                <div class="col-lg-10">
                                                    <input class="form-control string required" required="required" aria-required="true" placeholder="Give Funnel a Name" type="text" name="funnel_name" id="funnel_name">
                                                </div>
                                            </div>
                                         
                                            <strong style="font-weight: bold;margin-top: 20px;">Select Group Tag:</strong>
                                            <div class="form-group string optional funnel_group_tag">
                                                <div class="col-lg-10">
                                                   
                                                    <select class="form-control string optional select2-offscreen" type="text" name="funnel_tag" id="funnel_group_tag" tabindex="-1">
                                                        <option value="ungroup">Select Group Tag</option>
                                                        
                                                        <option value="sharefunnels">Shared Funnels</option>
                                                        <option value="cookbook">Cookbook</option>
                                                    </select>
                                                </div>
                                            </div>





<strong style="font-weight: bold;margin-top: 20px;">Subdomain Name:</strong>
                                            <div class="form-group string optional funnel_group_tag">
                                                <div class="col-lg-8">
                                                   <input class="form-control string optional select2-offscreen" type="text" name="subdomain_name" id="xfunnel_io">
                                                   <span class="clearfix"></span>
                                                   <p class="response_text1" ></p>
                                                </div>
                                                 <div class="col-lg-4">
                                                     <p class="xfunnel_io" style="padding-top: 10px;font-size: 20px;">ianirafunnels.com</p>
                                                 </div>
                                            </div>






                                            <button class="btn btn-success pull-right createFunnelNew" type="submit" value="submit"   data-disable-with="Building Funnel..." style="float: right;margin-top: 15px;margin-right: 0;">
                                                <i class="fa fa-edit"></i> Build Funnel
                                            </button>
                                            <a class="btn btn-sm btn-default pull-left goBacktoGoal" href="#" style="display: none;    color: rgb(134, 132, 132);margin-top: 15px;float: left">
                                                <i class="fa fa-caret-left"></i> Go Back
                                            </a>
                                        </div>
                                    </div>
                                    </form>  
                                        
                                </div>

                                <div class="col-sm-5 doneImageofFunnel" style="display: block;margin-top: 34px">
                                    <div class="addfunnel_FinalShow addfunnel_FinalEmail">
                                        <div class="col-sm-12">
                                            <i class="fa fa-caret-left lilCaretleft"></i>
                                            <h4>Funnel Steps:</h4>
                                            <ol>
                                                <li>Email Landing Page</li>
                                                <li>Thank You / Download Page</li>
                                            </ol>
                                            <p>The best way to convert website traffic into leads is to send them through a page that's usually called a lead, optin, or squeeze page.</p>
                                            <a class="onboardingHelpVideo" href="javascript:void(0)">
                                                <i class="fa fa-youtube-play" style="margin-right: 6px"></i> Watch Explainer Video
                                            </a>
                                        </div>
                                        <!-- <div class="showfunnelimage_info_below">
                                            <img src="/assets/funnel_types/squeezeflow-2c168d67762650f9691237644a412602be1dc82ff501579ca770cd56fc079d08.png" alt="Squeezeflow">
                                        </div> -->
                                    </div>
                                    <div class="addfunnel_FinalShow addfunnel_FinalSales" style="display: none">
                                        <div class="col-sm-12">
                                            <i class="fa fa-caret-left lilCaretleft"></i>
                                            <h4>Funnel Steps:</h4>
                                            <ol>
                                                <li>Email Landing Page</li>
                                                <li>Sales Page</li>
                                                <li>Order Confirmation</li>
                                                <li>Thank You / Download Page</li>
                                            </ol>
                                            <p>So, you've got something to sell online? If so, then you're probably going to need one of our proven sales funnels.</p>
                                            <a class="onboardingHelpVideo" href="javascript:void(0)">
                                                <i class="fa fa-youtube-play" style="margin-right: 6px"></i> Watch Explainer Video
                                            </a>
                                        </div>
                                        <!--  <div class="showfunnelimage_info_below">
                                            <img src="/assets/funnel_types/sales-funnel-03591030181125164cf3b232e37114547156ef086b5200e4f95b7d3431c7eb6e.png" alt="Sales funnel">
                                        </div> -->
                                    </div>
                                    <div class="addfunnel_FinalShow addfunnel_FinalLaunch" style="display: none">
                                        <div class="col-sm-12">
                                            <i class="fa fa-caret-left lilCaretleft"></i>
                                            <h4>Funnel Steps:</h4>
                                            <ol>
                                                <li>Email Landing Page</li>
                                                <li>Free Video #1 - #4</li>
                                                <li>Order Page</li>
                                                <li>Thank You / Download Page</li>
                                            </ol>
                                            <p>The launch funnel was designed after years of testing different ways to launch a new product, and it's been proven to work in hundreds of different markets.</p>
                                            <a class="onboardingHelpVideo" href="javascript:void(0)">
                                                <i class="fa fa-youtube-play" style="margin-right: 6px"></i> Watch Explainer Video
                                            </a>
                                        </div>
                                        <!--    <div class="showfunnelimage_info_below">
                                            <img src="/assets/funnel_types/launch-funnel-b50cb247cfdc66cd52dfee47e86f251d631627a41af777b145367f8892952bd6.png" alt="Launch funnel">
                                        </div> -->
                                    </div>
                                    <div class="addfunnel_FinalShow addfunnel_FinalMembership" style="display: none">
                                        <div class="col-sm-12">
                                            <i class="fa fa-caret-left lilCaretleft"></i>
                                            <h4>Funnel Steps:</h4>
                                            <ol>
                                                <li>Register / Sign Up</li>
                                                <li>Memebership Area</li>
                                                <ul>
                                                    <li>Lessons</li>
                                                </ul>
                                            </ol>
                                            <p>So, technically a membership site isn't actually a funnel, it's a way to deliver your content to people in a fun and unique way.</p>
                                            <a class="onboardingHelpVideo" href="javascript:void(0)">
                                                <i class="fa fa-youtube-play" style="margin-right: 6px"></i> Watch Explainer Video
                                            </a>
                                        </div>
                                        <!-- <div class="showfunnelimage_info_below">
                                            <img src="/assets/funnel_types/membership-funnel-13ce712f597def5fb219d5615bcbde779f6f05bc39cb69d416ae98d6b377e722.png" alt="Membership funnel">
                                        </div> -->
                                    </div>
                                    <div class="addfunnel_FinalShow addfunnel_FinalWebinar" style="display: none">
                                        <div class="col-sm-12">
                                            <i class="fa fa-caret-left lilCaretleft"></i>
                                            <h4>Funnel Steps:</h4>
                                            <ol>
                                                <li>Webinar Sign Up</li>
                                                <li>Confirmation Page</li>
                                                <li>Live Webinar</li>
                                            </ol>
                                            <p>Would you like to do a live webinar, but you aren't happy with the look and feel of your webinar providers registration and confirmation pages? If so, then you are going to love our webinar funnels!</p>
                                            <a class="onboardingHelpVideo" href="javascript:void(0)">
                                                <i class="fa fa-youtube-play" style="margin-right: 6px"></i> Watch Explainer Video
                                            </a>
                            

$string =$subdomain_name;            </div>
                                        <!--    <div class="showfunnelimage_info_below">
                                            <img src="/assets/funnel_types/webinar-funnel-92b7984c1bf7347466830e6a980c07d83c7909589a0a8e1364799a19fc7fcd7e.png" alt="Webinar funnel">
                                        </div> -->
                                    </div>
                                    <div class="addfunnel_FinalShow addfunnel_FinalAutoWebinar" style="display: none">
                                        <div class="col-sm-12">
                                            <i class="fa fa-caret-left lilCaretleft"></i>
                                            <h4>Funnel Steps:</h4>
                                            <ol>
                                                <li>Webinar Sign Up</li>
                                                <li>Confirmation Page</li>
                                                <li>Auto Webinar</li>
                                                <li>Webinar Replay</li>
                                            </ol>
                                            <p>Do you have a webinar that's producing results, but you don't want to keep doing live presentations every night?</p>
                                            <a class="onboardingHelpVideo" href="javascript:void(0)">
                                                <i class="fa fa-youtube-play" style="margin-right: 6px"></i> Watch Explainer Video
                                            </a>
                                        </div>
                                        <!--   <div class="showfunnelimage_info_below">
                                            <img src="/assets/funnel_types/auto-webinar-funnel-b97f1c58c1ee3e6992cb113d31fcbc3fa1e4061be3badace4a7899654ddbce6f.png" alt="Auto webinar funnel">
                                        </div> -->
                                    </div>
                                    <div class="addfunnel_FinalShow addfunnel_FinalCustom" style="display: none">
                                        <div class="col-sm-12">
                                            <i class="fa fa-caret-left lilCaretleft"></i>
                                            <p>So, you have an idea for a funnel, and you want to build it out without any restrictions? If so, then our custom funnel is for you.</p>
                                            <p>You can literally build out any type of funnel you can dream of. Pick a sales letter template, add an upsell or two, create a downsell, add an automated webinar, a membership site, or more.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12" style="padding: 0 0;display: none;padding-bottom: 20px;padding-left: 0">
                            <div class="col-sm-9 funnelCreateInfo" style="padding-left: 0;padding: 10px 14px;font-size: 22px;font-weight: 500; color: #5A6A72;opacity: .8">
                            </div>
                            <div class="col-sm-3" style="padding-right: 0;padding-top: 10px;padding-right: 6px">
                                <div class="btn btn-success btn-block createFunnelNew" style="font-size: 21px;float: right">
                                    <i class="fa fa-arrow-right"></i> Setup Your Funnel
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 legendForFunnel" style="display: none">
                        <span>
<strong>
Types of Pages:
</strong>
</span>
                        <span>
<span>
<i class="fa fa-envelope"></i>
Squeeze
</span>
                        <span>
<i class="fa fa-download"></i>
Download / Thank You
</span>
                        <span>
<i class="fa fa-shopping-cart"></i>
Sales
</span>
                        <span>
<i class="fa fa-check-square-o"></i>
Order Confirmation
</span>
                        <span>
<i class="fa fa-microphone"></i>
Webinar
</span>
                        <span>
<i class="fa fa-youtube-play"></i>
Video
</span>
                        <span>
<i class="fa fa-graduation-cap"></i>
Members Area
</span>
                        </span>
                    </div>
                </div>
                
                <meta name="csrf-token" content="{!! csrf_token() !!}">
                <script>
                    $(function() {
                        // $('.decide_builder_image_cookbook').click(function() {
                        //     document.getElementById('cookbook_link').click();
                        //     woopra.track("started cookbook builder", {});
                        // });
                        $('.decide_builder_image_classic, #classic_link').click(function() {
                            $('.new_funnel_form').show();
                            $('.decide_builder_form').hide();
                            woopra.track("started classic builder", {});
                        });
                    })
                </script>

                <style>
                    .modal-body {
                        background-color: #fff;
                        overflow: hidden;
                        padding: 30px;
                        padding-top: 10px;
                    }
                    
                    .description {
                        color: #666666;
                        font-weight: normal;
                        margin-bottom: 30px;
                        margin-top: 20px;
                    }
                    
                    .decide_builder_image_cookbook {
                        height: 345px;
                        width: 100%;
                        padding: 0;
                        background: #ffffff url(https://blog.powerhouseaffiliate.com/wp-content/uploads/2015/07/Conversion-Funnel.jpg) no-repeat center center;
                        margin: auto;
                        background-size: cover;
                    }
                    
                    .decide_builder_image_classic {
                        height: 345px;
                        width: 100%;
                        background: #ffffff url('https://www.singlegrain.com/wp-content/uploads/2018/07/conversion-funnel-simple.png') no-repeat center center;
                        margin: auto;
                        background-size: cover;
                    }
                    
                    .funnelStartSplitTestText {
                        font-size: 13px !important;
                        letter-spacing: 0 !important
                    }
                    
                    .innerFunnel_block {
                        margin-bottom: 40px;
                    }
                    
                    .funnelStartSplitTestBlock.innerFunnel_block {
                        min-height: 203px;
                        margin-bottom: 0;
                        border: 2px solid #eeeeee !important;
                    }
                    
                    .modal {
                        z-index: 1040;
                    }
                    
                    .new_search {
                        position: relative;
                    }
                    
                    .new_search_btn {
                        position: absolute;
                        top: 0;
                        right: 0;
                    }
                    
                    .select2-container-multi .select2-choices .select2-search-choice {
                        padding-left: 20px;
                        margin-left: 3px;
                        margin-top: 3px;
                    }
                    
                    .select2-drop-active {
                        border: 1px solid #5897FB;
                        border-top: none;
                    }
                </style>

<script>
$(document).ready(function(){


// $("#searchfunnelName").on("keyup", function() {
//     var value = $(this).val().toLowerCase();
//     $(".article_ungrouped_header").filter(function() {
//       $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
//     });
//   });

// var txt = $('#searchfunnelName').val();
// $('.article_ungrouped_header h5').each(function(){
//     var showTxt = $(this).text();
//    if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
//        $(this).parents('.article_ungrouped_header').show();
//    }
//    else{
//     $(this).parents('.article_ungrouped').hide();
//    }
// });

 





// jQuery('#searchfunnelName').on("keyup input", function(){
// var value = jQuery(this).val().toLowerCase();


// $("#myFunnelr .article_ungrouped1").filter(function () {
// $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1).hide();

// var findtext = $(".article_ungrouped_header").find('h5').text();
// console.log(findtext);


// $('.article_ungrouped_header').each(function(){
//   var txt = $('h5').text();
//   if(txt){
//     $(this).parents('.article_ungrouped').show()   
// }
// else{
//     $('.article_ungrouped').hide()   
// }
    
     
// });


// $("#myFunnelr .row").filter(function () {
// $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);

});





 

//$("#modal_id1 .close").click(function(){
     
//});



                        $('.modal-content', window.parent.document).attr('style', 'width: 1000px');
                        $('.modal-dialog', window.parent.document).attr('style', 'width: 1000px !important');
                        if (parent.document.body.clientHeight < 800) {
                            $('.modal-dialog', window.parent.document).attr('style', 'width: 1000px !important;margin-top: 10px !important');
                        }

                        $('.funnelStartSplitTestBlock').mouseover(function() {
                            $(this).find('.btn').removeClass('btn-default');
                            $(this).find('.btn').addClass('btn-success');
                        })
                        $('.funnelStartSplitTestBlock').mouseleave(function() {
                            $(this).find('.btn').addClass('btn-default');
                            $(this).find('.btn').removeClass('btn-success');
                        })

                        function confirmation() {
                            var answer = confirm("Continue without a funnel name?");
                            if (answer) {
                                $('form.createNewFunnelForm').submit();
                            }
                        }
                        jQuery.fn.preventDoubleSubmission = function() {
                            $(this).on('submit', function(e) {
                                var $form = $(this);

                                if ($form.data('submitted') === true) {
                                    // Previously submitted - don't submit again
                                    e.preventDefault();
                                } else {
                                    // Mark it so that the next submit can be ignored
                                    $form.data('submitted', true);
                                }
                            });

                            // Keep chainability
                            return this;
                        };
                        $('form').preventDoubleSubmission();

                        $('.createFunnelNew').click(function() {
                            if ($('#funnel_name').val() == '') {
                                confirmation();
                            } else {
                                $('form.createNewFunnelForm').submit();
                            }
                        });

                        $('.onboardingHelpVideo').magnificPopup({
                            type: 'iframe'
                        });

                        $('.selection_optinfunnel').click(function() {
                            $('.addfunnel_FinalShow').hide();
                            $('.addfunnel_FinalEmail').show();
                            $('.goBacktoGoal').show();
                            $('.step2_text_f').text('Collect Emails');
                            $('.step4_text_f').text('Build Email Optin Funnel');
                        });

                        $('.selection_salesfunnel').click(function() {
                            $('.addfunnel_FinalShow').hide();
                            $('.addfunnel_FinalSales').show();
                            $('.goBacktoGoal').show();
                            $("#funnelactivitytype").val('salesfunnel');
                            $('.step2_text_f').text('Sell a Product');
                            $('.step4_text_f').text('Build Sales Funnel');

                        });
                        $('.selection_launchfunnel').click(function() {
                            $('.addfunnel_FinalShow').hide();
                            $('.addfunnel_FinalLaunch').show();
                            $('.goBacktoGoal').show();
                            $('.step2_text_f').text('Launch Your Product');
                            $('.step4_text_f').text('Build Launch Funnel');
                        });

                        $('.selection_membershipfunnel').click(function() {
                            $("#funnelactivitytype").val('membership');
                            $('.addfunnel_FinalShow').hide();
                            $('.addfunnel_FinalMembership').show();
                            $('.goBacktoGoal').show();
                            $('.step2_text_f').text('Membership');
                            $('.step4_text_f').text('Build Membership Funnel');
                        });

                        $('.selection_webinarfunnel').click(function() {
                            $("#funnelactivitytype").val('livewebinar');
                            //alert('livewebinar');
                            $('.addfunnel_FinalShow').hide();
                            $('.addfunnel_FinalWebinar').show();
                            $('.goBacktoGoal').show();
                            $('.step2_text_f').text('Host Webinar');
                            $('.step4_text_f').text('Live Webinar Funnel');

                        });
                        $('.selection_autowebinarfunnel').click(function() {
                            $('.addfunnel_FinalShow').hide();
                            $("#funnelactivitytype").val('webinarreplay');
                            $('.addfunnel_FinalAutoWebinar').show();
                            $('.goBacktoGoal').show();
                            $('.step2_text_f').text('Host Webinar');
                            $('.step4_text_f').text('Auto Webinar Funnel');
                        });

                        $('.selection_customfunnel').click(function() {
                            $('.addfunnel_FinalShow').hide();
                            $('.addfunnel_FinalCustom').show();
                            $('.goBacktoGoal').show();
                            $('.step2_text_f').text('Start from Scratch');
                            $('.step4_text_f').text('Custom Funnel');
                            $('.f_step2').show();
                            $('.f_step1').addClass('alreadyhasbeenSelected');
                            $('.step2_text_f').text('Start from Scratch');
                            $('.step4_text_f').text('Custom Funnel');
                        });

                        $('.innerFunnel_block').click(function() {
                            $('.funnelCreateInfo strong').html($(this).find(' .innerFunnel_headline small').html() + ' Funnel');
                            $('#funnel_funnel_template_id.new-funnel').val($(this).attr('data-tabtype'));
                            $('.secondOneforAddNewFunnel').hide();
                            $('.secondStepforAddNewFunnel').show();
                            $('.legendForFunnel').hide();
                            $('.f_step2').show();
                            $('.f_step1').addClass('alreadyhasbeenSelected');
                            $('.addNewFunnelProgressStepsDiv .col-sm-2, .addNewFunnelProgressStepsDiv .col-sm-3').removeClass('currentlySelected');
                            $('.addNewFunnelProgressStepsDiv .f_step3').addClass('currentlySelected');
                            setTimeout(function() {
                                $('#funnel_name').focus();
                            }, 300);
                        });

                        $('.selection_choosesalesfunnel').click(function() {
                            $("#funnelactivitytype").val('productlunch');
                            $("#funnel_step_times").val('3');
                            $('.choseSales').show();
                            $('.choseWebinar').hide();
                            $('.goback').show();
                            $('.step2_text_f').text('Sell Your Product');
                        });

                        $('.goBacktoGoal').click(function() {
                            $('.addNewFunnelProgressStepsDiv').show();
                            $('.secondOneforAddNewFunnel').show();
                            $('.secondStepforAddNewFunnel').hide();
                            $('.thirdStepforAddNewFunnel').hide();
                            $('.webinarStepforAddNewFunnel').hide();
                            $('.goBacktoGoal').hide();
                            $('.f_step2').hide();
                            $('.f_step4').hide();
                            $('.f_step3').removeClass('currentlySelected');
                            $('.f_step3').removeClass('alreadyhasbeenSelected');
                            $('.f_step1').removeClass('currentlySelected');
                            $('.f_step1').removeClass('alreadyhasbeenSelected');
                            $('.addNewFunnelProgressStepsDiv .col-sm-2, .addNewFunnelProgressStepsDiv .col-sm-3').removeClass('currentlySelected');
                            $('.addNewFunnelProgressStepsDiv .f_step1').addClass('currentlySelected');
                        });
                        $('.goBacktoGoal2').click(function() {
                            $('.addNewFunnelProgressStepsDiv').show();
                            $('.secondOneforAddNewFunnel').show();
                            $('.secondStepforAddNewFunnel').hide();
                            $('.thirdStepforAddNewFunnel').hide();
                            $('.webinarStepforAddNewFunnel').hide();
                            $('.goBacktoGoal').hide();
                            $('.f_step2').hide();
                            $('.f_step4').hide();
                            $('.f_step3').removeClass('currentlySelected');
                            $('.f_step3').removeClass('alreadyhasbeenSelected');
                            $('.f_step1').removeClass('currentlySelected');
                            $('.f_step1').removeClass('alreadyhasbeenSelected');
                            $('.addNewFunnelProgressStepsDiv .col-sm-2, .addNewFunnelProgressStepsDiv .col-sm-3').removeClass('currentlySelected');
                            $('.addNewFunnelProgressStepsDiv .f_step1').addClass('currentlySelected');
                        });

                        $('.innerFunnelSecond').click(function() {

                            $('#funnel_funnel_template_id.new-funnel').val($(this).attr('data-tabtype'));
                        
                   
                           
                            $('.secondOneforAddNewFunnel').hide();
                            $('.secondStepforAddNewFunnel').hide();
                            $('.webinarStepforAddNewFunnel').hide();
                            $('.thirdStepforAddNewFunnel').show();
                            $('.legendForFunnel').hide();
                            $('.f_step4').show();
                            $('.f_step3').addClass('alreadyhasbeenSelected');
                            $('.addNewFunnelProgressStepsDiv .col-sm-2, .addNewFunnelProgressStepsDiv .col-sm-3').removeClass('currentlySelected');
                            $('.addNewFunnelProgressStepsDiv .f_step5').addClass('currentlySelected');

                        });

                        $('.chooseOptinEmailFunnel').click(function() {
                           
                            $('.goBacktoGoal').show();
                            $('#funnel_funnel_template_id.new-funnel').val($(this).attr('data-tabtype'));
                            $("#funnelactivitytype").val('collectemail');
                            $("#funnel_step_times").val('3');
                            $('.secondOneforAddNewFunnel').hide();
                            $('.secondStepforAddNewFunnel').hide();
                            $('.webinarStepforAddNewFunnel').hide();
                            $('.thirdStepforAddNewFunnel').show();
                            $('.legendForFunnel').hide();
                            $('.f_step4').show();
                            $('.f_step3').addClass('alreadyhasbeenSelected');
                            $('.addNewFunnelProgressStepsDiv .col-sm-2, .addNewFunnelProgressStepsDiv .col-sm-3').removeClass('currentlySelected');
                            $('.addNewFunnelProgressStepsDiv .f_step5').addClass('currentlySelected');
                        });

                        $('.chooseWebinarFunnel').click(function() {
                            $('.step2_text_f').text('Host Webinar');
                            $('#funnel_funnel_template_id.new-funnel').val($(this).attr('data-tabtype'));
                            $("#funnel_step_times").val('3');
                            $('.secondOneforAddNewFunnel').hide();
                            $('.secondStepforAddNewFunnel').show();
                            $('.legendForFunnel').hide();
                            $('.f_step4').hide();
                            $('.choseSales').hide();
                            $('.choseWebinar').show();
                            $('.goback').show();
                            $('.addNewFunnelProgressStepsDiv .col-sm-2, .addNewFunnelProgressStepsDiv .col-sm-3').removeClass('currentlySelected');
                            $('.addNewFunnelProgressStepsDiv .f_step3').addClass('currentlySelected');
                            $(".f_step1 .addNewFunnelProgressBox").css('background', '#03ae78')
                        });

                        $('.chooseCustomFunnel').click(function() {
                            $('.step2_text_f').text('Custom Funnel');
                            $('#funnel_funnel_template_id.new-funnel').val($(this).attr('data-tabtype'));
                            $('.secondOneforAddNewFunnel').hide();
                            $('.secondStepforAddNewFunnel').show();
                            $('.legendForFunnel').hide();
                            $('.f_step4').hide();
                            $('.choseSales').hide();
                            $('.addfunnel_FinalCustom').show();
                            $('.addNewFunnelProgressStepsDiv .col-sm-2, .addNewFunnelProgressStepsDiv .col-sm-3').removeClass('currentlySelected');
                            $('.addNewFunnelProgressStepsDiv .f_step3').addClass('currentlySelected');
                            $('.f_step2').show();
                            $('.f_step1').addClass('alreadyhasbeenSelected');
                            $('.step2_text_f').text('Start from Scratch');
                            $('.step4_text_f').text('Custom Funnel');
                        });


                        $(".article_ungrouped1").click(function() {
                            $(this).next(".hideToggler").toggle();
                            $(this).find("h4 .fa").toggleClass("fa-caret-down fa-caret-left")
                        });

                       
                        $("#xfunnel_io").keyup(function(){
                          var funneldomain= $(this).val();
                          var regex = /^[0-9a-zA-Z\_]+$/;
                          if(funneldomain.length>0)
   {
    
   if(regex.test(funneldomain))
   {
                          $.ajax({

url:'domianvalidate',

type: 'post',

data: {_token:$('meta[name="csrf-token"]').attr('content'),funneldomain:funneldomain},


success: function(data1) {
   
  
if(data1 =="0")
{

$('.response_text1').show().text('Available Sub domain').css('color','green');
$('.createFunnelNew').css({'pointer-events':'','color':'','border-color':''});
}
else{
  
$('.response_text1').show().text('Not Available Sub domain').css('color','red');  
$('.createFunnelNew').css({'pointer-events':'none','color':'#ddd','border-color':'#ddd'});
}
   

},

error: function(xhr, ajaxOptions, thrownError) {

document.write(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
}

});

   }
   else{
      
$('.response_text1').show().text('Not Allow Special Character').css('color','red');  
$('.createFunnelNew').css('pointer-events','none');


   }
}
   else{
    $('.response_text1').hide();

   }


        })

                        // $('#funnel_funnel_template_id.new-funnel').val('1');

                        // $("#funnel_group_tag").select2({
                        //     tags: ["cookbook", "Shared Funnels"],
                        //     maximumSelectionSize: 1
                        // });

                        //                         $('#input-tags2').selectize({
                        //     plugins: ['restore_on_backspace'],
                        //     delimiter: ',',
                        //     persist: false,
                        //     create: function(input) {
                        //         return {
                        //             value: input,
                        //             text: input
                        //         }
                        //     }
                        // });

                    // });
                </script>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            $('.modal-header .close').click(function() {
                //console.log('close click');
               // $(this).parent().parent().parent().parent().remove();
              
               $('#modal_id1').modal('hide');
            });
        });
    </script>

    <span tabindex="0" style="float: right; width: 0px;"></span></div>

<style>
    .alreadyhasbeenSelected .addNewFunnelProgressBox {
        background: #03AE78;
        border: 5px solid #EEEEEE;
    }
    
    .border_top {
        border: 2px solid #eee;
        border-top: 3px solid #3e474f;
        padding: 30px;
        padding-bottom: 25px;
        background: #fff;
    }
    
    .border_top2 {
        padding: 30px;
        background: #f58b3c;
    }
    
    .plan_list1 li {
        color: #fff;
        font-weight: 400;
        font-size: 14px;
        margin-bottom: 5px;
        overflow: hidden;
    }
    
    .funnelStartSplitTestHeadline img {
        width: 34px !important;
        margin-top: 7px !important;
        margin-right: 3px !important;
    }
    
    .innerFunnel_block * {
        cursor: pointer;
    }
    
    .new_watch_btn,
    .new_watch_btn:active,
    .new_watch_btn:link {
            display: block;
    clear: both;
    font-weight: bold;
    text-transform: capitalize;
    color: #fff !important;
    background: #f58b3c;
    border: 2px solid #f58b3c !important;
    height: 40px;
    line-height: 28px;
    transition: 0.5s;
    box-shadow: 0 0 !important;
    outline: 0 !important;
    border-radius: 30px;
    outline: 0 !important;
    }
    
    .new_watch_btn:hover {
        background: #fff!important;
        color: #f58b3c!important;
    }
    
    .border_top3 {
        background: #ffffff;
        box-shadow: 0 0 2px 0 #ddd;
        padding: 30px 0;
    }
    
    .boder_bottom_only {
        border-bottom: 2px solid #fff;
        margin-bottom: 20px;
        margin-top: 10px;
        width: 100%;
        display: block;
        position: relative;
        border-radius: 20px;
    }
    
    .boder_bottom_only > span {
        position: absolute;
        top: -1px;
        left: 0;
        height: 4px;
        background: #1a171b;
        display: block;
        border-radius: 8px;
    }
    
    .circle_img2 {
        width: 160px;
        border: 2px solid #ddd;
        border-radius: 50%;
        outline: 0;
        box-shadow: 0 0;
    }
    
    .new_right1 h4 {
        margin: 0;
    }
    
    .new_right1 span {
        font-size: 12px;
        color: #777;
    }
    
    .circle_img {
        width: 50px;
    }
    
    .addfunnel_FinalShow .col-sm-12 i.lilCaretleft {
        float: left;
        margin-left: -30px;
        font-size: 26px;
        margin-top: -10px;
        color: #fff;
    }
    
 .article_ungrouped1 {
    background: #e8e8e8;
    overflow: hidden;
    padding: 5px;
    cursor: pointer;
    border: 1px solid #dedcdc;
}
    
    #cookbook_link,
    #classic_link {
        width: 200px;
        height: 40px;
        line-height: 32px;
        background: #f28d2c !important;
        border: 0 !important;
        color: #000 !important;
        -webkit-box-shadow: 0 0 2px 0 #777 !important;
        box-shadow: 0 0 2px 0 #777 !important;
        border-radius: 30px !important;
        margin: auto;
        margin-top: 50px;
        -webkit-transition: 0.5s;
        transition: 0.5s;
    }
    #cookbook_link:hover,
    #classic_link:hover{
        background: #fff !important;
        -webkit-box-shadow: 0 0 2px 0 #f28d2c !important;
        box-shadow: 0 0 2px 0 #f28d2c !important;
        color: #f28d2c !important;
    }
    
    .addfunnel_FinalShow h4 {
        margin-top: 0px;
        font-weight: bold;
        font-size: 17px;
        opacity: .8;
    }
    
    .addfunnel_FinalShow ol {
        margin-left: 0;
        padding: 15px;
        padding-top: 0;
        padding-bottom: 0;
        font-size: 15px;
        opacity: .7;
    }
    
    .article_ungrouped {
        background: #eee;
        overflow: hidden;
        padding: 10px 0;
    }
    
    .article_ungrouped_header h5 {
        font-size: 16px;
        color: #000000;
    }
    
    .article_ungrouped_header span {
        font-size: 12px;
        color: #777;
    }
    
    .rrr_ttt {
        margin-top: 30px;
    }
    
    .three_step ul li {
        text-align: center;
    }
    
    .number_3 {
        font-size: 24px;
        font-weight: 600;
        color: #777;
    }
    
    .watch_eb {
        padding-top: 9px;
        border: solid 1px #f58a3c;
        margin-top: 18px;
        padding-bottom: 9px;
        background: white;
    }
    
    .funnel_hacks {
        padding-right: 13px;
        text-align: right;
    }
    
    .get_adv {
        padding-right: 10px;
        text-align: right;
        padding-top: 15px;
    }
    .title_cookbook h4{
            font-size: 16px;
            line-height: 12px;
    }
    
    .oer,
    .fufg {
        text-align: center;
        border: 1px solid #ddd;
        border-bottom: 2px solid #ddd;
        background: #FAFAFA;
        margin: 0 10px;
        width: 300px;
        cursor: default;
        padding: 15px 20px;
        padding-top: 0;
    }
    
    .rrr_ttt ul {
        margin: 0;
        margin-top: 15px;
        padding: 0;
        list-style-type: none;
    }
    
    .rrr_ttt ul li a {
        background: #eeeeee;
        display: inline-block;
        text-align: center;
        margin-right: 13px;
        padding: 7px 30px;
        font-weight: 600;
        float: left;
        border: solid 1px #eeeeee;
        text-decoration: none;
        color: #777;
    }
    
    .rrr_ttt ul li a.active {
        background: #fff;
    }
    
    .input_fild_funnel {
        width: 100%;
        padding-left: 15px;
        padding-right: 185px;
        box-shadow: 0 0 !important;
        border: 0 !important;
        height: 45px;
        outline: 0 !important;
    }
    
    .addnewsvg {
        width: 18px;
        padding-bottom: 3px;
    }
    
    .funnel_search {
        margin-top: 90px;
    }
    
    .input_border {
        border: solid 2px #f58b3c;
        width: 100%;
        overflow: hidden;
        border-radius: 4px;
    }
    
    .input_text {
        border: solid 1px black;
        float: left;
        width: 77%;
    }
    
    .addnew_ffu {
        background-color: #f58b3c;
        color: white;
        font-weight: 600;
        height: 100%;
        display: inline-block;
        line-height: 47px;
        text-align: center;
        width: 178px;
        cursor: pointer;
    }
    
    #modal_id1 {
        z-index: 1050
    }
    /*--------------modal-------------------*/
    
    .modal-header {
        padding: 15px;
        border-bottom: 1px solid #f28d2c;
        background: #f28d2c;
        color: #fff;
    }
    
    .modal-header > h4 {
        display: inline-block;
        margin: 0;
        padding: 0;
    }
    
    .addNewFunnelProgressStepsDiv {
        width: 100%;
        margin: 10px 0;
        text-align: center;
        margin-bottom: 110px;
        padding: 0 15px;
    }
    
    .addNewFunnelProgressBar {
        position: relative;
        top: 12px;
    }
    
    .addNewFunnelProgressBar {
        width: 100%;
        padding: 3px;
        background: #eee;
        border-radius: 5px;
        margin-bottom: -20px;
    }
    
    .addNewFunnelProgressStepsDiv .col-sm-2.currentlySelected {
        color: #3D464D !important;
    }
    
    .addNewFunnelProgressStepsDiv .col-sm-2 {
        font-size: 13px;
        text-transform: uppercase;
        font-weight: bold;
        color: #3E474F;
    }
    
    .addNewFunnelProgressStepsDiv .col-sm-2 {
        color: #515c65 !important;
    }
    
    .currentlySelected .addNewFunnelProgressBox {
        background: #03AE78;
        border: 5px solid #eee;
        color: #3D464D !important;
    }
    
    .addNewFunnelProgressBox {
        width: 30px;
        height: 30px;
        border-radius: 100px;
        background: #eee;
        border: 5px solid #eee;
        margin: 0 auto;
        margin-bottom: 8px;
    }
    
    .addNewFunnelProgressStepsDiv i {
        display: none;
    }
    
    .createFunnelNew,
    .createFunnelNew:hover {
        margin-top: 15px;
        margin-right: 0;
        border-radius: 20px;
        box-shadow: 0 0 !important;
        width: 200px;
        height: 40px;
        background: #fff !important;
        color: #080;
        outline: 0 !important;
        border: 2px solid;
    }
    
    .funnelStartSplitTestBlock.innerFunnel_block {
        background: #fafafa !important;
        padding-top: 30px;
        padding-bottom: 30px;
        border: 2px solid #fff;
        cursor: pointer;
        margin-top: -20px;
        color: #3E474F;
        background: #ffffff;
        color: #666;
        padding: 0 15px;
    }
    
    .funnelStartSplitTestBlock.innerFunnel_block {
        min-height: 203px;
        margin-bottom: 0;
        cursor: pointer;
    }
    
    .funnelStartSplitTestBlock {
        border: 2px solid #F7F7F7;
        border-radius: 4px;
        min-height: 328px;
        text-align: center;
        margin-top: 31px;
    }
    
    .innerFunnel_block .funnelStartSplitTestHeadline {
        color: #3E474F;
        font-size: 21px;
    }
    
    .selection_optinfunnel {
        background: #f9f9f9;
    }
    
    .innerFunnel_block:hover .funnelStartSplitTestHeadline {
        color: #f28d2c;
    }
    
    .innerFunnel_block * {
        cursor: pointer;
    }
    
    .funnelStartSplitTestHeadline {
        font-weight: bold;
        font-size: 22px;
        color: #918E8E;
    }
    
    .innerFunnel_block:hover {
        border: 2px solid #f28d2c !important;
    }
    
    .funnelStartSplitTestBlock .funnelStartSplitTestButton2 {
        position: absolute;
        bottom: 30px;
        left: 35%;
    }
    
    .funnelStartSplitTestButton2 .btn,
    .funnelStartSplitTestButton2 .btn:visited,
    .funnelStartSplitTestButton2 .btn:active {
        border-radius: 30px !important;
    border: 2px solid #ddd !important;
    outline: 0 !important;
    box-shadow: 0 0 !important;
    background: #fff;
    color: #5a5a5a !important;
    }
    /*.funnelStartSplitTestButton2:hover .btn {
    background: #fff;
    color: #f28d2c;
}*/
    
    .addNewFunnelProgressStepsDiv .col-sm-2 {
        font-size: 13px;
        text-transform: uppercase;
        font-weight: bold;
        color: #3E474F;
    }
    
    .addNewFunnelProgressStepsDiv .col-sm-2.alreadyhasbeenSelected {
        color: #1AAD79 !important;
    }
    
    .addNewFunnelProgressStepsDiv .col-sm-3 span {
        opacity: .7;
    }
    
    .doneImageofFunnel p {
        font-size: 15px;
        opacity: .7;
    }
    
    .addNewFunnelProgressStepsDiv .col-sm-3 {
        font-size: 12px;
        text-transform: capitalize;
        font-weight: bold;
        color: #FF5722;
        opacity: 1;
    }
    
    .alreadyhasbeenSelected i {
        display: inline-block;
    }
    
    .funnelStartSplitTestButton2 .btn {
        background: #fff !important;
    }
    
    a.innerFunnelSecond {
        border-radius: 30px;
        outline: 0 !important;
        box-shadow: 0 0 !important;
        padding: 10px 20px;
        border: 2px solid #eee !important;
        transition: 0.5s; 
    }
    
    a.innerFunnelSecond:hover {
        background: #fff !important;
        border-color: #f28d2c !important;
        color: #f28d2c !important
    }
    
    .goBacktoGoal {
        color: rgb(134, 132, 132);
        margin-top: 15px;
        border-radius: 30px;
        outline: 0 !important;
        box-shadow: 0 0 !important;
        width: 100px;
        border: 2px solid #ddd !important;
    }

.main_btn_edit_delete {
    margin: 0;
    padding: 0;
}
.main_btn_edit_delete .btn_edit_delete {
    width: 40px;
}
.main_btn_edit_delete .btn_edit_delete a {
    background: transparent;
    color: #848484;
    border: 2px solid #eeeeee;
    margin: 5px;
    display: inline-block;
    border-radius: 50% !important;
    box-shadow: 0 0 !important;
    height: 35px;
    font-size: 18px;
    width: 35px;
    line-height: 30px;
    outline: 0 !important;
    margin-right: 10px;
    transition: 0.5s;
}

.main_btn_edit_delete li:nth-child(2):hover a {
    color: #4CAF50 !important;
    border: 2px solid #4CAF50 !important;
}
.main_btn_edit_delete li:nth-child(3):hover a {
    color: #F44336 !important;
    border: 2px solid #F44336 !important;
}
.response_text1{
    margin: 15px 0px 0px;
    margin-top: 15px;
margin-top: 15px;
color: green;
display: none;
margin-top: 3px;
font-weight: 600;

}
.successfully_message{
    position: fixed;
    bottom: 0;
    right: 0;
    background-color: #080;
    color: #fff;
    padding: 5px 15px;
    text-align: center;
    display: none;
}

</style>

 

<script>
        $(function() {


$(this).find('#subDomain_name').on("keyup", function(){
    var funneldomain= $(this).val();
var regex = /^[0-9a-zA-Z\_]+$/;

if(funneldomain.length > 0){
    $('.createFunnelNew').css('pointer-events','');
    $('.response_text1').show().text('Available Sub domain').css('color','green');
}
else{
    $('.createFunnelNew').css('pointer-events','none');
    $('.createFunnelNew').css({'pointer-events':'none','color':'#ddd','border-color':'#ddd'});
}
if(funneldomain.length>0) {
if(regex.test(funneldomain)) {
$.ajax({
        url:'domianvalidate',
        type: 'post',
        data: {_token:$('meta[name="csrf-token"]').attr('content'),funneldomain:funneldomain},
        success: function(data1) {  

     
        if(data1 =="0")
        {

        $('.response_text1').show().text('Available Sub domain').css('color','green');
        $('.createFunnelNew').css({'pointer-events':'','color':'','border-color':''});
        }
        else{
        
        $('.response_text1').show().text('Not Available Sub domain').css('color','red');  
        $('.createFunnelNew').css({'pointer-events':'none','color':'#ddd','border-color':'#ddd'});
        }
   

},

error: function(xhr, ajaxOptions, thrownError) {

document.write(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
}

});

   }
   else{
      
$('.response_text1').show().text('Not Allow Special Character').css('color','red');  
$('.createFunnelNew').css('pointer-events','none');


   }
}
   else{
    $('.response_text1').hide();

   }




});



            
$('.new_display_block2,.secondOneforAddNewFunnel_hide2').hide();
$('.new_display_block1,.thirdStepforAddNewFunnel__show1').show();



$(".btn_edit_delete .fa-pencil").on("click", function(){
var funnel_name = $(this).parents('.article_ungrouped').find(".article_ungrouped_header").find("h5").text();
var funnelId = $(this).parents('.similar_cls').find("#funnelId").val();
var funnelUrl = $(this).parents('.similar_cls').find("#funnelUrl").val();
var funneltype = $(this).parents('.similar_cls').find("#funneltype").val();
var funnel_url = $(this).parents('.article_ungrouped').find(".article_ungrouped_header").find("#funnelUrl").val();


var selectedVal = $("#funnel_group_tag").find('option').val();
 
//$('#funnel_group_tag option').attr('selected','');


$('#funnel_group_tag option').filter(":selected").removeAttr('selected');

if(funneltype == "ungrouped"){
    //$('#funnel_group_tag option[value="ungroup"]').attr('selected','');
    $('#funnel_group_tag option[value="ungroup"]').attr('selected','selected');
}
 
else if(funneltype == "sharedfunnels"){   
    $('#funnel_group_tag option[value="sharefunnels"]').attr('selected','selected');
}
else if(funneltype == "cookbook"){
    //$('#funnel_group_tag option[value="cookbook"]').attr('selected','');
    $('#funnel_group_tag option[value="cookbook"]').attr('selected','selected');
}
   


$('#funnel_name[name="funnel_name"]').val(funnel_name);
$('#subDomain_name').val(funnelUrl);
$('#funnelmainId').val(funnelId);


     
// $(this).parents('.article_ungrouped').hide();
   $("#modal_id2").modal("show");

    
}); 
 

$(".btn_edit_delete .fa-trash").on("click", function(){
    
    
    //alert($(this).parents('.article_ungrouped').find(".article_ungrouped_header").find("h5").text());
//alert($(this).parents('.article_ungrouped').find(".article_ungrouped_header").find("#funnelId ").val());
    //funnelId
    
$(this).parents('.article_ungrouped').hide();
//$("#modal_id2").modal("show")
$(".successfully_message").html("Delete Successfully!").slideDown(function(){
    $(".successfully_message").fadeOut();
});

}); 


             
        });
    </script>

<style>


</style>



<div class="successfully_message"></div>



<div id="modal_id2" class="modal fade" role="dialog" style="z-index:1055 !important">
     
    <div class="modal-dialog modal-lg" style="width: 1000px !important">
        <div class="modal-content" style="width: 1000px">
            <div class="modal-header modal-new-header"><span class="close">×</span>
                <span class="loader"></span>
                <h4>Build a Funnel</h4></div>
            <div class="modal-body opened">
                <div class="funnelViewBlock2 decide_builder_form new_display_block0" style="display: none;">
                    <!-- <h1>Build a Funnel</h1> -->
                    <div class="row">
                        <div class="col-md-6" style="text-align: center;">
                            <h3>Cookbook Builder Process</h3>
                            <div class="decide_builder_image_cookbook"></div>
                            <!-- <div class="description">Explore different types of funnels and pre-done Funnels in this "Cookbook" style process and find the right Funnel for your business.</div> -->
                            <a class="btn btn-block btn-warning" data-popup-title="Add New Funnel" href="/cookbook" id="cookbook_link" title="New Funnel">
                                <i class="fa fa-book"></i> Start Cookbook
                            </a>
                        </div>
                        <div class="col-md-6" style="text-align: center; border-left: 1px solid #eee">
                            <h3>Classic Funnel Builder</h3>
                            <div class="decide_builder_image_classic open-dialog" data-popup-title="Add New Funnel" href="/funnels/new" title=" New Funnel"></div>
                            <!-- <div class="description">Our original step by step builder that's super simple and giving a basic funnel scaffolding fast. Select from Lead, Sales, or Event Funnels!</div> -->
                            <a class="btn btn-block btn-warning open-dialog" data-popup-title="Add New Funnel" href="#" id="classic_link" title=" New Funnel">
                                <i class="fa fa-plus"></i> Create New Funnel
                            </a>
                        </div>
                    </div>
                </div>
                <style>
                    .new_funnel_form {
                        display: none;
                    }

                </style>
                <div class="simple_form form-horizontal marginInputFix createNewFunnelForm new_funnel_form new_display_block1" style="display: block;">

                   
                    <div class="funnelViewBlock2">
                        <input class="new-funnel" name="funnel[funnel_template_id]" id="" value="" type="hidden">
                        <div class="col-sm-12 selectFunnelTypeBox clearfix">
                            <div class="addNewFunnelProgressStepsDiv">
                                <div class="addNewFunnelProgressBar"></div>
                                <div class="col-sm-2 f_step1 currentlySelected">
                                    <div class="addNewFunnelProgressBox"></div>
                                    <i class="fa fa-check"></i> Choose Goal
                                </div>
                                <div class="col-sm-3">
                                    <div class="f_step2" style="display: none;">
                                        <div class="addNewFunnelProgressBox smallBoxProgress"></div>
                                        <span class="step2_text_f">Start from Scratch</span>
                                    </div> 
                                </div>
                                <div class="col-sm-2 f_step3">
                                    <div class="addNewFunnelProgressBox"></div>
                                    <i class="fa fa-check"></i> Choose Type
                                </div>
                                <div class="col-sm-3">
                                    <div class="f_step4" style="display: none;">
                                        <div class="addNewFunnelProgressBox smallBoxProgress"></div>
                                        <span class="step4_text_f">Custom Funnel</span>
                                    </div>
                                </div>
                                <div class="col-sm-2 f_step5">
                                    <div class="addNewFunnelProgressBox"></div>
                                    <i class="fa fa-check"></i> Build Funnel
                                </div>
                            </div>
                            <div class="secondOneforAddNewFunnel secondOneforAddNewFunnel_hide2">
                       
                                <div class="col-sm-4">
                                    <div class="funnelStartSplitTestBlock innerFunnel_block chooseOptinEmailFunnel selection_optinfunnel" data-tabtype="1" style="padding-top: 20px;margin-top: -30px;">
                                        
                                        <div class="funnelStartSplitTestHeadline">
                                            <img src="https://californiapsychics.zendesk.com/hc/article_attachments/360001335208/DM_Icon_1.5.png" alt="Mail" width="30"> Collect Emails
                                        </div>
                                        <div class="funnelStartSplitTestText">
                                            Build a new email list and start building an engaging email list.
                                        </div>
                                        <div class="funnelStartSplitTestButton2">
                                            <a class="btn btn-default" href="#">
                                                <i class="fa fa-plus"></i> Choose
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="funnelStartSplitTestBlock innerFunnel_block selection_choosesalesfunnel" data-tabtype="1" style="padding-top: 20px;margin-top: -30px;">
                                       
                                        <div class="funnelStartSplitTestHeadline">
                                            <img src="https://poutios-digi-land.cloudaccess.host/images/webinar/webinar-icon-2.png" alt="Sales" width="30"> Sell Your Product
                                        </div>
                                        <div class="funnelStartSplitTestText">
                                            Sell your products or services with a variety of funnels.
                                        </div>
                                        <div class="funnelStartSplitTestButton2">
                                            <a class="btn btn-default" href="#">
                                                <i class="fa fa-plus"></i> Choose
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="funnelStartSplitTestBlock innerFunnel_block chooseWebinarFunnel" data-tabtype="1" style="padding-top: 20px;margin-top: -30px;">
                                        
                                        <div class="funnelStartSplitTestHeadline">
                                            <img src="https://i0.wp.com/xtalks.com/wp-content/uploads/2016/12/webinar-effectivness-pie-chart-1.png?fit=966%2C895&amp;ssl=1" alt="Webinar" width="30"> Host Webinar
                                        </div>
                                        <div class="funnelStartSplitTestText">
                                            Get an audience at your next webinar whether it is live or automated replay.
                                        </div>
                                        <div class="funnelStartSplitTestButton2">
                                            <a class="btn btn-default" href="#">
                                                <i class="fa fa-plus"></i> Choose
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4" style="padding-top: 20px">
                                    <a class="btn btn-default selection_customfunnel innerFunnelSecond" data-tabtype="31">Create a Custom Funnel</a>
                                </div>
                                <div class="col-sm-4 col-sm-offset-4" style="padding-top: 30px; text-align:right;">
                                    <a href="javascript:void(0)" target="_blank">Browse Funnel Marketplace</a>
                                </div>
                            </div>

                            <div class="secondStepforAddNewFunnel new_display_block2" style="display: none;">
                                <div class="choseSales" style="">
                                    <div class="col-sm-4">
                                        <div class="funnelStartSplitTestBlock innerFunnel_block innerFunnelSecond selection_salesfunnel" data-tabtype="101" style="padding-top: 20px;margin-top: -30px;">
                                            <!--  <div class="funnelStartSplitTestIcon">
                                                <img src="/images/createfunnels/sales.png" width="50" alt="Sales">
                                            </div> -->
                                            <div class="funnelStartSplitTestHeadline">
                                                <img src="https://cdn4.iconfinder.com/data/icons/seo-and-business-glyph-1/64/seo-and-business-glyph-1-20-512.png" alt="Sales"> Sales Funnel
                                            </div>
                                            <div class="funnelStartSplitTestText">
                                                Sell your products after collecting leads with a squeeze page.
                                            </div>
                                            <div class="funnelStartSplitTestButton2" style="margin-bottom: 10px">
                                                <a class="btn btn-default" href="#">
                                                    <i class="fa fa-plus"></i> Choose
                                                </a>
                                            </div>
                                            <div class="col-sm-12 smallFunnelImage" style="display: none;">
                                                <span class="showInfoOnTheFunnel" data-infotypefunnel="membership_salesfunnel">
Show Info
</span>
                                                <i class="fa fa-envelope" data-toggle="tooltip" title="Squeeze Page"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-usd" data-toggle="tooltip" title="Sales Page"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-check-square-o" data-toggle="tooltip" title="Order Confirmation"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-download" data-toggle="tooltip" title="Download Page"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="funnelStartSplitTestBlock innerFunnel_block innerFunnelSecond selection_launchfunnel" data-tabtype="61" style="padding-top: 20px;margin-top: -30px;">
                                            <!--  <div class="funnelStartSplitTestIcon">
                                                <img src="/images/createfunnels/launch.png" alt="Launch">
                                            </div> -->
                                            <div class="funnelStartSplitTestHeadline">
                                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTEqpK6T8wnXkA4fCTpQdS9HjWO3qOPborRFfkdLdmQ2t9l3KJZ" alt="Launch"> Product Launch
                                            </div>
                                            <div class="funnelStartSplitTestText">
                                                Sell your product in a launch sequence with video pages.
                                            </div>
                                            <div class="funnelStartSplitTestButton2" style="margin-bottom: 10px">
                                                <a class="btn btn-default" href="#">
                                                    <i class="fa fa-plus"></i> Choose
                                                </a>
                                            </div>
                                            <div class="col-sm-12 smallFunnelImage" style="display: none;">
                                                <span class="showInfoOnTheFunnel" data-infotypefunnel="launch_salesfunnel">
Show Info
</span>
                                                <i class="fa fa-envelope" data-toggle="tooltip" title="Squeeze Page"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-youtube-play" data-toggle="tooltip" title="Video Page"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-shopping-cart" data-toggle="tooltip" title="Order Page"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-check-square-o" data-toggle="tooltip" title="Confirmation Page"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-download" data-toggle="tooltip" title="Download Page"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4" style="margin-bottom: 0">
                                        <div class="funnelStartSplitTestBlock innerFunnel_block innerFunnelSecond selection_membershipfunnel" data-tabtype="71" style="    margin-top: -29px;padding-top: 20px;margin-bottom: 0">
                                            <!--  <div class="funnelStartSplitTestIcon">
                                                <img src="/images/createfunnels/membership.png" alt="Membership">
                                            </div> -->
                                            <div class="funnelStartSplitTestHeadline">
                                                <img src="https://www.bwf.com/wp-content/uploads/2015/09/services-Board-Engagement.png" alt="Membership"> Membership
                                            </div>
                                            <div class="funnelStartSplitTestText">
                                                Sell your content with a membership that you can charge for.
                                            </div>
                                            <div class="funnelStartSplitTestButton2" style="margin-bottom: 10px">
                                                <a class="btn btn-default" href="#">
                                                    <i class="fa fa-plus"></i> Choose
                                                </a>
                                            </div>
                                            <div class="col-sm-12 smallFunnelImage" style="display: none;">
                                                <span class="showInfoOnTheFunnel" data-infotypefunnel="membership_salesfunnel">
Show Info
</span>
                                                <i class="fa fa-envelope" data-toggle="tooltip" title="Member Access Page"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-graduation-cap" data-toggle="tooltip" title="Members Area"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="choseWebinar" style="display: none;margin-top: -30px">
                                    <div class="col-sm-4">
                                        <div class="funnelStartSplitTestBlock innerFunnel_block innerFunnelSecond selection_webinarfunnel" data-tabtype="11" style="padding-top: 20px;margin-bottom: 0">
                                            <!--   <div class="funnelStartSplitTestIcon">
                                                <img src="/images/createfunnels/webinar.png" alt="Webinar">
                                            </div> -->
                                            <div class="funnelStartSplitTestHeadline">
                                                <img src="https://cdn4.iconfinder.com/data/icons/webinars-online-education-web-conferences/52/webinar_glyph-14-512.png" alt="Webinar"> Live Webinar
                                            </div>
                                            <div class="funnelStartSplitTestText">
                                                Sell your products after collecting leads with a squeeze page.
                                            </div>
                                            <div class="funnelStartSplitTestButton2" style="margin-bottom: 10px">
                                                <a class="btn btn-default" href="#">
                                                    <i class="fa fa-plus"></i> Choose
                                                </a>
                                            </div>
                                            <div class="col-sm-12 smallFunnelImage" style="display: none;">
                                                <span class="showInfoOnTheFunnel" data-infotypefunnel="membership_salesfunnel">
Show Info
</span>
                                                <i class="fa fa-envelope" data-toggle="tooltip" title="Webinar Registration"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-check-square-o" data-toggle="tooltip" title="Webinar Countdown"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-microphone" data-toggle="tooltip" title="Webinar Broadcast Room"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="funnelStartSplitTestBlock innerFunnel_block innerFunnelSecond selection_autowebinarfunnel" data-tabtype="21" style="padding-top: 20px;margin-bottom: 0">
                                            <!--   <div class="funnelStartSplitTestIcon">
                                                <img src="/images/createfunnels/autowebinar.png" alt="Autowebinar">
                                            </div> -->
                                            <div class="funnelStartSplitTestHeadline">
                                                <img src="https://uwm.edu/studentinvolvement/wp-content/uploads/sites/260/2019/01/Upcomingn-Events-Grid-Photo.png" alt="Autowebinar"> Webinar Replay
                                            </div>
                                            <div class="funnelStartSplitTestText">
                                                Showcase a replay of a webinar for evergreen lead generation.
                                            </div>
                                            <div class="funnelStartSplitTestButton2" style="margin-bottom: 10px">
                                                <a class="btn btn-default" href="#">
                                                    <i class="fa fa-plus"></i> Choose
                                                </a>
                                            </div>
                                            <div class="col-sm-12 smallFunnelImage" style="display: none;">
                                                <span class="showInfoOnTheFunnel" data-infotypefunnel="membership_salesfunnel">
Show Info
</span>
                                                <i class="fa fa-envelope" data-toggle="tooltip" title="Webinar Registration"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-check-square-o" data-toggle="tooltip" title="Webinar Countdown"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-microphone" data-toggle="tooltip" title="Webinar Broadcast Room"></i>
                                                <i class="fa fa-chevron-right" style="opacity: .2;margin: 0 5px;cursor: pointer"></i>
                                                <i class="fa fa-youtube-play" data-toggle="tooltip" title="Webinar Replay Room"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 goback" style="">
                                    <a class="btn btn-sm btn-default pull-left goBacktoGoal2" href="#" style="color: rgb(134, 132, 132);margin-top: 15px;margin-left: 20px;float: left">
                                        <i class="fa fa-caret-left"></i> Go Back
                                    </a>
                                </div>
                            </div>
                            <div class="thirdStepforAddNewFunnel thirdStepforAddNewFunnel__show1" style="padding-top: 30px; margin-top: -50px;">
                                <div class="savedFunnelViewHeader" style="margin-top: -30px;border-radius: 5px;"></div>
                                <div class="col-sm-7" style="clear: both;padding-bottom: 20px;padding-right: 5px;    margin-top: 20px;padding-left: 0;">
                                    
                                    <form method="POST" action="{{url('funnel/editadded/step')}}">
                                
                                    <input name="_token" type="hidden" value="{{csrf_token()}}"/>

                                       <input value="productlunch" name="funnel_type" id="funnelactivitytype" type="hidden">
                                            <input value="3" name="funnel_step_times" id="funnel_step_times" type="hidden">
                                            <input value="" name="funnelmainid" id="funnelmainId" type="hidden">
                                    <div class="selectYourFunnelDone clearfix" style="background: none;border-bottom-left-radius: 5px;border-bottom-right-radius: 5px;margin-left: -4px;padding: 20px 20px;padding-right: 10px;padding-top:0px;padding-bottom: 0">
                                        <div class="col-sm-12" style="padding-top: 0px; padding: 0px 0px">
                                            <strong style="font-weight: bold">Name:</strong>
                                            <div class="form-group string required funnel_name">
                                                <div class="col-lg-10">
                                                    <input class="form-control string required" required="required" aria-required="true" placeholder="Give Funnel a Name" name="funnel_name" id="funnel_name" type="text">
                                                </div>
                                            </div>
                                         
                                            <strong style="font-weight: bold;margin-top: 20px;">Select Group Tag:</strong>
                                            <div class="form-group string optional funnel_group_tag">
                                                <div class="col-lg-10">
                                                   
                                                    <select class="form-control string optional select2-offscreen" type="text" name="funnel_tag" id="funnel_group_tag" tabindex="-1">
                                                        <option value="ungroup">Select Group Tag</option>
                                                        
                                                        <option value="sharefunnels">Shared Funnels</option>
                                                        <option value="cookbook">Cookbook</option>
                                                    </select>
                                                </div>
                                            </div>





<strong style="font-weight: bold;margin-top: 20px;">Subdomain Name:</strong>
                                            <div class="form-group string optional funnel_group_tag">
                                                <div class="col-lg-8">
                                                   <input class="form-control string optional select2-offscreen" name="edit_subdomain_name" id="subDomain_name" type="text">
                                                   <span class="clearfix"></span>
                                                   <p class="response_text1"></p>
                                                </div>
                                                 <div class="col-lg-4">
                                                     <p class="xfunnel_io" style="padding-top: 10px;font-size: 20px;">ianirafunnels.com</p>
                                                 </div>
                                            </div>
 

                                            <button class="btn btn-success pull-right createFunnelNew" type="submit" value="submit" data-disable-with="Building Funnel..." style="float: right;margin-top: 15px;margin-right: 0;">
                                                <i class="fa fa-edit"></i> Build Funnel
                                            </button>
                                            <a class="btn btn-sm btn-default pull-left goBacktoGoal" href="#" style="color: rgb(134, 132, 132); margin-top: 15px; float: left;">
                                                <i class="fa fa-caret-left"></i> Go Back
                                            </a>
                                        </div>
                                    </div>
                                    </form>  
                                        
                                </div>

                                <div class="col-sm-5 doneImageofFunnel" style="display: block;margin-top: 34px">
                                    <div class="addfunnel_FinalShow addfunnel_FinalEmail" style="display: none;">
                                        <div class="col-sm-12">
                                            <i class="fa fa-caret-left lilCaretleft"></i>
                                            <h4>Funnel Steps:</h4>
                                            <ol>
                                                <li>Email Landing Page</li>
                                                <li>Thank You / Download Page</li>
                                            </ol>
                                            <p>The best way to convert website traffic into leads is to send them through a page that's usually called a lead, optin, or squeeze page.</p>
                                            <a class="onboardingHelpVideo" href="javascript:void(0)">
                                                <i class="fa fa-youtube-play" style="margin-right: 6px"></i> Watch Explainer Video
                                            </a>
                                        </div>
                                        <!-- <div class="showfunnelimage_info_below">
                                            <img src="/assets/funnel_types/squeezeflow-2c168d67762650f9691237644a412602be1dc82ff501579ca770cd56fc079d08.png" alt="Squeezeflow">
                                        </div> -->
                                    </div>
                                    <div class="addfunnel_FinalShow addfunnel_FinalSales" style="display: none">
                                        <div class="col-sm-12">
                                            <i class="fa fa-caret-left lilCaretleft"></i>
                                            <h4>Funnel Steps:</h4>
                                            <ol>
                                                <li>Email Landing Page</li>
                                                <li>Sales Page</li>
                                                <li>Order Confirmation</li>
                                                <li>Thank You / Download Page</li>
                                            </ol>
                                            <p>So, you've got something to sell online? If so, then you're probably going to need one of our proven sales funnels.</p>
                                            <a class="onboardingHelpVideo" href="javascript:void(0)">
                                                <i class="fa fa-youtube-play" style="margin-right: 6px"></i> Watch Explainer Video
                                            </a>
                                        </div>
                                        <!--  <div class="showfunnelimage_info_below">
                                            <img src="/assets/funnel_types/sales-funnel-03591030181125164cf3b232e37114547156ef086b5200e4f95b7d3431c7eb6e.png" alt="Sales funnel">
                                        </div> -->
                                    </div>
                                    <div class="addfunnel_FinalShow addfunnel_FinalLaunch" style="display: none;">
                                        <div class="col-sm-12">
                                            <i class="fa fa-caret-left lilCaretleft"></i>
                                            <h4>Funnel Steps:</h4>
                                            <ol>
                                                <li>Email Landing Page</li>
                                                <li>Free Video #1 - #4</li>
                                                <li>Order Page</li>
                                                <li>Thank You / Download Page</li>
                                            </ol>
                                            <p>The launch funnel was designed after years of testing different ways to launch a new product, and it's been proven to work in hundreds of different markets.</p>
                                            <a class="onboardingHelpVideo" href="javascript:void(0)">
                                                <i class="fa fa-youtube-play" style="margin-right: 6px"></i> Watch Explainer Video
                                            </a>
                                        </div>
                                        <!--    <div class="showfunnelimage_info_below">
                                            <img src="/assets/funnel_types/launch-funnel-b50cb247cfdc66cd52dfee47e86f251d631627a41af777b145367f8892952bd6.png" alt="Launch funnel">
                                        </div> -->
                                    </div>
                                    <div class="addfunnel_FinalShow addfunnel_FinalMembership" style="display: none">
                                        <div class="col-sm-12">
                                            <i class="fa fa-caret-left lilCaretleft"></i>
                                            <h4>Funnel Steps:</h4>
                                            <ol>
                                                <li>Register / Sign Up</li>
                                                <li>Memebership Area</li>
                                                <ul>
                                                    <li>Lessons</li>
                                                </ul>
                                            </ol>
                                            <p>So, technically a membership site isn't actually a funnel, it's a way to deliver your content to people in a fun and unique way.</p>
                                            <a class="onboardingHelpVideo" href="javascript:void(0)">
                                                <i class="fa fa-youtube-play" style="margin-right: 6px"></i> Watch Explainer Video
                                            </a>
                                        </div>
                                        <!-- <div class="showfunnelimage_info_below">
                                            <img src="/assets/funnel_types/membership-funnel-13ce712f597def5fb219d5615bcbde779f6f05bc39cb69d416ae98d6b377e722.png" alt="Membership funnel">
                                        </div> -->
                                    </div>
                                    <div class="addfunnel_FinalShow addfunnel_FinalWebinar" style="display: none">
                                        <div class="col-sm-12">
                                            <i class="fa fa-caret-left lilCaretleft"></i>
                                            <h4>Funnel Steps:</h4>
                                            <ol>
                                                <li>Webinar Sign Up</li>
                                                <li>Confirmation Page</li>
                                                <li>Live Webinar</li>
                                            </ol>
                                            <p>Would you like to do a live webinar, but you aren't happy with the look and feel of your webinar providers registration and confirmation pages? If so, then you are going to love our webinar funnels!</p>
                                            <a class="onboardingHelpVideo" href="javascript:void(0)">
                                                <i class="fa fa-youtube-play" style="margin-right: 6px"></i> Watch Explainer Video
                                            </a>
                                        </div>
                                        <!--    <div class="showfunnelimage_info_below">
                                            <img src="/assets/funnel_types/webinar-funnel-92b7984c1bf7347466830e6a980c07d83c7909589a0a8e1364799a19fc7fcd7e.png" alt="Webinar funnel">
                                        </div> -->
                                    </div>
                                    <div class="addfunnel_FinalShow addfunnel_FinalAutoWebinar" style="display: none">
                                        <div class="col-sm-12">
                                            <i class="fa fa-caret-left lilCaretleft"></i>
                                            <h4>Funnel Steps:</h4>
                                            <ol>
                                                <li>Webinar Sign Up</li>
                                                <li>Confirmation Page</li>
                                                <li>Auto Webinar</li>
                                                <li>Webinar Replay</li>
                                            </ol>
                                            <p>Do you have a webinar that's producing results, but you don't want to keep doing live presentations every night?</p>
                                            <a class="onboardingHelpVideo" href="javascript:void(0)">
                                                <i class="fa fa-youtube-play" style="margin-right: 6px"></i> Watch Explainer Video
                                            </a>
                                        </div>
                                        <!--   <div class="showfunnelimage_info_below">
                                            <img src="/assets/funnel_types/auto-webinar-funnel-b97f1c58c1ee3e6992cb113d31fcbc3fa1e4061be3badace4a7899654ddbce6f.png" alt="Auto webinar funnel">
                                        </div> -->
                                    </div>
                                    <div class="addfunnel_FinalShow addfunnel_FinalCustom" style="">
                                        <div class="col-sm-12">
                                            <i class="fa fa-caret-left lilCaretleft"></i>
                                            <p>So, you have an idea for a funnel, and you want to build it out without any restrictions? If so, then our custom funnel is for you.</p>
                                            <p>You can literally build out any type of funnel you can dream of. Pick a sales letter template, add an upsell or two, create a downsell, add an automated webinar, a membership site, or more.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12" style="padding: 0 0;display: none;padding-bottom: 20px;padding-left: 0">
                            <div class="col-sm-9 funnelCreateInfo" style="padding-left: 0;padding: 10px 14px;font-size: 22px;font-weight: 500; color: #5A6A72;opacity: .8">
                            </div>
                            <div class="col-sm-3" style="padding-right: 0;padding-top: 10px;padding-right: 6px">
                                <div class="btn btn-success btn-block createFunnelNew" style="font-size: 21px;float: right">
                                    <i class="fa fa-arrow-right"></i> Setup Your Funnel
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 legendForFunnel" style="display: none">
                        <span>
<strong>
Types of Pages:
</strong>
</span>
                        <span>
<span>
<i class="fa fa-envelope"></i>
Squeeze
</span>
                        <span>
<i class="fa fa-download"></i>
Download / Thank You
</span>
                        <span>
<i class="fa fa-shopping-cart"></i>
Sales
</span>
                        <span>
<i class="fa fa-check-square-o"></i>
Order Confirmation
</span>
                        <span>
<i class="fa fa-microphone"></i>
Webinar
</span>
                        <span>
<i class="fa fa-youtube-play"></i>
Video
</span>
                        <span>
<i class="fa fa-graduation-cap"></i>
Members Area
</span>
                        </span>
                    </div>
                </div>
           

                
               </div>
        </div>
    </div>


</div>



 










<script>
  $(function() {

$('.new_display_block2,.secondOneforAddNewFunnel_hide2').hide();
$('.new_display_block1,.thirdStepforAddNewFunnel__show1').show();

$('.modal-new-header .close').click(function() {
//console.log('close click');
// $(this).parent().parent().parent().parent().remove();

$('#modal_id2').modal('hide');
});


$('.modal-header .close').click(function() {
//console.log('close click');
// $(this).parent().parent().parent().parent().remove();
$('#modal_id1').modal('hide');
});
        });
    </script>
 
 
@endsection