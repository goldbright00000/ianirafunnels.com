<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Admin Login</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{ asset('admin-css/vendors/iconfonts/mdi/css/materialdesignicons.min.css')}}">
  <link rel="stylesheet" href="{{ asset('admin-css/vendors/css/vendor.bundle.base.css')}}">
  <link rel="stylesheet" href="{{ asset('admin-css/vendors/css/vendor.bundle.addons.css')}}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link rel="stylesheet" href="{{ asset('admin-css/css/style.css')}}">
  <!-- endinject -->
  
  <style>
      #ErrorMessage
      {
          
      position: absolute;
z-index: 999;
top: -62px;
width: 95%;
      }
  </style>
  
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
      <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
   
           
    @if (session('success'))
    <div id="sucessfullyMessage" class="alert alert-success animated fadeIn sucess_message">
        <button type="button" class="close s_close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
       
 
             {{ session('success') }}
        </strong>
    </div>
@endif
    

 
  
@if (session('fail'))
    <div id="ErrorMessage"   class="alert alert-danger animated fadeIn sucess_message">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
              {{ session('fail') }}
        </strong>
    </div>
@endif

                       
            
                       
                       
                       
                       
            <div class="auto-form-wrapper">
<center>Admin Login</center>
              <form action="login-admin" method="post" >
 {{ csrf_field() }}
                <div class="form-group">
                  <label class="label">Username</label>
                  <div class="input-group">
                      <input type="email" class="form-control" @if(session('email')) value="{{ session('email') }}" @endif  name="username" placeholder="Username" required="true">
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-check-circle-outline"></i>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="label">Password</label>
                  <div class="input-group">
                    <input type="password" class="form-control" name="password"  @if(session('password')) value="{{ session('password') }}" @endif  placeholder="*********" required="true">
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-check-circle-outline"></i>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <input type="submit" value="login" class="btn btn-primary submit-btn btn-block" name="submit">
                </div>
              
              
              
              </form>
            </div>
            
            <p class="footer-text text-center">copyright © 2019. All rights reserved.</p>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
    <script>
        
        setTimeout(function() {
    $('#sucessfullyMessage').fadeOut('fast');
}, 2700);

 $('.close').click(function(){
     
    $('sucessfullyMessage').hide('fast')
     
 });
 
 setTimeout(function() {
    $('#ErrorMessage').fadeOut('fast');
}, 2700);
 
 
 
 
 
        </script>
  <script src="{{ asset('admin-css/vendors/js/vendor.bundle.base.js')}}"></script>
  <script src="{{ asset('admin-css/vendors/js/vendor.bundle.addons.js')}}"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="{{ asset('admin-css/js/off-canvas.js')}}"></script>
  <script src="{{ asset('admin-css/js/misc.js')}}"></script>
  <!-- endinject -->
</body>

</html>



