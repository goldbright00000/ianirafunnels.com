@extends('admin.default')
@section('title','Admin Dashboard')
@section('content')
  

  
  <div class="row">
            <div class="col-lg-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Orders</h4>
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="searchData">
                      <thead>
                        <tr>
                        
                            <th>
                 S.l
                          </th>
                            
                            
                          <th>
                        Name
                          </th>
                          
                          
                          <th>
                        Email 
                          </th>
                          
                          
                          <th>
                            User Register Time
                          </th>
                         
                        </tr>
                      </thead>
                      <tbody>
                      
                     
                         
                      @foreach($member as $key => $member_row )
                      
                        <tr>
                  
                        
                          <td class="font-weight-medium">
                       {{++$key}}
                          </td>
                          
                          <td>
                          {{$member_row->name}}
                          </td>
                          
                          
                          <td>
                          {{$member_row->email}}
                          </td>
                          
                          
                           <td>
                          {{$member_row->created_at}}
                          </td>
                        </tr>
                       
                        @endforeach
                 
                  
                      
                      
                      
                      
                      
                      
                      
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endsection