<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>@yield('title')</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{ asset('admin-css/vendors/iconfonts/mdi/css/materialdesignicons.min.css')}}">
  <link rel="stylesheet" href="{{ asset('admin-css/vendors/css/vendor.bundle.base.css')}}">
  <link rel="stylesheet" href="{{ asset('admin-css/vendors/css/vendor.bundle.addons.css')}}">

  <link rel="stylesheet" href="{{ asset('admin-css/css/style.css')}}" type="text/css">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{ asset('admin-css/img/funnel_logo.pn')}}" />
  
  
<link rel="shortcut icon" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css')}}" type="text/css"/>
  
 <link rel="shortcut icon" href="{{asset('https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css')}}" type="text/css"/>
    
 <link rel="stylesheet" href="{{ asset('admin-css/vendors/iconfonts/font-awesome/css/font-awesome.css')}}" type="text/css" >

  <style>
      .sidebar
      {
           border-right: 1px solid #d5d5d5;
   
      }
      .nav .nav-item, .navbar-nav .nav-item {
   
    border-bottom: 1px solid #e2e2e2;
      }
        .sub-menu .nav-item
        {
         border-bottom:none;   
        }
        
  </style>
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="">
          <img src="{{ asset('img/funnel_logo.png')}}" alt="" />
        </a>
        <a class="navbar-brand brand-logo-mini" href="">
          <img src="{{ asset('img/funnel_logo.png')}}" alt="" />
        </a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
       <!--- <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
          <li class="nav-item">
            <a href="#" class="nav-link">Schedule
              <span class="badge badge-primary ml-1">New</span>
            </a>
          </li>
          <li class="nav-item active">
            <a href="#" class="nav-link">
              <i class="mdi mdi-elevation-rise"></i>Reports</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="mdi mdi-bookmark-plus-outline"></i>Score</a>
          </li>
        </ul>--->
        <ul class="navbar-nav navbar-nav-right">
         
          <li class="nav-item dropdown">
            <!---<a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
              <i class="mdi mdi-bell"></i>
              <span class="count">4</span>
            </a>--->
            
          </li>
          <li class="nav-item dropdown d-none d-xl-inline-block">
            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <span class="profile-text">Hello, {{session('name')}} !</span>
              <img class="img-xs rounded-circle" src="{{ asset('img/funnel_logo.png')}}">
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
              <a class="dropdown-item p-0">
                <div class="d-flex border-bottom">
                  <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                    <i class="mdi mdi-bookmark-plus-outline mr-0 text-gray"></i>
                  </div>
                  <div class="py-3 px-4 d-flex align-items-center justify-content-center border-left border-right">
                    <i class="mdi mdi-account-outline mr-0 text-gray"></i>
                  </div>
                  <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                    <i class="mdi mdi-alarm-check mr-0 text-gray"></i>
                  </div>
                </div>
              </a>
                <a class="dropdown-item" href="{{url('admin/profile-edit')}}">
                Profile Edit
              </a>
              <a href="{{url('admin-dashboard/change-password')}}"   class="dropdown-item mt-2">
               Change Password
              </a>
             
                
              <a class="dropdown-item" href="{{url('logout/dashboard')}}">
                Sign Out
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <div class="nav-link">
              <div class="user-wrapper">
                <div class="profile-image">
                  <img src="{{ asset('img/funnel_logo.png')}}" alt="">
                </div>
                <div class="text-wrapper">
                  <p class="profile-name">Admin</p>
                  <div>
                    <small class="designation text-muted">Manager</small>
                    <span class="status-indicator online"></span>
                  </div>
                </div>
              </div>
             <!--- <button class="btn btn-success btn-block">New Project
                <i class="mdi mdi-plus"></i>
              </button>--->
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('login/dashboard')}}">
              <i class="menu-icon mdi mdi-television"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
          
          
       
          <li class="nav-item">
            <a class="nav-link" href="{{url('login/register-member')}}">
              <i class="menu-icon mdi mdi-table"></i>
              <span class="menu-title">Register Member</span>
            </a>
          </li>
          
         
          
            <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-content-copy"></i>
              <span class="menu-title">Funnel Category</span>
             <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="{{url('login/add-funnel')}}">Add Funnel Category</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('funnel/all-funnel-category')}}">All Funnel Category</a>
                </li>
              </ul>
            </div>
          </li>
          
          
          
           <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-subcategory" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-book-open"></i>
              <span class="menu-title">  Funnel Sub Category</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-subcategory">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                    <a class="nav-link" href="{{url('admin/add-funnel')}}"> Add Sub Funnel</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('admin/all-subcategory-funnel')}}">View Sub Funnel</a>
                </li>
              </ul>
            </div>
          </li>
          
          
              <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-sub" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-folder-outline"></i>
              <span class="menu-title"> Add Funnel</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-sub">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                    <a class="nav-link" href="{{url('login/funnel-add')}}"> Add  Funnel </a>
                </li>
                
                 <li class="nav-item">
                  <a class="nav-link" href="{{url('admin/all-sub-funnel')}}">All Funnel</a>
                </li>
            
              </ul>
            </div>
          </li>
          
          
          
           <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-oo" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-folder-outline"></i>
              <span class="menu-title"> Add Template </span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-oo">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                    <a class="nav-link" href="{{url('admin/funnel_page_add')}}">Add Template</a>
                </li>
                
                 <li class="nav-item">
                  <a class="nav-link" href="{{url('admin/sub-funnel-edit')}}">View Template</a>
                </li>
            
              </ul>
            </div>
          </li>
          
          
          
          
          
          
        <!---- <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
              <i class="menu-icon mdi mdi-restart"></i>
              <span class="menu-title">User Pages</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="auth">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="../../pages/samples/blank-page.html"> Blank Page </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="../../pages/samples/login.html"> Login </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="../../pages/samples/register.html"> Register </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="../../pages/samples/error-404.html"> 404 </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="../../pages/samples/error-500.html"> 500 </a>
                </li>
              </ul>
            </div>
          </li>--->
          
          
          
          
          
          
          
          
          
        
         
        </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel">
   
 @yield('content')
        
        
        
        
        
    
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2019
              <a href="" target="_blank"></a>Ianirafunnels. All rights reserved.</span>
           
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
   <script src="{{asset('https://code.jquery.com/jquery-3.3.1.js')}}"></script>
  
  <script src="{{asset('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js')}}"></script>
          
<script src="{{asset('https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js')}}"></script>

  <script src="{{ asset('admin-css/vendors/js/vendor.bundle.base.js')}}"></script>
  <script src="{{ asset('admin-css/vendors/js/vendor.bundle.addons.js')}}"></script>

  <script src="{{ asset('admin-css/js/off-canvas.js')}}"></script>
  <script src="{{ asset('admin-css/js/misc.js')}}"></script>
 
  <script src="{{ asset('admin-css/js/dashboard.js')}}"></script>
  <script>
      $(document).ready(function() {
    $('#searchData').DataTable();
} );
      
      </script>
  <!-- End custom js for this page-->
</body>

</html>