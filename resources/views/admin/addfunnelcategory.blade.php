@extends('admin.default')
@section('title','Funnel Category')
@section('content')
<style>
    .sucess_message {
    margin: 0;
        margin-bottom: 0px;
    margin-bottom: 0px;
    position: absolute !important;
    top: 12%;
    left: 50%;
    transform: translate(-50%, -50%);
    position: absolute;
    width: 24%;
    height: 41px;
    line-height: 15px;
    text-align: left;
    z-index: 999999;
}
.s_close
{
   line-height: 15px; 
}

margin-right: 1.25rem;
width: 16px;
line-height: 1;
font-size: 18px;
color: #979797;

    </style>


 <div class="row purchace-popup">
            <div class="col-12">
              <span class="d-block d-md-flex align-items-center">
                  <p><a href="{{url('login/dashboard')}}"><b>Home</b></a>&nbsp; >> Funnel Category
  </p>
                
              </span>
            </div>
          </div>
<div class="content-wrapper">
          <div class="row">
            <div class="col-md-6 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Funnel Category</h4>
                    
                      
                      <form class="forms-sample" method="post" action="{{url('funnel/add-category')}}" >
                          
                          
                          {{ csrf_field() }}
                       
                          
                        <div class="form-group">
                          <label for="exampleInputEmail1">Name</label>
                          <input type="text" class="form-control" id="exampleInputEmail1"  value="" name="funnel_name" placeholder="Enter email" required="true" >
                        </div>
                      
                          <input type="submit" value="submit"  class="btn btn-success mr-2" name="Submit">
                          <a href="{{url('login/dashboard')}}" class="btn btn-light">Cancel</a>
                      </form>
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
           
           
           
           
            
           
          </div>
        </div>

<script>
    setTimeout(function() {
    $('#sucessfullyMessage').fadeOut('fast');
}, 2700);

 $('.close').click(function(){
     
    $('sucessfullyMessage').hide('fast')
     
 });
 </script>
@endsection