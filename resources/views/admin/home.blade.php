@extends('admin.default')
@section('title','Admin Dashboard')
@section('content')
<div class="content-wrapper">
          
          <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
               
                      
                      <i class="mdi mdi-account-location text-info icon-lg" aria-hidden="true"></i>

                      
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Total Member Register</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0">{{$totalmember}}</h3>
                      </div>
                    </div>
                      
                      
                      
                  </div>
                  <p class="text-muted mt-3 mb-0">
                    <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> 
                  </p>
                </div>
              </div>
            </div>
              
              
              
              
           <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
               
                      
                      <i class="mdi mdi-account-location text-info icon-lg" aria-hidden="true"></i>

                      
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Today Member Register</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0">{{$todaymember}}</h3>
                      </div>
                    </div>
                      
                      
                      
                  </div>
                  <p class="text-muted mt-3 mb-0">
                    <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> 
                  </p>
                </div>
              </div>
            </div>
              
              
              
           <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
               
                      
                      <i class="mdi mdi-account-location text-info icon-lg" aria-hidden="true"></i>

                      
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Week Member Register</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0"></h3>
                      </div>
                    </div>
                      
                      
                      
                  </div>
                  <p class="text-muted mt-3 mb-0">
                    <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> 
                  </p>
                </div>
              </div>
            </div>
              
              
              
              
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
               
                      
                      <i class="mdi mdi-account-location text-info icon-lg" aria-hidden="true"></i>

                      
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Monthly Member Register</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0"></h3>
                      </div>
                    </div>
                      
                      
                      
                  </div>
                  <p class="text-muted mt-3 mb-0">
                    <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> 
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-7 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <div class="row d-none d-sm-flex mb-4">
                    <div class="col-4">
                      <!---<h5 class="text-primary">Unique Visitors</h5>
                      <p>34657</p>-->
                    </div>
                    <div class="col-4">
                   <!---   <h5 class="text-primary">Bounce Rate</h5>
                      <p>45673</p>--->
                    </div>
                    <div class="col-4">
                     <!--- <h5 class="text-primary">Active session</h5>
                      <p>45673</p>--->
                    </div>
                  </div>
                  <div class="chart-container">
                    <canvas id="dashboard-area-chart" height="80"></canvas>
                  </div>
                </div>
              </div>
                
                
            </div>
            <div class="col-lg-5 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h2 class="card-title text-primary mb-5">Performance History</h2>
                  <div class="wrapper d-flex justify-content-between">
                    <div class="side-left">
                      <p class="mb-2">The best performance</p>
                      <p class="display-3 mb-4 font-weight-light">98.2%</p>
                    </div>
                    <div class="side-right">
                      <small class="text-muted">2019</small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection

