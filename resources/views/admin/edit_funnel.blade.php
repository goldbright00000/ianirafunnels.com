@extends('admin.default')
@section('title','Funnel Category')
@section('content')
<style>
    .sucess_message {
    margin: 0;
        margin-bottom: 0px;
    margin-bottom: 0px;
    position: absolute !important;
    top: 12%;
    left: 50%;
    transform: translate(-50%, -50%);
    position: absolute;
    width: 24%;
    height: 41px;
    line-height: 15px;
    text-align: left;
    z-index: 999999;
    color:white;
    
}
.s_close
{
   line-height: 15px; 
}

margin-right: 1.25rem;
width: 16px;
line-height: 1;
font-size: 18px;
color: #979797;

    </style>

   
    @if(session()->has('sucess')) 

 <div id="sucessfullyMessage" class="alert alert-success animated fadeIn sucess_message">
        <button type="button" class="close s_close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
           {{ session('sucess') }}
        </strong>
    </div>
@endif
    
    
    
    
    
 <div class="row purchace-popup">
            <div class="col-12">
              <span class="d-block d-md-flex align-items-center">
                  <p><a href="{{url('login/dashboard')}}"><b>Home</b></a>&nbsp; >>Edit Template 
  </p>
                
              </span>
            </div>
          </div>
<div class="content-wrapper">
          <div class="row">
            <div class="col-md-6 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Funnel Sub Category</h4>
                    
                      
                     
                      
                      
                      <form class="forms-sample" method="post" action="{{ url('admin/funnel-update')}}" enctype="multipart/form-data">
                          
                          
                          {{ csrf_field() }}
                       

                        
                          <input type="hidden" value="{{$fetchdata->id}}" name="funnel_id" >
                       
                          
                          <div class="form-group">
                          <label for="exampleInputEmail1">Funnel Type</label>
                          
                          <select class="form-control" name="funnel_type">
                              
                               <?php $keyr=1; ?>
                         
                      @foreach($data as $fetchdata_row)
                    
                      <option value="<?php echo $fetchdata_row->id ?>"  <?php if($fetchdata_row['id']==$fetchdata->funnel_type){ ?>selected="true" <?php } ?> > {{$fetchdata_row->funnel_name}}  </option>
                              
                               @endforeach
                              
                          </select>
                          
                         
                        </div>
                         
                          
                            <div class="form-group funnelty"  id="funneltypes">
                          <label for="exampleInputEmail1">Template Price Type</label>
                          
                          <select class="form-control" name="funnel_price_type" id="funnelpricetype">
                              <option value=""> Select Price Type</option>
                              <option value="freetemplate" <?php if($fetchdata->funnel_price_type=="freetemplate"){ echo "selected='true'"; } ?> > Free Template</option>
                          <option value="paidtemplate" <?php if($fetchdata->funnel_price_type=="paidtemplate"){ echo "selected='true'"; } ?> > Paid Template </option>
                          </select>
                          
                         
                        </div>
                          
                          
                      
                        <div class="form-group">
                          <label for="exampleInputEmail1">Temnplate Name</label>
                          <input type="text" class="form-control" id="exampleInputEmail1"  value="{{$fetchdata->funnel_name}}" name="funnel_name" placeholder="Enter Name" required="true" >
                        </div>
                       
                          
                          
                                 <div class="form-group">
                          <label for="exampleInputEmail1">Temnplate Other Heading</label>
                          <input type="text" class="form-control" id="exampleInputEmail1" value="{{$fetchdata->funnel_other}}" value="" name="funnel_other" placeholder="Enter Name" >
                                 </div>
                       
                         <div class="form-group" id="funnelprice">
                          <label for="exampleInputEmail1">Template Price</label>
                          <input type="text" class="form-control" id="exampleInputEmail1"  value="{{$fetchdata->funnel_price}}" name="funnel_price" placeholder="Enter Name"  >
                        </div>
                       
                        
                           <div class="form-group">
                          <label for="exampleInputEmail1">Template Type</label>
                          <input type="text" class="form-control" id="exampleInputEmail1"  value="{{$fetchdata->type}}" name="type" placeholder="Enter Type" >
                        </div>
                          
                          <div class="form-group">
                          <label for="exampleInputEmail1">Number Of Page</label>
                          <input type="text" class="form-control" id="exampleInputEmail1"  value="{{$fetchdata->page}}" name="page" placeholder="Enter Number Of Page">
                        </div>
                          
                          <div class="form-group">
                          <label for="exampleInputEmail1">Template Time</label>
                          <input type="text" class="form-control" id="exampleInputEmail1"  value="{{$fetchdata->time}}" name="time" placeholder="Enter Number Of funnel Time Mints"  >
                        </div>
                        
                        
                        
                        
                        
                          
                          <div class="form-group">
                                            <label for="exampleInputEmail1">Image</label>
                                            <input type="file" name="image">
                              
                                 </div>
                          
                           <div class="form-group">
  <label for="comment">Comment:</label>
  <textarea class="form-control" rows="11" cols="220"  name="message"   id="comment">{{$fetchdata->message}}</textarea>
</div> 
                          
                          
                          
                          
                      
                          <input type="submit" value="submit"  class="btn btn-success mr-2" name="Submit">
                          <a href="{{url('login/dashboard')}}" class="btn btn-light">Cancel</a>
                      </form>
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
           
           
           
           
            
           
          </div>
        </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
     
   var selec = $('#funnelpricetype').val();

   
   if(selec=="paidtemplate")
   {
       $('#funnelprice').css("display","block");
   }
   else
   {
    $('#funnelprice').css("display","none");   
   }
     

    
    
    
    
    
    
    
     $('#funneltypes').on('change',function(){
     
   var selec = $('#funnelpricetype').val();
   
   if(selec=="paidtemplate")
   {
       $('#funnelprice').css("display","block");
   }
   else
   {
    $('#funnelprice').css("display","none");   
   }
     
 });
    
    setTimeout(function() {
    $('#sucessfullyMessage').fadeOut('fast');
}, 2700);

 $('.close').click(function(){
     
    $('sucessfullyMessage').hide('fast')
     
 });
 
 </script>
@endsection