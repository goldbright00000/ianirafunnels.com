@extends('admin.default')
@section('title','Funnel Category')
@section('content')
<style>
    .sucess_message {
    margin: 0;
        margin-bottom: 0px;
    margin-bottom: 0px;
    position: absolute !important;
    top: 12%;
    left: 50%;
    transform: translate(-50%, -50%);
    position: absolute;
    width: 24%;
    height: 41px;
    line-height: 15px;
    text-align: left;
    z-index: 999999;
    color:white;
    
}
.s_close
{
   line-height: 15px; 
}

margin-right: 1.25rem;
width: 16px;
line-height: 1;
font-size: 18px;
color: #979797;

    </style>

    @if(session()->has('sucess')) 

 <div id="sucessfullyMessage" class="alert alert-success animated fadeIn sucess_message">
        <button type="button" class="close s_close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
          {{session('sucess')}}
        </strong>
    </div>
@endif
    
    
    
    
    
 <div class="row purchace-popup">
            <div class="col-12">
              <span class="d-block d-md-flex align-items-center">
                  <p><a href="{{url('login/dashboard')}}"><b>Home</b></a>&nbsp; >>Add Template
  </p>
                
              </span>
            </div>
          </div>
<div class="content-wrapper">
          <div class="row">
            <div class="col-md-6 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Add Template</h4>
                    
                      
                     
                      
                      
                      <form class="forms-sample" method="post" action="{{url('post/other-subfunnel')}}" enctype="multipart/form-data">
                          
                          
                          {{ csrf_field() }}
                       

                          
                         
                          
                          
                          <div class="form-group"  id="funneltypes">
                          <label for="exampleInputEmail1">Template Price Type</label>
                          
                          <select class="form-control" name="funnel_price_type" id="funnelpricetype">
                              <option value=""> Select Price Type</option>
                              <option value="freetemplate"> Free Template</option>
                          <option value="paidtemplate"> Paid Template </option>
                          </select>
                          
                         
                        </div>
                          
                          
                          
                          
                          
                          
                          
                            <div class="form-group">
                          <label for="exampleInputEmail1">Template  Designs Type</label>
                          
                          <select class="form-control" name="template_des_type" >
                              <option value=""> Select Designs Type</option>
                                <option value="normaldesigns"> Normal Template </option>
                              <option value="premiumdesigns"> Premium Template</option>
                        
                          </select>
                          
                         
                        </div>
                          
                          
                          
                          
                        <div class="form-group">
                          <label for="exampleInputEmail1">Template Name</label>
                          <input type="text" class="form-control" id="exampleInputEmail1"  value="" name="funnel_name" placeholder="Enter Name" required="true" >
                        </div>
                          
                          

                         <div class="form-group" id="funnelprice">
                          <label for="exampleInputEmail1">Template Price</label>
                          <input type="text" class="form-control" id="exampleInputEmail1"  value="" name="funnel_price" placeholder="Enter Name"  >
                        </div>
                                    



                          
                          
                                 <div class="form-group">
                          <label for="exampleInputEmail1">Template Other Heading</label>
                          <input type="text" class="form-control" id="exampleInputEmail1"  value="" name="funnel_other" placeholder="Enter Other"  >
                        </div>
                          
                          
                          
                       
                          
                              
                          
                         
                          
                          
                           <div class="form-group">
                            <label for="exampleInputEmail1">Template Image</label>
                          <input type="file" class="form-control" id="exampleInputEmail1"  name="image" >
                        </div>
                         
                          
                    
                          
                           <div class="form-group">
  <label for="comment">Comment:</label>
  <textarea class="form-control" rows="11" cols="220"  name="message"   id="comment"></textarea>
</div> 
                          
                          
                          
                          
                      
                          <input type="submit" value="submit"  class="btn btn-success mr-2" name="Submit">
                          <a href="{{url('login/dashboard')}}" class="btn btn-light">Cancel</a>
                      </form>
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
           
           
           
           
            
           
          </div>
        </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    setTimeout(function() {
    $('#sucessfullyMessage').fadeOut('fast');
}, 2700);

 $('.close').click(function(){
     
    $('sucessfullyMessage').hide('fast')
     
 });
 
 
 
 
    
    
     $('#funneltypes').on('change',function(){
     
   var selec = $('#funnelpricetype').val();
   
   if(selec=="paidtemplate")
   {
       $('#funnelprice').css("display","block");
   }
   else
   {
    $('#funnelprice').css("display","none");   
   }
     
 });
    
 </script>
@endsection