@extends('admin.default')
@section('title','Admin Dashboard')
@section('content')
  <style>
    .sucess_message {
    margin: 0;
        margin-bottom: 0px;
    margin-bottom: 0px;
    position: absolute !important;
    top: 12%;
    left: 50%;
    transform: translate(-50%, -50%);
    position: absolute;
    width: 24%;
    height: 41px;
    line-height: 15px;
    text-align: left;
    z-index: 999999;
    color:white;
}
.s_close
{
   line-height: 15px; 
}

.table td img, .table th img {
    border-radius: 0% !important;
}


    </style>

   
     @if(session()->has('sucess')) 

 <div id="sucessfullyMessage" class="alert alert-success animated fadeIn sucess_message">
        <button type="button" class="close s_close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
           {{ session('sucess') }}
        </strong>
    </div>
@endif


  
  <div class="row">
            <div class="col-lg-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"> All Funnel Category</h4>
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="searchData">
                      <thead>
                        <tr>
                        
                            <th>
                 S.l
                          </th>
                            
                            <th>
                       Funnel  Name
                          </th>
                          <th>
                       Funnel Category Name
                          </th>
                          
                           <th>
                      Funnel  Other Heading
                          </th>
                          
                          
                          
                          <th>
                       Funnel  Price
                          </th>
                          
                          
                        
                          
                            <th>
             Funnel  Time
                          </th>
                           <th>
                      Funnel Image
                          </th>
                      
                           <th>
                       Edit /Delete
                          </th>
                            <th>
                      View
                          </th>
                         
                         
                        </tr>
                      </thead>
                      <tbody>
                      
                     <?php $keyr=1; ?>
                         
                      @foreach($fetchdata as $fetchdata_row)
                      
                        <tr id="funnel_hidden">
                  
                        
                          <td class="font-weight-medium">
                       {{$keyr++}}
                          </td>
                          
                          
                          <td>
                          {{$fetchdata_row->funnel_name}}
                          </td>
                          <td>
                          {{$fetchdata_row->sub_funnel_name}}
                          </td>
                          
                          <td>
                          {{$fetchdata_row->funnel_other}}
                          </td>
                          
                          
                          
                               <td>
                          {{$fetchdata_row->funnel_price}}
                          </td>
                           <td>
                          {{$fetchdata_row->funnel_time}}
                          </td>
                          
                          
                          
                          <td>@if ($fetchdata_row->funnel_image)
                              <img src="{{asset('public/uploads/funnel/'.$fetchdata_row->funnel_image)}}"  width="100px">
                             
                              @else
                              
                            <img src="{{asset('public/uploads/funnel/imagenotavailable.png')}}"  width="100px">
                              
                              @endif
                              
                          </td>
                          
                          
                         
                          
                          
                          
                   
                          <td>
                              <a href="{{url('admin/edit-funnel/'.$fetchdata_row->id)}}"> Edit</a>  
                       <span onclick="funneldelete(<?php echo $fetchdata_row->id; ?>);" >   / Delete </span>
                          </td>
                          
                           <td>
                            View
                          </td>
                          
                          
                          
                          
                        </tr>
                       
                        @endforeach
                 
                  
                      
                      
                      
                      
                      
                      
                      
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>

<div id="Mysucee1"></div>




<script>
    function funneldelete(id)
    {
 
    
    var deleteid=id;
 

var result = confirm("Want to delete?");
if (result) {

location.href = "{{url('admin/funnel-delete/')}}"+'/'+deleteid;
}
}
    
  
    setTimeout(function() {
    $('#sucessfullyMessage').fadeOut('fast');
}, 2700);

 $('.close').click(function(){
     
    $('sucessfullyMessage').hide('fast')
     
 });
 </script>
          @endsection