@extends('admin.default')
@section('title','Admin Dashboard')
@section('content')
<style>
    .sucess_message {
    margin: 0;
        margin-bottom: 0px;
    margin-bottom: 0px;
    position: absolute !important;
    top: 12%;
    left: 50%;
    transform: translate(-50%, -50%);
    position: absolute;
    width: 24%;
    height: 41px;
    line-height: 15px;
    text-align: left;
    z-index: 999999;
}
.s_close
{
   line-height: 15px; 
}
    </style>
    
    
    
    <div class="row purchace-popup">
            <div class="col-12">
              <span class="d-block d-md-flex align-items-center">
                  <p><a href="{{url('login/dashboard')}}"><b>Home</b></a>&nbsp; >> Chage Password
  </p>
                
              </span>
            </div>
          </div>
 @if (session()->has('password'))
    <div id="sucessfullyMessage" class="alert alert-success animated fadeIn sucess_message">
        <button type="button" class="close s_close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
            {!! session()->get('password') !!}
        </strong>
    </div>
@endif

@if(isset($sucess)) 

 <div id="sucessfullyMessage" class="alert alert-success animated fadeIn sucess_message">
        <button type="button" class="close s_close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>
          {{$sucess}}
        </strong>
    </div>
@endif
<div class="content-wrapper">
          <div class="row">
            <div class="col-md-6 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Change Password</h4>
                    
                      
                      <form class="forms-sample" method="post" action="{{url('admin-dashboard/password')}}" >
                          
                          
                          {{ csrf_field() }}
                          
                        <div class="form-group">
                          <label for="exampleInputEmail1">Old Password</label>
                          <input type="text" class="form-control" id="exampleInputEmail1"  value="" name="oldpassword" placeholder="Enter email" required="true" >
                        </div>
                        <div class="form-group">
                          <label for="exampleInputPassword1">New Password</label>
                          <input type="text" class="form-control" id="exampleInputPassword1" value="" name="newpassword" placeholder="Password" required="true">
                        </div>
                          <input type="submit" value="submit"  class="btn btn-success mr-2" name="Submit">
                          <a href="{{url('login/dashboard')}}" class="btn btn-light">Cancel</a>
                      </form>
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
           
           
           
           
            
           
          </div>
        </div>

<script>
    setTimeout(function() {
    $('#sucessfullyMessage').fadeOut('fast');
}, 2700);

 $('.close').click(function(){
     
    $('sucessfullyMessage').hide('fast')
     
 });
 </script>
@endsection